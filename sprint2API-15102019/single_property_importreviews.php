<?php

/**
 * Single property ratings imported reviews API
 */

add_action('rest_api_init', function(){
	register_rest_route('tvcapi', '/v2/single-property-imported-ratings',
		array(
			'methods' => 'POST',
			'callback'=> 'single_property_imported_ratings',
		)
	);
});

function single_property_imported_ratings() {

	// Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['Token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);

    if ($headers['Token'] != $token || empty($headers['Token'])) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Token is invalid', 'wpestate')), 400);
    }
    // END

	$property_id = sanitize_text_field( filter_input( INPUT_POST, 'property_id') );

	if (!isset($property_id) || !is_numeric($property_id)) {
		return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Only numeric value allowed!', 'wpestate')), 400);
	}

	// START Single Property Ratings Reviews Details
	global $wpdb;

	$comment_id = $wpdb->get_results("SELECT comment_ID, user_id, comment_content FROM wp_comments WHERE comment_post_ID = ".$property_id." AND comment_approved = 1", ARRAY_A);

	$fields_val 	  = wpestate_get_review_fields();
	$stars_fields     = array();
	$stars_fields_sum = array();
	$stars_averages   = array();
	$store 			  = array();

	$imported_count_total = 0;
	$airbnb_counts = $airbnb_total =  $tripad_counts = $tripad_total = $booking_counts = $booking_total = $expedia_counts = $expedia_total = $other_counts = $other_total = 0;
	
	$airbnb_comments = $tripad_comments = $booking_comments = $expedia_comments = $other_comments = array();
	for ($i=0; $i < count($comment_id); $i++) { 
		
		$reviews_values   = get_comment_meta( $comment_id[ $i ]['comment_ID'], 'review_stars', true );

		$imported_refer   = get_comment_meta( $comment_id[ $i ]['comment_ID'], 'ota_reference', true );

        /** 
          *	Imported Ratings Reviews
		  * 1. Airbnb	
          */
        $imported_ratings = get_comment_meta( $comment_id[ $i ]['comment_ID'], 'ota_stars', true );

        if ($imported_refer == 'airbnb') {
        	$airbnb_counts++;
        	$imported_count_total++;
        	$airbnb_total += intval( $imported_ratings );

        	$ref_by_airbnb = 'ref_by_'.$imported_refer;
        	// Star rating value
        	if ($imported_ratings > 0) {
        		// echo '--'.$imported_ratings;
				$airbnb_comments[ $i ]['rating_value'] = $imported_ratings;

				// ------------------------
				// Comment Id 
				$airbnb_comments[ $i ]['comment_id'] = $comment_id[ $i ]['comment_ID']; 

				// Profile Image 
				$userid_agent = get_user_meta($comment_id[ $i ]['user_id'], 'user_agent_id', true);
				$thumb_id     = get_post_thumbnail_id($userid_agent);
		        $preview      = wp_get_attachment_image_src($thumb_id, 'thumbnail'); 
		        $airbnb_comments[ $i ]['profile_image'] = $preview[0];
		        if (empty($airbnb_comments[ $i ]['profile_image'])) {
			        $airbnb_comments[ $i ]['profile_image'] = get_template_directory_uri().'/img/default_user_agent.gif';
		        }

				// Reviewer Name
				$userid_agent   =   get_user_meta($comment_id[ $i ]['user_id'], 'user_agent_id', true);
				$airbnb_comments[ $i ]['reviewer_name']  =   get_the_title($userid_agent);
				if (empty( $airbnb_comments[ $i ]['reviewer_name'] )) {

					$admin_email   = get_option('admin_email');
		            $the_user      = get_user_by('email', $admin_email);
		            $admin_user_id = $the_user->ID;
		            $admin_name    = get_user_by('id', $admin_user_id);
		            $airbnb_comments[ $i ]['reviewer_name'] = $admin_name->display_name;
				}

				// Date
				$airbnb_comments[ $i ]['date'] = esc_html__( 'Posted on ','wpestate' ).get_comment_date('j M Y',$comment_id[ $i ]['comment_ID']);
				
				// Comment Content
				$airbnb_comments[ $i ]['content'] = esc_html__( $comment_id[ $i ]['comment_content'],'wpestate' );
        	}
        }

        // 2. TripAdvisor
        if ($imported_refer == 'tripadvisor') {
        	$tripad_counts++;
        	$imported_count_total++;
        	$tripad_total += intval( $imported_ratings );
        	$ref_by_trip   = 'ref_by_'.$imported_refer;
        	// Star rating value
        	if ($imported_ratings > 0) {
        		// echo '--'.$imported_ratings;
				$tripad_comments[ $i ]['rating_value'] = $imported_ratings;

				// ------------------------
				// Comment Id 
				$tripad_comments[ $i ]['comment_id'] = $comment_id[ $i ]['comment_ID'];

				// Profile Image 
				$userid_agent = get_user_meta($comment_id[ $i ]['user_id'], 'user_agent_id', true);
				$thumb_id     = get_post_thumbnail_id($userid_agent);
		        $preview      = wp_get_attachment_image_src($thumb_id, 'thumbnail'); 
		        $tripad_comments[ $i ]['profile_image'] = $preview[0];
		        if (empty($tripad_comments[ $i ]['profile_image'])) {
			        $tripad_comments[ $i ]['profile_image'] = get_template_directory_uri().'/img/default_user_agent.gif';
		        }

				// Reviewer Name
				$userid_agent   =   get_user_meta($comment_id[ $i ]['user_id'], 'user_agent_id', true);
				$tripad_comments[ $i ]['reviewer_name']  =   get_the_title($userid_agent);
				if (empty( $tripad_comments[ $i ]['reviewer_name'] )) {

					$admin_email   = get_option('admin_email');
		            $the_user      = get_user_by('email', $admin_email);
		            $admin_user_id = $the_user->ID;
		            $admin_name    = get_user_by('id', $admin_user_id);
		            $tripad_comments[ $i ]['reviewer_name'] = $admin_name->display_name;
				}

				// Date
				$tripad_comments[ $i ]['date'] = esc_html__( 'Posted on ','wpestate' ).get_comment_date('j M Y',$comment_id[ $i ]['comment_ID']);
				
				// Comment Content
				$tripad_comments[ $i ]['content'] = esc_html__( $comment_id[ $i ]['comment_content'],'wpestate' );
        	}
        }

        // 3. Booking.com
        if ($imported_refer == 'booking') {
        	$booking_counts++;
        	$imported_count_total++;
        	$booking_total += intval( $imported_ratings );
        	$ref_by_booking = 'ref_by_'.$imported_refer;
        	// Star rating value
        	if ($imported_ratings > 0) {
        		// echo '--'.$imported_ratings;
				$booking_comments[ $i ]['rating_value'] = $imported_ratings;

				// ------------------------
				// Comment Id 
				$booking_comments[ $i ]['comment_id'] = $comment_id[ $i ]['comment_ID'];

				// Profile Image 
				$userid_agent = get_user_meta($comment_id[ $i ]['user_id'], 'user_agent_id', true);
				$thumb_id     = get_post_thumbnail_id($userid_agent);
		        $preview      = wp_get_attachment_image_src($thumb_id, 'thumbnail'); 
		        $booking_comments[ $i ]['profile_image'] = $preview[0];
		        if (empty($booking_comments[ $i ]['profile_image'])) {
			        $booking_comments[ $i ]['profile_image'] = get_template_directory_uri().'/img/default_user_agent.gif';
		        }

				// Reviewer Name
				$userid_agent   =   get_user_meta($comment_id[ $i ]['user_id'], 'user_agent_id', true);
				$booking_comments[ $i ]['reviewer_name']  =   get_the_title($userid_agent);
				if (empty( $booking_comments[ $i ]['reviewer_name'] )) {

					$admin_email   = get_option('admin_email');
		            $the_user      = get_user_by('email', $admin_email);
		            $admin_user_id = $the_user->ID;
		            $admin_name    = get_user_by('id', $admin_user_id);
		            $booking_comments[ $i ]['reviewer_name'] = $admin_name->display_name;
				}

				// Date
				$booking_comments[ $i ]['date'] = esc_html__( 'Posted on ','wpestate' ).get_comment_date('j M Y',$comment_id[ $i ]['comment_ID']);
				
				// Comment Content
				$booking_comments[ $i ]['content'] = esc_html__( $comment_id[ $i ]['comment_content'],'wpestate' );
        	}
        }

        // 4. Expedia
        if ($imported_refer == 'expedia') {
        	$expedia_counts++;
        	$imported_count_total++;
        	$expedia_total += intval( $imported_ratings );
        	$ref_by_expedia = 'ref_by_'.$imported_refer;
        	// Star rating value
        	if ($imported_ratings > 0) {
        		// echo '--'.$imported_ratings;
				$expedia_comments[ $i ]['rating_value'] = $imported_ratings;

				// ------------------------
				// Comment Id 
				$expedia_comments[ $i ]['comment_id'] = $comment_id[ $i ]['comment_ID'];

				// Profile Image 
				$userid_agent = get_user_meta($comment_id[ $i ]['user_id'], 'user_agent_id', true);
				$thumb_id     = get_post_thumbnail_id($userid_agent);
		        $preview      = wp_get_attachment_image_src($thumb_id, 'thumbnail'); 
		        $expedia_comments[ $i ]['profile_image'] = $preview[0];
		        if (empty($expedia_comments[ $i ]['profile_image'])) {
			        $expedia_comments[ $i ]['profile_image'] = get_template_directory_uri().'/img/default_user_agent.gif';
		        }

				// Reviewer Name
				$userid_agent   =   get_user_meta($comment_id[ $i ]['user_id'], 'user_agent_id', true);
				$expedia_comments[ $i ]['reviewer_name']  =   get_the_title($userid_agent);
				if (empty( $expedia_comments[ $i ]['reviewer_name'] )) {

					$admin_email   = get_option('admin_email');
		            $the_user      = get_user_by('email', $admin_email);
		            $admin_user_id = $the_user->ID;
		            $admin_name    = get_user_by('id', $admin_user_id);
		            $expedia_comments[ $i ]['reviewer_name'] = $admin_name->display_name;
				}

				// Date
				$expedia_comments[ $i ]['date'] = esc_html__( 'Posted on ','wpestate' ).get_comment_date('j M Y',$comment_id[ $i ]['comment_ID']);
				
				// Comment Content
				$expedia_comments[ $i ]['content'] = esc_html__( $comment_id[ $i ]['comment_content'],'wpestate' );
        	}
        }

        // 5. Other
        if ($imported_refer == 'other') {
        	$other_counts++;
        	$imported_count_total++;
        	$other_total += intval( $imported_ratings );
        	$ref_by_other = 'ref_by_'.$imported_refer;
        	// Star rating value
        	if ($imported_ratings > 0) {
        		// echo '--'.$imported_ratings;
				$other_comments[ $i ]['rating_value'] = $imported_ratings;

				// ------------------------
				// Comment Id 
				$other_comments[ $i ]['comment_id'] = $comment_id[ $i ]['comment_ID'];
				
				// Profile Image 
				$userid_agent = get_user_meta($comment_id[ $i ]['user_id'], 'user_agent_id', true);
				$thumb_id     = get_post_thumbnail_id($userid_agent);
		        $preview      = wp_get_attachment_image_src($thumb_id, 'thumbnail'); 
		        $other_comments[ $i ]['profile_image'] = $preview[0];
		        if (empty($other_comments[ $i ]['profile_image'])) {
			        $other_comments[ $i ]['profile_image'] = get_template_directory_uri().'/img/default_user_agent.gif';
		        }

				// Reviewer Name
				$userid_agent   =   get_user_meta($comment_id[ $i ]['user_id'], 'user_agent_id', true);
				$other_comments[ $i ]['reviewer_name']  =   get_the_title($userid_agent);
				if (empty( $other_comments[ $i ]['reviewer_name'] )) {

					$admin_email   = get_option('admin_email');
		            $the_user      = get_user_by('email', $admin_email);
		            $admin_user_id = $the_user->ID;
		            $admin_name    = get_user_by('id', $admin_user_id);
		            $other_comments[ $i ]['reviewer_name'] = $admin_name->display_name;
				}

				// Date
				$other_comments[ $i ]['date'] = esc_html__( 'Posted on ','wpestate' ).get_comment_date('j M Y',$comment_id[ $i ]['comment_ID']);
				
				// Comment Content
				$other_comments[ $i ]['content'] = esc_html__( $comment_id[ $i ]['comment_content'],'wpestate' );
        	}
        }

	}
	// print_r($imported_comments);



	/*
	 * Imported OTA reviews
	*/
	// 1. Airbnb Average
	$airbnb_avg = 0;
	$airbnb_div = $airbnb_total / $airbnb_counts;
	$airbnb_avg = number_format( $airbnb_div, 1, '.', '' );

	$airbnb_average = $airbnb_avg;

	if ( $airbnb_avg >= 4 ) {
		$airbnb_avg = esc_html__('Ranked '.$airbnb_avg. ' of 5 Exceptional!', 'wpestate');
	} elseif ( $airbnb_avg >= 3 && $airbnb_avg < 4 ) {
		$airbnb_avg = esc_html__('Ranked '.$airbnb_avg. ' of 5 Excellent!', 'wpestate');
	} elseif ( $airbnb_avg >= 2 && $airbnb_avg < 3 ) {
		$airbnb_avg = esc_html__('Ranked '.$airbnb_avg. ' of 5 Good', 'wpestate');
	} elseif ( $airbnb_avg >= 1 && $airbnb_avg < 2 ) {
		$airbnb_avg = esc_html__('Ranked '.$airbnb_avg. ' of 5 Fair', 'wpestate');
	}

	// 2. TripAdvisor Average
	$tripad_avg = 0;
	$tripad_div = $tripad_total / $tripad_counts;
	$tripad_avg = number_format( $tripad_div, 1, '.', '' );
	
	$tripad_average = $tripad_avg;
	if ( $tripad_avg >= 4 ) {
		$tripad_avg = esc_html__('Ranked '.$tripad_avg. ' of 5 Exceptional!', 'wpestate');
	} elseif ( $tripad_avg >= 3 && $tripad_avg < 4 ) {
		$tripad_avg = esc_html__('Ranked '.$tripad_avg. ' of 5 Excellent!', 'wpestate');
	} elseif ( $tripad_avg >= 2 && $tripad_avg < 3 ) {
		$tripad_avg = esc_html__('Ranked '.$tripad_avg. ' of 5 Good', 'wpestate');
	} elseif ( $tripad_avg >= 1 && $tripad_avg < 2 ) {
		$tripad_avg = esc_html__('Ranked '.$tripad_avg. ' of 5 Fair', 'wpestate');
	}

	// 3. Booking Average
	$booking_avg = 0;
	$booking_div = $booking_total / $booking_counts;
	$booking_avg = number_format( $booking_div, 1, '.', '' );
	
	$booking_average = $booking_avg;

	if ( $booking_avg >= 4 ) {
		$booking_avg = esc_html__('Ranked '.$booking_avg. ' of 5 Exceptional!', 'wpestate');
	} elseif ( $booking_avg >= 3 && $booking_avg < 4 ) {
		$booking_avg = esc_html__('Ranked '.$booking_avg. ' of 5 Excellent!', 'wpestate');
	} elseif ( $booking_avg >= 2 && $booking_avg < 3 ) {
		$booking_avg = esc_html__('Ranked '.$booking_avg. ' of 5 Good', 'wpestate');
	} elseif ( $booking_avg >= 1 && $booking_avg < 2 ) {
		$booking_avg = esc_html__('Ranked '.$booking_avg. ' of 5 Fair', 'wpestate');
	}

	// 4. Expedia Average
	$expedia_avg = 0;
	$expedia_div = $expedia_total / $expedia_counts;
	$expedia_avg = number_format( $expedia_div, 1, '.', '' );
	
	$expedia_average = $expedia_avg;
	
	if ( $expedia_avg >= 4 ) {
		$expedia_avg = esc_html__('Ranked '.$expedia_avg. ' of 5 Exceptional!', 'wpestate');
	} elseif ( $expedia_avg >= 3 && $expedia_avg < 4 ) {
		$expedia_avg = esc_html__('Ranked '.$expedia_avg. ' of 5 Excellent!', 'wpestate');
	} elseif ( $expedia_avg >= 2 && $expedia_avg < 3 ) {
		$expedia_avg = esc_html__('Ranked '.$expedia_avg. ' of 5 Good', 'wpestate');
	} elseif ( $expedia_avg >= 1 && $expedia_avg < 2 ) {
		$expedia_avg = esc_html__('Ranked '.$expedia_avg. ' of 5 Fair', 'wpestate');
	}

	// 5. Other Average
	$other_avg = 0;
	$other_div = $other_total / $other_counts;
	$other_avg = number_format( $other_div, 1, '.', '' );
	
	$other_average = $other_avg;
	
	if ( $other_avg >= 4 ) {
		$other_avg = esc_html__('Ranked '.$other_avg. ' of 5 Exceptional!', 'wpestate');
	} elseif ( $other_avg >= 3 && $other_avg < 4 ) {
		$other_avg = esc_html__('Ranked '.$other_avg. ' of 5 Excellent!', 'wpestate');
	} elseif ( $other_avg >= 2 && $other_avg < 3 ) {
		$other_avg = esc_html__('Ranked '.$other_avg. ' of 5 Good', 'wpestate');
	} elseif ( $other_avg >= 1 && $other_avg < 2 ) {
		$other_avg = esc_html__('Ranked '.$other_avg. ' of 5 Fair', 'wpestate');
	}


	$imported_comments = array_merge($airbnb_comments, $tripad_comments, $booking_comments, $expedia_comments, $other_comments);

	// Pagination
    // $defualt_display  = sanitize_text_field( filter_input( INPUT_POST, 'defualt_display' ) );
    $defualt_display  = 2;


    // -----------------------
    $ota_reference = sanitize_text_field( filter_input( INPUT_POST, 'ota_reference' ) );
	$current_page  = sanitize_text_field( filter_input( INPUT_POST, 'current_page' ) );
	$per_page      = sanitize_text_field( filter_input( INPUT_POST, 'per_page' ) );

	$current_page  = ( $current_page == '' || $current_page == 0 ) ? 1 : (int)$current_page; 

    $per_page      = ( $per_page == '' || $per_page == 0 ) ? (int)$defualt_display : (int)$per_page;


    // $air_total_pages  = ceil(count($airbnb_comments) / $per_page);
    // $airbnb_comments  = array_chunk($airbnb_comments, $per_page);
    // $airbnb_data      = $airbnb_comments [$current_page - 1];

    // 1. Airbnb  
    if ( isset( $ota_reference ) && $ota_reference == 'ref_by_airbnb' ) {

    	$air_total_pages  = ceil(count($airbnb_comments) / $per_page);
	    $airbnb_comments  = array_chunk($airbnb_comments, $per_page);
	    $airbnb_data      = $airbnb_comments [$current_page - 1];
    
    } else {
    	$air_total_pages  = ceil(count($airbnb_comments) / $per_page);
    	$airbnb_data      = $airbnb_comments;
    }

    // 2. TripAdvisor
    if ( isset( $ota_reference ) && $ota_reference == 'ref_by_tripadvisor' ) {

    	$trip_total_pages  = ceil(count($tripad_comments) / $per_page);
	    $tripad_comments   = array_chunk($tripad_comments, $per_page);
	    $tripad_data       = $tripad_comments [$current_page - 1];
   
    } else {
    	$trip_total_pages  = ceil(count($tripad_comments) / $per_page);
    	$tripad_data       = $tripad_comments;
    }

    // 3. Booking.com
    if ( isset( $ota_reference ) && $ota_reference == 'ref_by_booking' ) {
    	
    	$book_total_pages  = ceil(count($booking_comments) / $per_page);
	    $booking_comments  = array_chunk($booking_comments, $per_page);
	    $booking_data      = $booking_comments [$current_page - 1];
    } else {
    	$book_total_pages  = ceil(count($booking_comments) / $per_page);
    	$booking_data      = $booking_comments;
    }

    // 4. Expedia
    if ( isset( $ota_reference ) && $ota_reference == 'ref_by_expedia' ) {

    	$expedia_total_pages= ceil(count($expedia_comments) / $per_page);
	    $expedia_comments   = array_chunk($expedia_comments, $per_page);
	    $expedia_data       = $expedia_comments [$current_page - 1];

    } else {
    	$expedia_total_pages= ceil(count($expedia_comments) / $per_page);
    	$expedia_data       = $expedia_comments;
    }

    // 5. Other 
    if ( isset( $ota_reference ) && $ota_reference == 'ref_by_other' ) {
    	
    	$other_total_pages  = ceil(count($other_comments) / $per_page);
	    $other_comments     = array_chunk($other_comments, $per_page);
	    $other_data         = $other_comments [$current_page - 1];

    } else {
    	$other_total_pages  = ceil(count($other_comments) / $per_page);
    	$other_data         = $other_comments;
    }

    // ------------Done-----------
    // 1. Airbnb 
 //    $air_current_page = sanitize_text_field( filter_input( INPUT_POST, 'air_current_page' ) );
 //    $air_per_page     = sanitize_text_field( filter_input( INPUT_POST, 'air_per_page' ) );
    
 //    $air_current_page = ( $air_current_page == '' || $air_current_page == 0 ) ? 1 : (int)$air_current_page; 

 //    $air_per_page     = ( $air_per_page == '' || $air_per_page == 0 ) ? (int)$defualt_display : (int)$air_per_page;

 //    $air_total_pages  = ceil(count($airbnb_comments) / $air_per_page);
 //    $airbnb_comments  = array_chunk($airbnb_comments, $air_per_page);
 //    $airbnb_data      = $airbnb_comments [$air_current_page - 1];


 //    // $air_defultshow    = 2;
 //    // $airbnb_defultshow = array_chunk($airbnb_comments, $air_defultshow);
	// // $limit = 2;
	// // print_r($airbnb_defultshow);

 //    // 2. TripAdvisor 
 //    $trip_current_page = sanitize_text_field( filter_input( INPUT_POST, 'trip_current_page' ) );
 //    $trip_per_page     = sanitize_text_field( filter_input( INPUT_POST, 'trip_per_page' ) );
    
 //    $trip_current_page = ( $trip_current_page == '' || $trip_current_page == 0 ) ? 1 : (int)$trip_current_page; 

 //    $trip_per_page     = ( $trip_per_page == '' || $trip_per_page == 0 ) ? (int)$defualt_display : (int)$trip_per_page;

 //    $trip_total_pages  = ceil(count($tripad_comments) / $trip_per_page);
 //    $tripad_comments   = array_chunk($tripad_comments, $trip_per_page);
 //    $tripad_data       = $tripad_comments [$trip_current_page - 1];

 //    // 3. Booking.com 
 //    $book_current_page = sanitize_text_field( filter_input( INPUT_POST, 'book_current_page' ) );
 //    $book_per_page     = sanitize_text_field( filter_input( INPUT_POST, 'book_per_page' ) );
    
 //    $book_current_page = ( $book_current_page == '' || $book_current_page == 0 ) ? 1 : (int)$book_current_page; 

 //    $book_per_page     = ( $book_per_page == '' || $book_per_page == 0 ) ? (int)$defualt_display : (int)$book_per_page;

 //    $book_total_pages  = ceil(count($booking_comments) / $book_per_page);
 //    $booking_comments  = array_chunk($booking_comments, $book_per_page);
 //    $booking_data      = $booking_comments [$book_current_page - 1];

 //    // 4. Expedia 
 //    $expedia_current_page = sanitize_text_field( filter_input( INPUT_POST, 'expedia_current_page' ) );
 //    $expedia_per_page     = sanitize_text_field( filter_input( INPUT_POST, 'expedia_per_page' ) );
    
 //    $expedia_current_page = ( $expedia_current_page == '' || $expedia_current_page == 0 ) ? 1 : (int)$expedia_current_page; 

 //    $expedia_per_page     = ( $expedia_per_page == '' || $expedia_per_page == 0 ) ? (int)$defualt_display : (int)$expedia_per_page;

 //    $expedia_total_pages  = ceil(count($expedia_comments) / $expedia_per_page);
 //    $expedia_comments     = array_chunk($expedia_comments, $expedia_per_page);
 //    $expedia_data         = $expedia_comments [$expedia_current_page - 1];

    // 5. Other 
    // $other_current_page = sanitize_text_field( filter_input( INPUT_POST, 'other_current_page' ) );
    // $other_per_page     = sanitize_text_field( filter_input( INPUT_POST, 'other_per_page' ) );
    
    // $other_current_page = ( $other_current_page == '' || $other_current_page == 0 ) ? 1 : (int)$other_current_page; 

    // $other_per_page     = ( $other_per_page == '' || $other_per_page == 0 ) ? (int)$defualt_display : (int)$other_per_page;

    // $other_total_pages  = ceil(count($other_comments) / $other_per_page);
    // $other_comments     = array_chunk($other_comments, $other_per_page);
    // $other_data         = $other_comments [$other_current_page - 1];
    // echo "--> ".$imported_refer."\n";
	// Imported Data
	$_imported = array(
		
		// Imported Reviews
		array(
			'import_reviews'     => esc_html__('Imported Reviews ' .'('.$imported_count_total.' Reviews)', 'wpestate'),
			'import_count_total' => $imported_count_total,

			'comments_data' => array(
				
				// 1. Airbnb
				array(
					'id'           => $ref_by_airbnb,
					'total_page'   => $air_total_pages,
					'current_page' => $current_page,
					'reference_by' => esc_html__('Airbnb ' .'('.$airbnb_counts.' Reviews)', 'wpestate'),
					'avg_val'      => $airbnb_average,
					'avg'          => $airbnb_avg,
					'comments'     => array_values($airbnb_data),
				),

				// 2. TripAdvisor
				array(
					'id'           => $ref_by_trip,
					'total_page'   => $trip_total_pages,
					'current_page' => $current_page,
					'reference_by' => esc_html__('Tripadvisor ' .'('.$tripad_counts.' Reviews)', 'wpestate'),
					'avg_val'      => $tripad_average,
					'avg'          => $tripad_avg,
					'comments'     => array_values($tripad_data),
				),

				// 3. Booking.com
				array(
					'id'           => $ref_by_booking,
					'total_page'   => $book_total_pages,
					'current_page' => $current_page,
					'reference_by' => esc_html__('Booking ' .'('.$booking_counts.' Reviews)', 'wpestate'),
					'avg_val'          => $booking_average,
					'avg'          => $booking_avg,
					'comments'     => array_values($booking_data),
				),

				// 4. Expedia
				array(
					'id'           => $ref_by_expedia,
					'total_page'   => $expedia_total_pages,
					'current_page' => $current_page,
					'reference_by' => esc_html__('Expedia ' .'('.$expedia_counts.' Reviews)', 'wpestate'),
					'avg_val'      => $expedia_average,
					'avg'          => $expedia_avg,
					'comments'     => array_values($expedia_data),
				),

				// 5. Other 
				array(
					'id'		   => $ref_by_other,
					'total_page'   => $other_total_pages,
					'current_page' => $current_page,
					'reference_by' => esc_html__('Other ' .'('.$other_counts.' Reviews)', 'wpestate'),
					'avg_val'      => $other_average,
					'avg'      	   => $other_avg,
					'comments'     => array_values($other_data),
				),
			),

			// 3. Booking.com
			// 'booking_comnt' => array(
			// 	'b_total_page' => $book_total_pages,
			// 	'b_cur_page'   => $book_current_page,
			// 	'booking'      => esc_html__('Booking ' .'('.$booking_counts.' Reviews)', 'wpestate'),
			// 	'avg'          => $booking_avg,
			// 	'booking_data' => array_values($booking_data),
			// ),

			// 4. Expedia
			// 'expedia_comnt' => array(
			// 	'e_total_page' => $expedia_total_pages,
			// 	'e_cur_page'   => $expedia_current_page,
			// 	'expedia'      => esc_html__('Expedia ' .'('.$expedia_counts.' Reviews)', 'wpestate'),
			// 	'avg'          => $expedia_avg,
			// 	'Expedia_data' => array_values($expedia_data),
			// ),

			// 5. Other
			// 'other_comnt' => array(
			// 	'o_total_page' => $other_total_pages,
			// 	'o_cur_page'   => $other_current_page,
			// 	'other'      => esc_html__('Other ' .'('.$other_counts.' Reviews)', 'wpestate'),
			// 	'avg'      	 => $other_avg,
			// 	'other_data' => array_values($other_data),
			// ),
		),

	); 
	return new WP_REST_Response(
		array(
			'response_code'  => "200", 
			'data' => $_imported,
			),
		200);

}