<?php

/**
 * JOIN LOYALTY REWARD CLUB API
 */

add_action('rest_api_init', function(){
	register_rest_route('tvcapi', '/v2/join-loyalty-reward-club',
		array(
			'methods' => 'GET',
			'callback'=> 'join_loyalty_reward_club',
		)
	);
});

function join_loyalty_reward_club() {

	// Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['Token'] );
    $token 	  = get_user_meta($token_id[1], 'oauth_token', true);
    
    if (empty($headers['Token']) || $headers['Token'] != $token) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

 	$is_admin = user_can( $token_id[1], 'manage_options' );
	$joined   = get_user_meta( $token_id[1], 'join_reward_club', true );

	if ( $joined == 1 ) {
		return new WP_REST_Response(array('response_code' => '200', 'message' => esc_html__('You have already joind.', 'wpestate')), 200);

	} elseif ( !$is_admin ) {

 		// $join_club = $_GET['join_reward_club'];
 		// if ( isset ( $join_club ) && $join_club == 1 ) {

	    	update_user_meta($token_id[1], 'join_reward_club', 1);
	    	return new WP_REST_Response(array('response_code' => '200', 'message' => esc_html__('Congratulations! You have successfully joined the loyalty reward ', 'wpestate')), 200);
	    // }

	    die();
 	}
}

