<?php 
/*
  Shopping Checkout - Traveller contact details 

*/
add_action('rest_api_init', function() {
    register_rest_route('tvcapi', '/v2/traveller-contact-details',
        array(
            'methods'  => 'POST',
            'callback' => 'shopping_checkout',
        )
    );
});
function shopping_checkout(){
    // Check Oath Token
    $headers = apache_request_headers();
    $token_id =  explode( "-qe_aw-", $headers['Token'] );
    $token = get_user_meta($token_id[1], 'oauth_token', true);

    if (empty($headers['Token']) || $headers['Token'] != $token) {
      // Error Message
      return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

    $b_id = $_GET['booking_id'];
    $p_id = $_GET['property_id'];
    
    $p_id = get_post($p_id);
        if(isset($b_id) && !empty($b_id) && isset($p_id) && !empty($p_id))
        {
           
            $args = array(

                    'chekout_contactname'   => $_POST['chekout_contactname'],
                    'travellername' => $_POST['travellername'],
                    'country_code'  => $_POST['country_code'],
                    'mobile'        => $_POST['mobile'],
                        
                    );
            
            $error = array();
            if( !isset( $args['chekout_contactname']) || !preg_match("/^([a-zA-Z' ]+)$/",$args['chekout_contactname'])) {
                return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter contactname or invalid contactname', 'wpestate')), 400);
            }elseif( !isset( $args['travellername']) || !preg_match("/^([a-zA-Z' ]+)$/",$args['travellername'])) {
                return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter travellername or invalid travellername', 'wpestate')), 400);
            }elseif( !isset($args['country_code']) || !preg_match("/^(\+)([1-9]{2})$/",$args['country_code'])) {
                return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter country code or invalid country code', 'wpestate')), 400);
            }elseif( !isset($_POST['mobile']) || !preg_match('/^[0-9]{10}+$/',$_POST['mobile'])) {
                return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter Phone Number or invalid Phone Number', 'wpestate')), 400);
            }
            else
            {  
                $phone = $args['country_code'].$args['mobile'];         
        
                                
                update_post_meta($b_id,'chekout_contactname', $args['chekout_contactname']);

                update_post_meta($b_id,'travellername', $args['travellername']);
                update_post_meta($b_id,'mobile', $phone);
                return new WP_REST_Response (array(
				        'contact_details' =>  $args),
				        200);
            }
        }
        else{
        	return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Property id is invalid', 'wpestate')), 401);
        } 
    
}



    