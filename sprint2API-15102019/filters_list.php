<?php

/**
 * Multiple FILTERS API
 */

add_action('rest_api_init', function() {
    register_rest_route('tvcapi', '/v2/filters-list',
        array(
            'methods'  => 'GET',
            'callback' => 'filters_list',
        )
    );
});

function filters_list() {

	// Property filters
	global $wpdb;

	// Ratings Reviews
	$rating = array();
	for ($r=1; $r <= 5; $r++) {
		$rating[] = array(
			'id' => "".$r."",
			'value' => "".$r."",
		);
	}

	// Budgets values
	$budget = array('id' => '25-1000', 'value' =>__('25-1000', 'wpestate'));
	

	// Property Filters (Property Category)
	$property_filter = $wpdb->get_results("SELECT  t.name, tt.term_id FROM wp_terms AS t  INNER JOIN wp_term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy IN ('property_category') ORDER BY t.name ASC", ARRAY_A);

	$property_cat = array();
	for ($p=0; $p < count($property_filter); $p++) {
		$property_cat[] = array(
			'id'       => $property_filter[$p]['term_id'],
			'value' => $property_filter[$p]['name']
		);
	}

	// 24-hour front desk (User can only checkin)
	$frontdeskopen = array('id' => '24', 'value' =>__('Front Desk Open 24/7', 'wpestate'));

	// Reservation Policy
	$free_cancel = array('id' => 'free', 'value' =>__('Free Cancellation', 'wpestate'));

	// Beach Acsess 
	$amenities_beach = $wpdb->get_results("SELECT term_id, name FROM `wp_amenities_filter` WHERE status = 1 AND new_amenity_id = 3", ARRAY_A);
	$amenities_beach_access = array();
	for ($a=0; $a < count($amenities_beach); $a++) {
		$amenities_beach_access[] = array(
			'id' 	=> $amenities_beach[$a]['term_id'],
			'value' => $amenities_beach[$a]['name']
		);
	}

	// Accommodation Type (Room type)
	$room_type = $wpdb->get_results("SELECT tt.term_id, t.name  FROM wp_terms AS t  INNER JOIN wp_term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy IN ('property_action_category') ORDER BY t.name ASC", ARRAY_A);
	$room_types = array();
	for ($rt=0; $rt < count($room_type); $rt++) { 
		$room_types[] = array(
			'id'    => $room_type[$rt]['term_id'],
			'value' => $room_type[$rt]['name']
		);
	}

	// Rooms Facilities
	$room_amenities = $wpdb->get_results( "SELECT tt.term_id, t.name, t.slug FROM wp_terms AS t  INNER JOIN wp_term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy IN ('amenities') AND tt.parent = 91 ORDER BY t.name ASC", ARRAY_A );
	for ($child=0; $child < count($room_amenities) ; $child++) { 

		$room_facilities[] = array(
			'id'    => $room_amenities[$child]['term_id'],
			'value' => $room_amenities[$child]['name']
		);
	}

	// Facilities
	$facilities_amenities = $wpdb->get_results( "SELECT tt.term_id, t.name, t.slug FROM wp_terms AS t  INNER JOIN wp_term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy IN ('amenities') AND tt.parent = 100 ORDER BY t.name DESC", ARRAY_A );
	for ($fa=0; $fa < count($facilities_amenities) ; $fa++) { 
		# code...
		$facilities[] = array(
			'id'    => $facilities_amenities[$fa]['term_id'],
			'value'	=> $facilities_amenities[$fa]['name']
		);
	}

	// Security
	$security_amenities = $wpdb->get_results( "SELECT tt.term_id, t.name, t.slug FROM wp_terms AS t  INNER JOIN wp_term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy IN ('amenities') AND tt.parent = 97 ORDER BY t.name DESC", ARRAY_A );
	for ($s=0; $s < count($security_amenities) ; $s++) {
		$security[] = array(
			'id'    => $security_amenities[$s]['term_id'],
			'value' => $security_amenities[$s]['name']
		);
	}

	// Property Accessibility
	$prop_access_amenities = $wpdb->get_results( "SELECT tt.term_id, t.name, t.slug FROM wp_terms AS t  INNER JOIN wp_term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy IN ('amenities') AND tt.parent = 150 ORDER BY t.name DESC", ARRAY_A );
	for ($pa=0; $pa < count($prop_access_amenities) ; $pa++) { 
		$property_access[] = array(
			'id'    => $prop_access_amenities[$pa]['term_id'],
			'value' => $prop_access_amenities[$pa]['name']
		);
	}

	// START ALL Filters Menu
	$filters_settings = array(
		// 1. Ratings 
		array(
			'display_title' => __('Property Class', 'wpestate'),
			'name'  => 'ratings',
			'values'=> $rating,
		),
		// 2. Budgets Values
		array(
			'display_title' => __('Your Budgets', 'wpestate'),
			'name'  => 'budgets',
			'values'=> array($budget),
		),
		// 3. Property Filters (Property Category)
		array(
			'display_title' => __('Property Filters', 'wpestate'),
			'name'			=> 'property_categories',
			'values'		=> $property_cat,
		),
		// 4. 24-hour front desk (User can only checkin)
		array(
			'display_title' => __('24-hour front desk', 'wpestate'),
			'name'			=> 'checkin24hours',
			'values'		=> array($frontdeskopen)
		),
		// 5. Reservation Policy
		array(
			'display_title' => __('Reservation Policy', 'wpestate'),
			'name'			=> 'free_cancellation',
			'values'		=> array($free_cancel)
		),
		// 6. Beach Acsess 
		array(
			'display_title' => __('Beach Access', 'wpestate'),
			'name'			=> 'beach_amenities',
			'values'		=> $amenities_beach_access
		),
		// 7. Accommodation Type (Room type)
		array(
			'display_title' => __('Accommodation Type', 'wpestate'),
			'name'			=> 'room_types',
			'values'		=> $room_types
		),
		// 8. Rooms Facilities
		array(
			'display_title' => __('Rooms Facilities', 'wpestate'),
			'name'  => 'room_facilities',
			'values'=> $room_facilities,
		),
		// 9. Facilities
		array(
			'display_title' => __('Facilities', 'wpestate'),
			'name'  => 'facilities',
			'values'=> $facilities,
		),
		// 10. Security
		array(
			'display_title' => __('Security', 'wpestate'),
			'name'  => 'security',
			'values'=> $security,
		),
		// 11. Property Accessibility
		array(
			'display_title' => __('Property Accessibility', 'wpestate'),
			'name'  => 'property_accessibility',
			'values'=> $property_access,
		),
	);
	// print_r($filters_settings);
	// END ALL Filters Menu
	return new WP_REST_Response(
		array (
			'response_code' => '200',
			'data'          => $filters_settings
		),
	200);
}