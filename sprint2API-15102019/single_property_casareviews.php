<?php

/**
 * Single property ratings casa reviews API
 */

add_action('rest_api_init', function(){
	register_rest_route('tvcapi', '/v2/single-property-casa-ratings',
		array(
			'methods' => 'POST',
			'callback'=> 'single_property_casa_ratings',
		)
	);
});

function single_property_casa_ratings() {

	// Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['Token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);

    if ($headers['Token'] != $token || empty($headers['Token'])) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Token is invalid', 'wpestate')), 400);
    }
    // END

	$property_id = sanitize_text_field( filter_input( INPUT_POST, 'property_id') );

	if (!isset($property_id) || !is_numeric($property_id)) {
		return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Only numeric value allowed!', 'wpestate')), 400);
	}

	// START Single Property Ratings Reviews Details
	global $wpdb;

	// echo "SELECT comment_ID, user_id, comment_content FROM wp_comments WHERE comment_post_ID = ".$property_id." AND comment_approved = 1";

	$total = $comments_count = 0;
	$comment_id = $wpdb->get_results("SELECT comment_ID, user_id, comment_content FROM wp_comments WHERE comment_post_ID = ".$property_id." AND comment_approved = 1", ARRAY_A);
	// print_r($comment_id);
	// $commentsid 	  = array_column($comment_id, 'comment_ID');
	// $comments   	  = array_column($comment_id, 'user_id');
	$fields_val 	  = wpestate_get_review_fields();
	$stars_fields     = array();
	$stars_fields_sum = array();
	$stars_averages   = array();
	$store 			  = array();

	// $imported_count_total = 0;
	$casa_total = $casa_comnts_count = 0;
	
	$comments_data = $airbnb_comments = $tripad_comments = $booking_comments = $expedia_comments = $other_comments = array();
	for ($i=0; $i < count($comment_id); $i++) { 
		// echo $comment_id[ $i ]['comment_ID'].',';
		// print_r($comment_id[ $i ]['comment_ID']);
		$reviews_values   = get_comment_meta( $comment_id[ $i ]['comment_ID'], 'review_stars', true );

		$imported_refer   = get_comment_meta( $comment_id[ $i ]['comment_ID'], 'ota_reference', true );
		
		// Casa website reviews list
		$tmp_feilds = json_decode($reviews_values, true);
		if ( $imported_refer == '' ) {
			
			$casa_comnts_count++;
			$tmp   		= json_decode($reviews_values, true);
			$casa_total += intval( $tmp['rating'] );

			// ------------------------
			// Profile Image 
			$comments_data[$i]['comment_id'] = $comment_id[ $i ]['comment_ID'];
			$userid_agent = get_user_meta($comment_id[ $i ]['user_id'], 'user_agent_id', true);
			$thumb_id     = get_post_thumbnail_id($userid_agent);
	        $preview      = wp_get_attachment_image_src($thumb_id, 'thumbnail'); 
	        $comments_data[ $i ]['profile_image'] = $preview[0];
	        if (empty($comments_data[ $i ]['profile_image'])) {
	        	
		        $comments_data[ $i ]['profile_image'] = get_template_directory_uri().'/img/default_user_agent.gif';
	        }

			// Reviewer Name
			$userid_agent   =   get_user_meta($comment_id[ $i ]['user_id'], 'user_agent_id', true);
			$comments_data[ $i ]['reviewer_name']  =   get_the_title($userid_agent);
			if (empty( $comments_data[ $i ]['reviewer_name'] )) {

				$admin_email   = get_option('admin_email');
	            $the_user      = get_user_by('email', $admin_email);
	            $admin_user_id = $the_user->ID;
	            $admin_name    = get_user_by('id', $admin_user_id);
	            $comments_data[ $i ]['reviewer_name'] = $admin_name->display_name;
			}

			// Star rating value
			if ( empty( $tmp['rating']) ) {
				# code...
				$comments_data[ $i ]['rating_value'] = esc_html__('0', 'wpestate');
			} else {
				$comments_data[ $i ]['rating_value'] = $tmp['rating'];
			}


			// Date
			$comments_data[ $i ]['date'] = esc_html__( 'Posted on ','wpestate' ).get_comment_date('j M Y',$comment_id[ $i ]['comment_ID']);
			
			// Comment Content
			$comments_data[ $i ]['content'] = esc_html__( $comment_id[ $i ]['comment_content'],'wpestate' );
			// ------------------------

		}
		// -----
		foreach ( $fields_val['fields'] as $field_key => $field_value ) {
            $stars_fields[ $field_key ][] = $tmp_feilds[ $field_key ];
        }

	}
	// Property Fields Calculations
	foreach ( $fields_val['fields'] as $field_key => $field_value ) {
		$stars_fields_sum[ $field_key ]   = array_sum( $stars_fields[ $field_key ] );
		// $a = count($stars_fields[ $field_key ]);
 		$f_round                    = round($stars_fields_sum[ $field_key ] / count($stars_fields[ $field_key ]), 1);
 		// echo "from-api-->";
		$stars_averages[ $field_key ] = wpestate_round_to_nearest_05( $f_round );
        $store[]                      = array('key' => $field_key, 'value' => $stars_averages[ $field_key ] );
	}
	$star_rating   = $casa_total / $casa_comnts_count;

	// Pagination
    $casa_cur_page = sanitize_text_field( filter_input( INPUT_POST, 'current_page' ) );
    $per_page      = sanitize_text_field( filter_input( INPUT_POST, 'per_page' ) );
    $per_page      = ( $per_page == '' || $per_page == 0 ) ? 2 : $per_page;
    $casa_cur_page = ( $casa_cur_page == '' || $casa_cur_page == 0 ) ? 1 : (int)$casa_cur_page;  

    // $per_page      = 2;
    $comment_pages = ceil(count($comments_data) / $per_page);
    $casa_comments = array_chunk($comments_data, $per_page);
    $coments_data  = $casa_comments [$casa_cur_page - 1];
	// Local Casa
	// echo "--".$coments_data;
	// print_r($coments_data);
	if ( $comment_pages == 0 ) {
		$coments_data = array();
	}/* else {
		$coments_data = $coments_data;
	}*/
	$local_casa = array(

		// Casa website
		// array(
			// 'casa_reviews' => esc_html__('Casa Website Reviews ' .'('.$casa_comnts_count.' Reviews)', 'wpestate'),
			// 'total_pages'  => $comment_pages,
			// 'current_page' => $casa_cur_page,
			'fields' => array(
					'total_reviews'    => $casa_comnts_count,
					'total_count_star' => number_format( $star_rating, 1, '.', '' ),
					'fields_values'	   => $store,
				),
			'comments' => array_values($coments_data),
		// ),

	); 
	return new WP_REST_Response(
		array(
			'response_code'  => "200", 
			'casa_reviews' => esc_html__('Casa Website Reviews ' .'('.$casa_comnts_count.' Reviews)', 'wpestate'),
			'total_pages'  => $comment_pages,
			'current_page' => $casa_cur_page,
			'data' => $local_casa,
			),
	200);

}