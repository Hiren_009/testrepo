<?php

/**
 * Stripe Payment Gateway API
 */

// add_action('rest_api_init', function(){
// 	register_rest_route('tvcapi', '/v2/stripe-pament',
// 		array(
// 			'methods' => 'GET',
// 			'callback'=> 'stripe_pament',
// 		)
// 	);
// });

function stripe_pament( $user_id, $booking_id, $invoice_id, $amount ) {

	echo "uid ".   $user_id."\n";
	echo "bid ".   $booking_id."\n";
	echo "inid ".  $invoice_id."\n";
	echo "amount ".$amount."\n";
	
	
    /*
	 * Stripe Payment Integration key 
     */
    require_once get_template_directory().'/libs/stripe/lib/Stripe.php';
	$allowed_html=array();
	$stripe_secret_key              =   esc_html( get_option('wp_estate_stripe_secret_key','') );
	$stripe_publishable_key         =   esc_html( get_option('wp_estate_stripe_publishable_key','') );
	$stripe = array(
	    "secret_key"      => $stripe_secret_key,
	    "publishable_key" => $stripe_publishable_key
	);

	// print_r($stripe);
	Stripe::setApiKey($stripe['secret_key']);   
    /*
	 * END Stripe Payment Integration key 
     */
    

    $current_user   =   get_userdata($user_id);
	$userID         =   $user_id;
	$user_email     =   $current_user->user_email;
	$username       =   $current_user->user_login;
	$submission_curency_status = esc_html( get_option('wp_estate_submission_curency','') );

    // Payment for booking
	// if( is_email($_POST['stripeEmail']) ){
	//     $stripeEmail = wp_kses($_POST['stripeEmail'],$allowed_html);
	//     $stripeEmail = 'api08102019.webdesk@gmail.com';
	// } else {
	//     // exit('none mail');
	//     return new WP_REST_Response( array ( 'response_code' => '401', 'message' => esc_html__('none mail', 'wpestate') ), 401);
	// }

	if ( isset( $booking_id ) ){
	    try {    

	    	$stripeEmail  = wp_kses($_POST['stripeEmail'],$allowed_html);
	        $stripeEmail  = 'wds.testing@gmail.com';
	        $token        = wp_kses($_POST['stripeToken'],$allowed_html);
	        $token  	  = 'tok_1FTiaYBU35d76cNnYNXcA4BK';
	        // $token  = $stripeToken;
	        // print_r($token);
	        $customer = Stripe_Customer::create(array(
	            'email' => $stripeEmail,
	            'card'  => $token
	        ));
	        // print_r($customer );
	        // die();
	        $userId     = $user_id;
	        $booking_id = $booking_id;
	        $depozit    = $amount;
	        $charge     = Stripe_Charge::create(array(
	            'customer' => $customer->id,
	            'amount'   => $depozit,
	            'currency' => $submission_curency_status
	        ));
	       	// print_r($charge);
	        $is_stripe    = 1;
	        $point_status = 0;
	       
	        wpestate_booking_mark_confirmed($booking_id,$invoice_id,$userId,$depozit,$user_email,$is_stripe, $point_status);  // $point_status added by vinod
	    	
	    	// wpestate_booking_mark_confirmed($booking_id,$invoice_id,$userId,$depozit,$user_email,$is_stripe, $point_status)."\n";
	    	// print_r(wpestate_booking_mark_confirmed($booking_id,$invoice_id,$userId,$depozit,$user_email,$is_stripe, $point_status));
	    }
	    catch ( Exception $e ) {

	    	// $error =    '<div class="alert alert-danger">
      //                   <strong>Error!</strong> '.$e->getMessage().'
      //               </div>';
	        print $e->getMessage();
	        // return array( 'error' => $e->getMessage() );
	    }

	    
	}
}