
<?php
/*
  Shopping Checkout Payment confirmation popup
*/
 add_action('rest_api_init', function() {
    register_rest_route('tvcapi', '/v2/booking-content-details',
        array(
            'methods'  => 'GET',
            'callback' => 'booking_content_details',
        )
    );
});
function booking_content_details(){
	global $wpdb;
	// Check Oath Token
	$headers = apache_request_headers();
	$token_id =  explode( "-qe_aw-", $headers['Token'] );
	$token = get_user_meta($token_id[1], 'oauth_token', true);

	if (empty($headers['Token']) || $headers['Token'] != $token) {
	  // Error Message
	  return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
	}
	// END

	$b_id = $_GET['booking_id'];
	$b_id = $wpdb->get_row("SELECT ID FROM wp_posts WHERE ID = '".$b_id."'",ARRAY_A);
	
	$property_id = $_GET['property_id'];
	$property_id = $wpdb->get_row("SELECT ID FROM wp_posts WHERE ID = '".$property_id."'",ARRAY_A);
	
	$check_in  = $_GET['check_in'];
	$check_out = $_GET['check_out'];

	$data = array();
	
	if(isset($b_id['ID']) && isset($property_id['ID']) && isset($check_in) && isset($check_out) && !empty($b_id['ID']) && !empty($property_id['ID']) && !empty($check_in) && !empty($check_in)){
	
		$booking_from_date          =   $check_in;
	    $booking_to_date            =   $check_out;

	 	$cancellation   =   get_post_meta($property_id['ID'], 'cancellation', true);
	 	
	   	$mega_details_array = wpml_mega_details_adjust( $property_id['ID'] );

	    	// NonRefundable message
		    if ($cancellation == 'NonRefundable') {

		        $c_value = $cancellation;

		    // Free Refund
		    } elseif ($cancellation == 'Free Refund') {

		        $c_value = $cancellation;

		    // 24 Hours
		    } elseif ($cancellation == '24 Hours') {

		        $allow = date( "d F Y", strtotime("-1 days",  strtotime($booking_from_date)) );

		        $today = date("M d Y");
		        if ( strtotime( $today ) <= strtotime( $allow ) ) {
		            $c_value = 'FREE cancellation before '.date("H:i"). " on ".date("d F Y", strtotime($allow));
		        }

		    // 48 Hours
		    } elseif ($cancellation == '48 Hours') {
		        $allow = date( "d F Y", strtotime("-2 days",  strtotime($booking_from_date)) );

		        $today = date("M d Y");
		        if ( strtotime( $today ) <= strtotime( $allow ) ) {
		            $c_value = "FREE cancellation before ".date("H:i"). " on ".date("d F Y", strtotime($allow));
		        }

		    // 72 Hours
		    } elseif ($cancellation == '72 Hours') {
		        $allow = date( "d F Y", strtotime("-3 days",  strtotime($booking_from_date)) );

		        $today = date("M d Y");
		        if ( strtotime( $today ) <= strtotime( $allow ) ) {
		            $c_value = "FREE cancellation before ".date("H:i"). " on ".date("d F Y", strtotime($allow));
		        }
		    } elseif ($cancellation == '1 Week') {
		        
		        $allow = date( "d F Y", strtotime("-7 days",  strtotime($booking_from_date)) );

		        $today = date("M d Y");
		        if ( strtotime( $today ) <= strtotime( $allow ) ) {
		            $c_value = "FREE cancellation before ".date("H:i"). " on ".date("d F Y", strtotime($allow));
		        } else {
		            $c_value = "Non Refundable";
		        }

		    } elseif ($cancellation == '2 Weeks') {
		        $allow = date( "d F Y", strtotime("-14 days",  strtotime($booking_from_date)) );

		        $today = date("M d Y");
		        if ( strtotime( $today ) <= strtotime( $allow ) ) {
		            $c_value = "FREE cancellation before ".date("H:i"). " on ".date("d F Y", strtotime($allow));
		        }
		    } elseif ($cancellation == '1 Month') {
		        $allow = date( "d F Y", strtotime("-1 month",  strtotime($booking_from_date)) );

		        $today = date("M d Y");
		        if ( strtotime( $today ) <= strtotime( $allow ) ) {
		            $c_value = "FREE cancellation before ".date("H:i"). " on ".date("d F Y", strtotime($allow));
		        }
		    } else {
		        $c_value = "Non Refundable";
		    }

	    // Check in and Checkout range message
	    $property_check_in = get_post_meta($property_id['ID'], 'check-in-hour', true);
	    $property_check_in_to = get_post_meta($property_id['ID'], 'check-in-hour-to', true);
	    
	    $property_check_out = get_post_meta($property_id['ID'], 'check-out-hour', true);
	    $property_check_out_to = get_post_meta($property_id['ID'], 'check-out-hour-to', true);
	    
	    $hours_check_out_24 = get_post_meta($property_id['ID'], 'hours_check_out_24', true);
	    $hours_check_in_24 = get_post_meta($property_id['ID'], 'hours_check_in_24', true);
	    
	    if ($hours_check_in_24 != '') {
	       $h_checkin = stripslashes($hours_check_in_24);
	    } elseif ($property_check_in != '') {
	       $h_checkin = stripslashes($property_check_in);
	        if($property_check_in_to !=''){
	            $h_checkin = $property_check_in_to;
	        }
	    }
	    if($hours_check_out_24 !=''){
	        $h_check_o = stripslashes($hours_check_out_24);
	        
	    }else if($property_check_out != ''){
	       $h_check_o =  stripslashes($property_check_out);
	        if($property_check_out_to !=''){
	            $h_check_o = $property_check_out_to;
	        }
	        
	    }

		//Your Booking Includes;
	   $return_amin = array();
	    $amenities_cats = get_terms( array('taxonomy' => 'amenities','hide_empty' => false,'parent' => 0) );
	    $amenities_terms = wp_get_object_terms( $property_id['ID'],  'amenities' );

	    if ( ! empty( $amenities_terms ) ) {
	        if ( ! is_wp_error( $amenities_terms) ) {
	            $i = 1;
	            
	            foreach( $amenities_terms as $term ) {

	                if ($i++ < 5) {
	                    $cartpage_amms = $term->name;
	                    $return_amin =  $cartpage_amms;
	                }
	            }
	            
	        }
	    } else {
	        $return_amin = 'This property not have any aminities.';
	    }
	    $pay_info = array();
	    $pay_info['payment'] = array(
	    				'0' => 'We use secure transmission',
	    				'1' => 'We protect your personal information',
	    				'2' => home_url().'/wp-content/uploads/2019/03/payment.png',
	    			);

	    //checkout print message 
	    $checkout_print = 'This payment will be processed in the U.S. WiFi issues have been addressed. Internet is available in all areas of the villa.Your reservation will be confirmed after clicking book. No need to call the property to confirm. Please make contact with '.get_the_title($property_id['ID']).' 72 hours prior to arrival. You can use the portal to message or email the property. To call the property use the phone number on your itinerary Payment is processed in full at the time of booking. Property Cancellation policy will apply firmly. The property will contact you 72 hours in advance to confirm check-in instructions.Guests are required to show a photo identification. Please note that all Special Requests are subject to availability and additional charges may apply.A damage deposit of USD '.get_post_meta($property_id['ID'], 'security_deposit', true).' is required on arrival. This will be collected as a cash payment. All cash deposits are refundable at check-out providing there are no damages by this property and due.Minimum Age for check in 18.';
	         $secure_msg = 'We use secure transmission and encrypted storage to protect your personal information.'; 
	         $ratesqouted = 'Rates are quoted in US dollars. Taxes and Fees due upon arrival at the property are quoted in US dollars and are payable in local currency';

	    // NonRefundable message
	    
	    if ($cancellation == 'Free Refund') {

	        $cancel = 'Cancel within ('.$cancellation.') of arrival and get a full refund.';

	        // 24 Hours
	    } elseif ($cancellation == '24 Hours') {

	        $cancel = "Cancel within (1 Day) of arrival and get a full refund.";

	        // 48 Hours
	    } elseif ($cancellation == '48 Hours') {

	        $cancel = "Cancel within (2 Days) of arrival and get a full refund.";

	        // 72 Hours
	    } elseif ($cancellation == '72 Hours') {
	        
	        $cancel = "Cancel within (3 Days) of arrival and get a full refund.";

	    } elseif ($cancellation == '1 Week') {
	        // echo "7 days";
	        $cancel = "Cancel within (7 Days) of arrival and get a full refund.";

	    } elseif ($cancellation == '2 Weeks') {

	        $cancel = "Cancel within (14 Days) of arrival and get a full refund.";

	    } elseif ($cancellation == '1 Month') {
	        $cancel = "Cancel within (1 month) of arrival and get a full refund.";
	    } elseif ($cancellation == 'NonRefundable') {
	         $cancel = "This is a nonrefundable reservation.";
	    } else {
	        $cancel = "This is a nonrefundable reservation.";
	    }
	    
	    // end cancellation msg
		
		//Refundable Deposit
		$booking_to_date   = esc_html(get_post_meta($b_id['ID'], 'booking_to_date', true)); 
	    $rf = get_post_meta($property_id['ID'], 'security_deposit', true);
	                if ($rf != 0) {
	                      $rf_data = esc_html__( 'Due on','wpestate').' '.date('M d, Y', strtotime($booking_to_date)).'$'.get_post_meta($property_id['ID'], 'security_deposit', true);
	                   } else {
	                        $rf_data =  "";
	                   }
	   
	    //country phone code
	    $contact_code = array(
					    	array(
						    	'key' 	=> "GB" ,
						    	'value' => 'UK +44',
						    ),
						    array(
						    	'key' 	=>  "US",
						    	'value' => 'USA +1',
						    ),
						    array(
						    	'key' 	=> "AFG",
						    	'value' => 'Afghanistan +93',
						    ),
						    array(
						    	'key' 	=> "ALB",
						    	'value' => 'Albania +355',
						    ),
						    array(
						    	'key' 	=> "AFG",
						    	'value' => 'Albania +355',
						    ),
						    array(
						    	'key' 	=> "DZ",
						    	'value' => 'Algeria +213',
						    ),
						    array(
						    	'key' 	=> "ASM",
						    	'value' => 'merican Samoa +1',
						    ),
						    array(
						    	'key' 	=> "AND",
						    	'value' => 'Andorra +376',
						    ),
						    array(
						    	'key' 	=> "AO",
						    	'value' => 'Angola +244',
						    ),
						    array(
						    	'key' 	=> "AI",
						    	'value' => 'Anguilla +1264',
						    ),
						    array(
						    	'key' 	=> "AR",
						    	'value' => 'Argentina +54',
						    ),
						    array(
						    	'key' 	=> "AM",
						    	'value' => 'Armenia +374',
						    ),
						    array(
						    	'key' 	=> "AW",
						    	'value' => 'Aruba +297',
						    ), 
						     array(
						    	'key' 	=> "AU",
						    	'value' => 'Australia +61',
						    ), 
						    array(
						    	'key' 	=> "AT",
						    	'value' => 'Austria +43',
						    ), 
						    array(
						    	'key' 	=> "AZ",
						    	'value' => 'Azerbaijan +994',
						    ), 
						    array(
						    	'key' 	=> "BS",
						    	'value' => 'Bahamas +1242',
						    ), 
						    array(
						    	'key' 	=> "BH",
						    	'value' => 'Bahrain +973',
						    ), 
						    array(
						    	'key' 	=> "BD",
						    	'value' => 'Bangladesh +880',
						    ),
						    array(
						    	'key' 	=> "BB",
						    	'value' => 'Barbados +1246',
						    ),
						    array(
						    	'key' 	=> "BY",
						    	'value' => 'Belarus +375',
						    ), 
						    array(
						    	'key' 	=> "VIR",
						    	'value' => 'U.S virgin islands +1',
						    ), 
						    array(
						    	'key' 	=> "UG",
						    	'value' => 'Uganda +256',
						    ), 
						    array(
						    	'key' 	=> "UA",
						    	'value' => 'Ukraine +380',
						    ), 
						    array(
						    	'key' 	=> "AE",
						    	'value' => 'United Arab Emirates +971',
						    ), 
						    array(
						    	'key' 	=> "UY",
						    	'value' => 'Uruguay +598',
						    ), 
						    array(
						    	
						    	'key' 	=> "UZ",
						    	'value' => 'zbekistan +7',
						    ),
						    array(
						    	'key' 	=> "VU",
						    	'value' => 'Vanuatu +678',
						    ),
						    array(
						    	'key' 	=> "VA",
						    	'value' => 'Vatican City +379',
						    ),
						    array(
						    	'key' 	=> "VE",
						    	'value' => 'Venezuela +58',
						    ),
						    array(
						    	'key' 	=> "VN",
						    	'value' => 'Vietnam +84',
						    ),
						    array(
						    	'key' 	=> "WF",
						    	'value' => 'Wallis &amp; Futuna +681',
						    ),
						    array(
						    	'key' 	=> "YE",
						    	'value' => 'Yemen (North)+969',
						    ),
						    array(
						    	'key' 	=> "YE",
						    	'value' => 'Yemen (South)+967',
						    ),
						    array(
						    	'key' 	=> "ZM",
						    	'value' => 'Zambia +260',
						    ),
						    array(
						    	'key' 	=> "ZW",
						    	'value' => 'Zimbabwe +263',
						    ),
					               
					    );
	    $prophours_msg  = ['24', '48', '72'];
	    $random = array_rand($prophours_msg);
	   	$p_msg = 'Limited supply in '.strip_tags(get_the_term_list($property_id['ID'], 'property_city', '', ', ', '')).': Reserve Immediately! '.rand(1, 10).' like '.get_the_title($property_id['ID']).' were booked in the last '. $prophours_msg[$random] .' hours.Book Now!';

	    //property title
	    $ptitle = get_the_title($property_id['ID']);

	    // Room Type
	    $prop_action_category_array = get_the_terms($property_id['ID'], 'property_action_category');
	    if(isset($prop_action_category_array[0])){
	        $prop_action_category_selected = $prop_action_category_array[0]->name;
	    } else {
	        $prop_action_category_selected = "";

	    }
	    // Property Type
	    $prop_category_array =   get_the_terms($property_id['ID'], 'property_category');
	    if(isset($prop_category_array[0])){
	         $prop_category_selected = $prop_category_array[0]->name;
	    }

		$data = array(
					array(
						'contact_code' => $contact_code,
					),	
					array(
						'p_message' => $p_msg,
					),
					array(
						'title'  		=> $ptitle,
						'room_type' 	=> $prop_action_category_selected,
						'property_type' => $prop_category_selected
					),
					array(

						'name' => 'Special Request',
						'value' => 'Most properties are capable of handling special/accessbility request (e.g Roll away beds, late check-in, accessible bathrooms). Once the booking have been confirmed you will be able to speak with the property directly via the portal using your username and password you entered below.',
					),
					array(
						'name'  => 'Refundable/NonRefundable',
						'value' => $c_value,
					),
					array(
						'name'  => 'Guest check in',
						'value' => $h_checkin,
					),
					array(
						'name'  => 'Check out',
						'value' => $h_check_o,
					),
					array(
						'name'  => 'Your Booking Includes',
						'value' => $return_amin,
					),
					array(
						'name'  => 'Payment Secure Info',
						'value' => $pay_info,
					),
					array (
						'name'  => 'Check out Print Meassage',
						'value' => $checkout_print,
					),
					array (
						'name'  => 'Secure Message',
						'value' => $secure_msg,
					),
					array (
						'name'  => 'Taxes and Fees',
						'value' => $ratesqouted,
					),
					array(
						'name'  => 'Cancellation Policy',
						'value' => $cancel,
					),
				);

		return new WP_REST_Response(
				array(
				  'response_code'  => "200", 
				  'data' => $data,
				),
			200);
	}else{
		return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Invalid URL', 'wpestate')), 401);
	}

}