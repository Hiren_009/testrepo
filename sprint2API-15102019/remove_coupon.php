<?php

/**
 * Remove Copuon Code API
 */

add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/remove-coupon',
        array(
            'methods' => 'POST',
            'callback'=> 'remove_coupon',
        )
    );
});

function remove_coupon() {
    
    // Check Oath Token
    $headers  = apache_request_headers();
    $token_id =  explode( "-qe_aw-", $headers['Token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);
    // $user_id  = 16;
    if (empty($headers['Token']) || $headers['Token'] != $token) {
        // Error Message
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

    // ---------------
    $property_id        =  sanitize_text_field ( filter_input ( INPUT_POST, 'property_id' ) );
    $booking_from_date  =  sanitize_text_field ( filter_input ( INPUT_POST, 'fromdate') );
    $booking_to_date    =  sanitize_text_field ( filter_input ( INPUT_POST, 'todate' ) );
    $guest_no           =  sanitize_text_field ( filter_input ( INPUT_POST, 'guest_no' ) );
    $adult_type_val     =  sanitize_text_field ( filter_input ( INPUT_POST, 'adult_type_val' ) );
    $child_type_val     =  sanitize_text_field ( filter_input ( INPUT_POST, 'child_type_val' ) );

    // $coupon_code        =  sanitize_text_field ( filter_input ( INPUT_POST, 'coupon_code' ) );
    $remove_coupon      =  sanitize_text_field ( filter_input ( INPUT_POST, 'remove_coupon' ) );

    $check_in           =  strtotime($booking_from_date);
    $check_out          =  strtotime($booking_to_date);

    // $property_id       = 8230;
    // $booking_from_date = '2019-10-10';
    // $booking_to_date   = '2019-10-12';
    // $coupon_code       = 'SAVE766587'; // Percentage
    // $coupon_code = 'SAVE757117'; // Fixed Multi Use
    // $coupon_code = 'SAVE747549'; // Fake

    $max_allow_guest   = intval( get_post_meta ( $property_id, 'max_allow_guest', true ) );
    $guest_dropdown_no = intval( get_option ( 'wp_estate_guest_dropdown_no','' ) );
    
    /** 
     * START VALIDATIONS
     * CHECK MIN DAYS SITUATION
     */

    $mega_details     = wpml_mega_details_adjust( $property_id );
    $min_days_booking = intval( get_post_meta($property_id, 'min_days_booking', true) );  
    $min_days_value   = 0;

    if ( is_array( $mega_details ) && array_key_exists ( $check_in,$mega_details ) ) {
        if( isset ( $mega_details[$check_in]['period_min_days_booking'] ) ) {
            $min_days_value = $mega_details[$check_in]['period_min_days_booking'];

            if( ($check_in + ($min_days_value-1) * 86400) > $check_out ) {
                
                return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select at least min '.$mega_details[$check_in]['period_min_days_booking'].' Days ', 'wpestate')), 400);
            }
        }

    } elseif ( $min_days_booking > 0 ) {

        if( ( $check_in + ( $min_days_value - 1 ) * 86400 ) > $check_out ) {
            
            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select at least min '.$min_days_booking.' Days ', 'wpestate')), 400);
        }
    }
    /* --- END CHECK MIN DAYS SITUATION --- */

    if ( !isset($property_id) || !is_numeric($property_id) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Only numeric value allowed!', 'wpestate')), 400);
    }

    if( !isset( $booking_from_date ) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select first check in date', 'wpestate')), 400);
    }

    if( !isset( $booking_to_date ) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select check out date', 'wpestate')), 400);
    }
    
    $today = date("Y-m-d");
    if (strtotime($today) > $check_in) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Check in date should be today and onwards.', 'wpestate')), 400);
    }

    if ($check_in > $check_out) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Check out date should be greater than check in date.', 'wpestate')), 400);
    }

    if( !isset( $guest_no ) || !is_numeric($guest_no) ) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter no. of guests', 'wpestate')), 400);

    } elseif(  $guest_no < 1 ) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Guest should be greater than one', 'wpestate')), 400);

    } elseif ($guest_no > $guest_dropdown_no) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Guest should be '.$guest_dropdown_no.' or less', 'wpestate')), 400);

    } elseif ($guest_no > $max_allow_guest) {
        
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Max allowed guests are '.$max_allow_guest.'', 'wpestate')), 400);
    }

    if ( $guest_no > 1 ) {
        
        if ( !isset($adult_type_val) || !is_numeric($adult_type_val) ) {
            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select Guests!', 'wpestate')), 400);

        }
        if ( $adult_type_val == 0 || !is_numeric($adult_type_val) ) {
            
            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Children can go with at least one adult!', 'wpestate')), 400);

        }
        $total_guest = $adult_type_val + $child_type_val;
        if ( $guest_no != $total_guest ) {

            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Adult and Kids total should be equals to Guest!', 'wpestate')), 400);
        }
    }

    // COUPON VALIDATION
    if ( isset($_SESSION['myCoupon']['c_code']) && $_SESSION['myCoupon']['c_code'] == $coupon_code && $_SESSION['myCoupon']['user'] == $token_id[1] ) {
        
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Coupon already applied!', 'wpestate')), 400); 
    } 
    // END VALIDATIONS


    // ---------------


    if ( isset($_SESSION['myCoupon']) && $_SESSION['myCoupon']['user'] == $token_id[1] ) {

        if ( isset( $remove_coupon ) && $_SESSION['myCoupon']['c_code'] == $remove_coupon ) {

            unset($_SESSION['myCoupon']);
            // echo json_encode( array( 'response_code' => '200', 'message' => esc_html__('Removed your coupon code!', 'wpestate') ) );
            $booking_response = book_property( $data );

            $rmv_cpn = array( 'removed_coupon' => esc_html__('Removed your coupon code!', 'wpestate') );
            $merge_res = array_merge($rmv_cpn, $booking_response->data['data'] ); 
            // print_r($merge_res);
            return new WP_REST_Response ( array( 'response_code' => '200', 'data' => $merge_res ) );
            // return book_property( $data );

            // $book_prop_response = book_property( $data );
        
        }

    } else {

        return new WP_REST_Response ( array( 'response_code' => '200', 'message' => esc_html__('Already removed coupon code!', 'wpestate') ) );

    }
}