<?php
/*
* After filter single property listing 
*/
add_action('rest_api_init', function() {
    register_rest_route('tvcapi', '/v2/single-property-details',
        array(
            'methods'  => 'GET',
            'callback' => 'single_property_details',
        )
    );
});
function single_property_details(){
  // Check Oath Token
  $headers = apache_request_headers();
  $token_id =  explode( "-qe_aw-", $headers['Token'] );
  $token = get_user_meta($token_id[1], 'oauth_token', true);

  if (empty($headers['Token']) || $headers['Token'] != $token) {
      // Error Message
      return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
  }
  // END
  global $wpdb;
  
  $sid = $_GET['id'];
  
  $s_data = array();

  // Total Comments No. Local & Imported
  $total_comments = get_comments_number($sid);
  // if ( $total_comments > 0 ) {
  //   $total_comments = $total_comments.esc_html__(' Reviews', 'wpestate');
  // } else {
  //   $total_comments = $total_comments.esc_html__(' Review', 'wpestate');
  // }
  $reviews_count  = seprated_reviews_count ( $sid );
  // $casa_count     = $reviews_count['casa_count']; 
  // $imported_count = $reviews_count['import_count']; 

  // Is Favorite Porperty
  $user_option = 'favorites'.$token_id[1];
  $curent_fav  = get_option( $user_option );
  // print_r($curent_fav);
  if( $curent_fav ) {
      if ( in_array ( $sid, $curent_fav ) ){
          $is_favourite = true;
      } else {
          $is_favourite = false;
      }
  } else {
      $is_favourite = false;
  }

  //title
  $single_title       = get_the_title($sid);
  $s_description      = get_post_field('post_content', $sid);
  
  //Address
  $property_country   = get_post_meta($sid,'property_country',true);
  $property_county    = get_post_meta($sid,'property_county',true);
  $property_zip       = get_post_meta($sid,'property_zip',true);
  $property_address   = get_post_meta($sid,'property_address',true);
  $property_state     = get_post_meta($sid,'property_state',true);
  $property_city      = get_the_term_list($sid, 'property_city', '', ', ', '');
  $property_city      = strip_tags($property_city);

 //description
  $youtube            = get_post_meta($sid,'embed_video_type',true);
  $youtube_value      = get_post_meta($sid,'embed_video_id',true);
  
  $property_price     = get_post_meta($sid,'property_price',true);
  $extra_pay_options  = get_post_meta($sid,'extra_pay_options',true);
  
  $check_in_hour      = get_post_meta($sid,'check-in-hour',true);
  $check_in_hour_to   = get_post_meta($sid,'check-in-hour-to',true);
  $hours_check_in_24  = get_post_meta($sid,'hours_check_in_24',true);
  $check_out_hour     = get_post_meta($sid,'check-out-hour',true);
  $check_out_hour_to  = get_post_meta($sid,'check-out-hour-to',true);
  $hours_check_out_24 = get_post_meta($sid,'hours_check_out_24',true);

  // Check In Between
  if ( empty( $hours_check_in_24 ) ) {
    $check_in_between = $check_in_hour. ' - ' . $check_in_hour_to;
  } else {
    $check_in_between = $hours_check_in_24;
  }

  // Check Out Between
  if ( empty( $hours_check_out_24 ) ) {
    $check_out_between = $check_out_hour . ' - ' . $check_out_hour_to;
  } else {
    $check_out_between = $hours_check_out_24;
  }

  $late_check_in      = get_post_meta($sid,'late-check-in',true);
  $outdoor_facilities = get_post_meta($sid,'outdoor-facilities', true);
  $pro_size           = get_post_meta($sid,'property_size',true);
  $cancellation       = get_post_meta($sid,'cancellation',true);

  $property_latitude  = get_post_meta($sid,'property_latitude',true);
  $property_longitude = get_post_meta($sid,'property_longitude',true);

  $bedroom_desc       = get_post_meta($sid,'bedroom_desc',true);
  $property_bedrooms  = get_post_meta($sid,'property_bedrooms',true);
  $property_bathrooms = get_post_meta($sid,'property_bathrooms',true);

  

  //badroom description
  $return_broom = array();
  $bedroom_desc = get_post_meta($sid, 'bedroom_desc', true);
  if(isset($bedroom_desc) && is_array($bedroom_desc) && count($bedroom_desc) != 0){
      $p= 1;
      foreach($bedroom_desc as $bedroom_desc_val){
        if($bedroom_desc_val!= ''){
         $p_des = ucwords('Bedroom '.$p.' description');
          $return_broom[] = array(
            'name' => $p_des,
            'value' => stripslashes($bedroom_desc_val),
          );
          $p++;
        }else{

          $return_broom[] = esc_html__('');
        }
        
      }
  }
     
  //amenities

  $return_string= array();    
  
  $amenities_cats = get_terms( array('taxonomy' => 'amenities','hide_empty' => false,'parent' => 0) );

  if ( ! empty( $amenities_cats ) ) {
      if ( ! is_wp_error( $amenities_cats) ) {
        for ($i = 0; $i < count($amenities_cats); $i++) {
         
         $return_string[$i]['name'] = html_entity_decode($amenities_cats[$i]->name);

          }
      }
  } 
      
  if ( ! empty( $amenities_cats ) && ! is_wp_error( $amenities_cats ) ){

    for ($i = 0; $i < count($amenities_cats); $i++) {
  
      $child_terms = get_terms( array('parent' => $amenities_cats[$i]->term_id, 'taxonomy' => 'amenities','hide_empty' => false,'order'=>'ASCE') );
        if(!empty($child_terms) && ! is_wp_error($child_terms)){
            for( $j = 0; $j < count( $child_terms ); $j++ ) {

                $term_name = $child_terms[$j]->name;
                $post_var_name =   str_replace(' ','_', trim($term_name) );
                $post_var_name =   wpestate_limit45(sanitize_title( $post_var_name ));
                $post_var_name =   sanitize_key($post_var_name);
                
                $value_label=$term_name;
                if (function_exists('icl_translate') ){
                    $value_label    =   icl_translate('wpestate','wp_estate_property_custom_amm_'.$value, $value ) ;                                      
                }
               
                  $return_string[$i]['value'][] =  $term_name;
            }
        }
    }
  }
  
  //get images

  $img_ids = $wpdb->get_results("SELECT wp_posts.ID FROM wp_posts WHERE post_type = 'attachment' AND post_parent=".$sid, ARRAY_A );
     $img_ids = array_column( $img_ids, "ID" );
  if (!empty($img_ids)) {
    for( $j = 0; $j < count( $img_ids ); $j++ ) {
      $arr_data[ $j ]['image_url'] = wp_get_attachment_url( $img_ids[ $j ] );

    }
  } else {
    $arr_data[]['image_url']  = wp_get_attachment_url(get_post_thumbnail_id($sid));
  }
  $feature_image = get_the_post_thumbnail_url($sid,'thumbnail');

  //verification host

  //$id_verification = array();
  $post_auth = get_post_field('post_author', $sid);
  $fb_verified = get_user_meta($post_auth, 'fb_verified', true);
  $google_verified = get_user_meta($post_auth, 'google_verified', true);
  $goverid_verified = get_user_meta($post_auth, 'user_id_verified', true);
  $pow_verified = get_user_meta($post_auth, 'idproof_verified', true);
  $email_verified = get_user_meta($post_auth, 'is_verified', true);

  // FB verification
  if ($fb_verified == 'yes') {
      $fb_verification = true;
  }
  else{
     $fb_verification = false; 
  }
  // Google verificaion
  if ($google_verified == 'yes') {
      $google_verification = true;
  }else{
     $google_verification = false; 
  }
  // Government ID verificaion
  if ($goverid_verified == '1') {
      $goverid_verification = true;
  }else{
     $goverid_verification = false; 
  }
  // Proof of Ownership verificaion
  if ($pow_verified == '1') {
      $pow_verification = true;
  } else {
     $pow_verification = false; 
  } 
  // Email verificaion
  if ($email_verified == '1') {
      $email_verification = true;
  } else {
     $email_verification = false; 
  }
  
  //listing details heading
  $prop_rooms     = intval ( get_post_meta($sid, 'property_rooms', true) );
  $propmega_details_array = wpml_mega_details_adjust( $sid );
    
  $prop_rooms = isset( $propmega_details_array[ strtotime( date("Y-m-d") ) ]['inventory_sell'] ) && $propmega_details_array[ strtotime( date("Y-m-d") ) ]['inventory_sell']!= '' ? (int)$propmega_details_array[ strtotime( date("Y-m-d") ) ]['inventory_sell'] : $prop_rooms;
   $prop_terms = get_the_terms( $sid, 'property_category' );

  $prop_category = $prop_terms[0]->name;

  $same_property = 'We have '.$prop_rooms.' '.$prop_category.' at  '.get_the_title( $sid );
     
  //price
  $property_price                 =   '' != get_post_meta($sid, 'basic_price_ner', true ) ? floatval( get_post_meta($sid, 'basic_price_ner', true ) ) : floatval(get_post_meta($sid, 'property_price', true) );
  $property_price_before_label    =   esc_html ( get_post_meta($sid, 'property_price_before_label', true) );
  $property_price_after_label     =   esc_html ( get_post_meta($sid, 'property_price_after_label', true) );

  $property_price_per_week        =   '' != get_post_meta( $sid, 'period_price_per_week_ner', true ) ? floatval ( get_post_meta( $sid, 'period_price_per_week_ner', true ) ) : floatval(get_post_meta($sid, 'property_price_per_week', true) );

  $property_price_per_month       =   '' != get_post_meta( $sid, 'period_price_per_month_ner', true ) ? floatval( get_post_meta( $sid, 'period_price_per_month_ner', true ) ) : floatval(get_post_meta($sid, 'property_price_per_month', true) );

  $cleaning_fee                   =   floatval(get_post_meta($sid, 'cleaning_fee', true) );
  $city_fee                       =   floatval(get_post_meta($sid, 'city_fee', true) );
  $cleaning_fee_per_day           =   floatval  ( get_post_meta($sid,  'cleaning_fee_per_day', true) );
  $city_fee_percent               =   floatval  ( get_post_meta($sid,  'city_fee_percent', true) );
  $city_fee_per_day               =   floatval   ( get_post_meta($sid, 'city_fee_per_day', true) );
  $price_per_guest_from_one       =   floatval   ( get_post_meta($sid, 'price_per_guest_from_one', true) );
 
  $checkin_change_over            =   floatval   ( get_post_meta($sid, 'checkin_change_over', true) );  
  $checkin_checkout_change_over   =   floatval   ( get_post_meta($sid, 'checkin_checkout_change_over', true) );  
  $min_days_booking               =   '' != get_post_meta( $sid, 'period_min_days_booking_ner', true ) ? intval( get_post_meta( $sid, 'period_min_days_booking_ner', true ) ) : intval( get_post_meta($sid, 'min_days_booking', true) );  
  $extra_price_per_guest          =   '' != get_post_meta( $sid, 'period_extra_price_per_guest_ner', true ) ? floatval( get_post_meta( $sid, 'period_extra_price_per_guest_ner', true ) ) : floatval( get_post_meta($sid, 'extra_price_per_guest', true) );  

  $cost_per_additional_child          = '' != get_post_meta( $sid, 'extra_child_ner', true ) ? floatval( get_post_meta( $sid, 'extra_child_ner', true ) ) : floatval( get_post_meta($sid, 'cost_per_additional_child', true) ); 

  $price_per_weekeend             =   '' != get_post_meta( $sid, 'period_price_per_weekeend_ner', true ) ? floatval ( get_post_meta( $sid, 'period_price_per_weekeend_ner', true ) ) : floatval ( get_post_meta($sid, 'price_per_weekeend', true) );  

  $security_deposit               =   floatval   ( get_post_meta($sid, 'security_deposit', true) );  
  $early_bird_percent             =   floatval   ( get_post_meta($sid, 'early_bird_percent', true) );  
  $early_bird_days                =   floatval   ( get_post_meta($sid, 'early_bird_days', true) );  
  
  $currency = esc_html( get_option('wp_estate_currency_label_main', '') );
  $where_currency = esc_html( get_option('wp_estate_where_currency_symbol', '') );


  $price_array = wpml_custom_price_adjust( $sid );

  $mega_details_array = wpml_mega_details_adjust( $sid );


  $property_price = isset( $price_array[ strtotime( date( "Y-m-d" ) ) ] ) && '' != $price_array[ strtotime( date( "Y-m-d" ) ) ] ? floatval( $price_array[ strtotime( date( "Y-m-d" ) ) ] ) : $property_price;

  if( isset( $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['promorate'] ) && '' != $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['promorate'] && 0 != $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['promorate'] ) {

    $property_price = floatval( $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['promorate'] );

  } elseif( '' != get_post_meta($sid, 'promorate_ner', true ) ) {

    $property_price = floatval( get_post_meta($sid, 'promorate_ner', true ) );
  }

  $property_price_per_week = isset( $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['period_price_per_week'] ) && '' != $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['period_price_per_week'] ? floatval( $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['period_price_per_week'] ) : $property_price_per_week;

  $property_price_per_month = isset( $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['period_price_per_month'] ) && '' != $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['period_price_per_month'] ? floatval( $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['period_price_per_month'] ) : $property_price_per_month;

  $min_days_booking = isset( $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['period_min_days_booking'] ) && '' != $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['period_min_days_booking'] ? floatval( $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['period_min_days_booking'] ) : $min_days_booking;

  $extra_price_per_guest = isset( $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['period_extra_price_per_guest'] ) && '' != $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['period_extra_price_per_guest'] ? floatval( $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['period_extra_price_per_guest'] ) : $extra_price_per_guest;

  $price_per_weekeend = isset( $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['period_price_per_weekeend'] ) && '' != $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['period_price_per_weekeend'] ? floatval( $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['period_price_per_weekeend'] ) : $price_per_weekeend;

  $cost_per_additional_child = isset( $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['extra_child'] ) && '' != $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['extra_child'] ? floatval( $mega_details_array[ strtotime( date( "Y-m-d" ) ) ]['extra_child'] ) : $cost_per_additional_child;

  $week_days=array(
      '0'=>esc_html__('All','wpestate'),
      '1'=>esc_html__('Monday','wpestate'), 
      '2'=>esc_html__('Tuesday','wpestate'),
      '3'=>esc_html__('Wednesday','wpestate'),
      '4'=>esc_html__('Thursday','wpestate'),
      '5'=>esc_html__('Friday','wpestate'),
      '6'=>esc_html__('Saturday','wpestate'),
      '7'=>esc_html__('Sunday','wpestate')
  );
  
  $th_separator   =   get_option('wp_estate_prices_th_separator','');
  $custom_fields  =   get_option( 'wp_estate_multi_curr', true);
  
  $property_price_show = wpestate_show_price_booking($property_price,$currency,$where_currency,1);
  $property_price_per_week_show        =  wpestate_show_price_booking($property_price_per_week,$currency,$where_currency,1);
  $property_price_per_month_show       =  wpestate_show_price_booking($property_price_per_month,$currency,$where_currency,1);
  $cleaning_fee_show                   =  wpestate_show_price_booking($cleaning_fee,$currency,$where_currency,1);
  $city_fee_show                       =  wpestate_show_price_booking($city_fee,$currency,$where_currency,1);
  
  $price_per_weekeend_show             =  wpestate_show_price_booking($price_per_weekeend,$currency,$where_currency,1);
  $extra_price_per_guest_show          =  wpestate_show_price_booking($extra_price_per_guest,$currency,$where_currency,1);

  $extra_price_per_additional_child_show          =  wpestate_show_price_booking($cost_per_additional_child,$currency,$where_currency,1);

  $security_deposit_show               =  wpestate_show_price_booking($security_deposit,$currency,$where_currency,1);
 
  $setup_weekend_status= esc_html ( get_option('wp_estate_setup_weekend','') );
  $weekedn = array( 
      0 => __("Sunday and Saturday","wpestate"),
      1 => __("Friday and Saturday","wpestate"),
      2 => __("Friday, Saturday and Sunday","wpestate")
  );

  $return_price = array();

  if($price_per_guest_from_one!=1) {

    if ($property_price != 0) {
      //Price per night
      $p_p_n = $property_price_before_label.$property_price_show.$property_price_after_label;
                
                       
    } else {

      $p_p_n = esc_html__('0');

    }

    if ($property_price_per_week != 0){
      //Price per night (7d+)
      $p_pn_d =  $property_price_per_week_show;

    } else {
      $p_pn_d = esc_html__('0');
    }

    if ($property_price_per_month != 0) {
      //Price per night (30d+)
      $p_ppm = $property_price_per_month_show;
    } else {
      $p_ppm = esc_html__('0');
    }

    if ($price_per_weekeend!=0) {
      //Price per weekend
      $p_pw = $price_per_weekeend_show;
    } else {
      $p_pw = esc_html__('0');
    }
 
    if ($extra_price_per_guest!=0){
      //Extra Price per guest
      $e_ppg = $extra_price_per_guest_show; 
    } else {
      $e_ppg = esc_html__('0');
    }

    if ($cost_per_additional_child != 0) {
      //Extra Price per additional child
      $e_ppdc =  $extra_price_per_additional_child_show;
    } else {
      $e_ppdc = esc_html__('0');
    }

  } else {
   
    if ($cost_per_additional_child!=0){
      //Extra Price per additional child
      $e_ppdc = $extra_price_per_additional_child_show;

    }else{

      $e_ppdc = esc_html__('0');

    }
  }
  $options_array=array(
      0   =>  esc_html__('Single Fee','wpestate'),
      1   =>  esc_html__('Per Night','wpestate'),
      2   =>  esc_html__('Per Guest','wpestate'),
      3   =>  esc_html__('Per Night per Guest','wpestate')
  );
  if ($cleaning_fee != 0) {
      //Cleaning Fee
      $c_f = $cleaning_fee_show.' '.$options_array[$cleaning_fee_per_day];
  } else {
    $c_f = esc_html__('0');
  }

  if ($city_fee != 0) {
    if($city_fee_percent==0){
       //City Tax Fee
      $c_t_f =  $city_fee_show.' '.$options_array[$city_fee_per_day]; 
    } else {
      //City Tax Fee of price per night
      $c_t_f =  $city_fee.'%'.' '.__('of price per night','wpestate');
    }
  }else{
    $c_t_f = esc_html__('0');
  }
  
  if ($min_days_booking!=0){
    //Minimum no of nights  
    $m_n_o_n = $min_days_booking;
  } else {
    $m_n_o_n = esc_html__('0');
  }
  
  if ($checkin_change_over!=0){
    //Booking starts only on 
    $b_s_o_o = $week_days[$checkin_change_over ];
  } else {
    $b_s_o_o = esc_html__('0');
  }
  
  if ($security_deposit!=0){
    //Security deposit 
    $s_d = $security_deposit_show;
  } else {
    $s_d = esc_html__('0');
  }
  
  if ($checkin_checkout_change_over!=0){
      //Booking starts/ends only on
      $b_soo = $week_days[$checkin_checkout_change_over];
  } else {
    $b_soo = esc_html__('0');
  }
  
  if($early_bird_percent!=0){
    //Early Booking Discount 
    $ebd = $early_bird_percent.'% '.esc_html__( 'discount','wpestate').' '.esc_html__( 'for bookings made','wpestate').' '.$early_bird_days.' '.esc_html__('nights in advance','wpestate');
  } else {
    $ebd = esc_html__('0');
  }

  $s_data   = array(

  'comments_count'=> get_comments_number($sid),

  // Seprated reviews count local & refer by
  'casa_count'    => $reviews_count['casa_count'],
  'imported_count'=> $reviews_count['import_count'],
  'is_favourite'  => $is_favourite,
  'property_name' => $single_title,
  'description'   => array(
                  // 'video'       => $youtube_value,
                  array(
                    'name'  => 'Description',
                    'value' => "".$s_description."",
                  ),
                  // Hote Policies 
                  // array(
                  //   'name'  => 'Check-in',
                  //   'value' => $check_in_between,
                  // ),
                  // array(
                  //   'name'  => 'Check-out',
                  //   'value' => $check_out_between,
                  // ),
                ),

  'images'        => array (
                        'gallery_images' => $arr_data,
                        'thumbnail' => $feature_image,
                      ),
  'address'       => array(
      array(
        'name'  => 'address',
        'value' => $property_address,
      ),
      array(
        'name'  => 'city',
        'value' => $property_city,
      ),
      array(
        'name'  => 'state',
        'value' => $property_state,
      ),
      array(
        'name'  => 'country',
        'value' => $property_country,
      ),array(
        'name'  => 'county',
        'value' => $property_county,
      ),
      array(
        'name'  => 'zipcode',
        'value' => $property_zip,
      ),
          
  ),
  'price_details' => array(
          array(
               'name' => 'Price per night',
               'value' => $p_p_n,
             ),
          array(
              'name'  => 'Price per night (7d+)',
              'value' => $p_pn_d,    
            ),
          array(
              'name'  => 'Price per night (30d+)',
              'value' => $p_ppm,
             ),
          array(
                'name' => 'Price per weekend',
                'value' => $p_pw,
              ),
          array(
                'name'  => 'Extra Price per guest',
                'value' => $e_ppg
              ),
          array(
                'name'  => 'Extra Price per additional child',
              'value' => $e_ppdc ,
              ),
          array(
                'name'  => 'Cleaning Fee',
                'value' => $c_f,
               ),
          array(
                'name'  => 'City Tax Fee',
                'value' => $c_t_f,
               ),
          array(
                'name'  => 'Minimum no of nights',
                'value' => "$m_n_o_n",
              ),
          array(
                'name'  => 'Booking starts only on',
                'value' => $b_s_o_o,
              ),
          array(
                'name'  => 'Security deposit',
                'value' => $s_d,
               ),
          array(
                'name'  => 'Booking starts/ends only on',
                'value' => $b_soo,
               ),
         

      ),
   'early_booking_discount' => array(
          'name'  => 'Early Booking Discount',
          'value' => $ebd,
            
   ),
  //'extra_option' => $free_service,
  
  'map' => array(
    'property_latitude' => $property_latitude,
    'property_longitude'  => $property_longitude ,
  ),
  'listing_details ' => array(

    array(
      'name'  => 'Same Property',
      'value' => $same_property,
    ),
    array(
      'name'  => 'Property ID',
      'value' => $sid,
    ),
    array(
      'name'  => 'Property Size',
      'value' => $pro_size,
    ),
    array(
      'name'  => 'Bedrooms',
      'value' => $property_bedrooms,
    ),
    array(
      'name'  => 'Bathrooms',
      'value' => $property_bathrooms,
    ),
    array(
      'name'  => 'Check In Between',
      'value' => $check_in_between,
    ),
    // array(
    //   'name'  => 'check_in_hour_to',
    //   'value' => $check_in_hour_to,
    // ),
    // array(
    //   'name'  => 'hours_check_in_24',
    //   'value' => $hours_check_in_24,
    // ),
    array(
      'name'  => 'Check Out Between',
      'value' => $check_out_between,
    ),
    // array(
    //   'name'  => 'check_out_hour_to',
    //   'value' => $check_out_hour_to,
    // ),
    // array(
    //   'name'  => 'hours_check_out_24',
    //   'value' => $hours_check_out_24,
    // ),
    array(
      'name'  => 'Late check in',
      'value' => $late_check_in,
    ),
    array(
      'name'  => 'Outdoor Facilities',
      'value' => $outdoor_facilities
    ),
    array(
      'name'  => 'Cancellation',
      'value' => $cancellation,
    ),
    
  ),
  'badroom_description' => $return_broom ,
  'features' => $return_string,
  'verificaion_host' => array(
    array(
      'name'  => 'Facebook Verified',
      'value' => $fb_verification,
    ),
    array(
      'name' => 'Google Verified',
      'value' => $google_verification,
    ),
    array(
      'name' => 'Government ID Verified',
      'value' => $goverid_verification,
    ),
    array(
      'name' => 'Proof of ownership Verified',
      'value' => $pow_verification,
    ),
    array(
      'name' => 'Email Verified',
      'value' => $email_verification,
    ),
  ),
);
  /*echo "<pre>";
  print_r($s_data);*/
  //return $s_data;
  return new WP_REST_Response(
      array(
          'response_code'  => "200", 
          'data' => $s_data,
      ),
  200);
}


// Seprated Count by Local CASA & IMPORTED REVIEWS
function seprated_reviews_count ( $property_id ) {
  
  global $wpdb;
  $comment_id = $wpdb->get_results("SELECT comment_ID, user_id, comment_content FROM wp_comments WHERE comment_post_ID = ".$property_id." AND comment_approved = 1", ARRAY_A);
  
  $casa_count = $imported_count = 0;
  for ($i=0; $i < count($comment_id); $i++) { 
    
    $imported_refer   = get_comment_meta( $comment_id[ $i ]['comment_ID'], 'ota_reference', true );

    // LOCAL CASA REVIEWS
    if ( empty( $imported_refer ) || $imported_refer == '' ) {
      $casa_count++;
    }

    if ( !empty( $imported_refer ) || $imported_refer != '' ) {
      $imported_count++;
    }
  }
  return array(
    'casa_count'   => $casa_count,
    'import_count' => $imported_count,
  );
}
