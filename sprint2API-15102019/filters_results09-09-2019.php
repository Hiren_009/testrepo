<?php

/**
 * Apply to filters results API
 */
add_action('rest_api_init', function(){
	register_rest_route('tvcapi', '/v2/filter-results',
		array(
			'methods' => 'POST',
			'callback'=> 'filter_results',
		)
	);
});

function filter_results() {

	// Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['Token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);

    if ($headers['Token'] != $token || empty($headers['Token'])) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Token is invalid', 'wpestate')), 400);
    }

    // var_dump($_POST);
    // END
	// START Get response from app
	global $wpdb;
    $prop_id = $properties = array();
	$destination    = sanitize_text_field( filter_input( INPUT_POST, 'destination') );
    $check_in_date  = sanitize_text_field( filter_input( INPUT_POST, 'check_in_date') );
    $check_out_date = sanitize_text_field( filter_input( INPUT_POST, 'check_out_date') );
    $guests         = sanitize_text_field( filter_input( INPUT_POST, 'guests' ) );
    $check_in  = strtotime($check_in_date);
    $check_out = strtotime($check_out_date);

    /** 
	 *	FILTER RESPONSE VALUES 
	 */

	$ratings 		 = sanitize_text_field( filter_input(INPUT_POST, 'ratings') );
	$budgets 		 = sanitize_text_field( filter_input(INPUT_POST, 'budgets') );
	
	$property_filter = sanitize_text_field( filter_input(INPUT_POST, 'property_categories') );
	$front_desk_open_24_7 = sanitize_text_field( filter_input(INPUT_POST, 'checkin24hours') );	
	$free_cancellation    = sanitize_text_field( filter_input(INPUT_POST, 'free_cancellation') );	
	$beach_amenity   = sanitize_text_field( filter_input(INPUT_POST, 'beach_amenities') );	
	$room_type       = sanitize_text_field( filter_input(INPUT_POST, 'room_types') );	
	$room_facility   = sanitize_text_field( filter_input(INPUT_POST, 'room_facilities') );	
	$facility        = sanitize_text_field( filter_input(INPUT_POST, 'facilities') );	
	$security        = sanitize_text_field( filter_input(INPUT_POST, 'security') );	
	$property_accessibility = sanitize_text_field( filter_input(INPUT_POST, 'property_accessibility') );


    // Validation for that will get response from app
    if( !isset( $destination ) || empty($destination) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter destination place', 'wpestate')), 400);
    }

    if( !isset( $check_in_date ) || empty($check_in_date)) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select first check in date', 'wpestate')), 400);
    }

    if( !isset( $check_out_date ) || empty($check_out_date)) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select check out date', 'wpestate')), 400);
    }
    $today = date("Y-m-d");
    if (strtotime($today) > $check_in) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Check in date should be today and onwards.', 'wpestate')), 400);
    }
    if ($check_in > $check_out) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Check out date should be greater than check in date.', 'wpestate')), 400);
    }
    if( !isset( $guests ) || empty($guests) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter no. of guests', 'wpestate')), 400);
    } elseif(  $guests < 1 ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Guest should be greater than one', 'wpestate')), 400);
    } elseif ($guests > 25) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Guest should be 25 or less', 'wpestate')), 400);
    }

    // ----------- Filter Validation Start -----------

    if ( '' != trim($ratings) ) {
    	$rating = explode(',', $ratings);
    	// print_r($rating);

    	for ($i=0; $i < count($rating); $i++) {

    		if ( !is_numeric( $rating[ $i ] )) {
    			return new WP_REST_Response(array("response_code" => "400", 'message' => esc_html__('Rating value is wrong!', 'wpestate')), 400);
    		}
    	}
	}
	// if ( $ratings < 0 || $ratings > 5) {
	// 	return new WP_REST_Response(array("response_code" => "400", 'message' => esc_html__('Rating should be between 1 to 5.', 'wpestate')), 400);
	// }

	if ('' != trim($budgets)) {
		$range_value = explode('-', $budgets);

		print_r($range_value);
		for ($i=0; $i < count($range_value); $i++) { 
			if (!is_numeric($range_value[$i])) {
				return new WP_REST_Response(array("response_code" => "401", 'message' => esc_html__('Budgets value is wrong!', 'wpestate')), 401);
			}
		}
	}

	$range_min 	 = $range_value[0];
	$range_max 	 = $range_value[1];

	if ('' != trim($property_filter)) {
		$property_filters = explode(',', $property_filter);
		// print_r($property_filters);
		for ($i=0; $i < count($property_filters); $i++) { 
			if (!is_numeric($property_filters[$i])) {
				return new WP_REST_Response(array("response_code" => "401", 'message' => esc_html__('Something went wrong!', 'wpestate')), 401);
			}
		}
	}

	$free_cancellation = ucfirst($free_cancellation);
	if ( 24 != isset($front_desk_open_24_7) && !is_numeric($front_desk_open_24_7)) {
		return new WP_REST_Response(array("response_code" => "400", 'message' => esc_html__('Only 24 Hours allowed!', 'wpestate')), 400);
	}
	if( '' != trim($free_cancellation) && !preg_match("/^[a-zA-Z]+$/", $free_cancellation) ) {
	    return new WP_REST_Response(array("response_code" => "401", 'message' => esc_html__('Something went wrong!', 'wpestate')), 401);
	}
	if ( '' != trim($beach_amenity) ) {
		$beach_amenity   = explode(',', $beach_amenity);
		for ($i=0; $i < count($beach_amenity); $i++) { 
			if ( !is_numeric($beach_amenity[$i]) ) {
				return new WP_REST_Response(array("response_code" => "401", 'message' => esc_html__('Something went wrong!', 'wpestate')), 401);
			}
		}
	}

	if ('' != trim($room_type)) {
		$room_type = explode(',', $room_type);
		for ($i=0; $i < count($room_type); $i++) { 
			if ( !is_numeric($room_type[$i]) ) {
				return new WP_REST_Response(array("response_code" => "401", 'message' => esc_html__('Something went wrong!', 'wpestate')), 401);
			}
		}
	}

	if ('' != trim($room_facility)) {
		$room_facility = explode(',', $room_facility);

		for ($i=0; $i < count($room_facility); $i++) { 
			if ( !is_integer($room_facility[$i]) ) {
				return new WP_REST_Response(array("response_code" => "401", 'message' => esc_html__('Something went wrong!', 'wpestate')), 401);
			}
		}
	}
	if ('' != trim($facility)) {
		$facility = explode(',', $facility);
		// $valid = 1;
		for( $i = 0; $i < count( $facility ); $i++ ) {
			if( ! is_numeric( $facility[ $i ] ) ) {
				return new WP_REST_Response(array("response_code" => "401", 'message' => esc_html__('Something went wrong!', 'wpestate')), 401);
			}
		}
	}

	if ( '' != trim($security)) {
		$security = explode(',', $security);
		for ($i=0; $i < count($security); $i++) { 
			if ( !is_numeric($security[$i]) ) {
				return new WP_REST_Response(array("response_code" => "401", 'message' => esc_html__('Something went wrong!', 'wpestate')), 401);
			}
		}
	}
	if ( '' != trim($property_accessibility)) {
		$property_accessibility = explode(',', $property_accessibility);
		// print_r($property_accessibility);
		for ($i=0; $i < count($property_accessibility); $i++) { 
			// echo $property_accessibility[$i];
			if ( !is_numeric(trim($property_accessibility[$i])) ) {
				return new WP_REST_Response(array("response_code" => "401", 'message' => esc_html__('Something went wrong!', 'wpestate')), 401);
			}
		}
	}
    // ----------- Validation End ---------------


    // Get properties IDs from above feild response
    $term_id = '';
    if( isset( $destination ) && '' != $destination ) {
        $term_id = $wpdb->get_results("SELECT `term_id` FROM `wp_terms` WHERE `name` LIKE '%".trim($destination)."%'", ARRAY_A);
        $term_id = implode( ",", array_column($term_id, 'term_id'));
    }

    if( $term_id != '' ) {
       // New Query  08-08-2019

       	$prop_id = $wpdb->get_results("SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN `wp_term_relationships` ON `wp_posts`.`ID` = `wp_term_relationships`.`object_id` INNER JOIN `wp_terms` ON `wp_term_relationships`.`term_taxonomy_id` IN (".$term_id.") INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE `wp_terms`.`name` LIKE '%".trim($destination)."%' OR (
            ( `wp_posts`.`post_title` LIKE '%".$destination."%' ) OR
            ( `wp_postmeta`.`meta_key` = 'property_address' AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' ) OR
            ( `wp_postmeta`.`meta_key` = 'property_state'   AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' ) OR
            ( `wp_postmeta`.`meta_key` = 'property_zip'          AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' ) OR
            ( `wp_postmeta`.`meta_key` = 'property_country' AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' ) 
        ) AND `wp_posts`.`post_type` = 'estate_property' AND `wp_posts`.`post_status` = 'publish' GROUP BY `wp_posts`.`ID`", ARRAY_A);

    } else {
        $prop_id = $wpdb->get_results("SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE `wp_posts`.`post_title` = '".$destination."' OR (
                ( `wp_postmeta`.`meta_key` = 'property_address' AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_state' AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_zip' AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_country' AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' )
            ) AND `wp_posts`.`post_type` = 'estate_property' AND `wp_posts`.`post_status` = 'publish'", ARRAY_A);

    }
    $prop_id = array_column($prop_id, "ID");
    
    
    // Check in & check out date
    if ( $prop_id && count($prop_id) > 0) {
        for ( $p = 0; $p < count( $prop_id ); $p++ ) {

            $property_id = $prop_id[ $p ];
            // ---
            $booking_ids = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts INNER JOIN wp_postmeta ON (wp_posts.ID = wp_postmeta.post_id ) INNER JOIN wp_postmeta AS mt1 ON ( wp_posts.ID = mt1.post_id ) INNER JOIN wp_postmeta AS mt2 ON ( wp_posts.ID = mt2.post_id ) WHERE 1=1 AND (( wp_postmeta.meta_key = 'booking_id' AND wp_postmeta.meta_value = '".$prop_id[ $p ]."' ) AND ( mt1.meta_key = 'booking_status' AND mt1.meta_value = 'confirmed' ) ) AND wp_posts.post_type = 'wpestate_booking' AND ((wp_posts.post_status = 'publish')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date ASC", ARRAY_A);

            $reservation_array = $booking_arr = $strt_dt = $sted_dt = $b_arr = array();
        
            if( $booking_ids ) {
                for( $j = 0; $j < count( $booking_ids ); $j++ ) {
                    $strt_dt[ $j ] = strtotime( get_post_meta( $booking_ids[ $j ]['ID'], 'booking_from_date', true ) );

                    $sted_dt[ $j ] = strtotime( get_post_meta( $booking_ids[ $j ]['ID'], 'booking_to_date', true ) );

                    for( $b = $strt_dt[ $j ]+86400; $b < $sted_dt[ $j ]; $b += 86400 ) {
                        $b_arr[] = $b;
                    } 
                }

                for( $i = 0; $i < count( $strt_dt ); $i++ ) {
                    if( array_key_exists( $strt_dt[ $i ], $booking_arr ) ) {
                        $booking_arr[ $strt_dt[ $i ] ] = $booking_arr[ $strt_dt[ $i ] ] + 1;
                    } else {
                        $booking_arr[ $strt_dt[ $i ] ] = 1;
                    }
                }

                for( $i = 0; $i < count( $b_arr ); $i++ ) {

                    if( array_key_exists( $b_arr[ $i ], $booking_arr ) ) {
                        $booking_arr[ $b_arr[ $i ] ] = $booking_arr[ $b_arr[ $i ] ] + 1;
                    } else {
                        $booking_arr[ $b_arr[ $i ] ] = 1;
                    }
                }
            }
            $reservation_array = $booking_arr;
            $mega_details = wpml_mega_details_adjust( $property_id );
            $guests_no = get_post_meta($property_id, 'max_allow_guest', true);
            $max_allow_guests = get_post_meta($property_id, 'overload_guest', true);
            // check property booking available or not
            for( $i = $check_in; $i < $check_out; $i += 86400 ) {
                if ( $reservation_array[ $i ] < $mega_details[ $i ]['inventory_sell'] || empty($reservation_array) ) { 
                    if ( $guests_no <= $guests && $max_allow_guests == 1 ) {
                        $properties[] = $property_id;
                    } elseif( $guests_no >= $guests ) {
                        $properties[] = $property_id;
                    } else {
                        break;
                    }
                } else {
                    break;
                }            
            }
            
        }
    } else {
        return new WP_REST_Response(array("response_code" => "404", 'message' => esc_html__('Could not found property', 'wpestate')), 404);
    }

    $propertiesid   =  array_values( array_unique( $properties ) );

    // echo "searchby--";
    // print_r($propertiesid);
    // exit();
	// END Get response from app 

	/**
	 * START Filter results to will be get properties IDs
	 */

	

	$properties_ratings = $_filtered_property = array();
	// 1. Rating Filter
	if( isset( $ratings ) && '' != trim( $ratings ) ) {

		$active_filters[] = array(
			'key' 	=> 'ratings',
			'value' => $rating
		);
		sort($rating);
		// print_r($rating);
		for ( $c = 0; $c < count( $propertiesid ); $c++ ) {
			
			$total = $comments_count = 0;
			$comment_id = $wpdb->get_results("SELECT comment_ID FROM wp_comments WHERE comment_post_ID = ".$propertiesid[ $c ]." AND comment_approved = 1", ARRAY_A);

			$commentsid = array_column($comment_id, 'comment_ID');
		
			for ($ci=0; $ci < count($commentsid) ; $ci++) {
				$comments_count++;
				$reviews_values = get_comment_meta( $commentsid[$ci], 'review_stars', true );
				$tmp = json_decode($reviews_values, true);
				$total += intval( $tmp['rating'] );
			}

			$average_val  = $total / $comments_count;
			$round_figure = round($average_val);

			for ($i=0; $i < count($rating); $i++) { 
				
				if( $round_figure == $rating[$i]) {
					$_filtered_property[] = $propertiesid[ $c ];
				} 
			}
		}
		if( count( $_filtered_property ) < 1 ) {
			// return 
			return new WP_REST_Response(array("response_code" => "404", 'message' => esc_html__('No properties available for the selected crietearia!', 'wpestate')), 404);
		}
	}
	// echo "ratings";
	// print_r($_filtered_property);
	/* 
	 * 2. Poperty Price Budgets
	 * Get Porperty Price & Basic promo rate AND Day & Date wise price show function
	 */
	$properties_ratings = count( $_filtered_property ) > 0 ? $_filtered_property : $propertiesid;
	unset( $_filtered_property );
	
	if( isset( $budgets ) && '' != $budgets ) {

		$active_filters[] = array(
			'key' 	=> 'budgets',
			'value' => array($budgets)
		);

		for ($b=0; $b < count($properties_ratings); $b++) { 
			// echo $properties_ratings[ $b ];
			$price_per_day 			  = floatval( get_post_meta($properties_ratings[ $b ], 'property_price', true) );
			$property_basic_promation = floatval( get_post_meta($properties_ratings[ $b ], 'property_basic_promation', true) );

			// Basic Price 
			$price_per_day  = '' != get_post_meta($properties_ratings[ $b ], 'basic_price_ner', true ) ? floatval( get_post_meta($properties_ratings[ $b ], 'basic_price_ner', true ) ) : floatval(get_post_meta($properties_ratings[ $b ], 'property_price', true));

			$price_array   = wpml_custom_price_adjust($properties_ratings[ $b ]);
			$price_per_day = isset( $price_array[ $check_in ] ) ? $price_array[ $check_in ] : $price_per_day;

			// Basic promo rate
			$property_basic_promation = '' != get_post_meta($properties_ratings[ $b ], 'promorate_ner', true ) ? floatval(get_post_meta($properties_ratings[ $b ], 'promorate_ner', true )) : floatval (get_post_meta($properties_ratings[ $b ], 'property_basic_promation', true) );

			$mega_details_array = wpml_mega_details_adjust( $properties_ratings[ $b ] );
			$property_basic_promation = isset( $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate'] ) && $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate']!= '' ? $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate'] : $property_basic_promation;

			// Basic & promo rate check
			$properties_price = $property_basic_promation != 0 ? $property_basic_promation : $price_per_day;
			// echo ' => '.$properties_price;

			if ($properties_price >= $range_min && $properties_price <= $range_max ) {
				$_filtered_property[] = $properties_ratings[ $b ];
			}
		}
		if( count( $_filtered_property ) < 1 ) {
			// return 
			return new WP_REST_Response(array("response_code" => "404", 'message' => esc_html__('No properties available for the selected crietearia!', 'wpestate')), 404);
		}
	} else {
		$_filtered_property = $properties_ratings;
	}

	/* 
	 * 3. Property Filters (property_categories)
	 */

	$property_categories = array();
	$properties_budgets = count( $_filtered_property ) > 0 ? $_filtered_property : $propertiesid;
	unset( $_filtered_property );
	if( isset( $property_filter ) && '' != $property_filter ) {
		
		$active_filters[] = array(
			'key' 	=> 'property_categories',
			'value' => $property_filter
		);

		for ($p=0; $p < count($properties_budgets); $p++) { 
			// echo $properties_budgets[$p];
			$property_cat_id = wp_get_post_terms($properties_budgets[ $p ], 'property_category',  array("fields" => 'ids'));
			sort($property_cat_id);
			sort($property_filter);
		    for ($pf=0; $pf < count($property_filters); $pf++) { 
				for ($pci=0; $pci < count($property_cat_id); $pci++) {
				    if ( $property_cat_id[$pci] == $property_filters[$pf] ) {
				    	$_filtered_property[] = $properties_budgets[ $p ];
				    }
				}
		    }
		}
		if( count( $_filtered_property ) < 1 ) {
			// return 
			return new WP_REST_Response(array("response_code" => "404", 'message' => esc_html__('No properties available for the selected crietearia!', 'wpestate')), 404);
		}
	} else {
		$_filtered_property = $properties_budgets;
	}
	// echo "Cat--";
	// print_r($_filtered_property);
	/* 
	 * 4. 24-hour front desk (checkin24hours)
	 */
	$properties_checkin24 = count( $_filtered_property ) > 0 ? $_filtered_property : $propertiesid;
	unset( $_filtered_property );

	if (isset($front_desk_open_24_7) && '' != $front_desk_open_24_7) {
		
		$active_filters[] = array(
			'key' 	=> 'checkin24hours',
			'value' => array($front_desk_open_24_7)
		);

		for ($i=0; $i < count($properties_checkin24); $i++) { 
			$checkin_to24 = get_post_meta($properties_checkin24[ $i ], 'hours_check_in_24', true);
			echo $checkin_to24;
			$checkin = explode(' ', $checkin_to24);

			if ( $checkin[0] == $front_desk_open_24_7 ) {
				$_filtered_property[] = $properties_checkin24[ $i ];
			}
		}
		if( count( $_filtered_property ) < 1 ) {
			return new WP_REST_Response(array("response_code" => "404", 'message' => esc_html__('No properties available for the selected crietearia!', 'wpestate')), 404);
		}
	} else {
		$_filtered_property = $properties_checkin24;
	}

	/* 
	 * 5. Reservation Policy (free_cancellation)
	 */
	$free_cancel = count( $_filtered_property ) > 0 ? $_filtered_property : $propertiesid;
	unset( $_filtered_property );

	if (isset($free_cancellation) && '' != $free_cancellation) {
		
		$active_filters[] = array(
			'key' 	=> 'free_cancellation',
			'value' => array($free_cancellation)
		);

		for ($i=0; $i < count($free_cancel); $i++) { 
			$freecancellation = get_post_meta($free_cancel[ $i ], 'cancellation', true);
			$cancellation = explode(' ', $freecancellation);
			if ($cancellation[0] == $free_cancellation) {
				$_filtered_property[] = $free_cancel[ $i ];
			}
		}
		if( count( $_filtered_property ) < 1 ) {
			return new WP_REST_Response(array("response_code" => "404", 'message' => esc_html__('No properties available for the selected crietearia!', 'wpestate')), 404);
		}
	} else {
		$_filtered_property = $free_cancel;
	}

	/* 
	 * 6. Beach Access (beach_amenities)
	 */
	$beach_amenities = count( $_filtered_property ) > 0 ? $_filtered_property : $propertiesid;
	unset( $_filtered_property );
	if (isset($beach_amenity) && '' != $beach_amenity) {
		
		$active_filters[] = array(
			'key' 	=> 'beach_amenities',
			'value' => $beach_amenity
		);

		sort($beach_amenity);
		for ($i=0; $i < count($beach_amenities); $i++) { 
			$beach_amenitiesid = wp_get_post_terms($beach_amenities[ $i ], 'amenities',  array("fields" => 'ids'));

			sort($beach_amenitiesid);
			sort($beach_amenity);
			for ($b_id=0; $b_id < count($beach_amenity); $b_id++) { 
				for ($b=0; $b < count($beach_amenitiesid); $b++) {
				    if ( $beach_amenitiesid[$b] == $beach_amenity[$b_id] ) {
				    	$_filtered_property[] = $beach_amenities[ $i ];
				    }
				}
			}
		}
		if( count( $_filtered_property ) < 1 ) {
			return new WP_REST_Response(array("response_code" => "404", 'message' => esc_html__('No properties available for the selected crietearia!', 'wpestate')), 404);
		}
	} else {
		$_filtered_property = $beach_amenities;
	}

	/* 
	 * 7. Accommodation Type (room_types)
	 */
	$room_types = count( $_filtered_property ) > 0 ? $_filtered_property : $propertiesid;
	unset( $_filtered_property );
	if (isset($room_type) && '' != $room_type) {
		
		$active_filters[] = array(
			'key' 	=> 'room_types',
			'value' => $room_type
		);

		sort($room_type);

		for ($rt=0; $rt < count($room_types); $rt++) { 
			$roomtypes = wp_get_post_terms($room_types[ $rt ], 'property_action_category',  array("fields" => 'ids'));
			sort($roomtypes);
			for ($r=0; $r < count($room_type); $r++) { 
				for ($rm=0; $rm < count($roomtypes); $rm++) {

				    if ( $roomtypes[$rm] == $room_type[$r] ) {
				    	$_filtered_property[] = $room_types[ $rt ];
				    }
				}
			}
		}
		if( count( $_filtered_property ) < 1 ) {
			// return 
			return new WP_REST_Response(array("response_code" => "404", 'message' => esc_html__('No properties available for the selected crietearia!', 'wpestate')), 404);
		}
	} else {
		$_filtered_property = $room_types;
	}

	/* 
	 * 8. Rooms Facilities (room_facilities)
	 */
	$room_facilities = count( $_filtered_property ) > 0 ? $_filtered_property : $propertiesid;
	unset( $_filtered_property );
	if (isset($room_facility) && '' != $room_facility) {
		
		$active_filters[] = array(
			'key' 	=> 'room_facilities',
			'value' => $room_facility
		);

		sort($room_facility);
		for ($i=0; $i < count($room_facilities); $i++) { 

			$room_amenities = wp_get_post_terms($room_facilities[$i], 'amenities',  array("fields" => 'ids'));
			sort($room_amenities);

			for ($rf_id=0; $rf_id < count($room_facility); $rf_id++) { 

				for ($rf=0; $rf < count($room_amenities); $rf++) { 

					if ($room_amenities[$rf] == $room_facility[$rf_id]) {
						$_filtered_property[] = $room_facilities[ $i ];
					}
				}
			}
		}
		if( count( $_filtered_property ) < 1 ) {
			return new WP_REST_Response(array("response_code" => "404", 'message' => esc_html__('No properties available for the selected crietearia!', 'wpestate')), 404);
		}
	} else {
		$_filtered_property = $room_facilities;
	}

	/* 
	 * 9. Facilities (facilities)
	 */
	$facilities = count( $_filtered_property ) > 0 ? $_filtered_property : $propertiesid;
	unset( $_filtered_property );
	if (isset($facility) && '' != $facility) {
		
		$active_filters[] = array(
			'key' 	=> 'facilities',
			'value' => $facility
		);

		sort($facility);
		for ($i=0; $i < count($facilities); $i++) { 
			$facilty_amenities = wp_get_post_terms($room_facilities[$i], 'amenities',  array("fields" => 'ids'));
			sort($room_amenities);

			for ($f_id=0; $f_id < count($facility); $f_id++) {
				for ($j=0; $j < count($facilty_amenities); $j++) { 
					if ($facilty_amenities[$j] == $facility[$f_id]) {
						$_filtered_property[] = $facilities[ $i ];
					}
				}
			}
		}
		if( count( $_filtered_property ) < 1 ) {
			return new WP_REST_Response(array("response_code" => "404", 'message' => esc_html__('No properties available for the selected crietearia!', 'wpestate')), 404);
		}
	} else {
		$_filtered_property = $facilities;
	}

	/* 
	 * 10. Security (security)
	 */
	$securitis = count( $_filtered_property ) > 0 ? $_filtered_property : $propertiesid;
	unset( $_filtered_property );
	if (isset($security) && '' != $security) {
		
		$active_filters[] = array(
			'key' 	=> 'security',
			'value' => $security
		);

		sort($security);
		for ($i=0; $i < count($securitis); $i++) {
			$securitis_ame = wp_get_post_terms($securitis[$i], 'amenities', array('fields' => 'ids'));
			sort($securitis_ame);
			for ($s_id=0; $s_id < count($security); $s_id++) { 
				for ($j=0; $j < count($securitis_ame); $j++) { 
					if ($securitis_ame[$j] == $security[$s_id]) {
						$_filtered_property[] = $securitis[ $i ];
					}
				}
			}

		}
		if( count( $_filtered_property ) < 1 ) {
			return new WP_REST_Response(array("response_code" => "404", 'message' => esc_html__('No properties available for the selected crietearia!', 'wpestate')), 404);
		}
	} else {
		$_filtered_property = $securitis;
	}
	// echo "sec--" ;
	// print_r($_filtered_property);
	/* 
	 * 11. Property Accessibility (property_accessibility)
	 */
	$property_access = count( $_filtered_property ) > 0 ? $_filtered_property : $propertiesid;
	unset( $_filtered_property );
	if (isset($property_accessibility) && '' != $property_accessibility) {
		
		$active_filters[] = array(
			'key' 	=> 'property_accessibility',
			'value' => $property_accessibility
		);

		sort($property_accessibility);
		for ($i=0; $i < count($property_access); $i++) {
			$property_access_ame = wp_get_post_terms($property_access[$i], 'amenities', array('fields' => 'ids'));
			sort($property_access_ame);
			print_r($property_access_ame);
			for ($pa_id=0; $pa_id < count($property_accessibility); $pa_id++) { 
				for ($j=0; $j < count($property_access_ame); $j++) { 
					if ($property_access_ame[$j] == $property_accessibility[$pa_id]) {
						$_filtered_property[] = $property_access[ $i ];
					}
				}
			}
		}
		if( count( $_filtered_property ) < 1 ) {
			return new WP_REST_Response(array("response_code" => "404", 'message' => esc_html__('No properties available for the selected crietearia!', 'wpestate')), 404);
		}
	} else {
		$_filtered_property = $property_access;
	}
	$property_ids = array_unique($_filtered_property);

	// END Filter results to will be get properties IDs

	/*
	 * Filterered Properties Listing
	 */

	// START SORT
    $p_price = array();
    for ($i=0; $i < count($property_ids); $i++) { 
           
        $price_per_day             = floatval( get_post_meta($property_ids[ $i ], 'property_price', true) );
        $property_basic_promation = floatval( get_post_meta($property_ids[ $i ], 'property_basic_promation', true) );

        // Basic Price 
        $price_per_day  = '' != get_post_meta($property_ids[ $i ], 'basic_price_ner', true ) ? floatval( get_post_meta($property_ids[ $i ], 'basic_price_ner', true ) ) : floatval(get_post_meta($property_ids[ $i ], 'property_price', true));

        $price_array   = wpml_custom_price_adjust($property_ids[ $i ]);
        $price_per_day = isset( $price_array[ $check_in ] ) ? $price_array[ $check_in ] : $price_per_day;

        // Basic promo rate
        $property_basic_promation = '' != get_post_meta($property_ids[ $i ], 'promorate_ner', true ) ? floatval(get_post_meta($property_ids[ $i ], 'promorate_ner', true )) : floatval (get_post_meta($property_ids[ $i ], 'property_basic_promation', true) );

        $mega_details_array = wpml_mega_details_adjust( $property_ids[ $i ] );
        $property_basic_promation = isset( $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate'] ) && $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate']!= '' ? $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate'] : $property_basic_promation;

        // Basic & promo rate check
        $properties_price = $property_basic_promation != 0 ? $property_basic_promation : $price_per_day;

        $p_price[$i]['p_price'] = $properties_price;
        $p_price[$i]['ID']      = $property_ids[ $i ];

    }

    if( isset( $_POST['sort_by'] ) && strcasecmp( $_POST['sort_by'], 'ASC') == 0 ) {
        uasort( $p_price , function( $aa, $bb ){
            return ( floatval( trim( $aa['p_price']  ) ) <= floatval(  trim( $bb['p_price']) ) ) ? -1 : 1;
        });
    } elseif( isset( $_POST['sort_by'] ) && strcasecmp( $_POST['sort_by'], 'DESC') == 0 ) {
        uasort( $p_price , function( $aa, $bb ){
            return ( floatval( trim( $aa['p_price'] ) ) <= floatval( trim( $bb['p_price']) ) ) ? 1 : -1;
        });
    }
    // print_r($p_price);
    $sort_by = array(
        array(
            'key'  => 'Default',
            'value'=> esc_html__('Default','wpestate'),
        ),
        array(
            'key'  => 'ASC',
            'value'=> esc_html__('Price Low to High','wpestate'),
        ),
         array(
            'key'  => 'DESC',
            'value'=> esc_html__('Price High to Low','wpestate'),
        ),
    );
    // END SORT
    $properties_id = array_column($p_price, 'ID');

    // Pagination
    $current_page   = sanitize_text_field( filter_input( INPUT_POST, 'current_page' ) );

    $per_page = 10;
    $total_pages = ceil(count($property_ids) / $per_page);

    $p_id = array_chunk($properties_id, $per_page);
    $properties = $p_id [$current_page - 1];
    // print_r($properties);

    // END pagination

	$p_data = array();
	for ($k=0; $k < count($properties); $k++) { 
        // echo $properties[ $k ];

        //Get Property ID
        $p_data[ $k ]['ID']    =  $properties[ $k ];

        // Property Image URL
        $post_thumb_id  = get_post_thumbnail_id( $properties[ $k ] );
        $post_thumb_url = wp_get_attachment_image_url($post_thumb_id, 'full');
        if ($post_thumb_url == false) {
            # code...
            $p_data[ $k ]['image_url'] = "";
        } else {
            $p_data[ $k ]['image_url'] = $post_thumb_url;
        }

        // Is Favorite Porperty
        $user_option = 'favorites'.$token_id[1];
        $curent_fav  = get_option( $user_option );
        if($curent_fav){
            if ( in_array ($properties[ $k ],$curent_fav) ){
                $p_data[ $k ]['is_favourite'] = true;
            } else {
                $p_data[ $k ]['is_favourite'] = false;
            }
        }

        //Get Property Title
        $p_data[ $k ]['title'] = get_the_title( $properties[ $k ] );

        // Get Property City
        $prop_city = get_the_terms($properties[ $k ], 'property_city');
        $this_cat = array();
        foreach ( $prop_city as $cat ) {
            $this_cat[] = $cat->name;
        }
        $p_data[ $k ]['city'] = implode( ",", $this_cat );

        // Get Property State
        $p_data[ $k ]['state'] = get_post_meta( $properties[ $k ], 'property_state', true );

        // // Get Property type
        $selected_prop_type = get_the_terms($properties[ $k ], 'property_category');
        $p_data[ $k ]['property_type'] = $selected_prop_type[0]->name;

        // // Get Property Zipcode
        $p_data[ $k ]['zipcode'] = get_post_meta( $properties[ $k ], 'property_zip', true);

        // Get Cancellation Policy
        $cancellation = get_post_meta($properties[ $k ],  'cancellation', true);
        if ($cancellation == 'NonRefundable') {
            $p_data[ $k ][ 'cancellation_policy' ] = esc_html__('Non Refundable', 'wpestate');
        } elseif ($cancellation == 'Free Refund') {
            $p_data[ $k ][ 'cancellation_policy' ] = esc_html__('Free Cancellation', 'wpestate');
        } else if ($cancellation == '24 Hours') {
            $allow = date('M d Y', strtotime("-1 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '.' Until '.date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '48 Hours') {
            $allow = date('M d Y', strtotime("-2 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '72 Hours') {
            $allow = date('M d Y', strtotime("-3 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '1 Week') {
            $allow = date('M d Y', strtotime("-7 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '2 Weeks') {
            $allow = date('M d Y', strtotime("-14 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '1 Month') {
            $allow = date('M d Y', strtotime("-1 month", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        }

        /* 
         * Get Porperty Price & Basic promo rate
         * Day & Date wise price show function
         */
        $price_per_day = floatval( get_post_meta($properties[ $k ], 'property_price', true) );
        $property_basic_promation = floatval( get_post_meta($properties[ $k ], 'property_basic_promation', true) );

        $price_per_day              =   '' != get_post_meta($properties[ $k ], 'basic_price_ner', true ) ? floatval( get_post_meta($properties[ $k ], 'basic_price_ner', true ) ) : floatval(get_post_meta($properties[ $k ], 'property_price', true));

        $price_array   = wpml_custom_price_adjust($properties[ $k ]);
        $price_per_day = isset( $price_array[ $check_in ] ) ? $price_array[ $check_in ] : $price_per_day;

        $property_basic_promation = '' != get_post_meta($properties[ $k ], 'promorate_ner', true ) ? floatval(get_post_meta($properties[ $k ], 'promorate_ner', true )) : floatval (get_post_meta($properties[ $k ], 'property_basic_promation', true) );

        $mega_details_array       = wpml_mega_details_adjust( $properties[ $k ] );
        $property_basic_promation = isset( $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate'] ) && $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate']!= '' ? $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate'] : $property_basic_promation;

        $where_currency = esc_html( get_option('wp_estate_where_currency_symbol', '') );
        $currency           =   esc_html( get_option('wp_estate_currency_label_main', '') );
        
        $p_data[ $k ]['property_basic_price'] = $currency.$price_per_day;
        // if (!empty($property_price)) {
        // }
        // $promo_rate = floatval (get_post_meta($properties[ $k ], 'property_basic_promation', true) );
        $p_data[ $k ]['basic_promorate'] = $currency.$property_basic_promation;
        // if (!empty($property_basic_promation)) {
        // }
        
        // Get Extra Options Fees
        $extra_pay_options = ( get_post_meta($properties[ $k ], 'extra_pay_options', true) );
        if (is_array($extra_pay_options) && !empty($extra_pay_options)) {
            $free_service = array();
            foreach ($extra_pay_options as $extra_services) {
                if ($extra_services[1] == 0) {
                    $free_service[] = esc_html__('Free ', 'wpestate').$extra_services[0];
                    // print_r($free_service[])
                    $p_data[ $k ]['extra_services'] = $free_service;
                }
            }
        }

        // // Get Review Ratings
        $args = array(
            'post_id' => $properties[ $k ]
        );
        $comments = get_comments($args);
        // print_r($comments);
        if (empty($comments) || '' == $comments) {
            $p_data[ $k ]['ratings'] = esc_html__('0');
        } else {
            $total = $ota_rev = $ota_stars = 0;
            foreach ($comments as $comment) {
                $rating = get_comment_meta( $comment->comment_ID , 'review_stars', true );

                // OTA STARS FIELD
                $ota_stars += get_comment_meta( $comment->comment_ID , 'ota_stars', true );

                $tmp = json_decode( $rating, TRUE );
                $total += intval( $tmp['rating'] );
                $ota_rev++;
            }
            
            $allrevs = $ota_stars + $total;
            if($allrevs){
                $all_avg = $allrevs / $ota_rev;
                $all_avg = number_format( $all_avg, 1, '.', '' );
                
                if ($all_avg > 0) {

                    if( $all_avg > 4.7 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Excellent!');
                    } elseif( $all_avg > 4.4 && $all_avg < 4.8 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Fantastic!');
                    } elseif( $all_avg > 3.9 && $all_avg < 4.5 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Wonderfull!');
                    } elseif( $all_avg > 3.4 && $all_avg < 4 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Great');
                    } elseif( $all_avg > 2.9 && $all_avg < 3.5 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Good');
                    } elseif( $all_avg >= 1 && $all_avg < 3 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Fair');
                    }
                }
            }
        }

        // Get Check in & Check Out Date
        $p_data[ $k ]['check_in_date'] = $check_in;
        $p_data[ $k ]['check_out_date']= $check_out;
    }

    // SORT 

    // if( isset( $_POST['sort_by'] ) && strcasecmp( $_POST['sort_by'], 'ASC') == 0 ) {
    //     uasort( $p_data , function( $a, $b ){
    //         return ( floatval(substr( trim( $a['property_basic_price'] ), 1 ) ) <= floatval( substr( trim( $b['property_basic_price']), 1 ) ) ) ? -1 : 1;
    //         // return ( floatval(substr( trim( $a['property_basic_price'] ), 1 ) ) <= floatval( substr( trim( $b['property_basic_price']), 1 ) ) ) ? -1 : 1;
    //     });
    // } elseif( isset( $_POST['sort_by'] ) && strcasecmp( $_POST['sort_by'], 'DESC') == 0 ) {
    //     uasort( $p_data , function( $a, $b ){

    //         return ( floatval(substr( trim( $a['property_basic_price'] ), 1 ) ) <= floatval( substr( trim( $b['property_basic_price']), 1 ) ) ) ? 1 : -1;
    //         // return ( floatval(substr( trim( $a['property_basic_price'] ), 1 ) ) <= floatval( substr( trim( $b['property_basic_price']), 1 ) ) ) ? 1 : -1;
    //     });
    // }
    
    // $sort_by = array(
    //     array(
    //         'key'  => 'Default',
    //         'value'=> esc_html__('Default','wpestate'),
    //     ),
    //     array(
    //         'key'  => 'ASC',
    //         'value'=> esc_html__('Price Low to High','wpestate'),
    //     ),
    //      array(
    //         'key'  => 'DESC',
    //         'value'=> esc_html__('Price High to Low','wpestate'),
    //     ),
    // );
    // END SORT
    if (empty($active_filters)) {
    	$active_filters = "";
    }
    
    if (!empty($p_data)) {
        return new WP_REST_Response (
            array(
                "response_code" => '200',
                'per_page'      => $per_page,
                'current_page'  => $current_page,
                'total_pages'   => $total_pages,
                'sort_by'       => $sort_by,
                'active_filters'=> $active_filters,
                'data'          => $p_data 

            ),
        200);
    } else {
        return new WP_REST_Response(
            array(
                "response_code" => "404",
                'message' => esc_html__('Could not find property', 'wpestate')), 404
            );
    }
}
