<?php

/**
 * Single property ratings casa reviews API
 */

add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/book-property',
        array(
            'methods' => 'POST',
            'callback'=> 'book_property',
        )
    );
});

function book_property( $data ) {

    // Check Oath Token
    $headers = apache_request_headers();
    $token_id =  explode( "-qe_aw-", $headers['Token'] );
    $token = get_user_meta($token_id[1], 'oauth_token', true);
    // $token_id[1] = 16;
    if (empty($headers['Token']) || $headers['Token'] != $token) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

    global $wpdb;

    // $guest_fromone      =  intval($_POST['guest_fromone']);
    $property_id        =  sanitize_text_field ( filter_input ( INPUT_POST, 'property_id' ) );
    $booking_from_date  =  sanitize_text_field ( filter_input ( INPUT_POST, 'fromdate') );
    $booking_to_date    =  sanitize_text_field ( filter_input ( INPUT_POST, 'todate' ) );
    $guest_no           =  sanitize_text_field ( filter_input ( INPUT_POST, 'guest_no' ) );
    $adult_type_val     =  sanitize_text_field ( filter_input ( INPUT_POST, 'adult_type_val' ) );
    $child_type_val     =  sanitize_text_field ( filter_input ( INPUT_POST, 'child_type_val' ) );
    $invoice_id         =  0;

    $check_in           =  strtotime($booking_from_date);
    $check_out          =  strtotime($booking_to_date);

    /* EXTRA OPTION CHARGE CALCULATIONS */
    $service_name = sanitize_text_field ( filter_input ( INPUT_POST, 'service_name' ) );
    // $service_name = 'airport_transfer,daily_maid_services,breakfast_daily,dinner_daily,daily_newspaper_delivery';

    // $price_per_day      =  floatval(get_post_meta($property_id, 'property_price', true));
    
    
    // $property_id       = 8230;
    // $booking_from_date = '2019-09-23';
    // $booking_to_date   = '2019-09-25';
    // $guest_no          = 5;
    // $adult_type_val    = 2;
    // $child_type_val    = 3;

    // ----------
    // $property_id       = 8230;
    // echo "--".$booking_from_date = '2019-09-23';
    // echo "--".$booking_to_date   = '2019-09-25';
    // echo "--".$guest_no          ."\n";
    // echo "--".$adult_type_val    ."\n";
    // echo "--".$child_type_val    ."\n";
    // ----------

    $max_allow_guest   = intval( get_post_meta ( $property_id, 'max_allow_guest', true ) );
    $guest_dropdown_no = intval( get_option ( 'wp_estate_guest_dropdown_no','' ) );
    
    /** 
     * START VALIDATIONS
     * CHECK MIN DAYS SITUATION
     */

    $mega_details     = wpml_mega_details_adjust( $property_id );
    $min_days_booking = intval( get_post_meta($property_id, 'min_days_booking', true) );  
    $min_days_value   = 0;

    if ( is_array( $mega_details ) && array_key_exists ( $check_in,$mega_details ) ) {
        if( isset ( $mega_details[$check_in]['period_min_days_booking'] ) ) {
            $min_days_value = $mega_details[$check_in]['period_min_days_booking'];

            if( ($check_in + ($min_days_value-1) * 86400) > $check_out ) {
                
                return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select at least min '.$mega_details[$check_in]['period_min_days_booking'].' Days ', 'wpestate')), 400);
            }
        }

    } elseif ( $min_days_booking > 0 ) {

        if( ( $check_in + ( $min_days_value - 1 ) * 86400 ) > $check_out ) {
            
            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select at least min '.$min_days_booking.' Days ', 'wpestate')), 400);
        }
    }

    /* --- END CHECK MIN DAYS SITUATION --- */

    if ( !isset($property_id) || !is_numeric($property_id) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Only numeric value allowed!', 'wpestate')), 400);
    }

    if(! check_avaibility( $booking_from_date, $booking_to_date, $property_id ) ) {
        return new WP_REST_Response(array('response_code' => '200', 'message' => esc_html__('Booking available not for the period of desire dates!', 'wpestate')), 200);
    }

    if( !isset( $booking_from_date ) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select first check in date', 'wpestate')), 400);
    }

    if( !isset( $booking_to_date ) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select check out date', 'wpestate')), 400);
    }
    
    $today = date("Y-m-d");
    if (strtotime($today) > $check_in) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Check in date should be today and onwards.', 'wpestate')), 400);
    }

    if ($check_in > $check_out) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Check out date should be greater than check in date.', 'wpestate')), 400);
    }

    if( !isset( $guest_no ) || !is_numeric($guest_no) ) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter no. of guests', 'wpestate')), 400);

    } elseif(  $guest_no < 1 ) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Guest should be greater than one', 'wpestate')), 400);

    } elseif ($guest_no > $guest_dropdown_no) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Guest should be '.$guest_dropdown_no.' or less', 'wpestate')), 400);

    } elseif ($guest_no > $max_allow_guest) {
        
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Max allowed guests are '.$max_allow_guest.'', 'wpestate')), 400);
    }

    if ( $guest_no > 1 ) {
        
        if ( !isset($adult_type_val) || !is_numeric($adult_type_val) ) {
            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select Guests!', 'wpestate')), 400);

        }
        if ( $adult_type_val == 0 || !is_numeric($adult_type_val) ) {
            
            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Children can go with at least one adult!', 'wpestate')), 400);

        } 
        $total_guest = $adult_type_val + $child_type_val;
        if ( $guest_no != $total_guest ) {

            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Adult and Kids total should be equals to Guest!', 'wpestate')), 400);
        }
    }
    // END VALIDATIONS


    $booking_array = wpestate_booking_price_custom($guest_no,$invoice_id, $property_id, $booking_from_date, $booking_to_date,$property_id,'','',$adult_type_val,$child_type_val,'');

    // print_r($booking_array);

    $data = array();

    $deposit_show   = '';
    $balance_show   = '';
    $currency       = esc_html( get_option('wp_estate_currency_label_main', '') ); //currency_symbol
    $where_currency = esc_html( get_option('wp_estate_where_currency_symbol', '') );//where_currency_symbol
  
    $price_show                         =   wpestate_show_price_booking($booking_array['default_price'],$currency,$where_currency,1);
    $total_price_show                   =   wpestate_show_price_booking($booking_array['total_price'],$currency,$where_currency,1);
    $deposit_show                       =   wpestate_show_price_booking($booking_array['deposit'],$currency,$where_currency,1);
    $balance_show                       =   wpestate_show_price_booking($booking_array['balance'],$currency,$where_currency,1);
    $city_fee_show                      =   wpestate_show_price_booking($booking_array['city_fee'],$currency,$where_currency,1);
    $cleaning_fee_show                  =   wpestate_show_price_booking($booking_array['cleaning_fee'],$currency,$where_currency,1);
    $total_extra_price_per_guest_show   =   wpestate_show_price_booking($booking_array['total_extra_price_per_guest'],$currency,$where_currency,1);
    $inter_price_show                   =   wpestate_show_price_booking($booking_array['inter_price'],$currency,$where_currency,1);
    $extra_price_per_guest              =   wpestate_show_price_booking($booking_array['extra_price_per_guest'],$currency,$where_currency,1);
    $security_deposit               =   floatval   ( get_post_meta($property_id, 'security_deposit', true) );
    $security_fee_show                  =   wpestate_show_price_booking($security_deposit,$currency,$where_currency,1);
    $early_bird_discount_show           =   wpestate_show_price_booking($booking_array['early_bird_discount'],$currency,$where_currency,1);
    $occupancy_tax_show = wpestate_show_price_booking( $booking_array['occupancy_tax'], $currency, $where_currency, 1 );


    if( $booking_array['price_per_guest_from_one'] == 1 ) {
        
        if( $booking_array['custom_period_quest'] != 1 ) {
            $data['extra_price_per_guest'] = $extra_price_per_guest.' x ';
        }
        
        // $data['count_day'] = $booking_array['count_days'].' '.esc_html__( 'nights','wpestate').' x '.$booking_array['curent_guest_no'].' '.esc_html__( 'guests','wpestate');
        $data[$booking_array['count_days'].' '.esc_html__( 'nights','wpestate').' x '.$booking_array['curent_guest_no'].' '.esc_html__( 'guests','wpestate')] = $inter_price_show;
        
        if( $booking_array['custom_period_quest'] == 1 ) {
           // $data['period_price'] =  ' - '.esc_html_e( ' period with custom price per guest','wpestate');
           $data[' - '.esc_html_e( ' period with custom price per guest','wpestate')] = $inter_price_show;
        }

    } else {
        if( $booking_array['has_custom'] == 1 ) {
            if( $booking_array['monthly_price'] == 1 ) {
                // $data['monthly_rate'] =  $booking_array['numberDays'].' '.esc_html__( 'nights with special monthly rate','wpestate');
                $data[$booking_array['numberDays'].' '.esc_html__( 'nights with special monthly rate','wpestate')] =  $inter_price_show;
            } elseif( $booking_array['weekly_price'] == 1 ) {
                // $data['weekly_rate'] =  $booking_array['numberDays'].' '.esc_html__( 'nights with special weekly rate','wpestate');
                $data[$booking_array['numberDays'].' '.esc_html__( 'nights with special weekly rate','wpestate')] =  $inter_price_show;
            } else {
                // $data['custom_rate'] =  $booking_array['numberDays'].' '.esc_html__( 'nights with custom price','wpestate');
                $data[$booking_array['numberDays'].' '.esc_html__( 'nights with custom price','wpestate')] = $inter_price_show;
            }
        }
        /*else if( $booking_array['has_wkend_price']===1 && $booking_array['cover_weekend']===1) {
            print  $booking_array['numberDays'].' '.esc_html__( 'nights with weekend price','wpestate');
        }*/
        else {
            
            $data['nights'] = $price_show.' x '.$booking_array['numberDays'].' '.esc_html__( 'nights','wpestate');
        }
    }

    // $data['inter_price'] = $inter_price_show; 

    if( $booking_array['has_guest_overload']!= 0 && $booking_array['total_extra_price_per_guest'] != 0 ) {

        // $data['extra_guests'] = esc_html__( 'Costs for ','wpestate').$booking_array['extra_guests'].' '.esc_html__('extra guests','wpestate');
        $data[esc_html__( 'Costs for ','wpestate').$booking_array['extra_guests'].' '.esc_html__('extra guests','wpestate')] = $total_extra_price_per_guest_show;
    }

    /*
    * Added By 64Bit
    * Calculate Coupon discount
    */

    /* Modified Code Vinod */
    $coupn           = array();
    $conditionfailed = 1;
    if( isset($_SESSION['myCoupon']) && $_SESSION['myCoupon']['user'] == $token_id[1] ) {
        $table = $wpdb->prefix.'coupon_record';
        $results = $wpdb->get_results("SELECT * FROM ".$table." WHERE coupon_code='".$_SESSION['myCoupon']['c_code']."' and prop_id = ".$property_id." AND coupon_status='Active' and ((start_date <= '".$booking_from_date."' AND end_date >= '".$booking_to_date."'))");

        $tbl = $wpdb->prefix.'coupon_usage';
        if( count($results) == 0 ) {
            $conditionfailed = 0;
        }

        foreach ( $results as $result ) {
         
            $times_used = $wpdb->get_row( "SELECT `time_used` FROM ".$tbl." WHERE `coupon_code`='".$_SESSION['myCoupon']['c_code']."' AND  user_id=".$token_id[1], ARRAY_A );  
            if ( $result->coupon_use == 1 && $times_used['time_used'] > 0 ) {
                $conditionfailed=0;
                unset($_SESSION['myCoupon']);
            }
        }

        if( $conditionfailed ) {

            $coupon_code = $_SESSION['myCoupon']['c_code'];
            if ( $_SESSION['myCoupon']['type'] == 'Fixed' ) {

                $total_dis = floatval( $_SESSION['myCoupon']['discount'] );
                $booking_array['total_price'] = number_format( floatval( $booking_array['total_price'] ) - $total_dis, 2, '.', '');
                $total_price_show = wpestate_show_price_booking($booking_array['total_price'],$currency,$where_currency,1);
                // echo "--".$total_dis;
                // $data['coupon_message'] = esc_html__( $coupon_code.' Applied '.$_SESSION['myCoupon']['discount'].' '.$_SESSION['myCoupon']['type'],'wpestate');
                $coupn['coupon_message'] = esc_html__( $coupon_code.' Applied '.$_SESSION['myCoupon']['discount'].' '.$_SESSION['myCoupon']['type'],'wpestate');

                // $data['coupon_text'] = esc_html__( 'Coupon Code ','wpestate');
                $data[esc_html__( 'Coupon Code ','wpestate')] = $coupon_code;
                $data['coupon_discount'] = wpestate_show_price_booking($total_dis,$currency,$where_currency,1);

                $coupn['coupon_code'] = $coupon_code;

            } else {

                $total_dis = ( floatval( $booking_array['inter_price'] ) + floatval( $booking_array['total_extra_price_per_guest'] ) ) * floatval( floatval( $_SESSION['myCoupon']['discount'] ) / 100 );
                $booking_array['total_price'] = number_format( floatval( $booking_array['total_price'] ) - $total_dis, 2, '.', '');
                $total_price_show = wpestate_show_price_booking($booking_array['total_price'],$currency,$where_currency,1);

                $c_type = esc_html__( '%', 'wpestate' );

                // $data['coupon_message'] = esc_html__( $coupon_code.' Applied '.$_SESSION['myCoupon']['discount']. $c_type,'wpestate');
                $coupn['coupon_message'] = esc_html__( $coupon_code.' Applied '.$_SESSION['myCoupon']['discount']. $c_type,'wpestate');
                // $data['coupon_text'] = esc_html__( 'Coupon Code ','wpestate');
                $data[esc_html__( 'Coupon Code','wpestate')] = $coupon_code;
                $data['coupon_discount'] = wpestate_show_price_booking($total_dis,$currency,$where_currency,1);

                $coupn['coupon_code'] = $coupon_code;

            }
        }

    } else {

        $coupn['coupon_message']  = "";
        // $data['coupon_text']     = "";
        $data[esc_html__('Coupon Code','wpestate')]     = "";
        $data['coupon_discount'] = "";

        $coupn['coupon_code']    = "";
    }
    /* Modified Code Vinod */
    /* Added By 64Bit     */

    // Added on 2019-08-03 - WDS
    $booking_array['total_price'] = floatval( $booking_array['total_price'] ) + floatval( $booking_array['total_extra_price_per_guest'] );
    $total_price_show = wpestate_show_price_booking($booking_array['total_price'],$currency,$where_currency,1);
    // Added on 2019-08-03 - WDS

    if( $booking_array['cleaning_fee'] != 0 && $booking_array['cleaning_fee'] != '' ) {
        // $data['cleaning_fee']  = esc_html__( 'Cleaning Fee','wpestate');
        $data[esc_html__( 'Cleaning Fee','wpestate')] = $cleaning_fee_show;
    } else {
        // $data['cleaning_fee']  = esc_html__( 'Cleaning Fee','wpestate');
        $data[esc_html__( 'Cleaning Fee','wpestate')] = "0";
    }
    // $booking_array['total_price'] += $booking_array['cleaning_fee'];
    
    if( $booking_array['city_fee'] != 0 && $booking_array['city_fee'] != '' ) {
        // $data['city_tax'] = esc_html__( 'City Tax','wpestate');
        $data[esc_html__( 'City Tax','wpestate')] = $city_fee_show;
    } else {
        // $data['city_tax'] = esc_html__( 'City Tax','wpestate');
        $data[esc_html__( 'City Tax','wpestate')] = "0";
    }
    // $booking_array['total_price'] += $booking_array['city_fee'];
    
    if( $booking_array['occupancy_tax'] != '' ) {
        // $data['occupancy_tax']   = esc_html__( 'Occupancy Tax','wpestate');
        $data[esc_html__( 'Occupancy Tax','wpestate')] = $occupancy_tax_show;
    } else {
        // $data['occupancy_tax']   = esc_html__( 'Occupancy Tax','wpestate');
        $data[esc_html__( 'Occupancy Tax','wpestate')] = "0";
    }     
    
    /* EXTRA OPTION CHARGE CALCULATIONS */
    $services = array();
    $total = 0;
    if ( !isset( $service_name) || empty( $service_name ) ) {
        $services['services'] = array();
    }
    if ( isset( $service_name ) ) {
    
        $service_name   = explode(',', $service_name);
        $value_computed = 0;
        $extra_pay_options = get_post_meta($property_id, 'extra_pay_options', true);

        foreach ( $service_name as $key => $extra_services ) { 

            $service_title = preg_replace("/[\s_]/", " ", $extra_services);
            $service_title = ucwords($service_title);

            foreach ($extra_pay_options as $key => $value) {

                if ( trim($service_title) == $value[0] ) {

                    $value_computed = wpestate_calculate_extra_options_value($booking_array['count_days'],  $guest_no, $value[2], $value[1]);
                   
                    $total += $value_computed;
                    // $data[$extra_services] = $service_title;
                    // $data[$extra_services.'_price'] = $currency.$value_computed;

                    $services['services'][] = array(
                        'key'  => $service_title,
                        'value'=> $currency.$value_computed,
                    );

                    $calc_with_early[] = $value_computed;
                    
                }
            }
        }
    }

    $booking_array['total_price'] += $total;
    $total_price_show = wpestate_show_price_booking($booking_array['total_price'],$currency,$where_currency,1);

    // END EXTRA OPTION CHARGE CALCULATIONS
    $extra_val = array();


    if( $booking_array['early_bird_discount'] != 0 && $booking_array['early_bird_discount'] != '') {

        $early_bird_percent = floatval(get_post_meta($property_id, 'early_bird_percent', true));
        // $data['early_discount']   = esc_html__( 'Early Booking Discount','wpestate');
        $new_early_bird = ($booking_array['inter_price'] + $booking_array['total_extra_price_per_guest'] + $total) * $early_bird_percent / 100;
        $data[esc_html__( 'Early Booking Discount','wpestate')] = $currency.$new_early_bird;

        // echo "-- ".$new_early_bird."\n";

        // echo "-- ".$booking_array['total_price'] -= $new_early_bird."\n";
        $total_price_show = wpestate_show_price_booking($booking_array['total_price'],$currency,$where_currency,1);

    }

    $booking_array['total_price'] += $booking_array['occupancy_tax'];
    $total_price_show = wpestate_show_price_booking($booking_array['total_price'],$currency,$where_currency,1);
    
    // $data['total']       = esc_html__( 'Total','wpestate');
    $data[esc_html__( 'Total','wpestate')] = $total_price_show;
    $extra_val['total_price'] = $booking_array['total_price'];
    
    $instant_booking=$instant_booking = floatval ( get_post_meta($property_id, 'instant_booking', true) ); 
    if( $instant_booking == 1 ) {   
        
        $extra_val['bal_due'] = esc_html__( 'Balance Due Now','wpestate').': ';
        if( floatval($booking_array['deposit']) != 0 ) {
            $extra_val['bal_due_price'] = $total_price_show;
        } else {
            $extra_val['bal_due_price'] = '0';
        }
        
        // if( floatval($booking_array['balance']) != 0 ) {
        //     $extra_val['remaining_bal'] = esc_html__( 'Balance remaining','wpestate');
        //     $extra_val['remaining_bal_price']  = $balance_show;
        // }
    }


    if( $security_deposit != 0 && $security_deposit != '' ) {
        $extra_val['security_deposit'] = esc_html__( 'Security Deposit (*refundable)','wpestate');
        $extra_val['security_deposit_price'] = $security_fee_show;
    }
    // print_r($booking_array);
    // print_r($data);
    $datas_ = array();
    foreach ($data as $key => $value) {
        
        $datas_['price'][] = array(

            'key'   => $key,
            'value' => $value,

        );

    }
    // print_r($datas);
    $_datas = array_merge( $extra_val, $coupn, $services, $datas_ );
    // die();
    // return $booking_data = $data;

    return new WP_REST_Response(array('response_code' => '200', 'data' => $_datas), 200);
}

// ------------------------------

// Check Proerty Booking Availbility
function check_avaibility( $book_from, $book_to, $listing_id, $internal=0 ) {

    $mega       =   wpml_mega_details_adjust($listing_id);
    $reservation_array = get_post_meta($listing_id, 'booking_dates',true);
    $total_booked_property_array = get_post_meta($listing_id, 'total_booked_property',true);
    if($reservation_array==''){
        $reservation_array = wpestate_get_booking_dates($listing_id);
    }
    
    $from_date      =   new DateTime($book_from);
    $from_date_unix =   $from_date->getTimestamp();

    $to_date        =   new DateTime($book_to);
    $to_date_unix_check   =   $to_date->getTimestamp();
    // $to_date->modify('yesterday');
       
    $to_date_unix   =   $to_date->getTimestamp();
    
    //check min days situation
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // if($internal==0){
    //     $min_days_booking   =   intval   ( get_post_meta($listing_id, 'min_days_booking', true) );  
    //     $min_days_value     =   0;
    //     if (is_array($mega) && array_key_exists ($from_date_unix,$mega)){
    //         if( isset( $mega[$from_date_unix]['period_min_days_booking'] ) ){
    //             $min_days_value=  $mega[$from_date_unix]['period_min_days_booking'];
    //             if( ($from_date_unix + ($min_days_value-1)*86400) > $to_date_unix ) {
    //                 // print 'stopdays';
    //                 // die();
    //                 // print_r($mega);
    //                 return false;
    //             }
    //         }
    //     }else if($min_days_booking > 0 ){
    //         if( ($from_date_unix + ($min_days_value-1)*86400) > $to_date_unix ) {
    //             // print 'stopdays';
    //             // die();
    //             return false;
    //         }
    //     }
    // }
    
    // check in check out days
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $checkin_checkout_change_over   =   floatval   ( get_post_meta($listing_id, 'checkin_checkout_change_over', true) ); 
    $weekday                        =   date('N', $from_date_unix);
    $end_bookday                    =   date('N', $to_date_unix_check);
    if (is_array($mega) && array_key_exists ($from_date_unix,$mega)){
        if( isset( $mega[$from_date_unix]['period_checkin_checkout_change_over'] ) &&  $mega[$from_date_unix]['period_checkin_checkout_change_over']!=0 ){
            $period_checkin_checkout_change_over=  $mega[$from_date_unix]['period_checkin_checkout_change_over'];

            if($weekday!= $period_checkin_checkout_change_over || $end_bookday !=$period_checkin_checkout_change_over) {
                /*print 'stopcheckinout';
                die();*/
                return false;
            }
        }
    } else if($checkin_checkout_change_over > 0 ) {

        if($weekday!= $checkin_checkout_change_over || $end_bookday !=$checkin_checkout_change_over) {
            // print 'stopcheckinout';
            // die();
            return false;
        }
    }
    // check in  days
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $checkin_change_over            =   floatval   ( get_post_meta($listing_id, 'checkin_change_over', true) );  
   
    if (is_array($mega) && array_key_exists ($from_date_unix,$mega)){
        if( isset( $mega[$from_date_unix]['period_checkin_change_over'] ) &&  $mega[$from_date_unix]['period_checkin_change_over']!=0){
            $period_checkin_change_over=  $mega[$from_date_unix]['period_checkin_change_over'];

            if($weekday!= $period_checkin_change_over) {
                // print 'stopcheckin';
                // die();
                return false;
            }
        }
    } else if($checkin_change_over > 0 ) {
        if($weekday!= $checkin_change_over) {
            // print 'stopcheckin';
            // die();
            return false;
        }
    }

    $property_rooms =   get_post_meta($listing_id, 'property_rooms', true);
    if($property_rooms == ''){
        $property_rooms = 1;
    }

    if( array_key_exists($from_date_unix,$reservation_array )) {
        
        $temp_array = array();           

        global $wpdb;
        $booking_ids = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id ) INNER JOIN wp_postmeta AS mt1 ON ( wp_posts.ID = mt1.post_id ) INNER JOIN wp_postmeta AS mt2 ON ( wp_posts.ID = mt2.post_id ) WHERE 1=1 AND ( ( ( wp_postmeta.meta_key = 'booking_id' AND wp_postmeta.meta_value = '".$listing_id."' ) AND ( mt1.meta_key = 'booking_status' AND mt1.meta_value = 'confirmed' ) ) AND ( ( mt2.meta_key = 'booking_from_date' AND mt2.meta_value >= '".$book_from."' ) AND ( mt2.meta_key = 'booking_from_date' AND mt2.meta_value <= '".$book_to."' ) ) ) AND wp_posts.post_type = 'wpestate_booking'AND ((wp_posts.post_status = 'publish')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date ASC", ARRAY_A);
    
        $temp_arr = array();
        if( $booking_ids ) {
            for( $j = 0; $j < count( $booking_ids ); $j++ ) {
                $st_dt = strtotime(get_post_meta( $booking_ids[ $j ]['ID'], 'booking_from_date', true ) );
                $ed_dt = strtotime(get_post_meta( $booking_ids[ $j ]['ID'], 'booking_to_date', true ) );
                for( $i = $st_dt; $i < $ed_dt; $i += 86400 ) {
                    $temp_arr[] = $i;
                }
            }
        }

        $temp_array = array_count_values( $temp_arr );
        // Line Added by 64Bit on 22 May 2019
        if ( isset( $mega[ $from_date_unix ]['inventory_sell'] ) ) {
            $property_rooms = $mega[ $from_date_unix ]['inventory_sell'];
        } 

        if(is_array( $total_booked_property_array ) && array_key_exists( $from_date_unix, $temp_array ) && $temp_array[ $from_date_unix ] >= $property_rooms /*&& 0 == $mega[ $from_date_unix ]['inventory_sell']*/ ){
           return false;
        }
        
        // Line added 64Bit 22 May 2019
        if( 0 == $mega[ $from_date_unix ]['inventory_sell'] ) {
            return false;
        }

    }
            
    // checking booking avalability
    while ($from_date_unix < $to_date_unix){
        $from_date->modify('tomorrow');
        $from_date_unix =   $from_date->getTimestamp();

       $temp_array2 = array();
        
        // Line added by 64Bit on 22 May 2019
        if ( isset( $mega[ $from_date_unix ]['inventory_sell'] ) ) {
            $property_rooms = $mega[ $from_date_unix ]['inventory_sell'];
        }
        
        if(is_array( $total_booked_property_array ) && array_key_exists( $from_date_unix, $temp_array ) && $temp_array[ $from_date_unix ] >= $property_rooms ){
            return false;
        }
    }
    return true;
}

// -----------------

// function price_calc() {}