<?php

/**
 * Single property ratings casa reviews API
 */

add_action('rest_api_init', function(){
	register_rest_route('tvcapi', '/v2/book-property',
		array(
			'methods' => 'POST',
			'callback'=> 'book_property',
		)
	);
});

function book_property( $data ) {

	// Check Oath Token
    $headers = apache_request_headers();
    $token_id =  explode( "-qe_aw-", $headers['Token'] );
    $token = get_user_meta($token_id[1], 'oauth_token', true);
    // $token_id[1] = 16;
    if (empty($headers['Token']) || $headers['Token'] != $token) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

    global $wpdb;

    // $guest_fromone      =  intval($_POST['guest_fromone']);
    $property_id        =  sanitize_text_field ( filter_input ( INPUT_POST, 'property_id' ) );
    $booking_from_date  =  sanitize_text_field ( filter_input ( INPUT_POST, 'fromdate') );
    $booking_to_date    =  sanitize_text_field ( filter_input ( INPUT_POST, 'todate' ) );
    $guest_no           =  sanitize_text_field ( filter_input ( INPUT_POST, 'guest_no' ) );
    $adult_type_val     =  sanitize_text_field ( filter_input ( INPUT_POST, 'adult_type_val' ) );
    $child_type_val     =  sanitize_text_field ( filter_input ( INPUT_POST, 'child_type_val' ) );
    $invoice_id         =  0;

    /* EXTRA OPTION CHARGE CALCULATIONS */
    $service_name = sanitize_text_field ( filter_input ( INPUT_POST, 'service_name' ) );
    // $service_name = 'airport_transfer,daily_maid_services,breakfast_daily,dinner_daily,daily_newspaper_delivery';

    // $price_per_day      =  floatval(get_post_meta($property_id, 'property_price', true));
    
    
    // $property_id       = 8230;
    // $booking_from_date = '2019-09-23';
    // $booking_to_date   = '2019-09-25';
    // $guest_no          = 5;
    // $adult_type_val    = 2;
    // $child_type_val    = 3;

    // ----------
    // $property_id       = 8230;
    // echo "--".$booking_from_date = '2019-09-23';
    // echo "--".$booking_to_date   = '2019-09-25';
    // echo "--".$guest_no          ."\n";
    // echo "--".$adult_type_val    ."\n";
    // echo "--".$child_type_val    ."\n";
    // ----------

    $max_allow_guest   = intval(get_post_meta($property_id, 'max_allow_guest', true));
    $guest_dropdown_no =   intval( get_option('wp_estate_guest_dropdown_no','') );
    // echo "-".$guest_dropdown_no;
    // START VALIDATIONS
    if ( !isset($property_id) || !is_numeric($property_id) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Only numeric value allowed!', 'wpestate')), 400);
    }

    if( !isset( $booking_from_date ) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select first check in date', 'wpestate')), 400);
    }

    if( !isset( $booking_to_date ) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select check out date', 'wpestate')), 400);
    }
    
    $check_in  = strtotime($booking_from_date);
    $check_out = strtotime($booking_to_date);
    $today = date("Y-m-d");
    if (strtotime($today) > $check_in) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Check in date should be today and onwards.', 'wpestate')), 400);
    }

    if ($check_in > $check_out) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Check out date should be greater than check in date.', 'wpestate')), 400);
    }

    if( !isset( $guest_no ) || !is_numeric($guest_no) ) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter no. of guests', 'wpestate')), 400);

    } elseif(  $guest_no < 1 ) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Guest should be greater than one', 'wpestate')), 400);

    } elseif ($guest_no > $guest_dropdown_no) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Guest should be '.$guest_dropdown_no.' or less', 'wpestate')), 400);

    } elseif ($guest_no > $max_allow_guest) {
        
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Max allowed guests are '.$max_allow_guest.'', 'wpestate')), 400);
    }

    if ( $guest_no > 1 ) {
        
        if ( !isset($adult_type_val) || !is_numeric($adult_type_val) ) {
            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select Guests!', 'wpestate')), 400);

        }
        if ( $adult_type_val == 0 || !is_numeric($adult_type_val) ) {
            
            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Children can go with at least one adult!', 'wpestate')), 400);

        } 
        $total_guest = $adult_type_val + $child_type_val;
        // $total_guest;
        if ( $guest_no != $total_guest ) {

            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Adult and Kids total should be equals to Guest!', 'wpestate')), 400);
        }
    }
    // END VALIDATIONS


    $booking_array = wpestate_booking_price_custom($guest_no,$invoice_id, $property_id, $booking_from_date, $booking_to_date,$property_id,'','',$adult_type_val,$child_type_val,'');

    $data = array();

    $deposit_show   = '';
    $balance_show   = '';
    $currency       = esc_html( get_option('wp_estate_currency_label_main', '') ); //currency_symbol
    $where_currency = esc_html( get_option('wp_estate_where_currency_symbol', '') );//where_currency_symbol
  
    $price_show                         =   wpestate_show_price_booking($booking_array['default_price'],$currency,$where_currency,1);
    $total_price_show                   =   wpestate_show_price_booking($booking_array['total_price'],$currency,$where_currency,1);
    $deposit_show                       =   wpestate_show_price_booking($booking_array['deposit'],$currency,$where_currency,1);
    $balance_show                       =   wpestate_show_price_booking($booking_array['balance'],$currency,$where_currency,1);
    $city_fee_show                      =   wpestate_show_price_booking($booking_array['city_fee'],$currency,$where_currency,1);
    $cleaning_fee_show                  =   wpestate_show_price_booking($booking_array['cleaning_fee'],$currency,$where_currency,1);
    $total_extra_price_per_guest_show   =   wpestate_show_price_booking($booking_array['total_extra_price_per_guest'],$currency,$where_currency,1);
    $inter_price_show                   =   wpestate_show_price_booking($booking_array['inter_price'],$currency,$where_currency,1);
    $extra_price_per_guest              =   wpestate_show_price_booking($booking_array['extra_price_per_guest'],$currency,$where_currency,1);
    $security_deposit               =   floatval   ( get_post_meta($property_id, 'security_deposit', true) );
    $security_fee_show                  =   wpestate_show_price_booking($security_deposit,$currency,$where_currency,1);
    $early_bird_discount_show           =   wpestate_show_price_booking($booking_array['early_bird_discount'],$currency,$where_currency,1);
    $occupancy_tax_show = wpestate_show_price_booking( $booking_array['occupancy_tax'], $currency, $where_currency, 1 );


    if( $booking_array['price_per_guest_from_one'] == 1 ) {
        
        if( $booking_array['custom_period_quest'] != 1 ) {
            $data['extra_price_per_guest'] = $extra_price_per_guest.' x ';
        }
        
        $data['count_day'] = $booking_array['count_days'].' '.esc_html__( 'nights','wpestate').' x '.$booking_array['curent_guest_no'].' '.esc_html__( 'guests','wpestate');
        
        if( $booking_array['custom_period_quest'] == 1 ) {
           $data['period_price'] =  ' - ';esc_html_e( ' period with custom price per guest','wpestate');
        }

    } else {
        if( $booking_array['has_custom'] == 1 ) {
            if( $booking_array['monthly_price'] == 1 ) {
                $data['monthly_rate'] =  $booking_array['numberDays'].' '.esc_html__( 'nights with special monthly rate','wpestate');
            } elseif( $booking_array['weekly_price'] == 1 ) {
                $data['weekly_rate'] =  $booking_array['numberDays'].' '.esc_html__( 'nights with special weekly rate','wpestate');
            } else {
                $data['custom_rate'] =  $booking_array['numberDays'].' '.esc_html__( 'nights with custom price','wpestate');
            }
        }
        /*else if( $booking_array['has_wkend_price']===1 && $booking_array['cover_weekend']===1) {
            print  $booking_array['numberDays'].' '.esc_html__( 'nights with weekend price','wpestate');
        }*/
        else {
            
            $data['nights'] = $price_show.' x '.$booking_array['numberDays'].' '.esc_html__( 'nights','wpestate');
        }
    }

    $data['inter_price'] = $inter_price_show; 

    if( $booking_array['has_guest_overload']!= 0 && $booking_array['total_extra_price_per_guest'] != 0 ) {

        $data['extra_guests'] = esc_html__( 'Costs for ','wpestate').$booking_array['extra_guests'].' '.esc_html__('extra guests','wpestate');
        $data['total_price_extra_guest'] = $total_extra_price_per_guest_show;
    }

    /*
    * Added By 64Bit
    * Calculate Coupon discount
    */

    /* Modified Code Vinod */

    $conditionfailed = 1;
    if( isset($_SESSION['myCoupon']) && $_SESSION['myCoupon']['user'] == $token_id[1] ) {
        $table = $wpdb->prefix.'coupon_record';
        $results = $wpdb->get_results("SELECT * FROM ".$table." WHERE coupon_code='".$_SESSION['myCoupon']['c_code']."' and prop_id = ".$property_id." AND coupon_status='Active' and ((start_date <= '".$booking_from_date."' AND end_date >= '".$booking_to_date."'))");

        $tbl = $wpdb->prefix.'coupon_usage';
        if( count($results) == 0 ) {
            $conditionfailed = 0;
        }

        foreach ( $results as $result ) {
         
            $times_used = $wpdb->get_row( "SELECT `time_used` FROM ".$tbl." WHERE `coupon_code`='".$_SESSION['myCoupon']['c_code']."' AND  user_id=".$token_id[1], ARRAY_A );  
            if ( $result->coupon_use == 1 && $times_used['time_used'] > 0 ) {
                $conditionfailed=0;
                unset($_SESSION['myCoupon']);
            }
        }

        if( $conditionfailed ) {

            $coupon_code = $_SESSION['myCoupon']['c_code'];
            if ( $_SESSION['myCoupon']['type'] == 'Fixed' ) {

                $total_dis = floatval( $_SESSION['myCoupon']['discount'] );
                $booking_array['total_price'] = number_format( floatval( $booking_array['total_price'] ) - $total_dis, 2, '.', '');
                $total_price_show = wpestate_show_price_booking($booking_array['total_price'],$currency,$where_currency,1);

                $data['coupon_code'] = esc_html__( 'Coupon Code ','wpestate'). $coupon_code;
                $data['coupon_discount'] = wpestate_show_price_booking($total_dis,$currency,$where_currency,1);

            } else {

                $total_dis = ( floatval( $booking_array['inter_price'] ) + floatval( $booking_array['total_extra_price_per_guest'] ) ) * floatval( floatval( $_SESSION['myCoupon']['discount'] ) / 100 );
                $booking_array['total_price'] = number_format( floatval( $booking_array['total_price'] ) - $total_dis, 2, '.', '');
                $total_price_show = wpestate_show_price_booking($booking_array['total_price'],$currency,$where_currency,1);

                $data['coupon_code'] = esc_html__( 'Coupon Code ','wpestate'). $coupon_code;
                $data['coupon_discount'] = wpestate_show_price_booking($total_dis,$currency,$where_currency,1);

            }
        }        
    }
    /* Modified Code Vinod */
    /* Added By 64Bit     */

    // Added on 2019-08-03 - WDS
    $booking_array['total_price'] = floatval( $booking_array['total_price'] ) + floatval( $booking_array['total_extra_price_per_guest'] );
    $total_price_show = wpestate_show_price_booking($booking_array['total_price'],$currency,$where_currency,1);
    // Added on 2019-08-03 - WDS

    if( $booking_array['cleaning_fee'] != 0 && $booking_array['cleaning_fee'] != '' ) {
        $data['cleaning_fee']  = esc_html__( 'Cleaning Fee','wpestate');
        $data['cleaning_fee_price'] = $cleaning_fee_show;
    } else {
        $data['cleaning_fee']  = esc_html__( 'Cleaning Fee','wpestate');
        $data['cleaning_fee_price'] = "0";
    }

    if( $booking_array['city_fee'] != 0 && $booking_array['city_fee'] != '' ) {
        $data['city_tax'] = esc_html__( 'City Tax','wpestate');
        $data['city_tax_price'] = $city_fee_show;
    } else {
        $data['city_tax'] = esc_html__( 'City Tax','wpestate');
        $data['city_tax_price'] = "0";
    }

    if( $booking_array['occupancy_tax'] != '' ) {
        $data['occupancy_tax']   = esc_html__( 'Occupancy Tax','wpestate');
        $data['occupancy_price'] = $occupancy_tax_show;
    } else {
        $data['occupancy_tax']   = esc_html__( 'Occupancy Tax','wpestate');
        $data['occupancy_price'] = "0";
    }     
    
    /* EXTRA OPTION CHARGE CALCULATIONS */

    $total = 0;
    if ( isset( $service_name ) ) {
    
        $service_name   = explode(',', $service_name);
        $value_computed = 0;
        $extra_pay_options = get_post_meta($property_id, 'extra_pay_options', true);

        foreach ( $service_name as $key => $extra_services ) { 

            $service_title = preg_replace("/[\s_]/", " ", $extra_services);
            $service_title = ucwords($service_title);

            foreach ($extra_pay_options as $key => $value) {

                if ( trim($service_title) == $value[0] ) {

                    $value_computed = wpestate_calculate_extra_options_value($booking_array['count_days'],  $guest_no, $value[2], $value[1]);
                   
                    $total += $value_computed;
                    // $data[$extra_services] = $service_title;
                    // $data[$extra_services.'_price'] = $currency.$value_computed;

                    $data['services'][] = array(
                        'key'  => $service_title,
                        'value'=> $currency.$value_computed,
                    );

                    $calc_with_early[] = $value_computed;
                    
                }
            }
        }
    }

    $booking_array['total_price'] += $total;
    $total_price_show = wpestate_show_price_booking($booking_array['total_price'],$currency,$where_currency,1);

    // END EXTRA OPTION CHARGE CALCULATIONS

    if( $booking_array['early_bird_discount'] != 0 && $booking_array['early_bird_discount'] != '') {

        $early_bird_percent = floatval(get_post_meta($property_id, 'early_bird_percent', true));
        $data['early_discount']   = esc_html__( 'Early Booking Discount','wpestate');
        $new_early_bird = ($booking_array['inter_price'] + $booking_array['total_extra_price_per_guest'] + $total) * $early_bird_percent / 100;
        $data['early_discount_price'] = $currency.$new_early_bird;

        $booking_array['total_price'] -= $new_early_bird;
        $total_price_show = wpestate_show_price_booking($booking_array['total_price'],$currency,$where_currency,1);

    }

    $booking_array['total_price'] += $booking_array['occupancy_tax'];
    $total_price_show = wpestate_show_price_booking($booking_array['total_price'],$currency,$where_currency,1);
    
    $data['total']       = esc_html__( 'Total','wpestate');
    $data['total_price'] = $total_price_show;
    
    $instant_booking=$instant_booking = floatval ( get_post_meta($property_id, 'instant_booking', true) ); 
    if( $instant_booking == 1 ) {   
        
        $data['bal_due'] = esc_html__( 'Balance Due Now','wpestate').': ';
        if( floatval($booking_array['deposit']) != 0 ) {
            $data['bal_due_price'] = $total_price_show;
        } else {
            $data['bal_due_price'] = '0';
        }
        
        // if( floatval($booking_array['balance']) != 0 ) {
        //     $data['remaining_bal'] = esc_html__( 'Balance remaining','wpestate');
        //     $data['remaining_bal_price']  = $balance_show;
        // }
    }


    if( $security_deposit != 0 && $security_deposit != '' ) {
        $data['security_deposit'] = esc_html__( 'Security Deposit (*refundable)','wpestate');
        $data['security_deposit_price'] = $security_fee_show;
    }
    // print_r($booking_array);
    // print_r($data);

     return new WP_REST_Response(array('response_code' => '200', 'data' => $data), 200);
}