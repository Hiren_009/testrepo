<?php

add_action('rest_api_init', function() {
    register_rest_route('tvcapi', '/v2/search-properties',
        array(
            'methods'  => 'POST',
            'callback' => 'search_properties',
        )
    );
});

function search_properties() {
	
    // Check Oath Token
    $headers = apache_request_headers();
    $token_id =  explode( "-qe_aw-", $headers['Token'] );
    $token = get_user_meta($token_id[1], 'oauth_token', true);

    if ($headers['Token'] != $token) {
        // Error Message
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Token is invalid', 'wpestate')), 400);
    }
    // END
	global $wpdb, $post;
    $prop_id = $properties = array();
	$destination    = sanitize_text_field( filter_input( INPUT_POST, 'destination') );
    $check_in_date  = sanitize_text_field( filter_input( INPUT_POST, 'check_in_date') );
    $check_out_date = sanitize_text_field( filter_input( INPUT_POST, 'check_out_date') );
    $guests         = sanitize_text_field( filter_input( INPUT_POST, 'guests' ) );

    // $destination = 'india';
    // $check_in_date = '2019-08-16';
    // $check_out_date = '2019-08-24';
    // $guests = 10; 
    if( !isset( $destination ) || empty($destination) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter destination place', 'wpestate')), 400);
    }

    if( !isset( $check_in_date ) || empty($check_in_date)) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select first check in date', 'wpestate')), 400);
    }

    if( !isset( $check_out_date ) || empty($check_out_date)) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select check out date', 'wpestate')), 400);
    }
    $check_in  = strtotime($check_in_date);
    $check_out = strtotime($check_out_date);
    
    $today = date("Y-m-d");
    // echo '---- '.$check_in;
    // echo ' match---- '.strtotime($today);
    if (strtotime($today) > $check_in) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('You can not select privious date', 'wpestate')), 400);
    }
    if ($check_in > $check_out) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('You can not select date less than check in date', 'wpestate')), 400);
    }
    if( !isset( $guests ) || empty($guests) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter no. of guests', 'wpestate')), 400);
    } elseif(  $guests < 1 ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Guest should be greater then one', 'wpestate')), 400);
    } elseif ($guests > 15) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Guest should be fifteen or less', 'wpestate')), 400);
    }
      
    //Convert it into a Unix timestamp using strtotime.
    $check_in  = strtotime($check_in_date);
    $check_out = strtotime($check_out_date);

    // echo strtotime('2019-08-09');
    // echo '-----> Check in Date -------';
    // echo $check_in;
    // echo '----- Check in Date ------->';
    // echo $check_out;
    $term_id = '';
    if( isset( $destination ) && '' != $destination ) {
        $term_id = $wpdb->get_results("SELECT `term_id` FROM `wp_terms` WHERE `name` LIKE '%".trim($destination)."%'", ARRAY_A);
        $term_id = implode( ",", array_column($term_id, 'term_id'));
    } 

    if( $term_id != '' ) {
       // New Query  08-08-2019
       $prop_id = $wpdb->get_results("SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN `wp_term_relationships` ON `wp_posts`.`ID` = `wp_term_relationships`.`object_id` INNER JOIN `wp_terms` ON `wp_term_relationships`.`term_taxonomy_id` IN (".$term_id.") INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE `wp_terms`.`name` LIKE '%".trim($destination)."%' OR (
            ( `wp_posts`.`post_title` LIKE '%".$destination."%' ) OR
            ( `wp_postmeta`.`meta_key` = 'property_address' AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' ) OR
            ( `wp_postmeta`.`meta_key` = 'property_state'   AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' ) OR
            ( `wp_postmeta`.`meta_key` = 'property_zip'          AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' ) OR
            ( `wp_postmeta`.`meta_key` = 'property_country' AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' ) 
        ) AND `wp_posts`.`post_type` = 'estate_property' AND `wp_posts`.`post_status` = 'publish' GROUP BY `wp_posts`.`ID`", ARRAY_A);

    } else {
        $prop_id = $wpdb->get_results("SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE `wp_posts`.`post_title` = '".$destination."' OR (
                ( `wp_postmeta`.`meta_key` = 'property_address' AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_state' AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_zip' AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_country' AND `wp_postmeta`.`meta_value` LIKE '%".$destination."%' )
            ) AND `wp_posts`.`post_type` = 'estate_property' AND `wp_posts`.`post_status` = 'publish'", ARRAY_A);

    }
    $prop_id = array_column($prop_id, "ID");
    
    
    // Check in & check out date
    if ( $prop_id && count($prop_id) > 0) {
        for ( $p = 0; $p < count( $prop_id ); $p++ ) {

            $property_id = $prop_id[ $p ];
            // ---
            $booking_ids = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts INNER JOIN wp_postmeta ON (wp_posts.ID = wp_postmeta.post_id ) INNER JOIN wp_postmeta AS mt1 ON ( wp_posts.ID = mt1.post_id ) INNER JOIN wp_postmeta AS mt2 ON ( wp_posts.ID = mt2.post_id ) WHERE 1=1 AND (( wp_postmeta.meta_key = 'booking_id' AND wp_postmeta.meta_value = '".$prop_id[ $p ]."' ) AND ( mt1.meta_key = 'booking_status' AND mt1.meta_value = 'confirmed' ) ) AND wp_posts.post_type = 'wpestate_booking' AND ((wp_posts.post_status = 'publish')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date ASC", ARRAY_A);
            // $booking_ids = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts INNER JOIN wp_postmeta ON (wp_posts.ID = wp_postmeta.post_id ) INNER JOIN wp_postmeta AS mt1 ON ( wp_posts.ID = mt1.post_id ) INNER JOIN wp_postmeta AS mt2 ON ( wp_posts.ID = mt2.post_id ) WHERE 1=1 AND (( wp_postmeta.meta_key = 'booking_id' AND wp_postmeta.meta_value = '".$prop_id[ $p ]."' ) AND ( mt1.meta_key = 'booking_status' AND mt1.meta_value = 'confirmed' ) ) AND wp_posts.post_type = 'wpestate_booking' AND ((wp_posts.post_status = 'publish')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date ASC ", ARRAY_A);
            $reservation_array = $booking_arr = $strt_dt = $sted_dt = $b_arr = array();
        
            if( $booking_ids ) {
                for( $j = 0; $j < count( $booking_ids ); $j++ ) {
                    $strt_dt[ $j ] = strtotime( get_post_meta( $booking_ids[ $j ]['ID'], 'booking_from_date', true ) );

                    $sted_dt[ $j ] = strtotime( get_post_meta( $booking_ids[ $j ]['ID'], 'booking_to_date', true ) );

                    for( $b = $strt_dt[ $j ]+86400; $b < $sted_dt[ $j ]; $b += 86400 ) {
                        $b_arr[] = $b;
                    } 
                }

                for( $i = 0; $i < count( $strt_dt ); $i++ ) {
                    if( array_key_exists( $strt_dt[ $i ], $booking_arr ) ) {
                        $booking_arr[ $strt_dt[ $i ] ] = $booking_arr[ $strt_dt[ $i ] ] + 1;
                    } else {
                        $booking_arr[ $strt_dt[ $i ] ] = 1;
                    }
                }

                for( $i = 0; $i < count( $b_arr ); $i++ ) {

                    if( array_key_exists( $b_arr[ $i ], $booking_arr ) ) {
                        $booking_arr[ $b_arr[ $i ] ] = $booking_arr[ $b_arr[ $i ] ] + 1;
                    } else {
                        $booking_arr[ $b_arr[ $i ] ] = 1;
                    }
                }
            }
            $reservation_array = $booking_arr;
            $mega_details = wpml_mega_details_adjust( $property_id );
            $guests_no = get_post_meta($property_id, 'max_allow_guest', true);
            $max_allow_guests = get_post_meta($property_id, 'overload_guest', true);
            // check property booking available or not
            for( $i = $check_in; $i < $check_out; $i += 86400 ) {
                if ( $reservation_array[ $i ] < $mega_details[ $i ]['inventory_sell'] || empty($reservation_array) ) { 
                    if ( $guests_no <= $guests && $max_allow_guests == 1 ) {
                        $properties[] = $property_id;
                    } elseif( $guests_no >= $guests ) {
                        $properties[] = $property_id;
                    } else {
                        break;
                    }
                } else {
                    break;
                }            
            }
            
        }
    } else {
        return new WP_REST_Response(array("response_code" => "404", 'message' => esc_html__('Could not found property', 'wpestate')), 404);
    }

    $properties   =  array_values( array_unique( $properties ) );
    // print_r($properties);

    // ---
    // Pagination
    $current_page   = sanitize_text_field( filter_input( INPUT_POST, 'current_page' ) );
    $properties_arr = implode(',', $properties);
    $per_page = 10;
    $offset = ( $current_page - 1 ) * $per_page;
    $total_pages = ceil(count($properties) / $per_page);
    // echo "SELECT ID FROM `wp_posts` WHERE ID IN (".$properties_arr.") AND `wp_posts`.`post_type` = 'estate_property' AND `wp_posts`.`post_status` = 'publish' LIMIT ".$offset.", ".$per_page."";
    $properties_res = $wpdb->get_results("SELECT ID FROM `wp_posts` WHERE ID IN (".$properties_arr.") AND `post_type` = 'estate_property' AND `post_status` = 'publish' ORDER BY ID DESC LIMIT ".$offset.", ".$per_page." ", ARRAY_A);
    $properties = array_column($properties_res, 'ID');
    
    // END pagination
    $p_data = array();
    // Start properties by above given properties ID
    for ($k=0; $k < count($properties); $k++) { 
        // echo $properties[ $k ];

        //Get Property ID
        $p_data[ $k ]['ID']    =  $properties[ $k ];

        // Property Image URL
        $post_thumb_id  = get_post_thumbnail_id( $properties[ $k ] );
        $post_thumb_url = wp_get_attachment_image_url($post_thumb_id, 'full');
        if ($post_thumb_url == false) {
            # code...
            $p_data[ $k ]['image_url'] = "";
        } else {
            $p_data[ $k ]['image_url'] = $post_thumb_url;
        }

        // Is Favorite Porperty
        $user_option = 'favorites'.$token_id[1];
        $curent_fav  = get_option( $user_option );
        if($curent_fav){
            if ( in_array ($properties[ $k ],$curent_fav) ){
                $p_data[ $k ]['is_favourite'] = true;
            } else {
                $p_data[ $k ]['is_favourite'] = false;
            }
        }

        //Get Property Title
        $p_data[ $k ]['title'] = get_the_title( $properties[ $k ] );

        // Get Property City
        $prop_city = get_the_terms($properties[ $k ], 'property_city');
        $this_cat = array();
        foreach ( $prop_city as $cat ) {
            $this_cat[] = $cat->name;
        }
        $p_data[ $k ]['city'] = implode( ",", $this_cat );

        // Get Property State
        $p_data[ $k ]['state'] = get_post_meta( $properties[ $k ], 'property_state', true );

        // Get Property type
        $selected_prop_type = get_the_terms($properties[ $k ], 'property_category');
        $p_data[ $k ]['property_type'] = $selected_prop_type[0]->name;

        // Get Property Zipcode
        $p_data[ $k ]['zipcode'] = get_post_meta( $properties[ $k ], 'property_zip', true);

        // Get Cancellation Policy
        $cancellation = get_post_meta($properties[ $k ],  'cancellation', true);
        if ($cancellation == 'NonRefundable') {
            $p_data[ $k ][ 'cancellation_policy' ] = esc_html__('Non Refundable', 'wpestate');
        } elseif ($cancellation == 'Free Refund') {
            $p_data[ $k ][ 'cancellation_policy' ] = esc_html__('Free Cancellation', 'wpestate');
        } else if ($cancellation == '24 Hours') {
            $allow = date('M d Y', strtotime("-1 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '.' Until '.date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '48 Hours') {
            $allow = date('M d Y', strtotime("-2 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '72 Hours') {
            $allow = date('M d Y', strtotime("-3 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '1 Week') {
            $allow = date('M d Y', strtotime("-7 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '2 Weeks') {
            $allow = date('M d Y', strtotime("-14 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '1 Month') {
            $allow = date('M d Y', strtotime("-1 month", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        }

        /* 
         * Get Porperty Price & Basic promo rate
         * Day & Date wise price show function
         */
        $price_per_day = floatval( get_post_meta($properties[ $k ], 'property_price', true) );
        $property_basic_promation = floatval( get_post_meta($properties[ $k ], 'property_basic_promation', true) );

        $price_array   = wpml_custom_price_adjust($properties[ $k ]);
        $price_per_day = isset( $price_array[ $check_in ] ) ? $price_array[ $check_in ] : $price_per_day;

        $mega_details_array       = wpml_mega_details_adjust( $properties[ $k ] );
        $property_basic_promation = isset( $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate'] ) && $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate']!= '' ? $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate'] : $property_basic_promation;

        $where_currency = esc_html( get_option('wp_estate_where_currency_symbol', '') );
        $currency           =   esc_html( get_option('wp_estate_currency_label_main', '') );
        
        $p_data[ $k ]['property_basic_price'] = $currency.$price_per_day;
        // if (!empty($property_price)) {
        // }
        // $promo_rate = floatval (get_post_meta($properties[ $k ], 'property_basic_promation', true) );
        $p_data[ $k ]['basic_promorate'] = $currency.$property_basic_promation;
        // if (!empty($property_basic_promation)) {
        // }
        
        // Get Extra Options Fees
        $extra_pay_options = ( get_post_meta($properties[ $k ], 'extra_pay_options', true) );
        if (is_array($extra_pay_options) && !empty($extra_pay_options)) {
            $free_service = array();
            foreach ($extra_pay_options as $extra_services) {
                if ($extra_services[1] == 0) {
                    $free_service[] = esc_html__('Free ', 'wpestate').$extra_services[0];
                    // print_r($free_service[])
                    $p_data[ $k ]['extra_services'] = $free_service;
                }
            }
        }

        // Get Review Ratings
        $args = array(
            'post_id' => $properties[ $k ]
        );
        $comments = get_comments($args);
        // print_r($comments);
        if (empty($comments) || '' == $comments) {
            $p_data[ $k ]['ratings'] = esc_html__('0');
        } else {
            $total = $ota_rev = $ota_stars = 0;
            foreach ($comments as $comment) {
                $rating = get_comment_meta( $comment->comment_ID , 'review_stars', true );

                // OTA STARS FIELD
                $ota_stars += get_comment_meta( $comment->comment_ID , 'ota_stars', true );

                $tmp = json_decode( $rating, TRUE );
                $total += intval( $tmp['rating'] );
                $ota_rev++;
            }
            
            $allrevs = $ota_stars + $total;
            if($allrevs){
                $all_avg = $allrevs / $ota_rev;
                $all_avg = number_format( $all_avg, 1, '.', '' );
                
                if ($all_avg > 0) {

                    if( $all_avg > 4.7 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5 Excellent!');
                    } elseif( $all_avg > 4.4 && $all_avg < 4.8 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5 Fantastic!');
                    } elseif( $all_avg > 3.9 && $all_avg < 4.5 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5 Wonderfull!');
                    } elseif( $all_avg > 3.4 && $all_avg < 4 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5 Great');
                    } elseif( $all_avg > 2.9 && $all_avg < 3.5 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5 Good');
                    } elseif( $all_avg >= 1 && $all_avg < 3 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5 Fair');
                    }
                }
            }
        }

        // Get Check in & Check Out Date
        $p_data[ $k ]['check_in_date'] = $check_in;
        $p_data[ $k ]['check_out_date']= $check_out;
    }

    // SORT 
    // ASC
    if( isset( $_POST['sort_by'] ) && strcasecmp( $_POST['sort_by'], 'ASC') == 0 ) {
        uasort( $p_data , function( $a, $b ){

            // return ( floatval(substr( trim( $a['property_basic_price'] ), 1 ) ) <= floatval( substr( trim( $b['property_basic_price']), 1 ) ) ) ? -1 : 1;
            return ( floatval(substr( trim( $a['property_basic_price'] ), 1 ) ) <= floatval( substr( trim( $b['property_basic_price']), 1 ) ) ) ? -1 : 1;
        });
    } elseif( isset( $_POST['sort_by'] ) && strcasecmp( $_POST['sort_by'], 'DESC') == 0 ) {
        uasort( $p_data , function( $a, $b ){

            // return ( floatval(substr( trim( $a['property_basic_price'] ), 1 ) ) <= floatval( substr( trim( $b['property_basic_price']), 1 ) ) ) ? -1 : 1;
            return ( floatval(substr( trim( $a['property_basic_price'] ), 1 ) ) <= floatval( substr( trim( $b['property_basic_price']), 1 ) ) ) ? 1 : -1;
        });
    }
    // print_r( $p_data );
    // END SORT
    
    if (!empty($p_data)) {
        return new WP_REST_Response (
            array(
                "response_code" => '200',
                'per_page'      => $per_page,
                'current_page'  => $current_page,
                'total_pages'   => $total_pages,
                'sort_by'       => array(
                    "Default"   => esc_html__('Default','wpestate'),
                    "ASC"       => esc_html__('Price Low to High','wpestate'),
                    "DESC"      => esc_html__('Price High to Low','wpestate')
                ),
                'data'          => $p_data
            ),
        200);
    } else {
        return new WP_REST_Response(
            array(
                "response_code" => "404",
                'message' => esc_html__('Could not find property', 'wpestate')), 404
            );
    }

}

