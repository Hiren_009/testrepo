<?php

/**
 * INSTANT BOOK PROPERTY API
 */

add_action('rest_api_init', function(){
	register_rest_route('tvcapi', '/v2/instant-booking',
		array(
			'methods' => 'POST',
			'callback'=> 'instant_booking',
		)
	);
});

function instant_booking(  ) {

	// Check Oath Token
    $headers = apache_request_headers();
    $token_id =  explode( "-qe_aw-", $headers['Token'] );
    $token = get_user_meta($token_id[1], 'oauth_token', true);
    $user_id = $token_id[1];
    // echo "->".$user_id; die();
    // if (empty($headers['Token']) || $headers['Token'] != $token) {
    //     // Error Message
    //     return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    // }
    // END

    $price_response = book_property( $_datas );
    $price_response = $price_response->data['data'];
    // print_r($price_response);
    // echo "--> ".$price_response->price;
    // die();
    
    $property_id        =  sanitize_text_field ( filter_input ( INPUT_POST, 'property_id' ) );
    $booking_from_date  =  sanitize_text_field ( filter_input ( INPUT_POST, 'fromdate') );
    $booking_to_date    =  sanitize_text_field ( filter_input ( INPUT_POST, 'todate' ) );
    $guest_no           =  sanitize_text_field ( filter_input ( INPUT_POST, 'guest_no' ) );
    $adult_type_val     =  sanitize_text_field ( filter_input ( INPUT_POST, 'adult_type_val' ) );
    $child_type_val     =  sanitize_text_field ( filter_input ( INPUT_POST, 'child_type_val' ) );
    $invoice_id         =  0;

    $check_in           =  strtotime($booking_from_date);
    $check_out          =  strtotime($booking_to_date);

    /* EXTRA OPTION CHARGE CALCULATIONS */
    $service_name = sanitize_text_field ( filter_input ( INPUT_POST, 'service_name' ) );
    // $service_name = 'airport_transfer,daily_maid_services,breakfast_daily,dinner_daily,daily_newspaper_delivery,';
    // $service_name = 'dinner_daily,daily_newspaper_delivery,';
    

    // $price_per_day      =  floatval(get_post_meta($property_id, 'property_price', true));
    
    // ----------

    $max_allow_guest   = intval( get_post_meta ( $property_id, 'max_allow_guest', true ) );
    $guest_dropdown_no = intval( get_option ( 'wp_estate_guest_dropdown_no','' ) );
    
    /** 
     * START VALIDATIONS
     * CHECK MIN DAYS SITUATION
     */

    $mega_details     = wpml_mega_details_adjust( $property_id );
    $min_days_booking = intval( get_post_meta($property_id, 'min_days_booking', true) );  
    $min_days_value   = 0;

    if ( is_array( $mega_details ) && array_key_exists ( $check_in,$mega_details ) ) {
        if( isset ( $mega_details[$check_in]['period_min_days_booking'] ) ) {
            $min_days_value = $mega_details[$check_in]['period_min_days_booking'];

            if( ($check_in + ($min_days_value-1) * 86400) > $check_out ) {
                
                return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select at least min '.$mega_details[$check_in]['period_min_days_booking'].' Days ', 'wpestate')), 400);
            }
        }

    } elseif ( $min_days_booking > 0 ) {

        if( ($check_in + ( $min_days_value - 1 ) * 86400 ) > $check_out ) {
            
            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select at least min '.$min_days_booking.' Days ', 'wpestate')), 400);
        }
    }

    /* --- END CHECK MIN DAYS SITUATION --- */

    if ( !isset($property_id) || !is_numeric($property_id) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Only numeric value allowed!', 'wpestate')), 400);
    }

    if(! check_avaibility( $booking_from_date, $booking_to_date, $property_id ) ) {
        return new WP_REST_Response(array('response_code' => '200', 'message' => esc_html__('Booking available not for the period of desire dates!', 'wpestate')), 200);
    }

    if( !isset( $booking_from_date ) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select first check in date', 'wpestate')), 400);
    }

    if( !isset( $booking_to_date ) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select check out date', 'wpestate')), 400);
    }
    
    $today = date("Y-m-d");
    if (strtotime($today) > $check_in) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Check in date should be today and onwards.', 'wpestate')), 400);
    }

    if ($check_in > $check_out) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Check out date should be greater than check in date.', 'wpestate')), 400);
    }

    if( !isset( $guest_no ) || !is_numeric($guest_no) ) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter no. of guests', 'wpestate')), 400);

    } elseif(  $guest_no < 1 ) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Guest should be greater than one', 'wpestate')), 400);

    } elseif ($guest_no > $guest_dropdown_no) {

        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Guest should be '.$guest_dropdown_no.' or less', 'wpestate')), 400);

    } elseif ($guest_no > $max_allow_guest) {
        
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Max allowed guests are '.$max_allow_guest.'', 'wpestate')), 400);
    }

    if ( $guest_no > 1 ) {
        
        if ( !isset($adult_type_val) || !is_numeric($adult_type_val) ) {
            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select Guests!', 'wpestate')), 400);

        }
        if ( $adult_type_val == 0 || !is_numeric($adult_type_val) ) {
            
            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Children can go with at least one adult!', 'wpestate')), 400);

        } 
        $total_guest = $adult_type_val + $child_type_val;
        if ( $guest_no != $total_guest ) {

            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Adult and Kids total should be equals to Guest!', 'wpestate')), 400);
        }
    }

    $instant_booking = floatval ( get_post_meta($property_id, 'instant_booking', true) );
    if( $instant_booking != 1 ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Something went wrong!', 'wpestate')), 400);
    }
    // END VALIDATIONS

    /*
     * START PREAPARE & CREATE BOOKING PROPERTY POST
     */
    $currency       = esc_html( get_option('wp_estate_currency_label_main', '') );
    // $from    = $current_user->user_login;
    // $comment = '';
    $status  = 'pending';

    $booking_guest_no = 0;
    if( isset ( $_POST['booking_guest_no'] ) ) {
        $booking_guest_no = intval($_POST['booking_guest_no']);
    }

    if( isset ( $_POST['confirmed'] ) ) {

        if (intval($_POST['confirmed']) == 1 ) {
            $status = 'confirmed';
        }

    }

    // PREPARE get property details 
    $owner_id               =   wpsestate_get_author($property_id);
    $rcapi_listing_id       =   get_post_meta($property_id,'rcapi_listing_id',true);
    $early_bird_percent     =   floatval(get_post_meta($property_id, 'early_bird_percent', true));
    $early_bird_days        =   floatval(get_post_meta($property_id, 'early_bird_days', true));
    $taxes_value            =   floatval(get_post_meta($property_id, 'property_taxes', true));

    $fromdate               =   $booking_from_date;
    $to_date                =   $booking_to_date;

    $event_name             =   esc_html__( 'Booking Request','wpestate');
    $security_deposit       =   get_post_meta(  $property_id,'security_deposit',true);
    $full_pay_invoice_id    =   0;
    $to_be_paid             =   0;

    $post = array(
        'post_title'    => $event_name,
        'post_content'  => $comment,
        'post_status'   => 'publish', 
        'post_type'     => 'wpestate_booking' ,
        'post_author'   => $user_id
    );
    // $booking_id = wp_insert_post($post );
    $booking_id = '23';
    $post = array(
        'ID'            => $booking_id,
        'post_title'    => $event_name.' '.$booking_id
    );
    wp_update_post( $post );


    global $wpdb;
    if( isset($_SESSION['myCoupon']) && $_SESSION['myCoupon']['user'] == $user_id ) {
        
        // $times_used = $wpdb->get_row( "SELECT `time_used` FROM $wpdb->prefix.coupon_usage WHERE `coupon_code`='".$_SESSION['myCoupon']."'", ARRAY_A );
        $tbl = $wpdb->prefix.'coupon_usage';
        $qry = "INSERT INTO ".$tbl." (`booking_id`,`coupon_code`,`use_date`,`user_id`,`time_used`,`property_id`) VALUES (".$booking_id.", '".$_SESSION['myCoupon']['c_code']."', '".date("Y-m-d")."', ".$user_id.", 1,".$property_id.")";
        
        $wpdb->query( $qry );
        
        /* Vinod Rathore */
        $check_coupon_used = $wpdb->get_row("SELECT coupon_use FROM ".$wpdb->prefix."coupon_record WHERE `id`='".$_SESSION['myCoupon']['c_id']."'",ARRAY_A);

        if($check_coupon_used->coupon_use==1){
            $check_coupon_used = $wpdb->get_row("SELECT status FROM ".$wpdb->prefix."coupon_guest WHERE `id`='".$_SESSION['myCoupon']['c_id']."' and status=0",ARRAY_A);
            if(coupon($check_coupon_used) > 0)
                $wpdb->update($wpdb->prefix."coupon_guest",array('status'=>1),array('coupon_id'=>$_SESSION['myCoupon']['c_id']));
        }
        /******* Vinod Rathore **********/
    }

    update_post_meta($booking_id, 'booking_status', $status);
    update_post_meta($booking_id, 'booking_id', $property_id);
    update_post_meta($booking_id, 'owner_id', $owner_id);
    update_post_meta($booking_id, 'booking_from_date', $fromdate);
    update_post_meta($booking_id, 'booking_to_date', $to_date);
    update_post_meta($booking_id, 'booking_invoice_no', 0);
    update_post_meta($booking_id, 'booking_pay_ammount', 0);
    update_post_meta($booking_id, 'booking_guests', $guest_no);

    $extra_service = explode(',', $service_name);
    $j = -1;
    for ($i = 0; $i < count($extra_service); $i++) { 

        if (!empty( trim( $extra_service[ $i ] ) )) {
            
            $j++;
            $str .= $j.',';
        }
    }
    update_post_meta($booking_id, 'extra_options', rtrim($str,","));
    update_post_meta($booking_id, 'security_deposit', $security_deposit);
    update_post_meta($booking_id, 'full_pay_invoice_id', $full_pay_invoice_id);
    update_post_meta($booking_id, 'to_be_paid', $to_be_paid);
    update_post_meta($booking_id, 'early_bird_percent', $early_bird_percent);
    update_post_meta($booking_id, 'early_bird_days', $early_bird_days);
    update_post_meta($booking_id, 'booking_taxes', $taxes_value);
    // update_user_meta($booking_id, 'cancellation', get_post_meta( $property_id, 'cancellation', true) );

    

    // Re build the reservation array 
    $reservation_array  = get_post_meta($property_id, 'booking_dates',true);
    if( $reservation_array == '' ){
        $reservation_array = wpestate_get_booking_dates($property_id);
    }
    
    $total_booked_property_array  = get_post_meta($property_id, 'total_booked_property',true);
    
    update_post_meta($property_id, 'booking_dates', $reservation_array); 


    $booking_array      =   wpestate_booking_price_custom($booking_guest_no,$invoice_id, $property_id, $fromdate, $to_date,$booking_id,$extra_options_array,'',$adult_type_val,$child_type_val,$adult_type_single);

    update_post_meta($booking_id, 'booking_details_info', $booking_array );


    // Updating the booking details 
    update_post_meta($booking_id, 'to_be_paid', $booking_array['deposit']);
    update_post_meta($booking_id, 'booking_taxes', $booking_array['taxes']);
    update_post_meta($booking_id, 'service_fee', $booking_array['service_fee']);
    update_post_meta($booking_id, 'taxes', $booking_array['taxes'] );
    update_post_meta($booking_id, 'occupancy_tax', $booking_array['occupancy_tax']);
    update_post_meta($booking_id, 'youearned', $booking_array['youearned']);
    update_post_meta($booking_id, 'custom_price_array',$booking_array['custom_price_array']);
    update_post_meta($booking_id, 'balance',$booking_array['balance']);
    update_post_meta($booking_id, 'total_price',$booking_array['total_price']);

    /* Badrilal code start */
    @session_start();
    if(isset($_SESSION['previous_location']) && $_SESSION['previous_location']=="MembersOnlyDiscount"){
        
        update_post_meta($booking_id, 'member_discount',1);
        if(isset($_SESSION['campaign_id']) && $_SESSION['campaign_id']!="" && is_numeric($_SESSION['campaign_id']))
            update_post_meta($booking_id, 'campaign_id',$_SESSION['campaign_id']);

           $fdgfg= $wpdb->update($wpdb->prefix."campaign_list",array('room_night_sold'=>$_SESSION['room_night_sold'],'total_campains_booking'=>$_SESSION['total_campains_booking'],'total_revenue'=>($_SESSION['total_revenue']+$booking_array['total_price'])),array('id'=>$_SESSION['campaign_id']));
    } else {
        update_post_meta($booking_id, 'member_discount',0);
    }
    
    /* Badrilal code end */

    update_post_meta($booking_id, 'adult_type_val',$adult_type_val);
    update_post_meta($booking_id, 'child_type_val',$child_type_val);

    update_post_meta( $booking_id, 'coupon_code', $price_response['coupon_code'] );
    update_post_meta( $booking_id, 'discount', preg_replace('/[^\d.]/', '', $price_response['coupon_discount']) );

    /* rcapi update booking info */
    $property_author = wpsestate_get_author($property_id);
        
    if( $user_id != $property_author){
        $rcapi_listing_id    = get_post_meta($property_id,'rcapi_listing_id',true);
        $add_booking_details = array(

            "booking_status"            =>  $status,
            "original_property_id"      =>  $property_id,
            "rcapi_listing_id"          =>  $rcapi_listing_id,
            "book_author"               =>  $user_id,
            "owner_id"                  =>  $owner_id,
            "booking_from_date"         =>  $fromdate,
            "booking_to_date"           =>  $to_date,
            "booking_invoice_no"        =>  0,
            "booking_pay_ammount"       =>  $booking_array['deposit'],
            "booking_guests"            =>  $booking_guest_no,
            "extra_options"             =>  $extra_options,
            //"security_deposit"          =>  $booking_array['security_deposit'],
            "full_pay_invoice_id"       =>  0,
            "to_be_paid"                =>  $booking_array['deposit'],
            "youearned"                 =>  $booking_array['youearned'],
            "service_fee"               =>  $booking_array['service_fee'],
            "booking_taxes"             =>  $booking_array['taxes'],
            "total_price"               =>  $booking_array['total_price'],
            "custom_price_array"        =>  $booking_array['custom_price_array'],
            "submission_curency_status" =>  esc_html( get_option('wp_estate_submission_curency','') ),
            "balance"                   =>  $booking_array['balance']
        );
        // update on API if is the case

        if($booking_array['balance'] > 0){
            update_post_meta($booking_id, 'booking_status_full','waiting' );
            $add_booking_details['booking_status_full'] =   'waiting';
        }
        rcapi_save_booking($booking_id,$add_booking_details);
    }
    /* end rcapi update booking info */

    //check if period already reserverd
    // wpestate_check_for_booked_time_custom($fromdate,$to_date,$reservation_array,$total_booked_property_array,$property_id);
    //end check

    /*
     * -----------------------------------------------
     * END PREAPARE & CREATE BOOKING PROPERTY POST
     * -----------------------------------------------
     * START PREAPARE & GENERATE INVOICE PROPERTY POST
     * -----------------------------------------------
     */

    $billing_for   =  esc_html__( 'Reservation fee','wpestate');
    $type          =  esc_html__( 'One Time','wpestate');
    $pack_id       =  $booking_id; // booking id
   
    $time          =  time(); 
    $date          =  date('Y-m-d H:i:s',$time); 
    $user_id       =  wpse119881_get_author($booking_id);

    $is_featured   =  '';
    $is_upgrade    =  '';
    $paypal_tax_id =  '';

    //$invoice_id =  wpestate_booking_insert_invoice($billing_for,$type,$pack_id,$date,$user_id,$is_featured,$is_upgrade,$paypal_tax_id,$details,$price,$owner_id);
    $invoice_id =  wpestate_booking_insert_invoice_custom($billing_for,$type,$pack_id,$date,$user_id,$is_featured,$is_upgrade,$paypal_tax_id,$details,$price,$owner_id);       

    // update booking status
    if( $user_id != $property_author){
        update_post_meta($booking_id, 'booking_status', 'waiting');
        update_post_meta($booking_id, 'booking_invoice_no', $invoice_id);
        $booking_details =array(
                'booking_status'     => 'waiting',
                'booking_invoice_no' => $invoice_id
        );
        $rcapi_booking_id = get_post_meta($booking_id,'rcapi_booking_id',true);
        // print_r($rcapi_booking_id);
        // rcapi_edit_booking($booking_id,$rcapi_booking_id,$booking_details);
    // echo "--hell".$booking_id;
    }
    
    //update invoice data
    update_post_meta($invoice_id, 'early_bird_percent', $early_bird_percent);
    update_post_meta($invoice_id, 'early_bird_days', $early_bird_days);
    update_post_meta($invoice_id, 'booking_taxes', $taxes_value);
    update_post_meta($invoice_id, 'booking_taxes', $booking_array['taxes']);  
    update_post_meta($invoice_id, 'service_fee', $booking_array['service_fee']);
    update_post_meta($invoice_id, 'occupancy_tax', $booking_array['occupancy_tax']);
    update_post_meta($invoice_id, 'youearned', $booking_array['youearned']);
    update_post_meta($invoice_id, 'depozit_to_be_paid', $booking_array['deposit']);
    update_post_meta($invoice_id, 'item_price', $booking_array['total_price']); 
    update_post_meta($invoice_id, 'custom_price_array',$booking_array['custom_price_array']);
    update_post_meta($invoice_id, 'balance',$booking_array['balance']);

    update_post_meta( $invoice_id, 'coupon_code', $price_response['coupon_code'] );
    update_post_meta( $invoice_id, 'discount', preg_replace('/[^\d.]/', '', $price_response['coupon_discount']) );
    
    if($booking_array['balance'] == 0){
        update_post_meta($invoice_id, 'is_full_instant',1);
        update_post_meta($booking_id, 'is_full_instant',1);
    }
    /* END UPDATE GENERATE INVOICE METADATA */

    // send notifications
    $receiver        =  get_userdata($user_id);
    $receiver_email  =  $receiver->user_email;
    $receiver_login  =  $receiver->user_login;
    $from            =  $owner_id;
    $to              =  $user_id;
    $subject         =  esc_html__( 'New Invoice','wpestate');
    $description     =  esc_html__( 'A new invoice was generated for your booking request','wpestate');

    // wpestate_add_to_inbox($user_id,$user_id,$to,$subject,$description,1);
    wpestate_add_to_inbox_custom( $property_id, $booking_id, $from, $from, $to, $subject, $description, 1 );
    wpestate_send_booking_email('newinvoice',$receiver_email);

    //set conversation
    do_action( 'after_booking_created', array( 'owner_id' => $owner_id, 'booking_id' => $booking_id, 'property_id' => $property_id ) );


    /*
     * -----------------------------------------------
     * END PREAPARE & GENERATE INVOICE PROPERTY POST
     * -----------------------------------------------
     * START PREAPARE CHECKOUT PAYMENT PRICE RESPONCE 
     * -----------------------------------------------
     */
    $data = array();
    
    // BOOKING & INVOICE ID
    $data['booking_id'] = $booking_id;
    $data['invoice_id'] = $invoice_id;

    // JOIN LOYALTY REWARD CLUB
    $user_id = $token_id[1];
    $join = get_user_meta($user_id, 'join_reward_club', true);

    if ( !empty( $join ) ) {
        $data['is_join_reward_club'] = $join;
    } else {
        $data['is_join_reward_club'] = "0";
    }

    $apply_reward_p   =  sanitize_text_field ( filter_input ( INPUT_POST, 'apply_reward_p' ) );
    $apply_reward_p   = 1;
    $rewardpoits =  count_rewards_points( $user_id, $apply_reward_p );

    // WILL EARN LOYALTY REWARD POINTS
    if ( $join == 1 ) {
        $data['will_earn_points'] = $rewardpoits['getpoints'];
    } else {
        $data['will_earn_points'] = "";
    }

    // AVAILABLE REWARD POINTS IN DOLLER
    if ( !empty( $rewardpoits['avail_point_cal'] ) || $rewardpoits['avail_point_cal'] != 0 ) {
        # code...
        $data['avail_$'] = $currency.$rewardpoits['avail_point_cal'];
    } else {
        $data['avail_$'] = "";

    }

    // FINAL TOTAL PRICE AFTER APPLIED POINTS
    // echo "-".$amount               = $price_response['total_price'];
    if ( !isset( $apply_reward_p ) || $apply_reward_p == 0 ) {
        $data['total_amount'] = $price_response['total_price'];
        $amount               = $price_response['total_price'];
    } else {
        $data['total_amount'] = $currency.$rewardpoits['apply_points'];
        $amount               = $rewardpoits['apply_points'];
    }

    $amount = preg_replace('/[^\d.]/', '', floatval($amount));
    $amount = $amount * 100;
    // echo "decimal--".$amount;
    $price_res = array_merge( $price_response, $data );
    // print_r($price_res);

    /* START STRIPE PAYMENT VALIDATIONS */
    if( isset( $user_id ) && !is_numeric( $user_id ) ) {
        return new WP_REST_Response( array ( 'response_code' => '401', 'message' => esc_html__('In user id womething went wrong', 'wpestate') ), 401);
    }

    if( isset( $invoice_id ) && !is_numeric( $invoice_id ) ) {
        return new WP_REST_Response( array ( 'response_code' => '401', 'message' => esc_html__('In invoice_id womething went wrong', 'wpestate') ), 401);
    }

    if( isset( $booking_id ) && !is_numeric( $booking_id  ) ) {
        return new WP_REST_Response( array ( 'response_code' => '401', 'message' => esc_html__('In booking_id womething went wrong', 'wpestate') ), 401);
    }

    if( isset( $amount ) && !is_numeric( $amount ) ) {
        return new WP_REST_Response( array ( 'response_code' => '401', 'message' => esc_html__('In amount price something went wrong', 'wpestate') ), 401);
    }
    /* 
     ------------------------------------------------
     * END STRIPE PAYMENT VALIDATIONS 
     ------------------------------------------------
     * START BOOKING AMOUNT CHARGE FROM STRIP PAYMENT
     ------------------------------------------------
     */
    stripe_pament( $user_id, $booking_id, $invoice_id, $amount );
    
    /* END BOOKING AMOUNT CHARGE FROM STRIP PAYMENT */
    
    // return new WP_REST_Response( array ( 'response_code' => '200', 'data' => $price_res ), 200);

    /*
     * END PREAPARE CHECKOUT PAYMENT PRICE RESPONCE
     */
}


/*
 * COUNT LOYALTY REWARD POINTS FUNCTION
 */
function count_rewards_points( $user_id, $apply_reward_points ) {
    
    $price_response   = book_property( $data );
    $price_response   = $price_response->data['data'];
    $user_total_point = get_user_meta($user_id, 'loyalty_reward_point', true);
    $total_point      = preg_replace('/[^\d.]/', '', $price_response['total_price']);

    // if( $user_total_point == '' ) {

    //     $user_total_point = 0; 
    // }

    // if ($user_total_point >= 0 && $user_total_point <= 4999) {       
        
    //     $per      = 1;
    //     $getpoint = $total_point * $per; 
    //     // $getpoint = (isset( $apply_reward_points == 1) ? ); 
    //     // $getpoint = $user_total_point * 1; 

    // } elseif ($user_total_point >= 4999 && $user_total_point <= 9999) {
        
    //     $per      = 1.2;
    //     $getpoint = $total_point * $per;
    //     // $getpoint = $user_total_point * 1.2;

    // } else {
        
    //     $per      = 1.4;
    //     $getpoint = $total_point * $per;
    //     // $getpoint = $total_point * 1.4;

    // }

    //RESET LOYALTY REWARD POOINTS AFTER APPLIED REWARD POINTS IN TOTAL PRICE 


    // CALCULATE AVAILABLE TOTAL REWARD POINTS
    $avail_point     =  $user_total_point;
    $avail_point_cal =  $user_total_point / 100;

    if ( !empty( $avail_point_cal ) ) {
        $avail_point_cal = number_format( $avail_point_cal, 2 );
    } else {
        $avail_point_cal = "";
    }
    // echo "convert-in-dollars- ".$avail_point_cal."\n";
    // FINAL TOTAL AFTER APPLIED REWARD POINTS
    if ( isset( $apply_reward_points ) && $apply_reward_points == 1 ) {
        
        $apply_points = preg_replace('/[^\d.]/', '', $price_response['total_price']) - preg_replace('/[^\d.]/', '', $avail_point_cal);

        // echo "total_amount- ".$apply_points."\n";
        $apply_points = $apply_points > 0 ? $apply_points : 0;

    } elseif ( isset( $apply_reward_points ) && $apply_reward_points == 0 ) {
        $apply_points = preg_replace('/[^\d.]/', '', $price_response['total_price']);
    } else {
        $apply_points = "";
    }
    // return $avail_point_cal;
    // return $avail_point = round( $avail_point );
    // return $getpoint = round( $getpoint );

    if( $user_total_point == '' ) {

        $user_total_point = 0; 
    }

    if ($user_total_point >= 0 && $user_total_point <= 4999) {       
        
        $per      = 1;
        $getpoint = $total_point * $per; 
        $getpoint = (isset( $apply_reward_points ) == 1 ) ? $apply_points * $per : $getpoint; 
        // $getpoint = $user_total_point * 1; 

    } elseif ($user_total_point >= 4999 && $user_total_point <= 9999) {
        
        $per      = 1.2;
        $getpoint = $total_point * $per;
        // $getpoint = $user_total_point * 1.2;
        $getpoint = (isset( $apply_reward_points ) == 1 ) ? $apply_points * $per : $getpoint; 

    } else {
        
        $per      = 1.4;
        $getpoint = $total_point * $per;
        // $getpoint = $total_point * 1.4;
        $getpoint = (isset( $apply_reward_points ) == 1 ) ? $apply_points * $per : $getpoint; 

    }







    return array( 
            'getpoints'       => round( $getpoint ),
            'avail_point_cal' => $avail_point_cal,
            'apply_points'    => $apply_points,
        );
}