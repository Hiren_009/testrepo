<?php

/**
 * Extra Options Fees API
 */

add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/extra-options',
        array(
            'methods' => 'GET',
            'callback'=> 'extra_options',
        )
    );
});

function extra_options() {

    // Check Oath Token
    $headers = apache_request_headers();
    $token_id =  explode( "-qe_aw-", $headers['Token'] );
    $token = get_user_meta($token_id[1], 'oauth_token', true);
    $user_id = 16;
    if (empty($headers['Token']) || $headers['Token'] != $token) {
        
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

    $property_id  =  $_GET['id'];
    // $property_id  =  8230;
    // $property_id       =  8197;
    // $property_id       =  8208;

    // if ( !isset($property_id) && !is_numeric($property_id)) {
    //     return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Only numeric value allowed!', 'wpestate')), 400);
    // }
    $currency          = esc_html( get_option('wp_estate_currency_label_main', '') );
    $extra_pay_options = get_post_meta($property_id, 'extra_pay_options', true);
    $options_array = array(
        0   =>  esc_html__('Single Fee','wpestate'),
        1   =>  esc_html__('Per Night','wpestate'),
        2   =>  esc_html__('Per Guest','wpestate'),
        3   =>  esc_html__('Per Night per Guest','wpestate')
    );
    // print_r($extra_pay_options);
    $extra_opt = array();
    
    if ( !empty($extra_pay_options) ) {
        
        for ($i=0; $i < count($extra_pay_options); $i++) { 
            $service_title = preg_replace("/[\s_]/", "_", $extra_pay_options[ $i ][0]);
            // print_r(strtolower($service_title));
            $extra_opt[ $i ]['service_id']           = strtolower($service_title);
            $extra_opt[ $i ]['service_title']        = $extra_pay_options[ $i ][0];
            if ( $extra_pay_options[ $i ][1] == 0 ) {
                $extra_opt[ $i ]['service_charge']       = esc_html__('Free', 'wpestate');
            }  else {
                $extra_opt[ $i ]['service_charge']       = $currency.$extra_pay_options[ $i ][1];
            }
            // echo "--".$extra_pay_options[ $i ][2];
            if ( $extra_pay_options[ $i ][2] != undefined ) {
                
                $extra_opt[ $i ]['service_charge_title'] = $options_array[ $extra_pay_options[ $i ][2] ];
            } else {
                $extra_opt[ $i ]['service_charge_title'] = "";
            }
            // $extra_opt[ $i ]['service_charge_id']    = $extra_pay_options[ $i ][2];
        }
        // print_r($extra_opt);

        return new WP_REST_Response(
            array(
                'response_code'  => "200", 
                'data' => $extra_opt,
            ),
        200);
    }
    die();
}

// -------------------------------------------

/**
 * Extra Options Fees Apply API
 */

// add_action('rest_api_init', function(){
//     register_rest_route('tvcapi', '/v2/extra-options-apply',
//         array(
//             'methods' => 'GET',
//             'callback'=> 'extra_options_apply',
//         )
//     );
// });

// function extra_options_apply() {

//     // Check Oath Token
//     $headers  = apache_request_headers();
//     $token_id = explode( "-qe_aw-", $headers['Token'] );
//     $token    = get_user_meta($token_id[1], 'oauth_token', true);
//     $user_id  = 16;
//     // if (empty($headers['Token']) || $headers['Token'] != $token) {
//     //     // Error Message
//     //     return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
//     // }
//     // END

//     $property_id       =  intval($_POST['property_id']);
//     $extra_pay_service =  sanitize_text_field( filter_input( INPUT_POST, 'extra_pay_input') );
//     $property_id       =  8230;
//     // $property_id       =  8197;
//     // $property_id       =  8208;
//     // $property_id       =  8200;
//     $extra_pay_input = '3,2,1,0';

//     if ( !isset($property_id) ) {
//         return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Only numeric value allowed!', 'wpestate')), 400);
//     }
//     $extra_pay_input = explode(',', $extra_pay_input);
//     sort($extra_pay_input);

//     $extra_pay_options = get_post_meta($property_id, 'extra_pay_options', true);
//     // print_r($extra_pay_options);

//     for ($i=0; $i < count($extra_pay_input); $i++) { 
        
//         for ($j=0; $j < count($extra_pay_options); $j++) { 
            
//                 // echo "->".$extra_pay_options[ $j ][2];
//                 // print_r($extra_pay_options);
//             if ( $extra_pay_input[ $i ] == $extra_pay_options[ $j ][2] ) {
//                 $extra_option_value = wpestate_calculate_extra_options_value( $count_days, $total_guests, $extra_pay_options[ $j ][2], $extra_pay_options[ $j ][1] );
//                 // echo "---------";
//                 $total_price = $total_price + $extra_option_value;
//                 // echo "--".$total_price;
//                 print_r($extra_option_value);
//             }
//         }
//     }

// }