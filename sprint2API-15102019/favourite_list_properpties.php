<?php


/**
 * Favorite List Properties API
 */

add_action('rest_api_init', function() {
    register_rest_route('tvcapi', '/v2/favorite-list-properties',
        array(
            'methods'  => 'GET',
            'callback' => 'favorite_list_properties',
        )
    );
});

function favorite_list_properties() {

    global $wpdb;

    // Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['Token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);

    if (empty($headers['Token']) || $headers['Token'] != $token) {
        // Error Message
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END
    $user_option    = 'favorites'.$token_id[1];
    // $user_option    = 'favorites'.$user_id;
    $properties_id  = get_option($user_option);
    $properties_id  = implode(',', $properties_id);
    
    $query = 'SELECT ID FROM `wp_posts` WHERE post_type = "estate_property" AND post_status = "publish" AND ID  IN ('.$properties_id.') ';
    
    $p_data = array();
    $properties_id  = $wpdb->get_results($wpdb->prepare($query,$properties_id),ARRAY_A);
    
    // Favourite List properties
   
    $all_ID = $properties_id;
    // Pagination
    // $current_page   = sanitize_text_field( filter_input( INPUT_POST, 'current_page' ) );

    $current_page = $_GET['current_page'];
    $per_page     = $_GET['per_page'];

    $current_page = ( $current_page == '' || $current_page == 0 ) ? 1 : (int)$current_page;

    $per_page     = ( $per_page == '' || $per_page == 0 ) ? count($all_ID) : (int)$per_page;

    $total_pages  = ceil(count($all_ID) / $per_page);
    $p_id         = array_chunk($all_ID, $per_page);
    $properties   = $p_id [$current_page - 1];
    // END pagination

   
    // Start properties by above given properties ID
    for ($k=0; $k < count($properties); $k++) { 



        //Get Property ID
        $p_data[ $k ]['ID']    =  $properties[ $k ]['ID'];
        
        // Property Image URL
        $post_thumb_id  = get_post_thumbnail_id( $properties[ $k ]['ID'] );
        $post_thumb_url = wp_get_attachment_image_url($post_thumb_id, 'full');
        if ($post_thumb_url == false) {
            # code...
            $p_data[ $k ]['image_url'] = "";
        } else {
            $p_data[ $k ]['image_url'] = $post_thumb_url;
        }

        // Is Favorite Porperty
        $user_option = 'favorites'.$token_id[1];
        //$user_option = 'favorites'.$user_id;
        $curent_fav  = get_option( $user_option );
        if($curent_fav){
            if ( in_array ($properties[ $k ]['ID'],$curent_fav) ){
                $p_data[ $k ]['is_favourite'] = true;
            } else {
                $p_data[ $k ]['is_favourite'] = false;
            }
        }

        //Get Property Title
        $p_data[ $k ]['title'] = get_the_title( $properties[ $k ]['ID'] );

        // Get Property City
        $prop_city = get_the_terms($properties[ $k ]['ID'], 'property_city');
        $this_cat = array();
        foreach ( $prop_city as $cat ) {
            $this_cat[] = $cat->name;
        }
        $p_data[ $k ]['city'] = implode( ",", $this_cat );

        // Get Property State
        $p_data[ $k ]['state'] = get_post_meta( $properties[ $k ]['ID'], 'property_state', true );

        // Get Property type
        $selected_prop_type = get_the_terms($properties[ $k ]['ID'], 'property_category');
        $p_data[ $k ]['property_type'] = $selected_prop_type[0]->name;

        // Get Property Zipcode
         $p_data[ $k ]['zipcode'] = get_post_meta( $properties[ $k ]['ID'], 'property_zip', true);

        // Get Cancellation Policy
        $cancellation = get_post_meta($properties[ $k ]['ID'],  'cancellation', true);
        // print_r($cancellation);
        if ($cancellation == 'NonRefundable') {
            $p_data[ $k ][ 'cancellation_policy' ] = esc_html__('Non Refundable', 'wpestate');
        } elseif ($cancellation == 'Free Refund') {
            $p_data[ $k ][ 'cancellation_policy' ] = esc_html__('Free Cancellation', 'wpestate');
        } else if ($cancellation == '24 Hours') {
            $allow = date('M d Y', strtotime("-1 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '.' Until '.date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '48 Hours') {
            $allow = date('M d Y', strtotime("-2 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '72 Hours') {
            $allow = date('M d Y', strtotime("-3 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '1 Week') {
            $allow = date('M d Y', strtotime("-7 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '2 Weeks') {
            $allow = date('M d Y', strtotime("-14 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '1 Month') {
            $allow = date('M d Y', strtotime("-1 month", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '') {
            $p_data[ $k ][ 'cancellation_policy' ] = '';
        }

        /* 
         * Get Porperty Price & Basic promo rate
         * Day & Date wise price show function
         */
        $price_per_day = floatval( get_post_meta($properties[ $k ]['ID'], 'property_price', true) );
        $property_basic_promation = floatval( get_post_meta($properties[ $k ]['ID'], 'property_basic_promation', true) );

        $price_array   = wpml_custom_price_adjust($properties[ $k ]['ID']);
        $price_per_day              =   '' != get_post_meta($properties[ $k ]['ID'], 'basic_price_ner', true ) ? floatval( get_post_meta($properties[ $k ]['ID'], 'basic_price_ner', true ) ) : floatval(get_post_meta($properties[ $k ]['ID'], 'property_price', true));

        $price_per_day = isset( $price_array[ $check_in ] ) ? $price_array[ $check_in ] : $price_per_day;

        $property_basic_promation = '' != get_post_meta($properties[ $k ]['ID'], 'promorate_ner', true ) ? floatval(get_post_meta($properties[ $k ]['ID'], 'promorate_ner', true )) : floatval (get_post_meta($properties[ $k ]['ID'], 'property_basic_promation', true) );

        $mega_details_array       = wpml_mega_details_adjust( $properties[ $k ]['ID'] );
        $property_basic_promation = isset( $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate'] ) && $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate']!= '' ? $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate'] : $property_basic_promation;

        $where_currency = esc_html( get_option('wp_estate_where_currency_symbol', '') );
        $currency           =   esc_html( get_option('wp_estate_currency_label_main', '') );
        
        $p_data[ $k ]['property_basic_price'] = $currency.$price_per_day;
        // if (!empty($property_price)) {
        // }
        // $promo_rate = floatval (get_post_meta($properties[ $k ], 'property_basic_promation', true) );
        $p_data[ $k ]['basic_promorate'] = $currency.$property_basic_promation;
        // if (!empty($property_basic_promation)) {
        // }
        
        // Get Extra Options Fees
        $extra_pay_options = ( get_post_meta($properties[ $k ]['ID'], 'extra_pay_options', true) );
        if ( empty( $extra_pay_options ) ) {
            $p_data[ $k ]['extra_services'] = array();
        }
        if (is_array($extra_pay_options) && !empty($extra_pay_options)) {
            $free_service = array();
            foreach ($extra_pay_options as $extra_services) {
                if ($extra_services[1] == 0) {
                    $free_service[] = esc_html__('Free ', 'wpestate').$extra_services[0];
                    // print_r($free_service[])
                    $p_data[ $k ]['extra_services'] = $free_service;
                } else {
                    $p_data[ $k ]['extra_services'] = array();
                }
            }
        }

        // Get Review Ratings
        $args = array(
            'post_id' => $properties[ $k ]['ID']
        );
        $comments = get_comments($args);
        // print_r($comments);
        if (empty($comments) || '' == $comments) {
            $p_data[ $k ]['ratings'] = esc_html__('0/5');
            $p_data[ $k ]['rating_status'] = "";
        } else {
            $total = $ota_rev = $ota_stars = 0;
            foreach ($comments as $comment) {
                $rating = get_comment_meta( $comment->comment_ID , 'review_stars', true );

                // OTA STARS FIELD
                $ota_stars += get_comment_meta( $comment->comment_ID , 'ota_stars', true );

                $tmp = json_decode( $rating, TRUE );
                $total += intval( $tmp['rating'] );
                $ota_rev++;
            }
            
            $allrevs = $ota_stars + $total;
            if($allrevs){
                $all_avg = $allrevs / $ota_rev;
                $all_avg = number_format( $all_avg, 1, '.', '' );
                
                if ($all_avg > 0) {

                    if( $all_avg > 4.7 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Excellent!');
                    } elseif( $all_avg > 4.4 && $all_avg < 4.8 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Fantastic!');
                    } elseif( $all_avg > 3.9 && $all_avg < 4.5 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Wonderfull!');
                    } elseif( $all_avg > 3.4 && $all_avg < 4 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Great');
                    } elseif( $all_avg > 2.9 && $all_avg < 3.5 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Good');
                    } elseif( $all_avg >= 1 && $all_avg < 3 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Fair');
                    }
                }
            }
        }

    }
   // echo "<pre>";
   //  print_r($p_data);
    if (!empty($p_data)) {
        return new WP_REST_Response (
            array(
                "response_code" => '200',
                'per_page'      => $per_page,
                'current_page'  => $current_page,
                'total_pages'   => $total_pages,
                /*'sort_by'       => $sort_by,*/
                'data'          => array_values($p_data),
            ),
        200);
    }else{

     return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('favourite property are not exist', 'wpestate')), 401);
    }
}