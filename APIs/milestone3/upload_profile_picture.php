<?php
/**
 * NOTIFICATION DATA API 
 */

add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/upload-profile-picture',
        array(
            'methods' => 'POST',
            'callback'=> 'upload_profile_picture'
        )
    );
});

function upload_profile_picture() { 

	// Check Oauth Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);

    if ( empty( $headers['token'] ) || $headers['token'] != $token ) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END


    $file = array(
        'name'      => $_FILES['upload_profile_pic']['name'],
        'type'      => $_FILES['upload_profile_pic']['type'],
        'tmp_name'  => $_FILES['upload_profile_pic']['tmp_name'],
        'error'     => $_FILES['upload_profile_pic']['error'],
        'size'      => $_FILES['upload_profile_pic']['size']
    );
    
    $extension = pathinfo($_FILES["upload_profile_pic"]["name"], PATHINFO_EXTENSION);
	if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'gif') {

		if ( $_FILES['upload_profile_pic']['tmp_name'] !== false ) {

	    	require_once(ABSPATH . '/wp-load.php');             
		    require_once(ABSPATH . 'wp-admin' . '/includes/file.php');
		    require_once(ABSPATH . 'wp-admin' . '/includes/image.php');
		    $uploaded_file = wp_handle_upload($file, array('test_form' => false));
		    
		    if (isset($uploaded_file['file'])) {
		        $file_loc   =   $uploaded_file['file'];
		        $file_name  =   basename($file['name']);
		        $file_type  =   wp_check_filetype($file_name);
				$user_id    = $token_id[1];

		        $attachment = array(
		        	'post_author'    => $user_id,
		            'post_mime_type' => $file['type'],
		            'post_title'     => $file['name'],
		            'post_content'   => '',
		            'post_status'    => 'inherit'
		        );

		        $attach_id      =   wp_insert_attachment($attachment, $file_loc);
		        $attach_data    =   wp_generate_attachment_metadata($attach_id, $file_loc);
		        wp_update_attachment_metadata($attach_id, $attach_data);
		        $data = array('data' => $attach_data, 'id' => $attach_id);
		        
		        //---------
		        $img_url = wp_get_attachment_image_src( $data['id'], 'wpestate_property_listings' ); 

			    update_user_meta( $user_id, 'custom_picture', $img_url[0] );
			    update_user_meta( $user_id, 'small_custom_picture', $data['id'] );
			    
			    return new WP_REST_Response(array('response_code' => '200',  'data' => esc_html__('Successfully upload file! '. $img_url[0], 'wpestate')), 200);
		    }

	    } else {
		    return new WP_REST_Response(
	            array(
	                'response_code' => '400',
	                'message'       => esc_html__( 'Something went wrong','wpestate')
	            ), 400);
		}
	} else {
	    return new WP_REST_Response(
            array(
                'response_code' => '400',
                'message'       => esc_html__( 'Sorry, only JPG, JPEG, PNG & GIF file are allowed.','wpestate')
            ), 400);
	}
    
}