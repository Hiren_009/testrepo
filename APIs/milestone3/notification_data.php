<?php
/**
 * NOTIFICATION DATA API 
 */

add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/notification-data',
        array(
            'methods' => 'POST',
            'callback'=> 'notification_data'
        )
    );
});

function notification_data() {

	// Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);

    if ( empty( $headers['token'] ) || $headers['token'] != $token ) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

    /* START PAGINATION */
    $current_page = sanitize_text_field( filter_input( INPUT_POST, 'current_page' ) );
    $current_page = ( $current_page == '' || $current_page == 0 ) ? 1 : (int)$current_page;
    $per_page     = sanitize_text_field( filter_input( INPUT_POST, 'per_page' ) );
    // $per_page     = 1;
    $per_page     = ( !isset( $per_page ) || $per_page == '' || $per_page == 0 ) ? 1 : $per_page;

    $offset       = ( $current_page - 1 ) * $per_page; 
    /* END PAGINATION */

    //$uid = 16;
    // $uid = 87;
    $uid = $token_id[1];
    
    $option_type = sanitize_text_field( filter_input( INPUT_POST, 'option_type' ) );
    // $option_type = 'new_reservation';
    // $option_type = 'cancelled';
    // $option_type = 'admin_messages';
    // $option_type = 'all';

    if ( isset($option_type) && $option_type == 'new_reservation' ) {
        $data = new_reservation( $uid, $per_page, $offset );
         /*print_r($data);
         die();*/
        $data = $data['data'];
        // $total_page = id_count( $data );
    } elseif ( isset($option_type) && $option_type == 'cancelled' ) {
        $data = cancelled_reservation( $uid, $per_page, $offset );
        $data = $data['data'];
    } elseif ( isset($option_type) && $option_type == 'admin_messages' ) {
        $data = get_admin_messges( $uid, $per_page, $offset );
        $data = $data['data'];
    } elseif ( isset($option_type) && $option_type == 'all' ) {

        $all_id = all_notify_data( $uid );
        foreach ($all_id as $value) {
            foreach ($value as $key) {
                $combine_id[] = $key;
            }
        }
        // echo "-----Below Post type -----"."\n";
        $ID_s       = array_chunk( $combine_id, $per_page );
        $all_ids    = $ID_s[ $current_page - 1 ];
        $total_page = ceil( count( $combine_id ) / $per_page );
        // print_r($all_ids);
        // echo "-- total_page -- ".$total_page."\n";
        for ($i=0; $i < count( $all_ids ); $i++) {

            $p_type = get_post_type( $all_ids[$i] );
            if ( $p_type == "wpestate_booking"  ) {

                $p_IDs[] =  $all_ids[$i];
                $b_data  = booking_info( $uid, array_values($p_IDs), $total_page/*, $all_*/ );
                // echo "-- ".$p_IDs."\n";
                // print_r($p_IDs);
            } elseif ( $p_type == "admin_messages" ) {

                $p_IDss[]   =  $all_ids[$i];
                $admin_data = admin_message_details( $uid, array_values($p_IDss), $total_page/*, $all_*/ );
            }
        }       
        // echo "-- B Data -- "."\n";
        // print_r($b_data['data']);
        // echo "-- Admin Data -- "."\n";
        // print_r($admin_data['data']);
        $all_data_ = array( $b_data['data'], $admin_data['data'] );
        // echo "-- All Data -- "."\n";
        // print_r($all_data_)."\n";
        $all_data_ = array_filter( $all_data_ );
        // $data_     = array_values( $all_data_ );
        $data      = array();
        foreach ($all_data_ as $alll ) {
            foreach ($alll as $key ) {
                $data[] = $key;
            }
        }
        // print_r($data);
    } elseif ( empty( $data ) ) {
        return new WP_REST_Response(array('response_code' => '404', 'message' => esc_html__('No data', 'wpestate')), 404);
    }
    $page_count = id_count( $data ); 
    if ( !empty( $page_count ) ) {
        $page_count = id_count( $data );
    } else {

        $page_count = "";
    }
    if ( !empty( $data ) ) {
        # code...
        return new WP_REST_Response( 
            array( 
                'response_code' => '200',
                'per_page'      => $per_page,
                'current_page'  => $current_page,
                'total_page'    => $page_count,
                // 'total_page'    => id_count( $data ),
                'data'          => $data 
            ),
        200 );
    } else {
        return new WP_REST_Response(array('response_code' => '404', 'message' => esc_html__('No data', 'wpestate')), 404);
    }

}


// function set_all_notify() {

// }