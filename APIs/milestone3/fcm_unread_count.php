<?php

/**
 * UNREAD COUNT PUSH NOTIFICATION API
 */
// up_push_new_message
add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/unread-notification-count',
        array(
            'methods' => 'GET',
            'callback'=> 'unread_notification_count'
        )
    );
});

function unread_notification_count() { 

	// Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);
    
    if (empty($headers['token']) || $headers['token'] != $token) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

    $unread_count = fcm_unread_count( $token_id[1] );
    if ( !empty( $unread_count ) ) {
    	$unread_counter = $unread_count;
    } else {
    	$unread_counter = 0;
    }
	return new WP_REST_Response( array( 'response_code' => '200', 'unread_count' => $unread_counter ), 200 );
}