<?php
function change_password(){
	$allowed_html   =   array();
	
	$user_id   = sanitize_text_field ( wp_kses( $_POST['user_id'] ,$allowed_html) );
    $oldpass   = sanitize_text_field ( wp_kses( $_POST['oldpass'] ,$allowed_html) );
    $newpass   = sanitize_text_field ( wp_kses( $_POST['newpass'] ,$allowed_html) );
    $renewpass = sanitize_text_field ( wp_kses( $_POST['renewpass'] ,$allowed_html) );

    if($newpass == '' || $renewpass == '') {
        return new WP_REST_Response(
            array(
                'response_code'  => '400',
                'message' => esc_html__('The new password is blank','wpestate')
            ), 400);
    }
    if($newpass != $renewpass) {
        return new WP_REST_Response(
            array(
                'response_code'  => '400',
                'message' => esc_html__('Passwords do not match','wpestate')
            ), 400);
    }

    $user = get_user_by( 'id', $user_id );
    if ( $user && wp_check_password( $oldpass, $user->data->user_pass, $user_id) ){
        wp_set_password( $newpass, $user_id );
        return new WP_REST_Response(
            array(
                'response_code'  => '200',
                'message' => esc_html__('Password Updated - You will need to logout and login again ','wpestate')
            )
        );
    }else {
        return new WP_REST_Response(
            array(
                'response_code'  => '400',
                'message' => esc_html__('Old Password is not correct','wpestate')
            ), 400);
    }
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'tvcapi', '/v2/change_password',
        array (
            'methods'  => 'POST',
            'callback' => 'change_password',
        )
    );
});
