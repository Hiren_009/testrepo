<?php
function my_porfile(){
    
	// Check Oath Token
	$headers = apache_request_headers();
	$token_id =  explode( "-qe_aw-", $headers['token'] );
	$token = get_user_meta($token_id[1], 'oauth_token', true);

	if (empty($headers['token']) || $headers['token'] != $token) {
	  // Error Message
	  return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
	}

	//$user 	    = $_POST['user_id'];
	$first_name = sanitize_text_field( $_POST['first_name'] );
	$last_name  = sanitize_text_field( $_POST['last_name'] );
	$phone_num  = sanitize_text_field( $_POST['phone_num'] );
	$moblie_num = sanitize_text_field( $_POST['moblie_num']);

	$email      = sanitize_text_field( $_POST['email'] );
	$about_me   = sanitize_text_field( $_POST['description'] );
	$i_live_in  = sanitize_text_field( $_POST['live_in'] );
	$i_speak    = sanitize_text_field( $_POST['i_speak'] );
	// $picture    = sanitize_text_field( $_POST['profile_photo'] );
    $picture    = get_user_meta( $token_id[1], 'custom_picture', true );
   /* $fbverified       = sanitize_text_field( $_POST['fb_verified'] );
    $googleverified   = sanitize_text_field( $_POST['google_verified'] );
    $emailverified    = sanitize_text_field( $_POST['email_verified'] );*/
   /* $user_id_verified = get_user_meta($id,'user_id_verified',true);
    $ownership_verified = get_user_meta($id,'idproof_verified',true);*/
	

    $data = array();
    $usertype = get_user_meta($token_id[1],'user_type',true);
    
    if ( isset( $first_name )){
        if(preg_match("/^([a-zA-Z' ]+)$/",$first_name)  /*|| $_POST['first_name'] == ''*/){
            update_user_meta( $token_id[1], 'first_name', $first_name );
        }else{
           return new WP_REST_Response(array('register'=>false,'message'=>esc_html__( "digit not allowed in name",'wpestate')), 422); 
        }
   
    }else{

        update_user_meta($token_id[1],'first_name','');
    }
    if ( isset( $last_name )){
        if(preg_match("/^([a-zA-Z' ]+)$/",$last_name) /*|| $_POST['last_name'] == ''*/){
         update_user_meta($token_id[1], 'last_name', $last_name );
        }else{
           return new WP_REST_Response(array('register'=>false,'message'=>esc_html__( "digit not allowed in name",'wpestate')), 422); 
        }
    }else{

        update_user_meta($token_id[1],'last_name','');
    }
    if(isset($phone_num)){
       if(filter_var($phone_num, FILTER_SANITIZE_NUMBER_INT) /*|| $_POST['phone_num'] == '' */){
            update_user_meta($token_id[1],'phone',$phone_num);
        }else{
            return new WP_REST_Response(array('register'=>false,'message'=>esc_html__( "The phone number does not look right!",'wpestate')), 422);
        }
    }else{

        update_user_meta($token_id[1],'phone','');
    }
    if(isset($moblie_num)){

        if(filter_var($moblie_num, FILTER_SANITIZE_NUMBER_INT) /*|| $_POST['moblie_num'] == ''*/ ){
            update_user_meta($token_id[1],'mobile',$moblie_num);
        }else{
            return new WP_REST_Response(array('register'=>false,'message'=>esc_html__( "The moblie number does not look right!",'wpestate')), 422);
        }
    }else{

        update_user_meta($token_id[1],'mobile','');
    }
    if(isset($about_me)){
        update_user_meta($token_id[1],'description',$about_me);
    }else{

        update_user_meta($token_id[1],'description','');
    }
    if(isset($i_live_in)){
        update_user_meta($token_id[1],'live_in',$i_live_in);
    }else{

        update_user_meta($token_id[1],'live_in','');
    }
    if(isset($i_speak)){
      
        update_user_meta($token_id[1],'i_speak',$i_speak);
    
    }else{

        update_user_meta($token_id[1],'i_speak','');
    }
    if(!empty($email)){
       
        $domain = substr(strrchr($email, "@"), 1);
        if(filter_var($email,FILTER_VALIDATE_EMAIL) === false) {
            
            return new WP_REST_Response(array('register'=>false,'message'=>esc_html__( "The email does not look right!",'wpestate')), 422);
        }elseif( !checkdnsrr ($domain) ){
           
            return new WP_REST_Response(array('register'=>false,'message'=>esc_html__( 'The email domain doesn not look right!','wpestate')), 422);
        }else{
            
                update_user_meta($token_id[1],'user_email',$email);
            }
    }
    // if(!empty($picture)){
    // 	update_user_meta($token_id[1],'custom_picture',$picture);
    // }else{
    //     update_user_meta($token_id[1],'custom_picture','');

    // }

    /*if($usertype == 0)
    {
        if($fbverified == 1){
           $fbv = update_user_meta($user, 'fb_verified','yes');
        }
        else{
            $fbv = update_user_meta($user, 'fb_verified','no');
        }
        if($googleverified == 1){
            $gv  = update_user_meta($user, 'google_verified','yes');
        }
        else{
            $gv  = update_user_meta($user, 'google_verified','no');
        }
        update_user_meta($user, 'email_verified',$emailverified);

    }*/
    $f_n   = get_user_meta($token_id[1],'first_name',true);
    $l_n   = get_user_meta($token_id[1],'last_name',true);
    $p     = get_user_meta($token_id[1],'phone',true);
    $m     = get_user_meta($token_id[1],'mobile',true);
    $a_m   = get_user_meta($token_id[1],'description',true);
    $live  = get_user_meta($token_id[1],'live_in',true);
    $speak = get_user_meta($token_id[1],'i_speak',true);
    /*$g_email = get_user_meta($user,'user_email',true);
    $p_photo = get_user_meta($user,'custom_picture',true);
    $fb_verified = get_user_meta($user,'fb_verified',true);
    
    if($fb_verified == 'yes'){
        $f = 1;
    }else{
        $f = 0;
    }
   
    $google_verified = get_user_meta($user,'google_verified',true);
    
    if($google_verified == 'yes'){
        $g = 1;
    }else{
        $g = 0;
    }
    
    $email_verified = get_user_meta($user,'email_verified',true);*/
   // $user    = get_userdata( $token_id[1] );

    $data['user_id']         = $token_id[1];
    $data['first_name']      = $f_n;
    $data['last_name']       = $l_n;
    $data['email']           = $email;
    $data['phone']           = $p;
    $data['moblie']          = $m;
    $data['about_me']        = $a_m;
    $data['live_in']         = $live;
    $data['i_speak']         = $speak;
    $data['profile_photo']   = get_user_meta( $token_id[1], 'custom_picture', true );
    $data['user_type']       = $usertype;
    /*if($usertype == 0){
        $data['fb_verified']     = $f;
        $data['google_verified'] = $g;
        $data['email_verified']  = $email_verified;
    }*/

	
        
    return new WP_REST_Response(
      array(
          'response_code'  => "200", 
          'data' => $data,
      ),200);
   
    

}
add_action( 'rest_api_init', function () {
    register_rest_route( 'tvcapi', '/v2/my_porfile',
        array (
            'methods'  => 'POST',
            'callback' => 'my_porfile',
        )
    );
});