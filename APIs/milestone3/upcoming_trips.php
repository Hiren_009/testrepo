<?php

/**
 * List Of Upcoming Trips API
 */

add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/upcoming-trips',
        array(
            'methods' => 'POST',
            'callback'=> 'upcoming_trips'
        )
    );
});

function upcoming_trips() {

    // Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);

    if ( empty( $headers['token'] ) || $headers['token'] != $token ) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END
    $currency = esc_html( get_option('wp_estate_currency_label_main', '') );


    global $wpdb;

    // START PAGINATION
    $current_page = sanitize_text_field( filter_input( INPUT_POST, 'current_page' ) );
    $current_page = ( $current_page == '' || $current_page == 0 ) ? 1 : (int)$current_page;
    
    $per_page = sanitize_text_field( filter_input( INPUT_POST, 'per_page' ) );
    $per_page = ( !isset( $per_page ) || $per_page == '' || $per_page == 0 ) ? 1 : $per_page;

    $offset = ( $current_page - 1 ) * $per_page; 

    // END PAGINATION

    global $wpdb;
    $user_type = get_user_meta( $token_id[1], 'user_type', true );
    if ( $user_type == "1" ) {
        
        /* SHOW GUEST UPCAOMING RESERVATION LIST */
        /* BOOKING ID's SELECT ONLY OLD DATE WISE */ 
        $bookID_date_filter = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID,wp_postmeta.meta_key,wp_postmeta.meta_value FROM wp_posts INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1 = 1 AND wp_posts.post_author = $token_id[1] AND wp_posts.post_type = 'wpestate_booking' AND `wp_posts`.`post_status` = 'publish' AND (wp_postmeta.meta_key = 'booking_to_date' AND wp_postmeta.meta_value > DATE_FORMAT(NOW(),'%Y-%m-%d')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC ", ARRAY_A);

        $bookID_date_filter = array_column($bookID_date_filter, 'ID');
        $bookID_date_filter = implode(',', $bookID_date_filter);
        
        if ( !empty( $bookID_date_filter ) ) {
            $bookID_date_filter = $bookID_date_filter;
        } else {
            $bookID_date_filter = "null";
        }

        /* BOOKING ID's SELECT BOOKING STATUS CONFIRMED OR CANCELED */ 
        $booked_ID = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS `wp_postmeta`.`post_id` FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($bookID_date_filter) AND meta_key = 'booking_status' AND (meta_value= 'confirmed' OR meta_value ='canceled') GROUP BY wp_postmeta.post_id DESC", ARRAY_A);
    // var_dump($booked_ID);
        $booked_ID   = array_column($booked_ID, 'post_id');
        $booked_ID   = search_by_trips( $booked_ID );
        $total_pages = ceil( count( $booked_ID ) / $per_page );

        /* BOOKING ID's SELECT TO SET PAGINATION RECORDS */ 
        $booking_ids = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS post_id FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($bookID_date_filter) AND meta_key = 'booking_status' AND (meta_value= 'confirmed' OR meta_value ='canceled') GROUP BY wp_postmeta.post_id DESC LIMIT ".$offset.", ".$per_page." ", ARRAY_A);

        $booked_ids = array_column($booking_ids, 'post_id');
        // $booked_ids = array_values( booked_prev_ids( $booked_ids ) );
        $booked_ids = search_by_trips( $booked_ids );

    } elseif ( $user_type == 0 ) {
        
        /* SHOW RESERVATION BY WHO TO RESERVERVE HOST PROPERTIES */
        /* BOOKING ID's SELECT ONLY OLD DATE WISE */ 
        $bookID_date_filter = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID,wp_postmeta.meta_key,wp_postmeta.meta_value FROM wp_posts INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1 = 1 AND /*wp_posts.post_author = $token_id[1] AND*/ wp_posts.post_type = 'wpestate_booking' AND `wp_posts`.`post_status` = 'publish' AND (wp_postmeta.meta_key = 'booking_to_date' AND wp_postmeta.meta_value > DATE_FORMAT(NOW(),'%Y-%m-%d')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC ", ARRAY_A);
        $bookID_date_filter = array_column($bookID_date_filter, 'ID');
        $bookID_date_filter = implode(',', $bookID_date_filter);
        
        if ( !empty( $bookID_date_filter ) ) {
            $bookID_date_filter = $bookID_date_filter;
        } else {
            $bookID_date_filter = "null";
        }
        
        /* BOOKING ID's SELECT BOOKING STATUS CONFIRMED OR CANCELED */
         
        $booked_ID_ = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS `wp_postmeta`.`post_id` FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($bookID_date_filter) AND meta_key = 'booking_status' AND (meta_value= 'confirmed' OR meta_value ='canceled') GROUP BY wp_postmeta.post_id DESC", ARRAY_A);
        $booked_ID_ = array_column($booked_ID_, 'post_id');
        $booked_ID_ = implode( ',', $booked_ID_);

        if ( !empty( $booked_ID_ ) ) {
            $booked_ID_ = $booked_ID_;
        } else {
            $booked_ID_ = "null";
        }

        $booked_ID = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS `wp_postmeta`.`post_id` FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($booked_ID_) AND ( meta_key = 'owner_id' AND meta_value= $token_id[1] ) GROUP BY wp_postmeta.post_id DESC", ARRAY_A);

        $booked_ID   = array_column($booked_ID, 'post_id');
        $booked_ID   = search_by_trips( $booked_ID );
        $total_pages = ceil( count( $booked_ID ) / $per_page );

        $b_id = implode( ',', $booked_ID);
        
        /* BOOKING ID's SELECT TO SET PAGINATION RECORDS */ 
        $booking_ids = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS post_id FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($b_id) AND ( meta_key = 'owner_id' AND meta_value = $token_id[1] ) GROUP BY wp_postmeta.post_id DESC LIMIT ".$offset.", ".$per_page." ", ARRAY_A);

        $booked_ids = array_column($booking_ids, 'post_id');
        // $booked_ids = array_values( booked_prev_ids( $booked_ids ) );
        $booked_ids = search_by_trips( $booked_ids );

    }

    

    $data = array();
    if ( !empty( $booked_ids ) ) {
        for ($i=0; $i < count( $booked_ids ); $i++) { 
            
            $prop_id  = get_post_meta( $booked_ids[$i], 'booking_id', true );
            // $hosts_ID = wpsestate_get_author($prop_id);
            // if ( $token_id[1] != $hosts_ID ) {
            /*
             * Check about stay
             * Conversion ID 
             */
            $conv_id = $wpdb->get_var( "SELECT `id` FROM `wp_yobro_conversation` WHERE booking_id = ".$booked_ids[$i] );
            // echo 'Conv ID '.$cov_id;
            if ( !empty( $conv_id ) ) {
                $data[ $i ]['conv_id'] = $conv_id;
            } else {
                $data[ $i ]['conv_id'] = "";
            }

            $boo_from = strtotime( get_post_meta( $booked_ids[$i], 'booking_from_date', true ) );
            $boo_to   = strtotime( get_post_meta( $booked_ids[$i], 'booking_to_date', true ) );

            
            $data[ $i ]['reservation_date'] = strtotime(get_the_time('Y-m-d', $booked_ids[$i]));
            
            $is_cancel = get_post_meta( $booked_ids[ $i ], 'booking_status', true);
            if ( $is_cancel == 'canceled') {
                # code...
                $data[ $i ]['booking_status'] = esc_html__('Cancelled', 'wpestate');

            } /*elseif ( strtotime(date("Y-m-d")) >= $boo_to ) {
                // $data[ $i ]['is_stay'] = esc_html__('In Stay', 'wpestate');
                $data[ $i ]['booking_status'] = esc_html__('In Stay', 'wpestate');
            }*/ elseif ( $boo_from > strtotime(date("Y-m-d")) ) {
                $data[ $i ]['booking_status'] = esc_html__('Upcoming Stay', 'wpestate');
                // $data[ $i ]['is_cancel'] = esc_html__('Cancel', 'wpestate');
            } else {
                $data[ $i ]['booking_status'] = esc_html__('In Stay', 'wpestate');
            }
            

            $prop_id = get_post_meta( $booked_ids[$i], 'booking_id', true );

            $data[ $i ]['property_name'] = get_the_title( $prop_id );
            $data[ $i ]['booking_id']    = $booked_ids[$i];
            $data[ $i ]['book_from_date']= $boo_from;  
            $data[ $i ]['book_to_date']  = $boo_to; 

            // CONTACT INFO
            $data[ $i ]['contact_name']   = get_post_meta( $booked_ids[$i], 'chekout_contactname', true );
            $data[ $i ]['traveller_name'] = get_post_meta( $booked_ids[$i], 'travellername', true );

            // GUESTS INFO
            $booking_guests = get_post_meta( $booked_ids[ $i ], 'booking_guests', true);
            $adult_guests   = get_post_meta( $booked_ids[ $i ], 'adult_type_val', true);
            $child_guests   = get_post_meta( $booked_ids[ $i ], 'child_type_val', true);
            if ( empty( $child_guests ) ) {
                
                if ( $booking_guests > 1 ) {
                    $data[ $i ]['guest'] = $booking_guests.esc_html__(' Adults ', 'wpestate'); 
                } else {
                    $data[ $i ]['guest'] = $booking_guests.esc_html__(' Adult ', 'wpestate'); 
                }
            } else {
                if ( $adult_guests  > 1 && $child_guests == 1) {
                    # code...
                    $data[ $i ]['guest'] = $adult_guests.esc_html__(' Adults ', 'wpestate').$child_guests.esc_html__(' Kid','wpestate'); 
                } elseif ( $adult_guests == 1 && $child_guests > 1 ) {
                    $data[ $i ]['guest'] = $adult_guests.esc_html__(' Adult ', 'wpestate').$child_guests.esc_html__(' Kids','wpestate'); 
                } elseif ($adult_guests == 1 && $child_guests == 1) {
                    $data[ $i ]['guest'] = $adult_guests.esc_html__(' Adult ', 'wpestate').$child_guests.esc_html__(' Kid','wpestate'); 
                }else{
                    $data[ $i ]['guest'] = $adult_guests.esc_html__(' Adults ', 'wpestate').$child_guests.esc_html__(' Kids','wpestate');
                }
                
            }
            /* TOTAL NO OF GUESTS */
            
            // No of Night
            $timeDiff             = abs( $boo_from - $boo_to );
            $count_days           = $timeDiff/86400;  // 86400 seconds in one day
            $data[ $i ]['nights'] = "$count_days";

            $data[ $i ]['total']  = $currency.number_format( get_post_meta( $booked_ids[ $i ], 'total_price', true) );

            // CHECKOUT TIME
            $propid = get_post_meta( $booked_ids[ $i ], 'booking_id', true );
            $check_out_hour     = get_post_meta($propid,'check-out-hour',true);
            $check_out_hour_to  = get_post_meta($propid,'check-out-hour-to',true);
            $hours_check_out_24 = get_post_meta($propid,'hours_check_out_24',true);
            if ( empty( $hours_check_out_24 ) ) {
                $data[ $i ]['check_out_between'] = $check_out_hour . ' - ' . $check_out_hour_to;
            } else {
                $data[ $i ]['check_out_between'] = $hours_check_out_24;
            }

            $data[$i]['imageUrl'] = wp_get_attachment_url(get_post_thumbnail_id($propid));
            // }
        }
    } else {
        return new WP_REST_Response( array( 'response_code' => '404', 'message' => esc_html__( 'No Reservation', 'wpestate' ) ), 404 );
    }
    // print_r($data);
    
    return new WP_REST_Response( 
            array( 
                'response_code' => '200',
                'per_page'      => $per_page,
                'current_page'  => "$current_page",
                'total_pages'   => "$total_pages",
                'data'          => $data 
            ),
        200 );
}