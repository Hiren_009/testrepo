<?php

/**
 * Booking Details
 */

add_action( 'rest_api_init', function () {
    register_rest_route( 'tvcapi', '/v2/guest_booking_details',
        array (
            'methods'  => 'GET',
            'callback' => 'booking_details_for_guest_side',
        )
    );
});
 
function booking_details_for_guest_side() {

	 // Check Oath Token
	$headers  = apache_request_headers();
	$token_id =  explode( "-qe_aw-", $headers['token'] );
	$token    = get_user_meta($token_id[1], 'oauth_token', true);

	if (empty($headers['token']) || $headers['token'] != $token) {
	  return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
	}
	
	$bookid = $_GET['booking_id'];
	$book_id_check = get_post($bookid);

	$data = array();
	if(isset($book_id_check)){
		$booking_from_date  =   esc_html(get_post_meta($bookid, 'booking_from_date', true));
		$booking_from_date  =   strtotime(date('M d, Y', strtotime($booking_from_date)));
		
		$booking_to_date    =   esc_html(get_post_meta($bookid, 'booking_to_date', true)); 
		$booking_to_date    =   strtotime(date('M d, Y', strtotime($booking_to_date)));

		// total and currency
		$booking_total  =  esc_html(get_post_meta($bookid, 'total_price', true));
		$property_id = esc_html(get_post_meta($bookid, 'booking_id', true));
		
		$currency2 = esc_html( get_option('wp_estate_currency_symbol', '') );
		$currency       = esc_html( get_option('wp_estate_currency_label_main', '') ); //currency_symbol
	    $where_currency = esc_html( get_option('wp_estate_where_currency_symbol', '') );
		$coupon_type="";
	        if(get_post_meta($bookid,'coupon_code',true) != ""){
	    
	            global $wpdb;
	            $coupon_val=get_post_meta($bookid,'coupon_code',true);
	            //$total = get_post_meta($bookid,'booking_id',true);
	            $pro_details=get_post($property_id);
	            $result=$wpdb->get_row('select * from '.$wpdb->prefix.'coupon_record where prop_id='.$property_id.' and coupon_code="'.$coupon_val.'"');
	           
	            if(count($result)==1){
	                if(user_can($result->user_id,'administrator'))
	                       $coupon_type = "( Admin ) ";
	                    else
	                       $coupon_type = "( Host ) ";
					if(($pro_details->post_author==$result->user_id)  || check_admin() || ($_POST['page_from']=="reservation")){                    
	                    $priceafterdiscount=$booking_total-(get_post_meta($bookid,'discount',true));
	            	}
	            }
	        }

	        if((!empty($posts_array) && $posts_array[0]->post_status == "publish")){
	           $total_cu = $currency.'0';
	        }
	        else{  
	           $total_cu = $currency.(isset($priceafterdiscount) ? $priceafterdiscount : $booking_total); 
	        }
	    //end total and currency

	    //check in - check out time

		$check_in_time  = esc_html(get_post_meta($property_id, 'check-in-hour', true));
		$check_in_time_to  = esc_html(get_post_meta($property_id, 'check-in-hour-to', true));

		$hours_check_in_24  = esc_html(get_post_meta($property_id, 'hours_check_in_24', true));

		$hours_check_out_24  = esc_html(get_post_meta($property_id, 'hours_check_out_24', true));

		$check_out_time  = esc_html(get_post_meta($property_id, 'check-out-hour', true));
		$check_out_time_to  = esc_html(get_post_meta($property_id, 'check-out-hour-to', true));

		if(isset($hours_check_in_24) && !empty($hours_check_in_24)){
	        $checkinpolicies = '24 Hours';
	    }else{
	        $checkinpolicies = $check_in_time." to ".$check_in_time_to; 
	    }
	    
	    if(isset($hours_check_out_24) && !empty($hours_check_out_24)){
	        $checkoutpolicies = '24 Hours';
	    }else{
	        $checkoutpolicies = $check_out_time." to ".$check_out_time_to;  
	    }
	    //ends
	    
	    //rules_and_restrictions

	    $the_post= get_post( $bookid);
	    $book_author=$the_post->post_author;
	    
	    $firstname = get_user_meta( $book_author, 'first_name', true );
	    $lastname = get_user_meta( $book_author, 'last_name', true ); 

	    //ends

	    // contact details
	    $contactname = get_post_meta($bookid, 'chekout_contactname', true);
	    $traveller   = get_post_meta($bookid, 'travellername', true);
	    $contact_no  = get_post_meta($bookid, 'mobile', true);

	    //$current_user = wp_get_current_user();
	    //$userID       = $current_user->ID;
	    $userID = $token_id[1];
	    
	    $user_mobile  = get_the_author_meta('mobile' , $userID );

	    	if(isset($user_mobile)){

	    		$u_mobile = $user_mobile;
	    	
	    	}else{

	    		$u_mobile = '';
	    	}

	    $booking_guests = get_post_meta($bookid, 'booking_guests', true);

	    $reserved    = 'Reserved for '.$firstname.' '.$lastname;
	    $guest_only  =  'Guest only '.$booking_guests;
	    $check_start = 'Check-in time starts at '.$check_in_time;
	    //end
	   

	    //cancellation_and_changes

	    $propertytitle = get_the_title($property_id);
	    $max_allow_guest = esc_html(get_post_meta($property_id, 'max_allow_guest', true));

	    $pro_t = $propertytitle." place a maximum cap on the number of guest that is allowed to occupy the accommodations. The maximum number of guest is ".$max_allow_guest;
	    //$cancelltion_text = $propertytitle." charges the following cancellation/change fees.";

	    // Refundable Statements Cases:
	    $refundpolicy = esc_html(get_post_meta($property_id, 'cancellation', true));
	    if ($refundpolicy == 'NonRefundable') {
	    // 1. NonRefundable
	       
	        $refund_content = "The accommodation(s) booked will NOT automatically refund you in full/partially in accordance with the policy if a canellation is done prior to arrival. The reservation is not refundable. Should you change or cancel this reservation in part or whole for any reason, your payment will not be refunded. No refunds (in part of full) will be issued for late check-in or early check-out.";
	    }elseif ($refundpolicy == 'Free Refund') {
	    // 2. Free Refund
	       
	        $refund_content = "The accommodation(s) booked will automatically refund you in full/partially in accordance with the policy if a canellation is done anytime prior to arrival. Should you change or cancel this reservation in part or whole for any reason, your payment will not be refunded. No refunds (in part of full) will be issued for late check-in or early check-out.";
	    }elseif ($refundpolicy == '24 Hours' || $refundpolicy == '48 Hours' || $refundpolicy == '72 Hours' || $refundpolicy == '1 Week' || $refundpolicy == '2 Week' || $refundpolicy == '1 Month' ) {
	    // 3. Rest cases
	        
	        $refund_content = "The accommodation(s) booked will automatically refund you in full/partially in accordance with the policy if a canellation is done ".$refundpolicy." prior to arrival. Should you change or cancel this reservation in part or whole for any reason, your payment will not be refunded. No refunds (in part of full) will be issued for late check-in or early check-out.";
	    }else{
	    	$refund_content = '';
	    }
	    //end

	    //earn point
	   $p_author = get_post_field( 'post_author', $bookid );
	  if (get_user_meta($p_author, 'join_reward_club', true) == 1) {
		   $earn_for_this = get_post_meta($bookid, 'earned_reward_point', true);
		   $earn_for_this=round($earn_for_this);
		    if ($earn_for_this != '' && $earn_for_this != '0') {
		        $earn_for_this = number_format($earn_for_this);
		    }
		}
		//end
		$booking_array = array();
		$booking_array = get_post_meta( $bookid, 'booking_details_info', true );
	    
	    $number_of_guest_covered_under_night_rate = esc_html(get_post_meta($property_id, 'number_of_guest_covered_under_night_rate', true));  
	    $cost_per_additional_child = esc_html(get_post_meta($property_id, 'cost_per_additional_child', true));

	    $invoice_id 	=   get_post_meta($bookid,'booking_invoice_no',true);
	    $default_price  =   get_post_meta($invoice_id, 'default_price', true); 
		    if($booking_array['numberDays']>=7 && $booking_array['numberDays']< 30){
		            $default_price=$booking_array['week_price'];
	        }else if($booking_array['numberDays']>=30){
	            $default_price=$booking_array['month_price'];
	        }
	    $price_show  =   wpestate_show_price_booking_for_invoice($default_price,$currency,$where_currency,0,1);
	    if($booking_array['price_per_guest_from_one']==1){
	        $e_price =  $extra_price_per_guest.' x '.$booking_array['count_days'].' '.esc_html__( 'nights','wpestate').' x '.$booking_array['curent_guest_no'].' '.esc_html__( 'guests','wpestate');
	    } else{ 
	        
	        if($booking_array['has_custom']){
	            if( $booking_array['monthly_price'] == 1 ) {
	                $new_price_to_show = esc_html__( 'Special monthly rate','wpestate');
	            } elseif( $booking_array['weekly_price'] == 1 ) {
	                $new_price_to_show = esc_html__( 'Special weekly rate','wpestate');
	            } else {
	                $new_price_to_show = esc_html__( 'custom price','wpestate');
	            }
	        }else{
	            $new_price_to_show = $price_show;
	        }
	        
	        if($booking_array['numberDays'] == 1){
	            $e_price = $new_price_to_show.' x '.$booking_array['numberDays'].' '.esc_html__( 'Night','wpestate');
	        }else{
	            $e_price = $new_price_to_show.' x '.$booking_array['numberDays'].' '.esc_html__( 'Nights','wpestate');
	        }
	    }
	    //end
	    //additional
	    $extra_pay_options = get_post_meta($property_id, 'extra_pay_options', true);
	    $extrafeesopt = array(
	        0   =>  esc_html__('Single Fee','wpestate'),
	        1   =>  esc_html__('Per Night','wpestate'),
	        2   =>  esc_html__('Per Guest','wpestate'),
	        3   =>  esc_html__('Per Night per Guest','wpestate')
	    );
	    if(isset($extra_pay_options) && !empty($extra_pay_options)){
	        foreach($extra_pay_options as $option){
	            
	        $extra_opt = $option['0']." ".wpestate_show_price_booking($option['1'],$currency,$where_currency,1)." ".$extrafeesopt[$option['2']];
	            
	        }
	    }
	    //
	    $pro_title = get_the_title($property_id);
	    $prop_action_category_array = get_the_terms($property_id, 'property_action_category');
        // Room Type
        if(isset($prop_action_category_array[0])){
            $prop_action_category_selected = $prop_action_category_array[0]->name;
       		 $propaction_category_array = $prop_action_category_selected;
        }
	    // Property Type
	    $prop_category_array =   get_the_terms($property_id, 'property_category');
        if(isset($prop_category_array[0])){
             $prop_category_selected = $prop_category_array[0]->name;
       		 $propcategory_array = $prop_category_selected;
        }
	    $pro_add = $propaction_category_array.''.$propcategory_array.' in'.' '.strip_tags(get_the_term_list($property_id, 'property_city', '', ', ', '')).'/'.get_post_meta($property_id, 'property_country', true);
	    
	    //rating
	    $rating 		= get_post_meta($property_id, 'rating', true);
	    if(!empty($rating)){
	    	$rating_data = $rating;
	    }else{
	    	$rating_data = '';
	    }
		$comments_count =  wp_count_comments($property_id);
		$comments_count	= $comments_count->approved.' reviews';
		if (has_post_thumbnail($property_id)){
	        $propimg =get_the_post_thumbnail_url($property_id);
	    } else {
	        $propimg = $thumb_prop_default =  get_stylesheet_directory_uri().'/img/defaultimage_prop.jpg';
	    }
	    //$pro_date = date('M d, Y', strtotime($booking_from_date)).date('M d, Y', strtotime($booking_to_date));
	    
	    $inter_price_show   =   wpestate_show_price_booking_for_invoice($booking_array['inter_price'],$currency,$where_currency,0,1); 
	    
	    if($booking_array['cleaning_fee']!=0 && $booking_array['cleaning_fee']!= ''){
	       
	       $c_fee = wpestate_show_price_booking_for_invoice($booking_array['cleaning_fee'],$currency,$where_currency,0,1);
	  
	    }else{

	    	$c_fee = '';

	    }
	    // $this_user = get_user_meta( get_current_user_id(), 'user_type', true );
	    if( 'booking' == $_POST['page_from'] && isset( $booking_array['service_fee'] ) && !empty( $booking_array['service_fee'] ) ) {
	    
	    	$s_fee = wpestate_show_price_booking($booking_array['service_fee'],$currency,$where_currency,1);
	    }else{
			$s_fee = '';
		}

	   
	    if($booking_array['city_fee']!=0 && $booking_array['city_fee']!=''){
	       
	       $city_f = wpestate_show_price_booking($booking_array['city_fee'],$currency,$where_currency,1);
	    
	    }else{

	    	$city_f = '';

	    }

	    if( $booking_array['occupancy_tax'] != '' ) {
	       
	       $occupancytax = wpestate_show_price_booking($booking_array['occupancy_tax'],$currency,$where_currency,1);
	    
	    }else{

	    	$occupancytax = '';

	    }

	    //extra option
	    $extra_options = get_post_meta($bookid, 'extra_options', true);
         
        $extra_options_array    =   explode(',',$extra_options);
        for ($i=0; $i < count($extra_pay_options); $i++){
        
            if( isset($extra_pay_options[$i][0]) ){
                $extra_option_value                 =   wpestate_calculate_extra_options_value($booking_array['count_days'],$booking_guests,$extra_pay_options[$i][2],$extra_pay_options[$i][1]);
                $extra_option_value_show            =   wpestate_show_price_booking_for_invoice($extra_option_value,$currency,$where_currency,1,1);
                $extra_option_value_show_single     =   wpestate_show_price_booking_for_invoice($extra_pay_options[$i][1],$currency,$where_currency,0,1);

                $opt_key[$i]['key'] = $extra_pay_options[$i][0];
                $opt_key[$i]['value'] = strip_tags($extra_option_value_show.$extrafeesopt[$extra_pay_options[$i][2]]/*.$extra_option_value_show_single*/);
            }else{
            	$opt_key = array();
            }
        }
        if (empty( $extra_pay_options ) ) {
        	$opt_key = array();
        }
        //end
	    if( $booking_array['early_bird_discount'] > 0){
	        
	        // $e_b_discount =   wpestate_show_price_booking_for_invoice($booking_array['early_bird_discount'],$currency,$where_currency,1,1);
	        $e_b_discount =   $currency.number_format($booking_array['early_bird_discount']);
	    
	    }else{

	    	$e_b_discount = '';

	    }

	    $rf = get_post_meta($property_id, 'security_deposit', true);
	        if ($rf != 0) {
	            $refund_deposit = date('m-d-Y', $booking_to_date).' '.$currency.get_post_meta($property_id, 'security_deposit', true);
			} else {
			    $refund_deposit = "";
			}
	    
	    $booking_array['total_price'] = floatval( $booking_array['total_price'] ) + floatval( $booking_array['total_extra_price_per_guest'] );
	    $total_price_show = wpestate_show_price_booking($booking_array['total_price'],$currency,$where_currency,1);
	    $checkinpolicies = $check_start.'

Minimum check-in age is 18

Government ID required

Your room/unit will be guaranteed for late arrival. Some properties operate without a front desk and have check time limitations. Please contact the property 72 hours in advance.';
	
	$securitydeposit = $currency.get_post_meta($property_id, 'security_deposit', true);
	
	//upcoming/cancel status
	$boo_from = strtotime( get_post_meta( $bookid, 'booking_from_date', true ) );
    $boo_to   = strtotime( get_post_meta( $bookid, 'booking_to_date', true ) );

    $is_cancel = get_post_meta( $bookid, 'booking_status', true);
    if ( $is_cancel == 'canceled') {
        # code...
        $booking_status = esc_html__('Cancelled', 'wpestate');

    } elseif ( strtotime(date("Y-m-d")) <= $boo_to ) {
        // $data[ $i ]['is_stay'] = esc_html__('In Stay', 'wpestate');
        $booking_status = esc_html__('In Stay', 'wpestate');
    } elseif ( strtotime(date("Y-m-d")) < $boo_from ) {
        $booking_status = esc_html__('Upcoming Stay', 'wpestate');
        // $data[ $i ]['is_cancel'] = esc_html__('Cancel', 'wpestate');
    } else{

    	$booking_status = esc_html__('Previous stay', 'wpestate');
    }

    // GUESTS INFO
	$booking_guests = get_post_meta( $bookid, 'booking_guests', true);
	$adult_guests   = get_post_meta( $bookid, 'adult_type_val', true);
	$child_guests   = get_post_meta( $bookid, 'child_type_val', true);
    if ( empty( $child_guests ) ) {
        $adult_guest = $booking_guests.' '.'Adults'; 
        $child_guest = ''; 
    } else {
        $adult_guest = $adult_guests.' '.'Adults'; 
        $child_guest = $child_guests.' '.'Kids';
    }


    //membertype
    if (get_user_meta($p_author, 'join_reward_club', true) == 1) {
	    $user_total_point = get_user_meta($p_author, 'loyalty_reward_point', true);

	    if ($user_total_point > 0 && $user_total_point < 5000) {
	        $member_type = "Active Member";
	     
	    } elseif ($user_total_point > 4999 && $user_total_point < 10000) {  
	        $member_type = "Gold Member";
	    } else {
	        $member_type = "Platinum Member";
	    }
	}
    //end
    //saving price
    $bookingfrom_date = esc_html(get_post_meta($bookid, 'booking_from_date', true));
    $bookingto_date   = esc_html(get_post_meta($bookid, 'booking_to_date', true)); 
        
    $startTimeStamp = strtotime($bookingfrom_date);
	$endTimeStamp = strtotime($bookingto_date);
	$timeDiff = abs($endTimeStamp - $startTimeStamp);
	$countdays = $timeDiff/86400;
    $Prices_shown_after = wpestate_show_price_booking((floatval(get_post_meta($property_id, 'property_price', true)) - floatval(get_post_meta($property_id, 'property_basic_promation', true))) * $countdays,$currency,$where_currency,1);
    	if(!empty($booking_array['numberDays'])){
    		$night_days_data = $booking_array['numberDays'];
    	}else{
    		$night_days_data = '';
    	}
		$data = array(
			'booking_status'    => $booking_status,
			'booking_from_date' => $booking_from_date,
			'booking_to_date'   => $booking_to_date,
			'booking_id'        => $bookid,
			'total' 			=> $total_cu,
			'title' 		    => $pro_title,
			'property_type'     => $pro_add ,
			'rating' 		    => $rating_data,
			'comment' 		    => $comments_count,
			'image'			    => $propimg,
			'total_guest'	    => $booking_guests,
			'child_adult'		=> $adult_guest.$child_guest,
			'telephone_number'  => $contact_no,
			'nights'   			=> $night_days_data,
			'currency_symbol'   => $currency,
			'refundable_deposit' => $refund_deposit,
		  	'check_in_and_check_out' => array(
					array(
						'key'   => 'Check-in time',
						'value' => $checkinpolicies,
					),
					array(
						'key'   => 'Check-out time',
						'value' => $checkoutpolicies,
					),
					array(
						'key'   => 'Check-in policies',
						'value' =>  $checkinpolicies,
					),
								
				),

			'rules_and_restrictions' => array( array(
					//'guest_text' 	   => 'Guests',
					'reserved' 	 	   => $reserved,
					'guest_only'       => $guest_only,
					'member_type'      => $member_type,
					'reward_point' 	   => $earn_for_this,
					'telephone_number' => $contact_no,
					'property_content' => $pro_t,
					'cancellation_and_changes' => array(
						array(

							'key'   => 'cancellation_and_changes1',
							'value' => 'Plans often changes and in most cases at last minute. We understand and therefore we do not charge a cancel or change fee.',
						),
						array(

							'key'   => 'cancellation_and_changes2',
							'value' => 'When the property charges penalties and fees in accordance with their refund policies, the cost will be passed on to you.',
						),
						array(

							'key'   => 'cancellation_and_changes3',
							'value' => $propertytitle.'charges the following cancellation/change fees.',
						),
						array(

							'key'   => 'cancellation_and_changes4',
							'value' => $refund_content,
						),
						array(
							'key' => 'cancellation_and_changes5',
							'value' => 'Any extension of stay requires a new reservation. Base charges and fees may not be waived.',
						),
						
							
						
					),
					'accommodations_fees_and_surcharge' => 'The advertised price DOES NOT include any applicable hotel/accommodation service fees, charges for optional incidentals (such as minibar snacks or telephone calls), or regulatory surcharges. The hotel will assess these fees, charges, and surcharges upon check-in/check-out.',
					'payment' => 'Your credit card is charged for the full amount of the reservation excluding accommodation fees and surcharge at the time of booking. Availability of the accommodations and rates are not guaranteed until full payment is collected.',
							
				)),
			

			'price_summary' => array(array(
					'rate_conver_text' => 'The base rate covers ('.$booking_array['extra_guests'].' Number of guest covered under the night rate)'.$number_of_guest_covered_under_night_rate.'guests.',
					'adult_content' => 'Cost for additional adults is '.$booking_array['extra_price_per_guest'].'" USD per night',
					'child_content'=>'Cost for additional children is '.$cost_per_additional_child.' USD per night',
					'other_text'=>'This property considers guests of any age to be an adult.',
					'extra_guest_content' => 'Availability of accommodation in the same property for extra guests is not guaranteed.',
				'invoice_details'   => array(
									array(
										'key'   => $e_price,
										'value' => $inter_price_show,
									),
									array(
										'key'   => 'Cleaning fee',
										'value' => $c_fee,
									),
									array(
										'key'   => 'City fee',
										'value' => $city_f,
									),
									array(
										'key'   => 'Occupancy Tax',
										'value' => $occupancytax,
									),
									array(
										'key'   => 'Service Fee',
										'value' => $s_fee,
									),
									
									array(
										'key'   => 'Early Booking Discount',
										'value' => $e_b_discount,
									),
									/*array(
										'key'   => 'Total',
										'value' => $total_price_show,
									),*/
									array(
										'key'   => 'Reservation Fee Required',
										'value' => $total_price_show,
									),array(
										'key'   => 'Security Deposit',
										'value' => $securitydeposit,
									),				
								),
				'Total' => $total_price_show,
				'savings_price' => $Prices_shown_after,
				'room_confirmation' => 'Some accommodations will not accept guest details until 7 days prior to check in. In such a case, your hotel room is reserved, but your name is not yet on file with the accommodation.',
				)),
			'additional_hotal_fees' => array(array(
					'fees_and_deposits' => 'The below fees and deposits only apply if they are not included in your selected room rate.',
					'charge_property' => 'You will be asked to pay the following charges at the property:',
					'cash_deposit' => $currency2.' '.get_post_meta($property_id, 'security_deposit', true).' per stay',
					'property_other_data'=>'We have included all charges provided to us by the property. However, charges can vary, for example, based on length of stay or the room you book.',
					'call' => 'For special requests or questions about the property, please call the hotel directly at +1 (954) 591-1819',
				)),
			'extra_option' => $opt_key,
		);
		$user_type = get_post_meta($bookid,'owner_id',true);
		$user_type = get_user_meta($user_type,'user_type',true);
		// print_r($data);	
		if($user_type == 0){

			$u_type = 'Host';

		}elseif ($user_type == 1) {
			
			$u_type = 'Guest';
		
		}else{

			$u_type = 'you are not able to access';

		}
		return new WP_REST_Response(
		      array(
		          'response_code'  => "200", 
		          'data' => $data,
		          'user_type' => $u_type,
		      ),
	  		200);
	}else{
		return new WP_REST_Response(
            array(
                'response_code'  => '404',
                'message'=>esc_html__( 'booking not existing','wpestate')
            ), 404);
	}
}