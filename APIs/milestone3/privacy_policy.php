<?php
function privacy_policy()
{
	 // Check Oath Token
	  $headers = apache_request_headers();
	  $token_id =  explode( "-qe_aw-", $headers['token'] );
	  $token = get_user_meta($token_id[1], 'oauth_token', true);

	  if (empty($headers['token']) || $headers['token'] != $token) {
	      // Error Message
	      return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
	  }

	  $page = get_page(356);
	  $page_content = wp_filter_nohtml_kses(apply_filters('the_content', $page->post_content));
	  if($page){
	  	$data = array(
	  		'content' => $page_content
	  	);
	}
	 
	return new WP_REST_Response(
		array(
			'response_code'  => "200", 
			'data' => $data,
		),200);
}
add_action( 'rest_api_init', function () {
    register_rest_route( 'tvcapi', '/v2/privacy_policy',
        array (
            'methods'  => 'GET',
            'callback' => 'privacy_policy',
        )
    );
});