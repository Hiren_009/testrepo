<?php

/*
 * NOTIFICATION OPTIONS LIST SETTING API
 * CAN SET HOST & GUEST BOTH SIDE
 */

add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/notification-options-list',
        array(
            'methods' => 'GET',
            'callback'=> 'notification_options_list'
        )
    );
});

function notification_options_list() {

	// Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);

    if ( empty( $headers['token'] ) || $headers['token'] != $token ) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

    /* NOTIFICATION OPTIONS LIST */
    $option_list = unserialize( get_user_meta( $token_id[1], 'notify_options_setting', true ) );

    $user_type    = get_user_meta( $token_id[1], 'user_type', true );
    $data         = array();
    if ( isset( $user_type ) && $user_type == 0 ) {
    	foreach ($option_list['host_user'] as $key => $value) {
		    $notify_title = preg_replace("/[\s_]/", " ", $key);
	    	$data[] = array(
			    				'key'  => $key,
			    				'value'=> $value,
			    				'title'=> ucwords( $notify_title )
			    			);
	    }
    } elseif ( isset( $user_type ) && $user_type == 1 ) {
    	foreach ( $option_list['guest_user'] as $key => $value ) {
		    $notify_title = preg_replace("/[\s_]/", " ", $key);
	    	$data[] = array(
			    				'key'  => $key,
			    				'value'=> $value,
			    				'title'=> ucwords( $notify_title )
			    			);

	    }
    }
    // print_r($data); die();
    if ( !empty( $data ) ) {
    	
	    return new WP_REST_Response( array( 'response_code' => '200', 'data' => $data ), 200 );
    } else {
    	return new WP_REST_Response( array( 'response_code' => '404', 'message' => esc_html__('You have not available notification option', 'wpestate' ) ), 404 );
    }
}

/*
 * UPDATE OPTION NOTIFICATIONS LIST 
 * FOR BOTH (HOST - 0 & GUEST - 1)
 */
function update_notify_options_list( $user_id ) {

	$user_type = get_user_meta( $user_id, 'user_type', true );
	
	if ( isset( $user_type ) && $user_type == 1 ) {

		/** GUEST NOTIFICATIONS OPTIONS SETTING AREA **/
		$notify_options_setting['guest_user']['all'] = "1"; 
		$notify_options_setting['guest_user']['new_reservation'] = "1"; 
		$notify_options_setting['guest_user']['relocated_reservation'] = "1";
		$notify_options_setting['guest_user']['cancelled_reservation'] = "1"; 
		$notify_options_setting['guest_user']['new_reviews'] = "1";
		$notify_options_setting['guest_user']['guest_chat_messages'] = "1";
		$notify_options_setting['guest_user']['admin_messages'] = "1";
		$notify_options_setting['guest_user']['new_coupons'] = "1";

	} elseif ( isset( $user_type ) && $user_type == 0 ) {
		
		/** HOST NOTIFICATIONS OPTIONS SETTING AREA **/
		$notify_options_setting['host_user']['all'] = "1";
		$notify_options_setting['host_user']['new_reservation'] = "1"; 
		$notify_options_setting['host_user']['relocated_reservation'] = "1";
		$notify_options_setting['host_user']['cancelled_reservation'] = "1"; 
		$notify_options_setting['host_user']['new_reviews'] = "1";
		$notify_options_setting['host_user']['guest_chat_messages'] = "1";
		$notify_options_setting['host_user']['guest_emails'] = "1";
		$notify_options_setting['host_user']['admin_messages'] = "1";
		$notify_options_setting['host_user']['sold_out_rooms'] = "1"; 
	} else {
		return false;
	}
	update_user_meta( $user_id, 'notify_options_setting', serialize( $notify_options_setting ) );
	// return $notify_options_setting;
}