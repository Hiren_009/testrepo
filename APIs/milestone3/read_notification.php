<?php

/**
 * read PUSH NOTIFICATION API
 */
// up_push_new_message
add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/read-notification',
        array(
            'methods' => 'POST',
            'callback'=> 'read_notification'
        )
    );
});

function read_notification() {

	// Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);
    
    if (empty($headers['token']) || $headers['token'] != $token) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

    $id      = sanitize_text_field( filter_input( INPUT_POST, 'id' ) );
    $type    = sanitize_text_field( filter_input( INPUT_POST, 'type' ) );
    $is_read = sanitize_text_field( filter_input( INPUT_POST, 'is_read' ) );
    
    if ( !isset( $id ) || empty( $id ) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter input id', 'wpestate')), 400);
    } elseif ( !isset( $type ) || empty( $type ) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter input type', 'wpestate')), 400);
    } elseif ( !isset( $is_read ) || empty( $is_read ) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter input is_read', 'wpestate')), 400);
    }


    global $wpdb;
    $table_name  = $wpdb->prefix."fcm_push_notification_track";

    if ( isset( $type ) && $type == 'assigned_new_coupon' ) {

        $wpdb->query( "UPDATE $table_name SET seen = $is_read WHERE user_id = $token_id[1] AND type = '".$type."' " );
    } else {

        $wpdb->query( "UPDATE $table_name SET seen = $is_read WHERE user_id = $token_id[1] AND type_id = $id AND type = '".$type."' " );
    }
    
    $unread_count = fcm_unread_count( $token_id[1] );
    if ( !empty( $unread_count ) ) {
        $unread_count = $unread_count;
    } else {
        $unread_count = 0;
    }
    return new WP_REST_Response( array( 'response_code' => '200', 'unread_count' => $unread_count ), 200 );
}