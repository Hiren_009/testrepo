<?php

/*
 * FCM_push_notification
 */
function FCM_push_notification( $device_id, $msg_data ) {
    global $wpdb;
    //API URL of FCM
	$url = 'https://fcm.googleapis.com/fcm/send';

	/* api_key available in:
	Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key */    
	// $api_key = 'AAAADWkE5As:APA91bGEy1JXfMhbYXS_5geApTO-d-A9Z9XDfbCP6rAimGb_8HpssCMMdkWwU9KJGd7mIUThjht45ZRqN-w9E_t4lW3WJHE9Hw_M1ZljiXFY4neZ1vE331RiOJY21UvThckGUM7F14za';
    $api_key = 'AAAA4zkRq44:APA91bF_2E7mweLci414I-AVSroLiC2P2oyy66UsbhEjfqBFBs2LBTSXpd4Rol6XsivLdawCp7vP2BnzTInvB7ejQmBBA2GPweQbVckMovQyWFGWV-4tqO0KTnwjrWmvDKwsE0TGCoPO';
	    
	// if  ( $device_id['type'] == '' ) {
		
	// }
    /* START Unread Counter */
    fcm_push_notification_track( $msg_data );
    /* END Unread Counter */

	$notification_data = array (
	    'registration_ids' => array (
	        $device_id
	    ),
	    'data'         => $msg_data,
	    'notification' => array(
	    	'title'				=> get_bloginfo(),
	    	'body'  			=> $msg_data['message'], 
	    	'sound' 			=> 'default',
	    	'content_available' => false,
	        'priority'          => 'high',
            'badge'             =>  fcm_unread_count( $msg_data['user_id'] )
	    )
	);
   

	// Header Includes Content type and api key
	$headers = array(
	    'Content-Type:application/json',
	    'Authorization:key='.$api_key
	);          
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($notification_data));

	$result = curl_exec($ch);
	if ($result === FALSE) {
	    die('FCM Send Error: ' . curl_error($ch));
	}
	curl_close($ch);
	return $result;
}

/* 
 * NOTIFICATION DATA LIST PARAMETER WISE FUNCTIONS
 */

/* NEW RESERVATION */
function new_reservation( $user_ID, $per_page, $offset ) {

	global $wpdb;
    $user_type = get_user_meta( $user_ID, 'user_type', true );

    /* SHOW ONLY FOR GUEST USER */
    if ( $user_type == "1" ) {
        
        /* FILTER BOOKING ID's SELECT ONLY New Upcoming DATE WISE */
        $bookID_date_filter = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1 = 1 AND wp_posts.post_author = $user_ID AND wp_posts.post_type = 'wpestate_booking' AND `wp_posts`.`post_status` = 'publish' AND (wp_postmeta.meta_key = 'booking_from_date' AND wp_postmeta.meta_value > DATE_FORMAT(NOW(),'%Y-%m-%d')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC ", ARRAY_A);
        // print_r($bookID_date_filter);
        $bookID_date_filter = array_column( $bookID_date_filter, 'ID' );
        $bookID_date_filter = implode( ',', $bookID_date_filter );

        if ( !empty( $bookID_date_filter ) ) {
            $bookID_date_filter = $bookID_date_filter;
        } else {
            $bookID_date_filter = "null";
        }

        /* GET PER PAGE RECORD */ 
        $booked_ID = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS post_id FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($bookID_date_filter) AND ( meta_key = 'booking_status' AND meta_value = 'confirmed' ) GROUP BY wp_postmeta.post_id DESC LIMIT ".$offset.", ".$per_page." ", ARRAY_A);

        /* COUNT TOTAL BOOKED ID's */
        $count_booked_ID = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS `wp_postmeta`.`post_id` FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($bookID_date_filter) AND ( meta_key = 'booking_status' AND meta_value = 'confirmed' ) GROUP BY wp_postmeta.post_id DESC", ARRAY_A);

    } elseif ( $user_type == "0" ) {
        
        /* SHOW ONLY FOR HOST USER */
        /* FILTER BOOKING ID's SELECT ONLY New Upcoming DATE WISE */
        $bookID_date_filter = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1 = 1 AND /*wp_posts.post_author = $user_ID AND*/ wp_posts.post_type = 'wpestate_booking' AND `wp_posts`.`post_status` = 'publish' AND (wp_postmeta.meta_key = 'booking_from_date' AND wp_postmeta.meta_value > DATE_FORMAT(NOW(),'%Y-%m-%d')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC ", ARRAY_A);
        // print_r($bookID_date_filter);
        $bookID_date_filter = array_column( $bookID_date_filter, 'ID' );
        $bookID_date_filter = implode( ',', $bookID_date_filter );

        if ( !empty( $bookID_date_filter ) ) {
            $bookID_date_filter = $bookID_date_filter;
        } else {
            $bookID_date_filter = "null";
        }

        /* GET PER PAGE RECORD */ 
        $booked_ID_ = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS post_id FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($bookID_date_filter) AND ( meta_key = 'booking_status' AND meta_value = 'confirmed' ) GROUP BY wp_postmeta.post_id DESC", ARRAY_A);
        $booked_ID_ = array_column( $booked_ID_, 'post_id' );
        $booked_ID_ = implode( ',', $booked_ID_);


        if ( !empty( $booked_ID_ ) ) {
            $booked_ID_ = $booked_ID_;
        } else {
            $booked_ID_ = "null";
        }

        // $booked__ID = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS post_id FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($booked_ID_) AND ( meta_key = 'owner_id' AND meta_value = $user_ID ) GROUP BY wp_postmeta.post_id DESC", ARRAY_A);
        
        $booked_ID = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS post_id FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($booked_ID_) AND ( meta_key = 'owner_id' AND meta_value = $user_ID ) GROUP BY wp_postmeta.post_id DESC LIMIT ".$offset.", ".$per_page." ", ARRAY_A);

        /* COUNT TOTAL BOOKED ID's */
        $count_booked_ID = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS `wp_postmeta`.`post_id` FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($booked_ID_) AND ( meta_key = 'owner_id' AND meta_value = $user_ID ) GROUP BY wp_postmeta.post_id DESC", ARRAY_A);
    }
    
    $count_ID = array_column($count_booked_ID, 'post_id');
    // get_set_all_cover_id($count_booked_ID);
    $booked_ids = array_column($booked_ID, 'post_id');
    $total_page = ceil( count( $count_booked_ID ) / $per_page );
    return booking_info( $user_ID, $booked_ids, $total_page, $count_ID );
}

/* CANCELLED RESERVATION */
function cancelled_reservation( $user_ID, $per_page, $offset ) {

    global $wpdb;

    $user_type = get_user_meta( $user_ID, 'user_type', true );

    /* SHOW ONLY FOR GUEST USER */
    if ( $user_type == "1" ) {

        /* FILTER BOOKING ID's SELECT ONLY New Upcoming DATE WISE */ 
        // $bookID_date_filter = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1 = 1 AND wp_posts.post_author = $user_ID AND wp_posts.post_type = 'wpestate_booking' AND `wp_posts`.`post_status` = 'publish' AND (wp_postmeta.meta_key = 'booking_from_date' AND wp_postmeta.meta_value > DATE_FORMAT(NOW(),'%Y-%m-%d')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC ", ARRAY_A);

        // $bookID_date_filter = array_column( $bookID_date_filter, 'ID' );
        // $bookID_date_filter = implode( ',', $bookID_date_filter );

        /* GET PER PAGE RECORD */ 
        $booked_ID = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1 = 1 AND wp_posts.post_author = $user_ID AND wp_posts.post_type = 'wpestate_booking' AND `wp_posts`.`post_status` = 'publish' AND ( wp_postmeta.meta_key = 'booking_status' AND wp_postmeta.meta_value = 'canceled' ) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC LIMIT ".$offset.", ".$per_page." ", ARRAY_A);

        /* COUNT TOTAL BOOKED ID's */
        $count_booked_ID = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1 = 1 AND wp_posts.post_author = $user_ID AND wp_posts.post_type = 'wpestate_booking' AND `wp_posts`.`post_status` = 'publish' AND ( wp_postmeta.meta_key = 'booking_status' AND wp_postmeta.meta_value = 'canceled' ) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC ", ARRAY_A);

        $count_ID   = array_column($count_booked_ID, 'ID');
        $booked_ids = array_column($booked_ID, 'ID');
    
    } elseif ( $user_type == "0" ) {
        
        /* SHOW ONLY FOR HOST USER */
        /* FILTER BOOKING ID's SELECT ONLY New Upcoming DATE WISE */ 
        // $bookID_date_filter = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1 = 1 AND wp_posts.post_author = $user_ID AND wp_posts.post_type = 'wpestate_booking' AND `wp_posts`.`post_status` = 'publish' AND (wp_postmeta.meta_key = 'booking_from_date' AND wp_postmeta.meta_value > DATE_FORMAT(NOW(),'%Y-%m-%d')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC ", ARRAY_A);

        // $bookID_date_filter = array_column( $bookID_date_filter, 'ID' );
        // $bookID_date_filter = implode( ',', $bookID_date_filter );

        /* GET PER PAGE RECORD */ 
        $booked_ID_ = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1 = 1 AND /*wp_posts.post_author = $user_ID AND*/ wp_posts.post_type = 'wpestate_booking' AND `wp_posts`.`post_status` = 'publish' AND ( wp_postmeta.meta_key = 'booking_status' AND wp_postmeta.meta_value = 'canceled' ) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC", ARRAY_A);
        $booked_ID_ = array_column( $booked_ID_, 'ID' );
        $booked_ID_ = implode( ',', $booked_ID_);

        if ( !empty( $booked_ID_ ) ) {
            $booked_ID_ = $booked_ID_;
        } else {
            $booked_ID_ = "null";
        }

        $booked_ID = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS post_id FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($booked_ID_) AND ( meta_key = 'owner_id' AND meta_value = $user_ID ) GROUP BY wp_postmeta.post_id DESC LIMIT ".$offset.", ".$per_page." ", ARRAY_A);

        /* COUNT TOTAL BOOKED ID's */
        $count_booked_ID = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS `wp_postmeta`.`post_id` FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($booked_ID_) AND ( meta_key = 'owner_id' AND meta_value = $user_ID ) GROUP BY wp_postmeta.post_id DESC", ARRAY_A);

        $count_ID   = array_column($count_booked_ID, 'post_id');
        $booked_ids = array_column($booked_ID, 'post_id');

    }
    // $count_ID   = array_column($count_booked_ID, 'post_id');
    // $booked_ids = array_column($booked_ID, 'post_id');
    $total_page = ceil( count( $count_booked_ID ) / $per_page );
    return booking_info($user_ID, $booked_ids, $total_page, $count_ID );
}

/* GET FEW BOOKING INFO */
function booking_info( $user_ID, $booked_ids, $total_page, $all_ ) {
    // print_r( $all_ );
    $data = array();
    $currency = esc_html( get_option('wp_estate_currency_label_main', '') );
    if ( !empty( $booked_ids ) ) {
        for ($i=0; $i < count( $booked_ids ); $i++) { 
            
            $booking_status = get_post_meta( $booked_ids[ $i ], 'booking_status', true);
            if ( $booking_status == 'confirmed') {

                $is_read   = unread_notification_by_type( $user_ID, $booked_ids[ $i ], "new_reservation" );
            
                for ($j=0; $j < count( $is_read ); $j++) {
                    $data[ $i ]['is_read'] = $is_read[$j];
                }

                $data[ $i ]['booking_status'] = esc_html__('New Reservation', 'wpestate');
                $data[ $i ]['status_type']    = esc_html__('new_reservation', 'wpestate');
            } elseif ( $booking_status == 'canceled' ) {

                $is_read   = unread_notification_by_type( $user_ID, $booked_ids[ $i ], "cancellation" );
            
                for ($j=0; $j < count( $is_read ); $j++) {
                    $data[ $i ]['is_read'] = $is_read[$j];
                }

                $data[ $i ]['booking_status'] = esc_html__('Cancelled', 'wpestate');
                $data[ $i ]['status_type']    = esc_html__('cancellation', 'wpestate');
            } else {
                $data[ $i ]['booking_status'] = "";
            }
                
            /**
             * Check about stay
             */
            $boo_from = strtotime( get_post_meta( $booked_ids[$i], 'booking_from_date', true ) );
            $boo_to   = strtotime( get_post_meta( $booked_ids[$i], 'booking_to_date', true ) ); 

            $data[ $i ]['reservation_date'] = strtotime(get_the_time('Y-m-d', $booked_ids[$i]));
        

            $prop_id = get_post_meta( $booked_ids[$i], 'booking_id', true );

            $data[ $i ]['ID'] = $booked_ids[$i];
            $data[ $i ]['property_name'] = get_the_title( $prop_id );
            $data[ $i ]['booking_id']    = $booked_ids[$i];
            $data[ $i ]['book_from_date']= $boo_from;  
            $data[ $i ]['book_to_date']  = $boo_to; 

            // CONTACT INFO
            $data[ $i ]['contact_name']   = get_post_meta( $booked_ids[$i], 'chekout_contactname', true );
            $data[ $i ]['traveller_name'] = get_post_meta( $booked_ids[$i], 'travellername', true );

            /* GUESTS INFO */
            $booking_guests = get_post_meta( $booked_ids[ $i ], 'booking_guests', true);
            $adult_guests   = get_post_meta( $booked_ids[ $i ], 'adult_type_val', true);
            $child_guests   = get_post_meta( $booked_ids[ $i ], 'child_type_val', true);
            
            if ( empty( $child_guests ) ) {

                if ( $booking_guests > 1 ) {
                    $data[ $i ]['guest'] = $booking_guests.esc_html__(' Adults ', 'wpestate'); 
                } else {
                    $data[ $i ]['guest'] = $booking_guests.esc_html__(' Adult ', 'wpestate'); 
                }

            } else {
                if ( $adult_guests  > 1 && $child_guests == 1) {
                    $data[ $i ]['guest'] = $adult_guests.esc_html__(' Adults ', 'wpestate').$child_guests.esc_html__(' Kid','wpestate'); 
                } elseif ( $adult_guests == 1 && $child_guests > 1 ) {
                    $data[ $i ]['guest'] = $adult_guests.esc_html__(' Adult ', 'wpestate').$child_guests.esc_html__(' Kids','wpestate'); 
                } elseif ($adult_guests == 1 && $child_guests == 1) {
                    $data[ $i ]['guest'] = $adult_guests.esc_html__(' Adult ', 'wpestate').$child_guests.esc_html__(' Kid','wpestate'); 
                } else {
                    $data[ $i ]['guest'] = $adult_guests.esc_html__(' Adults ', 'wpestate').$child_guests.esc_html__(' Kids','wpestate');
                }
            }

            /* TOTAL NO OF GUESTS */
            
            // No of Night
            $timeDiff             = abs( $boo_from - $boo_to );
            $count_days           = $timeDiff/86400;  // 86400 seconds in one day
            $data[ $i ]['nights'] = "$count_days";

            $data[ $i ]['total']  = $currency.number_format( get_post_meta( $booked_ids[ $i ], 'total_price', true) );

            // CHECKOUT TIME
            $propid = get_post_meta( $booked_ids[ $i ], 'booking_id', true );
            $check_out_hour     = get_post_meta($propid,'check-out-hour',true);
            $check_out_hour_to  = get_post_meta($propid,'check-out-hour-to',true);
            $hours_check_out_24 = get_post_meta($propid,'hours_check_out_24',true);
            if ( empty( $hours_check_out_24 ) ) {
                $data[ $i ]['check_out_between'] = $check_out_hour . ' - ' . $check_out_hour_to;
            } else {
                $data[ $i ]['check_out_between'] = $hours_check_out_24;
            }
            $imageUrl = wp_get_attachment_url(get_post_thumbnail_id($propid));
            if ( !empty( $imageUrl ) ) {
                $data[$i]['imageUrl'] = $imageUrl;
            } else{
                $data[$i]['imageUrl'] = "";
            }
            $data[$i]['total_page'] = $total_page;
        }
    } else {
        return false;
    }

    // return $data;
    return array(
        'data' => $data,
        'ID'   => $all_
    );
}

/**
 * GET ADMIN MESSESSE BY TYPE 
 * ALL ( HOST + GUEST )
 * HOST OR GUEST
 */
function get_admin_messges( $user_ID, $per_page, $offset ) {

    /* GET ALL MESSAGES POSTS */
    global $wpdb;
    $user_type = get_user_meta( $user_ID, 'user_type', true );

    /* EXECUTE FOR IF ENABLE MESSAGE FOR ALL */
    if ( isset( $user_type ) && $user_type == '0' || $user_type == '1' ) {
        
        $count_mssgs_postID = $wpdb->get_results("SELECT wp_posts.ID FROM `wp_posts` INNER JOIN wp_postmeta ON wp_postmeta.post_id = wp_posts.ID WHERE 1=1 AND post_type = 'admin_messages' AND post_status = 'publish' AND ( wp_postmeta.meta_key = 'whom_to_send' AND wp_postmeta.meta_value = 'all' ) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC", ARRAY_A);
        $total_page = ceil( count( $count_mssgs_postID ) / $per_page );
        // do_action('get_total_page_count', $total_page );
        $mssgs_postID = $wpdb->get_results("SELECT wp_posts.ID FROM `wp_posts` INNER JOIN wp_postmeta ON wp_postmeta.post_id = wp_posts.ID WHERE 1=1 AND post_type = 'admin_messages' AND post_status = 'publish' AND ( wp_postmeta.meta_key = 'whom_to_send' AND wp_postmeta.meta_value = 'all' ) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC LIMIT ".$offset.", ".$per_page." ", ARRAY_A);

    } elseif (isset( $user_type ) && $user_type == '0') {

        /* EXECUTE FOR IF ENABLE MESSAGE ONLY FOR HOST */
        $count_mssgs_postID = $wpdb->get_results("SELECT wp_posts.ID FROM `wp_posts` INNER JOIN wp_postmeta ON wp_postmeta.post_id = wp_posts.ID WHERE 1=1 AND post_type = 'admin_messages' AND post_status = 'publish' AND ( wp_postmeta.meta_key = 'whom_to_send' AND wp_postmeta.meta_value = 'host' ) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC", ARRAY_A);
        $total_page = ceil( count( $count_mssgs_postID ) / $per_page );
        // do_action('get_total_page_count', $total_page );
        $mssgs_postID = $wpdb->get_results("SELECT wp_posts.ID FROM `wp_posts` INNER JOIN wp_postmeta ON wp_postmeta.post_id = wp_posts.ID WHERE 1=1 AND post_type = 'admin_messages' AND post_status = 'publish' AND ( wp_postmeta.meta_key = 'whom_to_send' AND wp_postmeta.meta_value = 'host' ) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC LIMIT ".$offset.", ".$per_page." ", ARRAY_A);

    } elseif (isset( $user_type ) && $user_type == '1') {

        /* EXECUTE FOR IF ENABLE MESSAGE ONLY FOR GUEST */
        $count_mssgs_postID = $wpdb->get_results("SELECT wp_posts.ID FROM `wp_posts` INNER JOIN wp_postmeta ON wp_postmeta.post_id = wp_posts.ID WHERE 1=1 AND post_type = 'admin_messages' AND post_status = 'publish' AND ( wp_postmeta.meta_key = 'whom_to_send' AND wp_postmeta.meta_value = 'guest' ) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC", ARRAY_A);
        $total_page = ceil( count( $count_mssgs_postID ) / $per_page );
        // do_action('get_total_page_count', $total_page );
        $mssgs_postID = $wpdb->get_results("SELECT wp_posts.ID FROM `wp_posts` INNER JOIN wp_postmeta ON wp_postmeta.post_id = wp_posts.ID WHERE 1=1 AND post_type = 'admin_messages' AND post_status = 'publish' AND ( wp_postmeta.meta_key = 'whom_to_send' AND wp_postmeta.meta_value = 'guest' ) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC LIMIT ".$offset.", ".$per_page." ", ARRAY_A);
    }

    $count_ID = array_column( $count_mssgs_postID, 'ID');
    $mssgs_postID = array_column($mssgs_postID, 'ID');
    // return array_merge( array( 'total_page' => $total_page, 'option_type' => 'admin_messages' ), admin_message_details( $user_ID, $mssgs_postID ) );
    return admin_message_details( $user_ID, $mssgs_postID, $total_page, $count_ID );
} 

function admin_message_details( $user_ID, $mssgs_postID, $total_page, $all_ ) {

    if ( !empty( $mssgs_postID ) ) {
        
        $data = array();
        for ($i = 0; $i < count( $mssgs_postID ); $i++) { 
            
            $user      = get_user_by( 'ID', $user_ID );
            $is_read   = unread_notification_by_type( $user_ID, $mssgs_postID[ $i ], "admin_messages" );
            
            for ($j=0; $j < count( $is_read ); $j++) {
                $data[ $i ]['is_read'] = $is_read[$j];
            }
            $data[ $i ]['ID'] = $mssgs_postID[ $i ];

            // Message title 
            $data[ $i ]['message_title'] = get_the_title( $mssgs_postID[ $i ] );

            // Message Content 
            $text_message  = apply_filters( 'the_content', get_post_field( 'post_content', $mssgs_postID[ $i ] ) );
            $rm_nline = strip_tags( str_replace('[user-name]', ucfirst($user->user_login), $text_message) );
            $data[ $i ]['message'] = preg_replace("/\r|\n/", " ", $rm_nline);;

            // Sent Date
            $data[ $i ]['date'] = get_the_date('D, M, jS Y', $mssgs_postID[ $i ]);
            $data[ $i ]['notification_type'] = esc_html__( 'Admin Message', 'wpestate');
            $data[ $i ]['status_type']    = esc_html__('admin_messages', 'wpestate');
            $data[ $i ]['total_page'] = $total_page;

        }

    } 
    // return $data;
    return array(
        'data' => $data,
        'ID'   => $all_
    );
}

function all( $uid, $per_page, $offset) {

    $a = array(
        new_reservation( $uid, $per_page, $offset ),
        cancelled_reservation( $uid, $per_page, $offset ),
        get_admin_messges( $uid, $per_page, $offset )
    );
    // for ($i=0; $i < count( $a ); $i++) { 
    //     # code...
    //     $merge_all_data[] = $a[$i]['ID'];
    //     // print_r($a[$i]['ID']);
    // }
    // print_r($merge_all_data);
    return $a;
}

/* GET ID COUNT */
function id_count( $data ) {
    $a = 1;
    for ( $i = 0; $i < count( $data ) ; $i++ ) { 
        $total_page = $data[$i]['total_page'];
        $a++;
    }
    // echo "- ";
    // print_r($a)."\n";
    // $b = ceil( count( $a ) / 1 );
    // // ceil( count( $count_booked_ID ) / $per_page )
    // echo "- ".$b."\n";
    return $total_page; 
}

/* UNREAD FCM PUSH APP NOTIFICATION TRACK */
function fcm_push_notification_track( $msg_data ) {

    global $wpdb;
    $wpdb->insert( $wpdb->prefix."fcm_push_notification_track", array( 
        'user_id' => $msg_data['user_id'],
        'type_id' => $msg_data['id'], 
        'type'    => $msg_data['type'], 
        'seen'    => "" 
    ));
}

/* UNREAD NOTIFICATION COUNT */
function fcm_unread_count( $user_id ) {

    global $wpdb;
    $unread_count = $wpdb->get_var( "SELECT count(user_id) FROM `wp_fcm_push_notification_track` WHERE 1=1 AND user_id = ".$user_id." AND seen = 0 GROUP BY user_id " );

    return $unread_count;
}

/* SET FCM UNREAD BY NOTIFICATION TYPE */
function unread_notification_by_type( $user_id, $id, $type ) {

    global $wpdb;
    $by_type = $wpdb->get_results( "SELECT `seen` FROM `wp_fcm_push_notification_track` WHERE 1=1 AND user_id = ".$user_id." AND type_id = ".$id." AND type = '".$type."' ", ARRAY_A );
    $by_type = array_column( $by_type, "seen" );
    return $by_type;
}

/* SEND PUSH NOTIFICATION AFTER BOOKING CONFIRMED FROM WEB & APP BOTH */
add_action( 'after_booking_confrimed', 'after_prop_booking_confrimed' );
function after_prop_booking_confrimed( $b_data ) {

    $to      = 'hirenc.webdesk@gmail.com';
    $from    = get_option('admin_email');
    $subject = "New Reservation to Call Hook";
    $message = "From New Reservation "."\n". " -Booking-ID ".$b_data['booking_id']."\n"." -User ID - ".$b_data['user_id'];
    $headers = array('Content-Type: text/html; charset=UTF-8',"From:" . $from);
    
    
    wp_mail( $to, $subject, $message, $headers );

    // SEND PUSH NOTIFICATION
    $current_user   =   get_userdata( $b_data['user_id'] );
    $username       =   $current_user->user_login;
    
    $owner_id        = get_post_meta( $b_data['booking_id'], 'owner_id', true );
    $device_id       = get_user_meta( $owner_id, 'device_token', true );
    $guest_device_id = get_user_meta( $b_data['user_id'], 'device_token', true );


    $property_id = get_post_meta( $b_data['booking_id'], 'booking_id', true );
    $notify_type = "new_reservation";
    $message     = esc_html__( 'Congratulations! '.$username.' booked your property', 'wpestate' );

    

    $guest = unserialize( get_user_meta( $b_data['user_id'], 'notify_options_setting', true ) );
    $host  = unserialize( get_user_meta( $owner_id, 'notify_options_setting', true ) );

    if ( $host['host_user']['new_reservation'] == 1 ) {

        $msg_data = array(
            'id'      => $b_data['booking_id'],
            'type'    => $notify_type,
            'message' => $message,
            'user_id' => $owner_id
        );
        FCM_push_notification( $device_id, $msg_data );
    } 
    if ( $guest['guest_user']['new_reservation'] == 1 ) {

        $msg_data = array(
            'id'      => $b_data['booking_id'],
            'type'    => $notify_type,
            'message' => $message,
            'user_id' => $b_data['user_id']
        );
        FCM_push_notification( $guest_device_id, $msg_data );
    }
}

/* SEND PUSH NOTIFICATION AFTER BOOKING CANCELLED FROM WEB  */
add_action( 'after_booking_cancelled', 'after_prop_booking_cancelled' );
function after_prop_booking_cancelled( $b_data ) {

    $to      = 'hirenc.webdesk@gmail.com';
    $from    = get_option('admin_email');
    $subject = "Action call from Cancellation property by guest user";
    $message = "Action call from Cancellation property by guest user"."\n"." -Booking ID ".$b_data['booking_id']."\n"." -User ID ".$b_data['user_id'];
    $headers = array('Content-Type: text/html; charset=UTF-8',"From:" . $from);
    
    
    wp_mail( $to, $subject, $message, $headers );



    // SEND PUSH NOTIFICATION
    $current_user   =   get_userdata( $b_data['user_id'] );
    $username       =   $current_user->user_login;
    
    $owner_id        = get_post_meta( $b_data['booking_id'], 'owner_id', true );
    $device_id       = get_user_meta( $owner_id, 'device_token', true );
    $guest_device_id = get_user_meta( $b_data['user_id'], 'device_token', true );


    $property_id = get_post_meta( $b_data['booking_id'], 'booking_id', true );
    $notify_type = 'cancellation';
    $message     = esc_html__( get_the_title( $property_id ).' '.$username.' Cancelled Booking', 'wpestate' );



    $guest = unserialize( get_user_meta( $b_data['user_id'], 'notify_options_setting', true ) );
    $host  = unserialize( get_user_meta( $owner_id, 'notify_options_setting', true ) );
    if ( $host['host_user']['cancelled_reservation'] == 1 ) {

        $msg_data = array(
            'id'      => $b_data['booking_id'],
            'type'    => $notify_type,
            'message' => $message,
            'user_id' => $owner_id
        );
        FCM_push_notification( $device_id, $msg_data );
    } 
    if ( $guest['guest_user']['cancelled_reservation'] == 1 ) {

        $msg_data = array(
            'id'      => $b_data['booking_id'],
            'type'    => $notify_type,
            'message' => $message,
            'user_id' => $b_data['user_id']
        );
        FCM_push_notification( $guest_device_id, $msg_data );
    }

    /**
     * Delete record from wp_api_tmp table
     */
    // DELETE FROM `wp_api_tmp` WHERE end_date < DATE_FORMAT(NOW(),'%Y-%m-%d')
    global $wpdb;
    $table_name  = $wpdb->prefix."api_tmp";
    $wpdb->delete( $table_name, array( 'booking_id' => $b_data['booking_id'] ) );
    
    /* Delete record from wp_api_tmp table */
}

/* SEND NOTIFICATION WHEN ASSIGNED NEW COUPON */
add_action( 'assigned_new_coupon', 'notify_assigned_new_coupon' );
function notify_assigned_new_coupon( $coupon_data ) {

	$copn_data = $coupon_data['user_id'];
	if ( is_array( $copn_data ) ) {
		$copn_arr = $copn_data;
	} else {
		$copn_arr[] = $copn_data; 
	}
	// print_r($copn_arr)."\n";
    for ($i = 0; $i < count( $copn_arr ); $i++) { 

        $guest = unserialize( get_user_meta( $copn_arr[$i], 'notify_options_setting', true ) );
        if ( $guest['guest_user']['new_coupons'] == 1 ) {
        
            $notify_type = "assigned_new_coupon";
            $message     = esc_html__( 'Congratulations! you have assigned new coupon '.$coupon_data['coupon_code'], 'wpestate' );
            // $message     = esc_html__( 'Congratulations! '.$username.' you have assigned new coupon '.$coupon_data['coupon_code'], 'wpestate' );

            $msg_data = array(
                'id'      => $coupon_data['coupon_id'],
                'type'    => $notify_type,
                'message' => $message,
                'user_id' => $copn_arr[$i]
            );
            // print_r($msg_data);
            $device_id = get_user_meta( $copn_arr[$i], 'device_token', true );
            FCM_push_notification( $device_id, $msg_data );

            // for testing
            $to      = 'hirenc.webdesk@gmail.com';
            $from    = get_option('admin_email');
            $subject = "Action call from assigned_new_coupon";
            $message = "<p>Copoun ID         ".$coupon_data['coupon_id']."</p>
                        <p>Notification Type ".$notify_type."</p>
                        <p>user_ID           ".$copn_arr[$i]."</p>
                        <p>Message           ".$message."</p>";
            $headers = array('Content-Type: text/html; charset=UTF-8',"From:" . $from);
            wp_mail( $to, $subject, $message, $headers );
        }

    }
}


// ---------------------------------------------------------
function all_notify_data( $user_ID ) {

    global $wpdb;
    $user_type = get_user_meta( $user_ID, 'user_type', true );
    
    // new res
    if ( $user_type == "1" ) {
        
        /* FILTER BOOKING ID's SELECT ONLY New Upcoming DATE WISE */
        $bookID_date_filter = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1 = 1 AND wp_posts.post_author = $user_ID AND wp_posts.post_type = 'wpestate_booking' AND `wp_posts`.`post_status` = 'publish' AND (wp_postmeta.meta_key = 'booking_from_date' AND wp_postmeta.meta_value > DATE_FORMAT(NOW(),'%Y-%m-%d')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC ", ARRAY_A);

        // print_r($bookID_date_filter);
        $bookID_date_filter = array_column( $bookID_date_filter, 'ID' );
        $bookID_date_filter = implode( ',', $bookID_date_filter );

        if ( !empty( $bookID_date_filter ) ) {
            $bookID_date_filter = $bookID_date_filter;
        } else {
            $bookID_date_filter = "null";
        }
        
        /*** NEW BOOKED ID's ***/
        $new_booked_ID = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS `wp_postmeta`.`post_id` FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($bookID_date_filter) AND ( meta_key = 'booking_status' AND meta_value = 'confirmed' ) GROUP BY wp_postmeta.post_id DESC", ARRAY_A);
        /*** END NEW BOOKED ID's ***/

        /*** FOR CANCELLATION ***/
        $cancelled_booking = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1 = 1 AND wp_posts.post_author = $user_ID AND wp_posts.post_type = 'wpestate_booking' AND `wp_posts`.`post_status` = 'publish' AND ( wp_postmeta.meta_key = 'booking_status' AND wp_postmeta.meta_value = 'canceled' ) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC ", ARRAY_A);
        $cancelled_booking = array_column($cancelled_booking, 'ID');
        /*** END FOR CANCELLATION ***/

    } elseif ( $user_type == "0" ) {
        
        /* SHOW ONLY FOR HOST USER */
        /* FILTER BOOKING ID's SELECT ONLY New Upcoming DATE WISE */
        $bookID_date_filter = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1 = 1 AND /*wp_posts.post_author = $user_ID AND*/ wp_posts.post_type = 'wpestate_booking' AND `wp_posts`.`post_status` = 'publish' AND (wp_postmeta.meta_key = 'booking_from_date' AND wp_postmeta.meta_value > DATE_FORMAT(NOW(),'%Y-%m-%d')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC ", ARRAY_A);
        // print_r($bookID_date_filter);
        $bookID_date_filter = array_column( $bookID_date_filter, 'ID' );
        $bookID_date_filter = implode( ',', $bookID_date_filter );

        if ( !empty( $bookID_date_filter ) ) {
            $bookID_date_filter = $bookID_date_filter;
        } else {
            $bookID_date_filter = "null";
        }

        /*** NEW BOOKED ID's ***/
        $booked_ID_ = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS post_id FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($bookID_date_filter) AND ( meta_key = 'booking_status' AND meta_value = 'confirmed' ) GROUP BY wp_postmeta.post_id DESC", ARRAY_A);

        $booked_ID_ = array_column( $booked_ID_, 'post_id' );
        $booked_ID_ = implode( ',', $booked_ID_);
        
        if ( !empty( $booked_ID_ ) ) {
            $booked_ID_ = $booked_ID_;
        } else {
            $booked_ID_ = "null";
        }        

        $new_booked_ID = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS `wp_postmeta`.`post_id` FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($booked_ID_) AND ( meta_key = 'owner_id' AND meta_value = $user_ID ) GROUP BY wp_postmeta.post_id DESC", ARRAY_A);

        /*** END NEW BOOKED ID's ***/

        /*** FOR CANCELLATION ***/
        $cancl_booked_ID_ = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1 = 1 AND /*wp_posts.post_author = $user_ID AND*/ wp_posts.post_type = 'wpestate_booking' AND `wp_posts`.`post_status` = 'publish' AND ( wp_postmeta.meta_key = 'booking_status' AND wp_postmeta.meta_value = 'canceled' ) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC", ARRAY_A);
        $cancl_booked_ID_ = array_column( $cancl_booked_ID_, 'ID' );
        $cancl_booked_ID_ = implode( ',', $cancl_booked_ID_);

        if ( !empty( $cancl_booked_ID_ ) ) {
            $cancl_booked_ID_ = $cancl_booked_ID_;
        } else {
            $cancl_booked_ID_ = "null";
        } 

        /* COUNT TOTAL BOOKED ID's */
        $cancelled_booking = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS `wp_postmeta`.`post_id` FROM `wp_postmeta` WHERE 1 = 1 AND post_id IN ($cancl_booked_ID_) AND ( meta_key = 'owner_id' AND meta_value = $user_ID ) GROUP BY wp_postmeta.post_id DESC", ARRAY_A);
        $cancelled_booking = array_column($cancelled_booking, 'post_id');
        /*** END FOR CANCELLATION ***/
    }

    /** ADMIN MESSAGESS ID's **/
    /* EXECUTE FOR IF ENABLE MESSAGE FOR ALL */
    if ( isset( $user_type ) && $user_type == '0' || $user_type == '1' ) {
        
        $admin_message = $wpdb->get_results("SELECT wp_posts.ID FROM `wp_posts` INNER JOIN wp_postmeta ON wp_postmeta.post_id = wp_posts.ID WHERE 1=1 AND post_type = 'admin_messages' AND post_status = 'publish' AND ( wp_postmeta.meta_key = 'whom_to_send' AND wp_postmeta.meta_value = 'all' ) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC", ARRAY_A);
        // $total_page = ceil( count( $admin_message ) / $per_page );
        

    } elseif (isset( $user_type ) && $user_type == '0') {

        /* EXECUTE FOR IF ENABLE MESSAGE ONLY FOR HOST */
        $admin_message = $wpdb->get_results("SELECT wp_posts.ID FROM `wp_posts` INNER JOIN wp_postmeta ON wp_postmeta.post_id = wp_posts.ID WHERE 1=1 AND post_type = 'admin_messages' AND post_status = 'publish' AND ( wp_postmeta.meta_key = 'whom_to_send' AND wp_postmeta.meta_value = 'host' ) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC", ARRAY_A);
        // $total_page = ceil( count( $admin_message ) / $per_page );
        

    } elseif (isset( $user_type ) && $user_type == '1') {

        /* EXECUTE FOR IF ENABLE MESSAGE ONLY FOR GUEST */
        $admin_message = $wpdb->get_results("SELECT wp_posts.ID FROM `wp_posts` INNER JOIN wp_postmeta ON wp_postmeta.post_id = wp_posts.ID WHERE 1=1 AND post_type = 'admin_messages' AND post_status = 'publish' AND ( wp_postmeta.meta_key = 'whom_to_send' AND wp_postmeta.meta_value = 'guest' ) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC", ARRAY_A);
        // $total_page = ceil( count( $admin_message ) / $per_page );
        
    }

    $admin_message = array_column( $admin_message, 'ID');
    /** END ADMIN MESSAGESS ID's **/

    $new_booked_ID = array_column($new_booked_ID, 'post_id');
    // $all_id_s = array(
    //     'new_reservation' => $new_booked_ID,
    //     'cacellation'     => $cancelled_booking,
    //     'admin_message'   => $admin_message
    // );
    $all_id_s = array(
        /*'new_reservation' =>*/ $new_booked_ID,
        /*'cacellation'     =>*/ $cancelled_booking,
        /*'admin_message'   =>*/ $admin_message
    );
    // $all_id_s = array_filter($all_id_s);
    
    return array_filter($all_id_s);
}


/**
 * SEARCH BY TRIPS PROPERTY
 */
function search_by_trips( $search_by_bookID ) {
    
    global $wpdb;

    $issearch = sanitize_text_field( filter_input( INPUT_POST, 'is_search' ) );
    if ( isset( $issearch ) && !empty( $issearch ) ) {

        $search_by_trips = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts WHERE 1=1 AND wp_posts.post_title LIKE '%$issearch%' AND wp_posts.post_type = 'estate_property' AND ((`wp_posts`.`post_status` = 'publish')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC", ARRAY_A);

        $propID = array_column($search_by_trips, 'ID');
        // print_r($propID);

        if (empty($propID)) {
            return;
        }

        for ( $i = 0; $i < count( $search_by_bookID ); $i++ ) { 

            for ($x=0; $x < count( $propID ); $x++) { 

                $p_ID = get_post_meta( $search_by_bookID[$i], 'booking_id', true );
                if ( $p_ID == $propID[$x] ) {
                    $is_search[] = $search_by_bookID[$i];
                }
            }

        }
    }
    if ( !empty( $is_search ) ) {
        return $is_search;
    } else {
        return $search_by_bookID;
    }
}