<?php
function my_profile_data(){
	
	// Check Oath Token
	$headers = apache_request_headers();
	$token_id =  explode( "-qe_aw-", $headers['token'] );
	$token = get_user_meta($token_id[1], 'oauth_token', true);

	if (empty($headers['token']) || $headers['token'] != $token) {
	  // Error Message
	  return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
	}
	$data = array();
	// $id  		= $token_id[1];
	$id  		= $_GET['user_id'];
	$first_name = get_user_meta($id,'first_name',true);
	$last_name  = get_user_meta($id,'last_name',true);
	$email      = get_user_by('id',$id);
	$email      = $email->user_email;
	$phone      = get_user_meta($id,'phone',true);
	$mobile     = get_user_meta($id,'mobile',true);
	$about_me   = get_user_meta($id,'description',true);
	$live_in	= get_user_meta($id,'live_in',true);
	$i_speak    = get_user_meta($id,'i_speak',true);
	$picture    = get_user_meta($id,'custom_picture',true);
	/*$fb_verified = get_user_meta($id,'fb_verified',true);
	if($fb_verified == 'yes'){
        $f = 1;
    }else{
        $f = 0;
    }
	$google_verified = get_user_meta($id,'google_verified',true);
	if($google_verified == 'yes'){
        $g = 1;
    }else{
        $g = 0;
    }
	$email_verified = get_user_meta($id,'email_verified',true);*/
	$usertype = get_user_meta($id,'user_type',true);


			$data['user_id']         = $id;
			$data['first_name']      = $first_name;
			$data['last_name']       = $last_name;
			$data['email']		     = $email;
			$data['phone']		     = $phone;
			$data['moblie']	         = $mobile;
			$data['about_me']        = $about_me;
			$data['live_in']         = $live_in;
			$data['i_speak']         = $i_speak;
			$data['profile_photo']   = $picture;
			//$data['user_type']       = $usertype;
			/*if($usertype == 0){
				$data['fb_verified']     = $f;
				$data['google_verified'] = $g;
				$data['email_verified']  = $email_verified;
			}*/
	$user = get_userdata( $id );

	if(isset($user->ID)){
		
		return new WP_REST_Response(
	      array(
	          'response_code'  => "200", 
	          'data' => $data,
	      ),200);
	}else{

		return new WP_REST_Response(
            array(
                'response_code'  => '400',
                'message'=>esc_html__( 'user not exist','wpestate')
            ), 400);
	}
}
add_action( 'rest_api_init', function () {
    register_rest_route( 'tvcapi', '/v2/my_profile_data',
        array (
            'methods'  => 'GET',
            'callback' => 'my_profile_data',
        )
    );
});