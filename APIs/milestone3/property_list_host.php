<?php
add_action( 'rest_api_init', function () {
    register_rest_route( 'tvcapi', '/v2/property_list_host',
        array (
            'methods'  => 'POST',
            'callback' => 'property_list_host',
        )
    );
});
function property_list_host(){
	global $wpdb;
	// Check Oath Token
	  $headers = apache_request_headers();
	  $token_id =  explode( "-qe_aw-", $headers['token'] );
	  $token = get_user_meta($token_id[1], 'oauth_token', true);

	  if (empty($headers['token']) || $headers['token'] != $token) {
	      // Error Message
	      return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
	  }

	// START PAGINATION
	$current_page = $_POST['current_page'];
	$current_page = ( $current_page == '' || $current_page == 0 ) ? 1 : (int)$current_page;
	$per_page = $_POST['per_page'];
	$per_page = ( !isset( $per_page ) || $per_page == '' || $per_page == 0 ) ? 1 : $per_page;

	$offset = ( $current_page - 1 ) * $per_page; 
	//END PAGINATION

	$host_id = $token_id[1];
	$data    = array();
	$check_host = get_user_meta($host_id,'user_type',true);

	if ( $check_host == 0 ) {

		$prop_id = $wpdb->get_results("SELECT ID FROM `wp_posts` WHERE 1 = 1 AND post_type = 'estate_property' AND post_status IN ('disabled','pending','publish','draft','expried') AND post_author = '".$host_id."'",ARRAY_A);

		$prop_id_column = array_column($prop_id, 'ID');
		$prop_id_column = search_by_property_title( $prop_id_column, $host_id );
    	// print_r(count($prop_id_column));
    	$total_pages = ceil( count( $prop_id_column ) / $per_page );
		
		$page_query = $wpdb->get_results("SELECT ID FROM `wp_posts` WHERE 1=1 AND post_type = 'estate_property' AND post_author = '$host_id' GROUP BY ID DESC LIMIT ".$offset.", ".$per_page." ",ARRAY_A);

		$page_query = array_column($page_query, 'ID');
		$page_query = search_by_property_title( $page_query, $host_id );

		for ($i=0; $i < count($page_query) ; $i++) { 
				
			$term = get_the_terms($page_query[$i] , 'property_category');
			if ( !empty( $term ) ) {
					$term_data   = $term[0]->name;
			} else {
					$term_data   = '';
			}
			$city = get_the_terms($page_query[$i] , 'property_city');
			if ( !empty( $city ) ) {
					$city_data   = $city[0]->name;
			} else {
					$city_data   = '';
			}
			$area_term = get_the_terms($page_query[$i] , 'property_area');

			if ( !empty( $area_term ) ) {
					$area_term_data   = $area_term[0]->name;
			} else {
					$area_term_data   = '';
			}
		    // Property Image URL
	        $post_thumb_id  = get_post_thumbnail_id( $page_query[$i] );
	        $post_thumb_url = wp_get_attachment_image_url($post_thumb_id, 'full');
	        if ($post_thumb_url == false) {
	            $image = "";
	        } else {
	            $image = $post_thumb_url;
	        }
	        $featured =   intval  ( get_post_meta($page_query[$i], 'prop_featured', true) );
			if ( $featured == 1 ) {
               $featured_text = 'featured';
            } else {
            	$featured_text = '';
            }
		
			$data[$i]['ID']     = $page_query[$i];
			$data[$i]['name']   = get_the_title($page_query[$i]);
			$data[$i]['status'] = get_post_status($page_query[$i]);
			$data[$i]['city']   = $city_data;
			$data[$i]['property_type'] = $term_data;
			$data[$i]['property_area'] = $area_term_data;
			$data[$i]['image_url']     = $image;
			$data[$i]['featured'] 	   = $featured_text;
		  
		}
		
	} else {
		return new WP_REST_Response( array( 'response_code' => '400', 'message' => esc_html__( 'Please enter valid id!', 'wpestate' ) ), 400 );
	}
	if ( !empty( $data ) ) {
		return new WP_REST_Response(
			      	array(
			          'response_code' => "200", 
			          'current_page'  => "$current_page",
			          'per_page'      => $per_page,
			          'total_pages'   => "$total_pages",
			          'data'          => $data,
			      	),
		  		200);
	} else {
		return new WP_REST_Response(
			      	array(
			          'response_code'  => "404", 
			          'message'        => esc_html__('You have not property available!')
			      	),
		  		404);
	}
}
function search_by_property_title( $property_id, $host_id ) {
    
    global $wpdb;

    $issearch = sanitize_text_field( filter_input( INPUT_POST, 'is_search' ) );

    if ( isset( $issearch ) && !empty( $issearch ) ) {
    	 
        $search_by_property = $wpdb->get_results("SELECT ID FROM `wp_posts` WHERE (CONVERT(`post_title` USING utf8) LIKE '%$issearch%') AND post_author = '$host_id' AND post_type = 'estate_property' AND post_status IN ('disabled','pending','publish','draft','expried') GROUP BY ID", ARRAY_A);
        $propID = array_column($search_by_property, 'ID');

        if (empty($propID)) {
            return;
        }

        for ( $i = 0; $i < count( $property_id ); $i++ ) { 

            for ( $x = 0; $x < count( $propID ); $x++ ) {

                $p_ID = $property_id[$i];
                if ( $p_ID == $propID[$x] ) {
                    $is_search[] = $propID[$x];
                }
            }
        }
    }
    if ( !empty( $is_search ) ) {
        return $is_search;
    } else {
        return $property_id;
    }
}