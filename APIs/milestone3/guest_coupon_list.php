<?php

/*
 * GUEST COUPON LIST API
 */

add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/guest-coupon-list',
        array(
            'methods' => 'GET',
            'callback'=> 'guest_coupon_list'
        )
    );
});

function guest_coupon_list() {

	// Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);

    if ( empty( $headers['token'] ) || $headers['token'] != $token ) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

    $per_page     = $_GET['per_page'];
    $current_page = $_GET['current_page'];
    $per_page     = ( !isset( $per_page ) || $per_page == '' || $per_page == 0 ) ? 1 : $per_page;
    $current_page = ( $current_page == '' || $current_page == 0 ) ? 1 : (int)$current_page;
    // $per_page     = sanitize_text_field( filter_input( INPUT_POST, 'per_page' ) );
    // $current_page = sanitize_text_field( filter_input( INPUT_POST, 'current_page' ) );
    // $per_page     = 100;

    $offset = ( $current_page - 1 ) * $per_page;


    global $wpdb;
    // SELECT wp_coupon_record.id, wp_coupon_record.prop_id, wp_posts.post_title AS property_title, wp_coupon_record.coupon_code, wp_coupon_record.start_date, DATE_FORMAT(wp_coupon_record.end_date, '%b %D %Y') AS end_date, wp_coupon_record.discount, wp_coupon_record.discount_type, DATE_FORMAT(wp_coupon_guest.assign_date, '%b %D %Y') AS assign_date FROM `wp_coupon_record` INNER JOIN wp_coupon_guest ON wp_coupon_record.id = wp_coupon_guest.coupon_id INNER JOIN wp_posts ON wp_coupon_record.prop_id = wp_posts.ID WHERE 1=1 AND wp_coupon_guest.guest_id = 484 AND wp_coupon_record.coupon_status = 'Active' AND MONTH( wp_coupon_record.start_date ) = MONTH(NOW())
    $guest_coupons = $wpdb->get_results( "SELECT wp_coupon_record.id, wp_coupon_record.prop_id, wp_posts.post_title AS property_title, wp_coupon_record.coupon_code, DATE_FORMAT(wp_coupon_record.start_date, '%b %D %Y') AS start_date, DATE_FORMAT(wp_coupon_record.end_date, '%b %D %Y') AS end_date, wp_coupon_record.discount, wp_coupon_record.discount_type, DATE_FORMAT(wp_coupon_guest.assign_date, '%b %D %Y') AS assign_date FROM `wp_coupon_record` INNER JOIN wp_coupon_guest ON wp_coupon_record.id = wp_coupon_guest.coupon_id INNER JOIN wp_posts ON wp_coupon_record.prop_id = wp_posts.ID WHERE 1=1 AND wp_coupon_guest.guest_id = $token_id[1] AND wp_coupon_record.coupon_status = 'Active' ORDER BY wp_coupon_record.id DESC LIMIT ".$offset.", ".$per_page."", ARRAY_A );

    $total_coupon = $wpdb->get_var( "SELECT COUNT( wp_coupon_record.id ) FROM `wp_coupon_record` INNER JOIN wp_coupon_guest ON wp_coupon_record.id = wp_coupon_guest.coupon_id INNER JOIN wp_posts ON wp_coupon_record.prop_id = wp_posts.ID WHERE 1=1 AND wp_coupon_guest.guest_id = $token_id[1] AND wp_coupon_record.coupon_status = 'Active' " );
    $total_page = ceil( $total_coupon / $per_page );
    
    if ( !empty( $guest_coupons ) ) {
        return new WP_REST_Response(
            array( 
                'response_code' => '200', 
                'current_page'  => $current_page,
                'per_page'      => $per_page,
                'total_page'    => $total_page,
                'data'          => $guest_coupons 
            ), 200 );
    } else {
        return new WP_REST_Response(array('response_code' => '404', 'message' => esc_html__('No data found', 'wpestate') ), 404);
    }
}   