<?php

/**
 * CHAT MESSANGER API
 */
// up_push_new_message
add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/chat-messanger',
        array(
            'methods' => 'POST',
            'callback'=> 'chat_messanger'
        )
    );
});

function chat_messanger() {

	// Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);

    if ( empty( $headers['token'] ) || $headers['token'] != $token ) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END
	
	/* START CHAT HISTORY */
	$conversation_id      = sanitize_text_field( filter_input( INPUT_POST, 'conversation_id' ) );
	$chat_message = sanitize_text_field( filter_input( INPUT_POST, 'chat_message' ) );

	// $chat_message = "Hey we are testing final...to store message...!..to pull message from Database...";
	
	if ( !is_numeric( $conversation_id ) || empty( $conversation_id ) ) {
		return new WP_REST_Response( array( 'response_code' => '400', 'message' => esc_html__( 'Conversion id allow only numeric!', 'wpestate' ) ), 400);
	} 
	// if ( !isset( $chat_message ) || empty( $chat_message) ) {
	// 	return new WP_REST_Response( array( 'response_code' => '400', 'message' => esc_html__( 'Please enter message!', 'wpestate' ) ), 400);
	// }

	// $conv_id = get_receiver_id_from_conversation_id( 670 );
	$conv_id = get_receiver_id_from_conversation_id( $conversation_id );
	// print_r($conv_id);

	/** START DO STORE THE MESSAGES FROM CHAT MESSANGER **/
	if ( isset( $chat_message ) && !empty( $chat_message) && isset( $conv_id )) {
		$message['conv_id']     = $conv_id['id'];
		$message['sender_id']   = $conv_id['sender'];
		$message['reciever_id'] = $conv_id['reciever'];
		$message['message']     = $chat_message;
		do_store_message( $message );

		/* Send Push Notification to device */
		// $to      = 'hirenc.webdesk@gmail.com';
	 //    $from    = get_option('admin_email');
	 //    $subject = "From chat message";
	 //    $message = "<p> Message ".$chat_message."</p>"
	 //    		   "<p> Sender ID ".$conv_id['sender']."</p>"
	 //    		   "<p> Reciever ID ".$conv_id['reciever']."</p>";
	 //    $headers = array('Content-Type: text/html; charset=UTF-8',"From:" . $from);
	 //    wp_mail( $to, $subject, $message, $headers );
		// echo "Reciever ID ".$conv_id['reciever'];
		// $device_id    = get_user_meta($token_id[1], 'device_token', true);
		$device_id    = get_user_meta($conv_id['reciever'], 'device_token', true);
		$user_id = $token_id[1];
		$msg_data     = array(
			'message' => $chat_message,
			'user_id' => $user_id
		);
		FCM_push_notification( $device_id, $msg_data );
	}

	/** END DO STORE THE MESSAGES FROM CHAT MESSANGER **/

	global $wpdb;
	$all_messagess = $wpdb->get_results("SELECT message,created_at FROM `wp_yobro_messages` WHERE conv_id = ".$conversation_id." ", ARRAY_A);

	if ( empty( $all_messagess ) ) {
		return new WP_REST_Response( array( 'response_code' => '404', 'message' => esc_html__( 'No any one message', 'wpestate' ) ), 404);
	}

	$data_msg     = array();
	for ($i=0; $i < count( $all_messagess ); $i++) {
			
		$do_store_sender_message = encrypt_decrypt($all_messagess[$i]['message'], $conv_id['sender'], 'decrypt');
		$do_store_receive_message = encrypt_decrypt($all_messagess[$i]['message'], $conv_id['reciever'], 'decrypt');

		if ( !isset( $do_store_receive_message ) || empty( $do_store_receive_message ) ) {

			$data_msg[ $i ]['send_receiver']     = "0";
			$data_msg[ $i ]['created_at'] = strtotime($all_messagess[$i]['created_at']);
			$data_msg[ $i ]['message']    = strip_tags( $do_store_sender_message );

		} else if( !isset( $do_store_sender_message ) || empty( $do_store_sender_message )  ) {

			$data_msg[ $i ]['send_receiver']   = "1";
			$data_msg[ $i ]['created_at'] = strtotime( $all_messagess[$i]['created_at'] );
			$data_msg[ $i ]['message']    = strip_tags( $do_store_receive_message );
		}

	}
	//if(!empty($data_msg)){
	return new WP_REST_Response( array( 'response_code' => '200', 'data' => array_values( $data_msg ) ), 200);
	/*}else{
	return new WP_REST_Response( array( 'response_code' => '200', 'data' =>  empty($data_msg ), 200);

	}*/
	/* END CHAT HISTORY */
}