<?php

/*
 * EDIT NOTIFICATION OPTIONS SETTING API
 * CAN EDIT HOST & GUEST BOTH SIDE
 */

add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/edit-notification-options',
        array(
            'methods' => 'POST',
            'callback'=> 'edit_notification_options'
        )
    );
});

function edit_notification_options() {

	// Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);

    if ( empty( $headers['token'] ) || $headers['token'] != $token ) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

    $all                   = sanitize_text_field( filter_input( INPUT_POST, 'all' ) );
    $new_reservation       = sanitize_text_field( filter_input( INPUT_POST, 'new_reservation' ) );
    $relocated_reservation = sanitize_text_field( filter_input( INPUT_POST, 'relocated_reservation' ) );
    $cancelled_reservation = sanitize_text_field( filter_input( INPUT_POST, 'cancelled_reservation' ) );
    $new_reviews = sanitize_text_field( filter_input( INPUT_POST, 'new_reviews' ) );
    $guest_chat_messages   = sanitize_text_field( filter_input( INPUT_POST, 'guest_chat_messages' ) );
    $admin_messages        = sanitize_text_field( filter_input( INPUT_POST, 'admin_messages' ) );
    $new_coupons           = sanitize_text_field( filter_input( INPUT_POST, 'new_coupons' ) );

    $guest_emails          = sanitize_text_field( filter_input( INPUT_POST, 'guest_emails' ) );
    $sold_out_rooms        = sanitize_text_field( filter_input( INPUT_POST, 'sold_out_rooms' ) );


    $user_type = get_user_meta( $token_id[1], 'user_type', true );

    // START VALIDATION
    if (!is_numeric( $all ) || $all > 1 ||
        !is_numeric( $new_reservation ) || $new_reservation > 1 || 
        !is_numeric( $relocated_reservation ) || $relocated_reservation > 1 || 
        !is_numeric( $cancelled_reservation ) || $cancelled_reservation > 1 || 
        !is_numeric( $new_reviews ) || $new_reviews > 1 || 
        !is_numeric( $guest_chat_messages ) || $guest_chat_messages > 1 || 
        !is_numeric( $admin_messages ) || $admin_messages > 1 /*|| 
        !is_numeric( $new_coupons ) || $new_coupons > 1*/ ) {
        
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Should be in numeric value and allow only 0 or 1', 'wpestate')), 400);
    }


    if ( isset( $user_type ) && $user_type == 1  ) {
        /* GUEST SIDE VALIDATION */
        if ( isset( $all ) && $all == 1 && !empty($all) )  {
            
            if ( $new_reservation == 0 || $relocated_reservation == 0 ||  $cancelled_reservation == 0 || $new_reviews == 0 || $guest_chat_messages == 0 || $admin_messages == 0 || $new_coupons == 0 ) {
                return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Should be all enable Guest', 'wpestate')), 400);
            } 
        }

        /* UPDATE GUEST NOTIFICATION OPTIONS LIST */

        $update_options['guest_user']['all']                   = $all;
        $update_options['guest_user']['new_reservation']       = $new_reservation;
        $update_options['guest_user']['relocated_reservation'] = $relocated_reservation;
        $update_options['guest_user']['cancelled_reservation'] = $cancelled_reservation;
        $update_options['guest_user']['new_reviews']           = $new_reviews;
        $update_options['guest_user']['guest_chat_messages']   = $guest_chat_messages;
        $update_options['guest_user']['admin_messages']        = $admin_messages;
        $update_options['guest_user']['new_coupons']           = $new_coupons;

        /* END UPDATE GUEST NOTIFICATION OPTIONS LIST */

    } elseif ( isset( $user_type ) && $user_type == 0 ) {
        
        /* HOST SIDE VALIDATION */
        if (!is_numeric( $guest_emails )   || $guest_emails > 1 ||
            !is_numeric( $sold_out_rooms ) || $sold_out_rooms > 1 ) {
            return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Should be in numeric value and allow only 0 or 1', 'wpestate')), 400);
        }
        if ( isset( $all ) && $all == 1 && !empty($all) )  {
        
            if ( $new_reservation == 0 || $relocated_reservation == 0 ||  $cancelled_reservation == 0 || $new_reviews == 0 || $guest_chat_messages == 0 || $admin_messages == 0 || /*$new_coupons == 0 ||*/ $sold_out_rooms == 0 ) {
                return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Should be all enable Host', 'wpestate')), 400);
            }

        }

        /* UPDATE HOST NOTIFICATION OPTIONS LIST */
        $update_options['host_user']['all']                   = $all;
        $update_options['host_user']['new_reservation']       = $new_reservation;
        $update_options['host_user']['relocated_reservation'] = $relocated_reservation;
        $update_options['host_user']['cancelled_reservation'] = $cancelled_reservation;
        $update_options['host_user']['new_reviews']           = $new_reviews;
        $update_options['host_user']['guest_chat_messages']   = $guest_chat_messages;
        $update_options['host_user']['guest_emails']          = $guest_emails;
        $update_options['host_user']['admin_messages']        = $admin_messages;
        $update_options['host_user']['sold_out_rooms']        = $sold_out_rooms;
        
        /* END UPDATE HOST NOTIFICATION OPTIONS LIST */
    }
    
    // END VALIDATION
    update_user_meta( $token_id[1], 'notify_options_setting', serialize( $update_options ) );
    $option_list = unserialize( get_user_meta( $token_id[1], 'notify_options_setting', true ) );
    $data = array();
    if ( isset( $user_type ) && $user_type == 0 ) {
        foreach ($option_list['host_user'] as $key => $value) {
            $notify_title = preg_replace("/[\s_]/", " ", $key);
            $data[] = array(
                                    'key'  => $key,
                                    'value'=> $value,
                                    'title'=> ucwords( $notify_title )
                                );
        }
    } elseif ( isset( $user_type ) && $user_type == 1 ) {
        foreach ( $option_list['guest_user'] as $key => $value ) {
            $notify_title = preg_replace("/[\s_]/", " ", $key);
            $data[] = array(
                                    'key'  => $key,
                                    'value'=> $value,
                                    'title'=> ucwords( $notify_title )
                                );

        }
    }

    if ( !empty( $data ) ) {
        
        return new WP_REST_Response( array( 'response_code' => '200', 'data' => $data ), 200 );
    } else {
        return new WP_REST_Response( array( 'response_code' => '404', 'message' => esc_html__('Something went wrong!', 'wpestate' ) ), 404 );
    }
}