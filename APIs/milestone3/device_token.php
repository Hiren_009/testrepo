<?php

/**
 * DEVICE TOKEN API
 */

add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/device-token',
        array(
            'methods' => 'POST',
            'callback'=> 'device_token'
        )
    );
});

function device_token() {

    

	// Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);

    if ( empty( $headers['token'] ) || $headers['token'] != $token ) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

    /** UPDATE DEVICE TOKEN & TYPE **/
    $device_token = sanitize_text_field( filter_input( INPUT_POST, 'device_token' ) );
    $device_type  = sanitize_text_field( filter_input( INPUT_POST, 'device_type' ) );

    if ( !isset( $device_token ) || empty( $device_token ) ) {
    	return new WP_REST_Response(
            array(
                'response_code' => '400',
                'message'       => esc_html__( 'Please enter device token!','wpestate')
            ), 400);

    } elseif ( !isset( $device_type ) || empty( $device_type ) ) {
    	return new WP_REST_Response(
            array(
                'response_code' => '400',
                'message'       => esc_html__( 'Please enter device type!','wpestate')
            ), 400);
    } else {

	    update_user_meta( $token_id[1], 'device_token', $device_token );
	    update_user_meta( $token_id[1], 'device_type', $device_type );
	    return new WP_REST_Response(
            array(
                'response_code' => '200',
                'device_token'  => $device_token,
                'device_type'   => $device_type

            ), 200);
    }
}