<?php

/**
 * Extra Options Fees API
 */

add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/extra-options',
        array(
            'methods' => 'GET',
            'callback'=> 'extra_options',
        )
    );
});

function extra_options() {

    // Check Oath Token
    $headers = apache_request_headers();
    $token_id =  explode( "-qe_aw-", $headers['token'] );
    $token = get_user_meta($token_id[1], 'oauth_token', true);
    $user_id = 16;
    if (empty($headers['token']) || $headers['token'] != $token) {
        
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

    $property_id  =  $_GET['id'];
    // $property_id  =  8230;
    // $property_id       =  8197;
    // $property_id       =  8208;

    // if ( !isset($property_id) && !is_numeric($property_id)) {
    //     return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Only numeric value allowed!', 'wpestate')), 400);
    // }
    $currency          = esc_html( get_option('wp_estate_currency_label_main', '') );
    $extra_pay_options = get_post_meta($property_id, 'extra_pay_options', true);
    $options_array = array(
        0   =>  esc_html__('Single Fee','wpestate'),
        1   =>  esc_html__('Per Night','wpestate'),
        2   =>  esc_html__('Per Guest','wpestate'),
        3   =>  esc_html__('Per Night per Guest','wpestate')
    );
    $extra_opt = array();
    
    if ( !empty($extra_pay_options) ) {
        
        for ($i=0; $i < count($extra_pay_options); $i++) { 
            $service_title = preg_replace("/[\s_]/", "_", $extra_pay_options[ $i ][0]);
            // print_r(strtolower($service_title));
            $extra_opt[ $i ]['service_id']           = strtolower($service_title);
            $extra_opt[ $i ]['service_title']        = $extra_pay_options[ $i ][0];
            if ( $extra_pay_options[ $i ][1] == 0 ) {
                $extra_opt[ $i ]['service_charge']       = esc_html__('Free', 'wpestate');
            }  else {
                $extra_opt[ $i ]['service_charge']       = $currency.$extra_pay_options[ $i ][1];
            }
            // echo "--".$extra_pay_options[ $i ][2];
            if ( $extra_pay_options[ $i ][2] != undefined ) {
                
                $extra_opt[ $i ]['service_charge_title'] = $options_array[ $extra_pay_options[ $i ][2] ];
            } else {
                $extra_opt[ $i ]['service_charge_title'] = "";
            }
            // $extra_opt[ $i ]['service_charge_id']    = $extra_pay_options[ $i ][2];
        }

        return new WP_REST_Response(
            array(
                'response_code'  => "200", 
                'data' => $extra_opt,
            ),
        200);
    
    } else {
        return new WP_REST_Response(
            array(
                'response_code'  => '404',
                'message'=>esc_html__( 'Extra Options not available','wpestate')
            ), 404);
    }
}