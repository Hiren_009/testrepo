<?php

/**
 * Stripe Payment Gateway API
 */

add_action('rest_api_init', function(){
	register_rest_route('tvcapi', '/v2/stripe-pament',
		array(
			'methods' => 'POST',
			'callback'=> 'stripe_pament',
		)
	);
});

function stripe_pament() {

	// Check Oath Token
  	$headers  = apache_request_headers();
  	$token_id = explode( "-qe_aw-", $headers['token'] );
  	$token    = get_user_meta($token_id[1], 'oauth_token', true);
  	$user_id  = $token_id[1];

  	if (empty($headers['token']) || $headers['token'] != $token) {
      return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
  	}
  	// END

	$booking_id  =  sanitize_text_field ( filter_input ( INPUT_POST, 'booking_id' ) );
	$invoice_id  =  sanitize_text_field ( filter_input ( INPUT_POST, 'invoice_id' ) );
	$amount      =  sanitize_text_field ( filter_input ( INPUT_POST, 'amount' ) );
	$amount      =  $amount * 100;
	$LRP_status  =  sanitize_text_field ( filter_input ( INPUT_POST, 'LRP_status' ) );

    /*
	 * Stripe Payment Integration key 
     */
    require_once get_template_directory().'/libs/stripe/lib/Stripe.php';
	$allowed_html = array();
	
	$stripe_secret_key              =   esc_html( get_option('wp_estate_stripe_secret_key','') );
	$stripe_publishable_key         =   esc_html( get_option('wp_estate_stripe_publishable_key','') );
	$stripe = array(
	    "secret_key"      => $stripe_secret_key,
	    "publishable_key" => $stripe_publishable_key
	);

	Stripe::setApiKey($stripe['secret_key']);   
    /*
	 * END Stripe Payment Integration key
	 * START VALIDATIONS
     */
	if ( !isset( $booking_id ) || empty( $booking_id ) || !is_numeric( $booking_id ) ) {
		return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Please enter booking id.', 'wpestate')), 401);
	}
	if ( !isset( $invoice_id ) || empty( $invoice_id ) || !is_numeric( $invoice_id ) ) {
		return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Please enter invoice id', 'wpestate')), 401);
	} 
	if ( !isset( $amount ) || empty( $amount ) || !is_numeric( $amount ) ) {
		return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Please set amount', 'wpestate')), 401);
	}
	$LRP_status = empty( $LRP_status ) ? $LRP_status = 0 : $LRP_status;
	if ( !isset( $LRP_status ) || !is_numeric( $LRP_status )  ) {
		return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Please set loyalty reward point status 0 or 1', 'wpestate')), 401);
	}

	/* START IF IS EXIST BOOKED B_ID OR NOT */
    global $wpdb;
    $b_id = $wpdb->get_var( "SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN wp_postmeta ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1=1 AND `wp_posts`.`ID` = $booking_id AND `wp_posts`.`post_type` = 'wpestate_booking' AND `wp_posts`.`post_status` = 'publish' AND ( (`wp_postmeta`.`meta_key` = 'booking_status' AND `wp_postmeta`.`meta_value` = 'confirmed' ) ) " );
    // print_r($b_id);
    if ( !empty( $b_id ) && $b_id == $booking_id ) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Already booked that are you using by booking id.', 'wpestate')), 401);
    }
    if ( get_post_status( $booking_id ) === FALSE ) {
        
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Please set booking id true.', 'wpestate')), 401);
    }
    /* END IF IS EXIST BOOKED B_ID OR NOT */

	// END VALIDATIONS
    

    $current_user   =   get_userdata($user_id);
	$userID         =   $user_id;
	$user_email     =   $current_user->user_email;
	$username       =   $current_user->user_login;
	$submission_curency_status = esc_html( get_option('wp_estate_submission_curency','') );

	if ( isset( $booking_id ) ){
	    try {    

	        $token        = wp_kses($_POST['stripeToken'],$allowed_html);
	        $customer = Stripe_Customer::create(array(
	            // 'email' => $stripeEmail,
	            'card'  => $token
	        ));
	        // print_r($customer );
	        // die();
	        $userId     = $user_id;
	        $booking_id = $booking_id;
	        $depozit    = $amount;
	        $charge     = Stripe_Charge::create(array(
	            'customer' => $customer->id,
	            'amount'   => $depozit,
	            'currency' => $submission_curency_status
	        ));
	       	// print_r($charge);
	        $is_stripe    = 1;
	        $point_status = $LRP_status;
	       
	        wpestate_booking_mark_confirmed($booking_id,$invoice_id,$userId,$depozit,$user_email,$is_stripe, $point_status);  // $point_status added by vinod
	    	
	    	// return new WP_REST_Response( array( 'response_code' => '200', 'message' => esc_html__('Payment successfully, Enjoy your trip!') ), 200);
	    	print json_encode( array( 'response_code' => '200', 'message' => esc_html__('Payment successfully, Enjoy your trip!') ) );
	    	die();
	    }
	    catch ( Exception $e ) {
	        // return array( 'error' => $e->getMessage() );
	        print json_encode( array( 'error' => $e->getMessage() ) );
	    	die();
	    }
	}
}