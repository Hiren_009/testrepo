<?php

/**
 * Favorite Properties API
 */

add_action('rest_api_init', function() {
    register_rest_route('tvcapi', '/v2/favorite-properties',
        array(
            'methods'  => 'POST',
            'callback' => 'favorite_properties',
        )
    );
});

function favorite_properties() {
	global $wpdb;
	// Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);

    if (empty($headers['token']) || $headers['token'] != $token) {
        // Error Message
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

    $user_option = 'favorites'.$token_id[1];
    $prop_id     = intval( $_POST['property_id']);
    $prop_id = $wpdb->get_row("SELECT * FROM wp_posts WHERE id = '" . $prop_id . "'", ARRAY_A);

    if( !isset( $prop_id['ID'] ) || empty($prop_id['ID']) || !is_numeric($prop_id['ID'])) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Something went wrong!', 'wpestate')), 400);
    }
    
    $curent_fav=get_option($user_option);
    
    if($curent_fav=='') { // if empy / first time
        $fav=array();
        $fav[]=$prop_id['ID'];
        update_option($user_option,$fav);
        // echo json_encode(array('added'=>true, 'response'=>esc_html__( 'addded','wpestate')));
        return new WP_REST_Response (
            array(
                "response_code" => '200',
                'is_favorite'   => true,
                'message' => esc_html__('Added to favorites list', 'wpestate')
            ),
        200);
    } else {
        if ( ! in_array ($prop_id['ID'],$curent_fav) ) {
            $curent_fav[]=$prop_id['ID'];                  
            update_option($user_option,$curent_fav);
            // echo json_encode(array('added'=>true, 'response'=>esc_html__( 'addded','wpestate')));
            return new WP_REST_Response (
                array(
                    "response_code" => '200',
                    'is_favorite'   => true,
                    'message' => esc_html__('Added to favorites list', 'wpestate')
                ),
            200);
        } else {
            if(($key = array_search($prop_id['ID'], $curent_fav)) !== false) {
                unset($curent_fav[$key]);
            }
            update_option($user_option,$curent_fav);
            // echo json_encode(array('added'=>false, 'response'=>esc_html__( 'removed','wpestate')));
            return new WP_REST_Response (
                array(
                    "response_code" => '200',
                    'is_favorite'   => false,
                    'message' => esc_html__('Removed to favorites list', 'wpestate')
                ),
            200);
        }
    }     
    die();
}