<?php
/**
 * AVAILABLE LRP API
 */

add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/available-LRP',
        array(
            'methods' => 'GET',
            'callback'=> 'available_LRP',
        )
    );
});

function available_LRP() {

	// Check Oath Token
	$headers  = apache_request_headers();
	$token_id = explode( "-qe_aw-", $headers['token'] );
	$token    = get_user_meta($token_id[1], 'oauth_token', true);

	if (empty($headers['token']) || $headers['token'] != $token) {
    return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
	}

	// AVAILABLE LRP IN DOLLERS
  // $LRP_data = LRP_and_member( $token_id[1] );
  $user_LRP  = get_user_meta($token_id[1], 'loyalty_reward_point', true);
  
	return new WP_REST_Response(array('response_code' => '200', 'available_LRP' => $user_LRP ), 200);
}