<?php

/**
 * BRAINTREE PAYMENT GATEWAY API
 */

add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/braintree-payment',
        array(
            'methods' => 'POST',
            'callback'=> 'braintree_payment',
        )
    );
});

function braintree_payment() {

	// Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);
    $user_id  = 87;
    $user_email = get_user_by('id', $token_id[1]);
    $user_email = $user_email->user_email;
    
    if (empty($headers['token']) || $headers['token'] != $token) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }


    /*
     * START VALIDATIONS
     */
    $nonce       =  $_POST["payment_method_nonce"];
    $booking_id  =  sanitize_text_field ( filter_input ( INPUT_POST, 'booking_id' ) );
	$invoice_id  =  sanitize_text_field ( filter_input ( INPUT_POST, 'invoice_id' ) );
	$amount      =  sanitize_text_field ( filter_input ( INPUT_POST, 'amount' ) );
	// $amount      =  str_replace(',', '', $amount);
	$LRP_status  =  sanitize_text_field ( filter_input ( INPUT_POST, 'LRP_status' ) );

	if ( !isset( $nonce ) || empty( $nonce ) ) {
		return new WP_REST_Response(array('response_code' => '422', 'message' => esc_html__('Please set nonce.', 'wpestate')), 422);
	}

	if ( !isset( $booking_id ) || empty( $booking_id ) || !is_numeric( $booking_id ) ) {
		return new WP_REST_Response(array('response_code' => '422', 'message' => esc_html__('Please enter booking id.', 'wpestate')), 422);
	}
	if ( !isset( $invoice_id ) || empty( $invoice_id ) || !is_numeric( $invoice_id ) ) {
		return new WP_REST_Response(array('response_code' => '422', 'message' => esc_html__('Please enter invoice id', 'wpestate')), 422);
	} 
	if ( !isset( $amount ) || empty( $amount ) || !is_numeric( $amount ) && $amount == 0 ) {
		return new WP_REST_Response(array('response_code' => '422', 'message' => esc_html__('Please set amount', 'wpestate')), 422);
	}
	$LRP_status = empty( $LRP_status ) ? $LRP_status = 0 : $LRP_status;
	if ( !isset( $LRP_status ) || !is_numeric( $LRP_status )  ) {
		return new WP_REST_Response(array('response_code' => '422', 'message' => esc_html__('Please set loyalty reward point status 0 or 1', 'wpestate')), 422);
	}

    /* START IF IS EXIST BOOKED B_ID OR NOT */
    global $wpdb;
    $b_id = $wpdb->get_var( "SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN wp_postmeta ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1=1 AND `wp_posts`.`ID` = $booking_id AND `wp_posts`.`post_type` = 'wpestate_booking' AND `wp_posts`.`post_status` = 'publish' AND ( (`wp_postmeta`.`meta_key` = 'booking_status' AND `wp_postmeta`.`meta_value` = 'confirmed' ) ) " );
    // print_r($b_id);
    if ( !empty( $b_id ) && $b_id == $booking_id ) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Already booked that are you using by booking id.', 'wpestate')), 401);
    }
    if ( get_post_status( $booking_id ) === FALSE ) {
        
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Please set booking id true.', 'wpestate')), 401);
    }
    /* END IF IS EXIST BOOKED B_ID OR NOT */
	// END VALIDATIONS

    // ---------------------- START Braintree test --------------------------------

    $keys = braintree_keys();

    require_once get_stylesheet_directory().'/libs/sprint2API/braintree_php-3.40.0/lib/Braintree.php';

    $gateway = new Braintree_Gateway([
        'environment' => $keys['braintree_env'],
        'merchantId'  => $keys['merchantId'],
        'publicKey'   => $keys['publicKey'],
        'privateKey'  => $keys['privateKey']
    ]);

    // or like this:
    $config = new Braintree_Configuration([
        'environment' => $keys['braintree_env'],
        'merchantId'  => $keys['merchantId'],
        'publicKey'   => $keys['publicKey'],
        'privateKey'  => $keys['privateKey']
    ]);

    $gateway = new Braintree\Gateway($config);
    
    // Then, create a transaction:
    $result = $gateway->transaction()->sale([
        'amount' => $amount,
        'paymentMethodNonce' => $nonce,
        'options' => [ 'submitForSettlement' => true ]
    ]);

    if ($result->success) {

        wpestate_booking_mark_confirmed($booking_id,$invoice_id,$token_id[1],$amount,$user_email, $LRP_status);
        print json_encode( 
            array( 
                'response_code' => '200', 
                'success' => true, 
                'transaction_id' => $result->transaction->id 
            ) 
        );

    } else if ($result->transaction) {
        print json_encode( 
            array( 
                'response_code' => '200', 
                'code' => $result->transaction->processorResponseCode, 
                'text' => $result->transaction->processorResponseText 
            ) 
        );
    } else {
        $err_handle = array();
        foreach($result->errors->deepAll() AS $error) {
            $err_handle['attribute'] = $error->attribute;
            $err_handle['code']      = $error->code;
            $err_handle['message']   = $error->message;
        }
        print json_encode( array( 'response_code' => '200', 'err_handle' => $err_handle ) );
    }
    die();
    // ---------------------- END Braintree test --------------------------------
    
}