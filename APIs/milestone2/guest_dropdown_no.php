<?php

/**
 * Guests Dropdown No API
 *
 */

add_action('rest_api_init', function() {
	register_rest_route('tvcapi', '/v2/guest-dropdown-no',
		array(
			'methods' => 'GET',
			'callback'=> 'guest_dropdown_no',
		)
	);
});

function guest_dropdown_no() {

	// Check Oath Token
    // $headers = apache_request_headers();
    // $token_id =  explode( "-qe_aw-", $headers['Token'] );
    // $token = get_user_meta($token_id[1], 'oauth_token', true);

    // if (empty($headers['Token']) || $headers['Token'] != $token) {

    //     return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    // }
    // END
	
	$guest_dropdown_no = intval( get_option('wp_estate_guest_dropdown_no','') );

	// $guest_dd_no = array();
	// for ($i=1; $i <= $guest_dropdown_no; $i++) {
		$guest_dd_no[] = array(
			'guest_dropdown_no' => $guest_dropdown_no,
			'term_conditions'   => site_url().'/terms-and-conditions/'
		);
	// }
	// print_r($guest_dd_no);
	return new WP_REST_Response(
        array(
            "response_code" => "200",
            'data' => $guest_dd_no,
        ), 
    200);
}