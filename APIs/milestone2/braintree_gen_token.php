<?php

/**
 * GENERATE clientToken FROM BRAINTREE API
 */

add_action('rest_api_init', function(){
    register_rest_route('tvcapi', '/v2/braintre-gen-clientToken',
        array(
            'methods' => 'GET',
            'callback'=> 'braintre_gen_clientToken',
        )
    );
});

function braintre_gen_clientToken() {
    
    // Check Oath Token
    $headers  = apache_request_headers();
    $token_id = explode( "-qe_aw-", $headers['token'] );
    $token    = get_user_meta($token_id[1], 'oauth_token', true);
    
    if (empty($headers['token']) || $headers['token'] != $token) {
        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END

	// $keys = braintree_keys();

	require_once get_stylesheet_directory().'/libs/APIs/braintree/lib/Braintree.php';
    
    // $gateway = new Braintree_Gateway([
    //     'environment' => $keys['braintree_env'],
    //     'merchantId'  => $keys['merchantId'],
    //     'publicKey'   => $keys['publicKey'],
    //     'privateKey'  => $keys['privateKey']
    // ]);
    $gateway = new Braintree_Gateway([
        'environment' => 'sandbox',
        'merchantId'  => '97yqwknksbfjd6yj',
        'publicKey'   => 'krc4yxycsw77z6pq',
        'privateKey'  => '7b36f3b0afc7cc5f43cf58006bc1d2d0'
    ]);
    echo "33333";
    $clientToken = $gateway->clientToken()->generate();

    return new WP_REST_Response( array( 'response_code' => '200', 'clientToken' => $clientToken ), 200);
}
