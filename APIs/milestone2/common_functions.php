<?php

/*
 * COUNT LOYALTY REWARD POINTS FUNCTION
 * 1. count_rewards_points()
 * 2. show_loyalty_reward_points()
 * 3. show_loyalty_reward_points()
 * This 3 functions using in INSTANT BOOKING & APPLY REWARDS POINTS API
 */
function count_rewards_points( $user_id, $apply_reward_points ) {
    
    $price_response   = book_property( $data );
    $price_response   = $price_response->data['data'];
    $user_total_point = get_user_meta($user_id, 'loyalty_reward_point', true);
    // $total_point      = preg_replace('/[^\d.]/', '', $price_response['total_price']);

    $total_point = str_replace('$', '', $price_response['total_price']);
    $total_point = str_replace(',', '', $total_point);

    /* 
     * RESET LOYALTY REWARD POOINTS AFTER APPLIED REWARD POINTS IN TOTAL PRICE
     * CALCULATE AVAILABLE TOTAL REWARD POINTS
     */
    $avail_point     =  $user_total_point;
    $avail_point_cal =  $user_total_point / 100;

    if ( !empty( $avail_point_cal ) ) {
        $avail_point_cal = number_format( $avail_point_cal, 2 );
    } else {
        $avail_point_cal = "";
    }

    // FINAL TOTAL AFTER APPLIED REWARD POINTS
    if ( isset( $apply_reward_points ) && $apply_reward_points == 1 ) {
        
        $total_pri = str_replace('$', '', $price_response['total_price']);
        $total_pri = str_replace(',', '', $total_pri);
        // $total_pri = preg_replace('/[^\d.]/', '', $price_response['total_price']);

        $avail_point_calc = str_replace(',', '', $avail_point_cal);
        $avail_point_calc = str_replace(',', '', $avail_point_cal);

        $apply_points = $total_pri - $avail_point_calc;
        $apply_points = $apply_points > 0 ? $apply_points : 0;

    } elseif ( isset( $apply_reward_points ) && $apply_reward_points == 0 ) {
        $apply_points = preg_replace('/[^\d.]/', '', $price_response['total_price']);
    } else {
        $apply_points = "";
    }

    if( $user_total_point == '' ) {
        $user_total_point = 0; 
    }

    if ($user_total_point >= 0 && $user_total_point <= 4999) {       
        
        $per      = 1;
        $getpoint = $total_point * $per; 
        $getpoint = (isset( $apply_reward_points ) == 1 ) ? $apply_points * $per : $getpoint; 
        // $getpoint = $user_total_point * 1; 

    } elseif ($user_total_point >= 4999 && $user_total_point <= 9999) {
        
        $per      = 1.2;
        $getpoint = $total_point * $per;
        // $getpoint = $user_total_point * 1.2;
        $getpoint = (isset( $apply_reward_points ) == 1 ) ? $apply_points * $per : $getpoint; 

    } else {
        
        $per      = 1.4;
        $getpoint = $total_point * $per;
        // $getpoint = $total_point * 1.4;
        $getpoint = (isset( $apply_reward_points ) == 1 ) ? $apply_points * $per : $getpoint; 

    }

    return array( 
            'getpoints'       => round( $getpoint ),
            'avail_point_cal' => $avail_point_cal,
            'apply_points'    => $apply_points,
        );
}

/*
 * SHOW LOYALTY REWARD POINTS FUNCTION
 */
function show_loyalty_reward_points( $user_id, $apply_reward_p ) {

    $price_response   = book_property( $data );
    $price_response   = $price_response->data['data'];

    $join     = get_user_meta($user_id, 'join_reward_club', true);
    $currency = esc_html( get_option('wp_estate_currency_label_main', '') );

    $LRP_data = array();
    if ( !empty( $join ) ) {
        $LRP_data['is_join_reward_club'] = $join;
    } else {
        $LRP_data['is_join_reward_club'] = "0";
    }

    $rewardpoits =  count_rewards_points( $user_id, $apply_reward_p );

    // WILL EARN LOYALTY REWARD POINTS
    if ( $join == 1 ) {
        $LRP_data['will_earn_points'] = "".$rewardpoits['getpoints']."";
    } else {
        $LRP_data['will_earn_points'] = "";
    }

    // AVAILABLE REWARD POINTS IN DOLLER
    if ( !empty( $rewardpoits['avail_point_cal'] ) || $rewardpoits['avail_point_cal'] != 0 ) {
        $LRP_data['available_points'] = $currency.$rewardpoits['avail_point_cal'];
    } else {
        $LRP_data['available_points'] = "";
    }

    // FINAL TOTAL PRICE AFTER APPLIED POINTS
    if ( !isset( $apply_reward_p ) || $apply_reward_p == 0 ) {

        if ( empty( $rewardpoits['avail_point_cal'] ) ) {
            $LRP_data['available_points'] =  '';
        } else {
            $LRP_data['available_points'] =  $currency.$rewardpoits['avail_point_cal'];
        }

        $LRP_data['applied_points']   =  "";
        $LRP_data['total_amount']     =  $price_response['total_price'];
        $amount                       =  $price_response['total_price'];
        $LRP_data['is_apply_LRP']     =  0;
    } else {
        $LRP_data['available_points'] =  "0";
        // $LRP_data['applied_points']   =  $currency.$rewardpoits['avail_point_cal'];

        if (empty( $rewardpoits['avail_point_cal'] ) ) {
            $LRP_data['applied_points'] = $rewardpoits['avail_point_cal'];
        } else {
            $LRP_data['applied_points'] = $currency.$rewardpoits['avail_point_cal'];
        }
        
        $LRP_data['total_amount']     =  $currency.$rewardpoits['apply_points'];
        $amount                       =  $rewardpoits['apply_points'];
        $LRP_data['is_apply_LRP']     =  1;
    }

    return $LRP_data;
}

/*
 * EXTRA PROPERTY DETAILS FUNCTION
 */
function extra_details( $property_id, $check_in, $check_out, $booking_id, $invoice_id ) {

    /*
     * START MERGE OTHER PROPERTY DETAILS
     */
    $extra_data = array();

    // BOOKING & INVOICE ID
    $extra_data['booking_id'] = $booking_id;
    $extra_data['invoice_id'] = $invoice_id;
    // Property Title
    $extra_data['property_title'] = get_the_title( ucwords ( $property_id ) );

    // Property mix data
    // Room Type
    $prop_action_category_array = get_the_terms($property_id, 'property_action_category');
    // print_r($prop_action_category_array);
    if( isset( $prop_action_category_array[0] ) ){
        $extra_data['room_type'] = $prop_action_category_array[0]->name;
        // $extra_data['room_type'] = $room_type;

    } else  {
        $extra_data['room_type'] = "";
    }
    // $extra_data['room_type'] = $room_type;
    // Property Type
    $prop_category_array =   get_the_terms($property_id, 'property_category');
    if(isset($prop_category_array[0])){
        $extra_data['property_type'] = $prop_category_array[0]->name;
        // $extra_data['property_type']    = $prop_type;
    } else {
        $extra_data['property_type'] = "";
    }

    $extra_data['property_city']    = strip_tags ( get_the_term_list($property_id, 'property_city', '', ', ', '') );
    $extra_data['property_country'] = get_post_meta($property_id, 'property_country', true);

    $extra_data['fromdate']         = $check_in;
    $extra_data['todate']           = $check_out;

    // $area = esc_html__('in ', 'wpestate').strip_tags ( get_the_term_list($property_id, 'property_city', '', ', ', '') ).'/'.get_post_meta($property_id, 'property_country', true);

    // $extra_data['where_property'] = $rm_type.' '.$prop_type.' '.$area;

    // AVAILABED AMINITES
    $amenities_cats = get_terms( array('taxonomy' => 'amenities','hide_empty' => false,'parent' => 0) );
    $amenities_terms = wp_get_object_terms( $property_id,  'amenities' );

    if ( ! empty( $amenities_terms ) ) {
        if ( ! is_wp_error( $amenities_terms) ) {
            // $i = 1;
            foreach( $amenities_terms as $term ) {

                // if ($i++ < 5) {
                    $cartpage_amms = $term->name;
                    // print '<span class="amms-list">* ' . $cartpage_amms . '</span>';
                    $extra_data['aminities_includes'][] = array( 'key' => $cartpage_amms );
                // }
            }
        }
    } else {
        // $extra_data['aminities_includes'][] = esc_html__('This property not have any aminities', 'wpestate');
        $extra_data['aminities_includes'] = array();
    }

    // REFUNDABLE DEPOSITE DUE ON
    $refundable_deposit_date = get_post_meta($property_id, 'security_deposit', true);
    if ( $refundable_deposit_date != 0 ) {
        $extra_data['refundable_deposit_date'] = esc_html__( 'Due on '.date("M d, Y", $check_out),'wpestate');
    } else {
        $extra_data['refundable_deposit_date'] = "";
    }

    /* CANCELLATION POLICY
     * WHEN USER CANCELLATION MESSAGE BOOKING PROPERTY 
     */
    $cancellation = get_post_meta($property_id, 'cancellation', true);

    $comn_msg0 = esc_html__('Cancel within ', 'wpestate');
    $comn_msg  = esc_html__(' of arrival and get a full refund.', 'wpestate');

    $today = date("M d Y");
    // echo "-- ".$allow = date( 'M d Y', strtotime( '-1 days', $check_out ) );
    if ( $cancellation == 'NonRefundable' ) {

        $extra_data['cancellation_policy'] = $comn_msg0. $cancellation . $comn_msg;
        $extra_data['allow_cancellation_date'] = $cancellation;

    } elseif ( $cancellation == 'Free Refund') {

        $extra_data['cancellation_policy'] = $comn_msg0. $cancellation . $comn_msg;
        $extra_data['allow_cancellation_date'] = $cancellation;

    } elseif ( $cancellation == '24 Hours' ) {

        $extra_data['cancellation_policy'] = $comn_msg0. esc_html__('1 Day', 'wpestate') . $comn_msg;

        $allow = date( 'M d Y', strtotime( '-1 days', $check_in ) );
        if ( strtotime( $today ) <= strtotime( $allow ) ) {
            $extra_data['allow_cancellation_date'] = esc_html__('FREE cancellation before '.'on '.date( 'M d, Y', strtotime($allow) ) );
        } else {
            $extra_data['allow_cancellation_date'] =  '';
        }

    } elseif ( $cancellation == '48 Hours' ) {

        $extra_data['cancellation_policy'] = $comn_msg0. esc_html__('2 Days', 'wpestate') . $comn_msg;

        $allow = date( 'M d Y', strtotime( '-2 days', $check_in ) );
        if ( strtotime( $today ) <= strtotime( $allow ) ) {
            $extra_data['allow_cancellation_date'] = esc_html__('FREE cancellation before '.'on '.date( 'M d, Y', strtotime($allow) ) );
        } else {
            $extra_data['allow_cancellation_date'] =  '';
        }

    } elseif ( $cancellation == '72 Hours' ) {

        $extra_data['cancellation_policy'] = $comn_msg0. esc_html__('3 Days', 'wpestate') . $comn_msg;

        $allow = date( 'M d Y', strtotime( '-3 days', $check_in ) );
        if ( strtotime( $today ) <= strtotime( $allow ) ) {
            $extra_data['allow_cancellation_date'] = esc_html__('FREE cancellation before '.'on '.date( 'M d, Y', strtotime($allow) ) );
        } else {
            $extra_data['allow_cancellation_date'] =  '';
        }

    } elseif ( $cancellation == '1 Week' ) {

        $extra_data['cancellation_policy'] = $comn_msg0. esc_html__('7 Days', 'wpestate') . $comn_msg;

        $allow = date( 'M d Y', strtotime( '-7 days', $check_in ) );
        if ( strtotime( $today ) <= strtotime( $allow ) ) {
            $extra_data['allow_cancellation_date'] = esc_html__('FREE cancellation before '.'on '.date( 'M d, Y', strtotime($allow) ) );
        } else {
            $extra_data['allow_cancellation_date'] =  '';
        }

    } elseif ( $cancellation == '2 Week' ) {

        $extra_data['cancellation_policy'] = $comn_msg0. esc_html__('14 Days', 'wpestate') . $comn_msg;

        $allow = date( 'M d Y', strtotime( '-14 days', $check_in ) );
        if ( strtotime( $today ) <= strtotime( $allow ) ) {
            $extra_data['allow_cancellation_date'] = esc_html__('FREE cancellation before '.'on '.date( 'M d, Y', strtotime($allow) ) );
        } else {
            $extra_data['allow_cancellation_date'] =  '';
        }

    } elseif ( $cancellation == '1 Month' ) {

        $extra_data['cancellation_policy'] = $comn_msg0. $cancellation . $comn_msg;

        $allow = date( 'M d Y', strtotime( '-1 month', $check_in ) );
        if ( strtotime( $today ) <= strtotime( $allow ) ) {
            $extra_data['allow_cancellation_date'] = esc_html__('FREE cancellation before '.'on '.date( 'M d, Y', strtotime($allow) ) );
        }
    } elseif ( empty( $cancellation ) ) {
        $extra_data['cancellation_policy']     = "";
        $extra_data['allow_cancellation_date'] = "";
    }

    // CHECK IN CHECKOUT MESSAGE
    $check_in_hour      = get_post_meta($property_id,'check-in-hour',true);
    $check_in_hour_to   = get_post_meta($property_id,'check-in-hour-to',true);
    $hours_check_in_24  = get_post_meta($property_id,'hours_check_in_24',true);
    $check_out_hour     = get_post_meta($property_id,'check-out-hour',true);
    $check_out_hour_to  = get_post_meta($property_id,'check-out-hour-to',true);
    $hours_check_out_24 = get_post_meta($property_id,'hours_check_out_24',true);
    // Check In Between
    if ( empty( $hours_check_in_24 ) ) {
        $extra_data['check_in'] = $check_in_hour. ' - ' . $check_in_hour_to;
    } else {
        $extra_data['check_in'] = $hours_check_in_24;
    }
    // Check Out Between
    if ( empty( $hours_check_out_24 ) ) {
        $extra_data['check_out'] = $check_out_hour . ' - ' . $check_out_hour_to;
    } else {
        $extra_data['check_out'] = $hours_check_out_24;
    }

    // CONTENT
    $extra_data['content'] = esc_html__('This payment will be processed in the U.S. WiFi issues have been addressed. Internet is available in all areas of the villa.Your reservation will be confirmed after clicking book. No need to call the property to confirm. Please make contact with '.get_the_title($property_id).' 72 hours prior to arrival. You can use the portal to message or email the property. To call the property use the phone number on your itinerary Payment is processed in full at the time of booking. Property Cancellation policy will apply firmly. The property will contact you 72 hours in advance to confirm check-in instructions.Guests are required to show a photo identification. Please note that all Special Requests are subject to availability and additional charges may apply.A damage deposit of USD '.get_post_meta($property_id, 'security_deposit', true).' is required on arrival. This will be collected as a cash payment. All cash deposits are refundable at check-out providing there are no damages by this property and due. Minimum Age for check in 18. We use secure transmission and encrypted storage to protect your personal information.', 'wpestate');

    return $extra_data;
    /*
     * END MERGE OTHER PROPERTY DETAILS
     */
}

/*
 * LRP & JOIN MEMBER CLUB FUNCTION
 * 1. loginuser
 * 2. fbsignin
 * 3. googlesignin
 * This function using above 3 API
 */
function LRP_and_member( $user_id ) {

    // AVAILABLE LRP IN DOLLERS
    $LRP_member = array();

    $user_LRP  = get_user_meta($user_id, 'loyalty_reward_point', true);

    /* USER MEMBERSHIP TYPE 
     * 1. 0 - 4,999 (1% Normal Member)
     * 2. 5,000 - 9,999 (1.2% Gold Member)
     * 3. 10,000 - Unlimited (1.4% Platinum Member)
     */
    if ($user_LRP >= 0 && $user_LRP <= 4999) {
        $member_type = esc_html__('Normal', 'wpestate');
    } else if ( $user_LRP >= 4999 && $user_LRP <= 9999 ) {
        $member_type = esc_html__('Gold', 'wpestate');
    } else {
        $member_type = esc_html__('Platinum', 'wpestate');
    }

    if ( !empty( $user_LRP ) ) {
        $currency = esc_html( get_option('wp_estate_currency_label_main', '') );
        $user_LRP = $user_LRP / 100;
        $LRP_member['available_LRP'] = $currency.number_format($user_LRP, 2);
        $LRP_member['membership_type'] = $member_type;
    } else {
        $LRP_member['available_LRP'] = "0";
        $LRP_member['membership_type'] = "";
    }

    // CLUB MEMBER YES OR NOT
    $is_club_member = get_user_meta($user_id, 'join_reward_club', true);
    if ( !empty( $is_club_member ) && $is_club_member == 1 ) {
        $LRP_member['is_club_member'] = true;
    } else {
        $LRP_member['is_club_member'] = false;
    }
    return $LRP_member;
}


/**
 * BRAINTREE KEYs
 * 1. braintree_gen_token.php 
 * 2. braintree_payment.php 
 */
function braintree_keys() {

    /*return array(

        'braintree_env' => esc_html( get_option( 'braintree_env', '') ), 
        'merchantId'    => esc_html( get_option( 'braintree_merchantId', '') ), 
        'publicKey'     => esc_html( get_option( 'braintree_publicKey', '') ), 
        'privateKey'    => esc_html( get_option( 'braintree_privateKey','') )

    );*/

    return array(

        'braintree_env' => 'sandbox', 
        'merchantId'    => '97yqwknksbfjd6yj', 
        'publicKey'     => 'krc4yxycsw77z6pq', 
        'privateKey'    => '7b36f3b0afc7cc5f43cf58006bc1d2d0'

    );
}

/* 
 * INSERT RECORD FOR MOBILE API
 */
function do_store_booked_prop_tmp_data( $property_id, $booking_id, $start_date, $end_date ) {
    
    $min_days_booking   = intval( get_post_meta($property_id, 'min_days_booking', true) );
    $mega_details       = wpml_mega_details_adjust( $property_id );

    $min_days_booking   = '' == $min_days_booking ? $min_days_booking = 0 : $min_days_booking;
    $check_in           = strtotime( $start_date );
    $min_days_value     = $mega_details[$check_in]['period_min_days_booking'];

    $min_days_book_stay = '' != $min_days_value ? $min_days_value : $min_days_booking;

    $max_allow_guest    = get_post_meta($property_id, 'max_allow_guest', true);

    $invetory_sell = $mega_details[$check_in]['inventory_sell'];
    
    global $wpdb;
    $wpdb->insert( $wpdb->prefix."api_tmp", array( 
        'property_id'       => $property_id,
        'booking_id'        => $booking_id,
        'start_date'        => $start_date, 
        'end_date'          => $end_date, 
        'min_days_stay'     => $min_days_book_stay, 
        'max_allowed_guest' => $max_allow_guest 
    ));
}

/* SEARCH RESULTS ID's */
function search_res_IDs( $destination, $check_in_date, $check_out_date, $guests, $offset, $per_page ) {

    global $wpdb;

    /* First search from terms id */
    $term_id = '';
    if( isset( $destination ) && '' != $destination ) {

        $term_id     = $wpdb->get_results("SELECT `term_id` FROM `wp_terms` WHERE `name` LIKE '%$destination%' LIMIT ".$offset.", ".$per_page." ", ARRAY_A);
        $all_term_id = $wpdb->get_results("SELECT `term_id` FROM `wp_terms` WHERE `name` LIKE '%$destination%'", ARRAY_A);

        $term_id     = implode( ",", array_column($term_id, 'term_id'));
        $all_term_id = implode( ",", array_column($all_term_id, 'term_id'));
    }

    /* Check how many booked properties */
    $diff = strtotime( $check_out_date ) - strtotime( $check_in_date ); 
    $diff = abs( round( $diff / 86400 ) );

    $exclude_prop_id = $wpdb->get_results("SELECT property_id FROM `wp_api_tmp` WHERE 1=1 AND start_date = '$check_in_date' AND end_date = '$check_out_date' AND min_days_stay <= '$diff' AND max_allowed_guest >= '$guests' ", ARRAY_A);
    $exclude_prop_id = array_column($exclude_prop_id, "property_id");
    $exclude_prop_id = implode(',',  $exclude_prop_id);
    // print_r($exclude_prop_id);

    /* START GET SHOULD BE ONLY LESS OR EQUAL GUEST NO FROM MAX_ALLO_GUEST*/ 
    // echo "SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1=1 AND `wp_posts`.`post_type` = 'estate_property' AND `wp_posts`.`post_status` = 'publish' AND ( `wp_postmeta`.`meta_key` = 'max_allow_guest' AND `wp_postmeta`.`meta_value` <= $guests )";
    $get_M_allow_guest = $wpdb->get_results( "SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1=1 AND `wp_posts`.`post_type` = 'estate_property' AND `wp_posts`.`post_status` = 'publish' AND ( `wp_postmeta`.`meta_key` = 'max_allow_guest' AND `wp_postmeta`.`meta_value` >= $guests ) ",ARRAY_A );
    $get_M_allow_guest = array_column( $get_M_allow_guest, 'ID' );
    $get_M_allow_guest = implode( ',', $get_M_allow_guest );
    // print_r($get_M_allow_guest);
    /* END GET SHOULD BE ONLY LESS OR EQUAL GUEST NO FROM MAX_ALLO_GUEST*/ 


    /* Get final propery IDs */
    if( $all_term_id != '' ) {

        if ( !empty( $exclude_prop_id ) ) {

           $prop_id = $wpdb->get_results("SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN `wp_term_relationships` ON `wp_posts`.`ID` = `wp_term_relationships`.`object_id` INNER JOIN `wp_terms` ON `wp_term_relationships`.`term_taxonomy_id` IN (".$all_term_id.") INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1=1 AND `wp_posts`.`ID` IN ($get_M_allow_guest) AND
            (
                ( `wp_terms`.`name` LIKE '%".trim($destination)."%' ) OR
                ( `wp_posts`.`post_title` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_address' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_state' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_zip' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_country' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) 
            ) AND `wp_posts`.`ID` NOT IN ($exclude_prop_id) AND `wp_posts`.`post_type` = 'estate_property' AND `wp_posts`.`post_status` = 'publish' GROUP BY `wp_posts`.`ID` LIMIT ".$offset.", ".$per_page." ", ARRAY_A);

            $counter_prop_id = $wpdb->get_results("SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN `wp_term_relationships` ON `wp_posts`.`ID` = `wp_term_relationships`.`object_id` INNER JOIN `wp_terms` ON `wp_term_relationships`.`term_taxonomy_id` IN (".$all_term_id.") INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1=1 AND `wp_posts`.`ID` IN ($get_M_allow_guest) AND
                (
                    ( `wp_terms`.`name` LIKE '%".trim($destination)."%' ) OR
                    ( `wp_posts`.`post_title` LIKE '%".trim($destination)."%' ) OR
                    ( `wp_postmeta`.`meta_key` = 'property_address' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                    ( `wp_postmeta`.`meta_key` = 'property_state'   AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                    ( `wp_postmeta`.`meta_key` = 'property_zip'          AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                    ( `wp_postmeta`.`meta_key` = 'property_country' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) 
                ) AND `wp_posts`.`ID` NOT IN ($exclude_prop_id) AND `wp_posts`.`post_type` = 'estate_property' AND `wp_posts`.`post_status` = 'publish' GROUP BY `wp_posts`.`ID` ", ARRAY_A);
        } else {

            $prop_id = $wpdb->get_results("SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN `wp_term_relationships` ON `wp_posts`.`ID` = `wp_term_relationships`.`object_id` INNER JOIN `wp_terms` ON `wp_term_relationships`.`term_taxonomy_id` IN (".$all_term_id.") INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1=1 AND `wp_posts`.`ID` IN ($get_M_allow_guest) AND
            (
                ( `wp_terms`.`name` LIKE '%".trim($destination)."%' ) OR
                ( `wp_posts`.`post_title` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_address' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_state' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_zip' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_country' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) 
            ) AND `wp_posts`.`post_type` = 'estate_property' AND `wp_posts`.`post_status` = 'publish' GROUP BY `wp_posts`.`ID` LIMIT ".$offset.", ".$per_page." ", ARRAY_A);

            $counter_prop_id = $wpdb->get_results("SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN `wp_term_relationships` ON `wp_posts`.`ID` = `wp_term_relationships`.`object_id` INNER JOIN `wp_terms` ON `wp_term_relationships`.`term_taxonomy_id` IN (".$all_term_id.") INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1=1 AND `wp_posts`.`ID` IN ($get_M_allow_guest) AND
                (
                    ( `wp_terms`.`name` LIKE '%".trim($destination)."%' ) OR
                    ( `wp_posts`.`post_title` LIKE '%".trim($destination)."%' ) OR
                    ( `wp_postmeta`.`meta_key` = 'property_address' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                    ( `wp_postmeta`.`meta_key` = 'property_state'   AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                    ( `wp_postmeta`.`meta_key` = 'property_zip'          AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                    ( `wp_postmeta`.`meta_key` = 'property_country' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) 
                ) AND `wp_posts`.`post_type` = 'estate_property' AND `wp_posts`.`post_status` = 'publish' GROUP BY `wp_posts`.`ID` ", ARRAY_A);
        }


    } else {
    
        if ( !empty( $exclude_prop_id ) ) {
            
            $prop_id = $wpdb->get_results("SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1=1 AND `wp_posts`.`ID` IN ($get_M_allow_guest) AND
            (

                ( `wp_posts`.`post_title` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_address' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_state' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_zip' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_country' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' )
            ) AND `wp_posts`.`ID` NOT IN ($exclude_prop_id) AND `wp_posts`.`post_type` = 'estate_property' AND `wp_posts`.`post_status` = 'publish' LIMIT ".$offset.", ".$per_page." ", ARRAY_A);


            $counter_prop_id = $wpdb->get_results("SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1=1 AND `wp_posts`.`ID` IN ($get_M_allow_guest) AND
            (

                ( `wp_posts`.`post_title` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_address' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_state' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_zip' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_country' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' )
            ) AND `wp_posts`.`ID` NOT IN ($exclude_prop_id) AND `wp_posts`.`post_type` = 'estate_property' AND `wp_posts`.`post_status` = 'publish' ", ARRAY_A);

        } else {

            $prop_id = $wpdb->get_results("SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1=1 AND `wp_posts`.`ID` IN ($get_M_allow_guest) AND
            (

                ( `wp_posts`.`post_title` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_address' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_state' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_zip' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_country' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' )
            ) AND `wp_posts`.`post_type` = 'estate_property' AND `wp_posts`.`post_status` = 'publish' LIMIT ".$offset.", ".$per_page." ", ARRAY_A);


            $counter_prop_id = $wpdb->get_results("SELECT `wp_posts`.`ID` FROM `wp_posts` INNER JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE 1=1 AND `wp_posts`.`ID` IN ($get_M_allow_guest) AND
            (

                ( `wp_posts`.`post_title` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_address' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_state' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_zip' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' ) OR
                ( `wp_postmeta`.`meta_key` = 'property_country' AND `wp_postmeta`.`meta_value` LIKE '%".trim($destination)."%' )
            ) AND `wp_posts`.`post_type` = 'estate_property' AND `wp_posts`.`post_status` = 'publish' ", ARRAY_A);

        }
    }

    $prop_id       = array_column($prop_id, "ID");
    $total_prop_id = array_column($counter_prop_id, "ID");
    // print_r($total_prop_id); 
    // for ($i=0; $i < count( $total_prop_id ); $i++) { 

    //     $a = wpestate_check_booking_valability($check_in_date,$check_out_date,$total_prop_id[$i] );
    //     $a = check_avaibility($check_in_date,$check_out_date,$total_prop_id[$i] );
    //     if ( !$a ) {
    //         # code...
    //         // echo "-- ". ."\n";
    //         echo $a. "Prop ID- ". $total_prop_id[$i]."\n";
    //     }
    // }
    // die();
    return array(
        'by_page' => $prop_id,
        'all_ID'  => $total_prop_id
    );
}


/* Show Properies Results Data By Search ID*/
function show_search_result( $properties, $check_in, $check_out, $userID ) {

    $p_data = array();
    // Start properties by above given properties ID
    for ($k=0; $k < count($properties); $k++) { 

        //Get Property ID
        $p_data[ $k ]['ID']    =  $properties[ $k ];

        // Property Image URL
        $post_thumb_id  = get_post_thumbnail_id( $properties[ $k ] );
        $post_thumb_url = wp_get_attachment_image_url($post_thumb_id, 'full');

        if ($post_thumb_url == false) {
            $p_data[ $k ]['image_url'] = "";
        } else {
            $p_data[ $k ]['image_url'] = $post_thumb_url;
        }

        // Is Favorite Porperty
        $user_option = 'favorites'.$userID;
        $curent_fav  = get_option( $user_option );
        if( $curent_fav ) {
            if ( in_array ($properties[ $k ],$curent_fav) ){
                $p_data[ $k ]['is_favourite'] = true;
            } else {
                $p_data[ $k ]['is_favourite'] = false;
            }
        } else {
            $p_data[ $k ]['is_favourite'] = false;
        }

        //Get Property Title
        $p_data[ $k ]['title'] = get_the_title( $properties[ $k ] );

        // Get Property City
        $prop_city = get_the_terms($properties[ $k ], 'property_city');
        $this_cat = array();
        foreach ( $prop_city as $cat ) {
            $this_cat[] = $cat->name;
        }
        $p_data[ $k ]['city'] = implode( ",", $this_cat );

        // Get Property State
        $p_data[ $k ]['state'] = get_post_meta( $properties[ $k ], 'property_state', true );

        // Get Property type
        $selected_prop_type = get_the_terms($properties[ $k ], 'property_category');
        if ( !empty( $selected_prop_type ) ) {
            $p_data[ $k ]['property_type'] = $selected_prop_type[0]->name;
        } else {
            $p_data[ $k ]['property_type'] = "";
        }

        // Get Property Zipcode
        // $p_data[ $k ]['zipcode'] = get_post_meta( $properties[ $k ], 'property_zip', true);

        // Get Cancellation Policy
        $cancellation = get_post_meta($properties[ $k ],  'cancellation', true);
        // print_r($cancellation);
        if ($cancellation == 'NonRefundable') {
            $p_data[ $k ][ 'cancellation_policy' ] = esc_html__('Non Refundable', 'wpestate');
        } elseif ($cancellation == 'Free Refund') {
            $p_data[ $k ][ 'cancellation_policy' ] = esc_html__('Free Cancellation', 'wpestate');
        } else if ($cancellation == '24 Hours') {
            $allow = date('M d Y', strtotime("-1 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '.' Until '.date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '48 Hours') {
            $allow = date('M d Y', strtotime("-2 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '72 Hours') {
            $allow = date('M d Y', strtotime("-3 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '1 Week') {
            $allow = date('M d Y', strtotime("-7 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '2 Weeks') {
            $allow = date('M d Y', strtotime("-14 days", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '1 Month') {
            $allow = date('M d Y', strtotime("-1 month", $check_in));
            $today = date("M d Y");
            if ( strtotime( $today ) <= strtotime( $allow ) ) {
                $p_data[ $k ][ 'cancellation_policy' ] = 'Free cancellation '."Until ".date("M dS Y", strtotime($allow));
            }
        } else if ($cancellation == '') {
            $p_data[ $k ][ 'cancellation_policy' ] = '';
        }

        /* 
         * Get Porperty Price & Basic promo rate
         * Day & Date wise price show function
         */
        $price_per_day = floatval( get_post_meta($properties[ $k ], 'property_price', true) );
        $property_basic_promation = floatval( get_post_meta($properties[ $k ], 'property_basic_promation', true) );

        $price_array   = wpml_custom_price_adjust($properties[ $k ]);
        $price_per_day              =   '' != get_post_meta($properties[ $k ], 'basic_price_ner', true ) ? floatval( get_post_meta($properties[ $k ], 'basic_price_ner', true ) ) : floatval(get_post_meta($properties[ $k ], 'property_price', true));

        $price_per_day = isset( $price_array[ $check_in ] ) && '' != $price_array[ $check_in ] || 0 != $price_array[ $check_in ] ? $price_array[ $check_in ] : $price_per_day;

        $property_basic_promation = '' != get_post_meta($properties[ $k ], 'promorate_ner', true ) ? floatval(get_post_meta($properties[ $k ], 'promorate_ner', true )) : floatval (get_post_meta($properties[ $k ], 'property_basic_promation', true) );

        $mega_details_array       = wpml_mega_details_adjust( $properties[ $k ] );
        $property_basic_promation = isset( $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate'] ) && $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate']!= '' ? $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate'] : $property_basic_promation;

        $where_currency = esc_html( get_option('wp_estate_where_currency_symbol', '') );
        $currency           =   esc_html( get_option('wp_estate_currency_label_main', '') );
        
        $p_data[ $k ]['property_basic_price'] = $currency.$price_per_day;
        // if (!empty($property_price)) {
        // }
        // $promo_rate = floatval (get_post_meta($properties[ $k ], 'property_basic_promation', true) );
        $p_data[ $k ]['basic_promorate'] = $currency.$property_basic_promation;
        // if (!empty($property_basic_promation)) {
        // }
        
        // Get Extra Options Fees
        $extra_pay_options = ( get_post_meta($properties[ $k ], 'extra_pay_options', true) );
        if ( empty( $extra_pay_options ) ) {
            $p_data[ $k ]['extra_services'] = array();
        }
        if (is_array($extra_pay_options) && !empty($extra_pay_options)) {
            $free_service = array();
            foreach ($extra_pay_options as $extra_services) {
                if ($extra_services[1] == 0) {
                    $free_service[] = esc_html__('Free ', 'wpestate').$extra_services[0];
                    // print_r($free_service[])
                    $p_data[ $k ]['extra_services'] = $free_service;
                } else {
                    $p_data[ $k ]['extra_services'] = array();
                }
            }
        }

        // Get Review Ratings
        $args = array(
            'post_id' => $properties[ $k ]
        );
        $comments = get_comments($args);
        // print_r($comments);
        if (empty($comments) || '' == $comments) {
            $p_data[ $k ]['ratings'] = esc_html__('0/5');
            $p_data[ $k ]['rating_status'] = "";
        } else {
            $total = $ota_rev = $ota_stars = 0;
            foreach ($comments as $comment) {
                $rating = get_comment_meta( $comment->comment_ID , 'review_stars', true );

                // OTA STARS FIELD
                $ota_stars += get_comment_meta( $comment->comment_ID , 'ota_stars', true );

                $tmp = json_decode( $rating, TRUE );
                $total += intval( $tmp['rating'] );
                $ota_rev++;
            }
            
            $allrevs = $ota_stars + $total;
            if($allrevs){
                $all_avg = $allrevs / $ota_rev;
                $all_avg = number_format( $all_avg, 1, '.', '' );
                
                if ($all_avg > 0) {

                    if( $all_avg > 4.7 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Excellent!');
                    } elseif( $all_avg > 4.4 && $all_avg < 4.8 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Fantastic!');
                    } elseif( $all_avg > 3.9 && $all_avg < 4.5 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Wonderfull!');
                    } elseif( $all_avg > 3.4 && $all_avg < 4 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Great');
                    } elseif( $all_avg > 2.9 && $all_avg < 3.5 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Good');
                    } elseif( $all_avg >= 1 && $all_avg < 3 ) {
                        $p_data[ $k ]['ratings'] = $all_avg. esc_html__('/5');
                        $p_data[ $k ]['rating_status'] = esc_html__('Fair');
                    }
                }
            }
        }

        // Get Check in & Check Out Date
        $p_data[ $k ]['check_in_date'] = $check_in;
        $p_data[ $k ]['check_out_date']= $check_out;
    }

    return $p_data;
}