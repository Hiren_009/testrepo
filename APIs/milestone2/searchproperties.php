<?php

add_action('rest_api_init', function() {
    register_rest_route('tvcapi', '/v2/search-properties',
        array(
            'methods'  => 'POST',
            'callback' => 'search_properties',
        )
    );
});

function search_properties() {
	
    // Check Oath Token
    $headers = apache_request_headers();
    $token_id =  explode( "-qe_aw-", $headers['token'] );
    $token = get_user_meta($token_id[1], 'oauth_token', true);

    if (empty($headers['token']) || $headers['token'] != $token) {

        return new WP_REST_Response(array('response_code' => '401', 'message' => esc_html__('Token is invalid', 'wpestate')), 401);
    }
    // END
	// global $wpdb, $post;
 //    $prop_id = $properties = array();
	$destination    = sanitize_text_field( filter_input( INPUT_POST, 'destination') );
    $check_in_date  = sanitize_text_field( filter_input( INPUT_POST, 'check_in_date') );
    $check_out_date = sanitize_text_field( filter_input( INPUT_POST, 'check_out_date') );
    $guests         = sanitize_text_field( filter_input( INPUT_POST, 'guests' ) );

    $guest_dropdown_no =   intval( get_option('wp_estate_guest_dropdown_no','') );
    $check_in  = strtotime($check_in_date);
    $check_out = strtotime($check_out_date);

    // $destination = 'india';
    // $check_in_date = '2019-08-16';
    // $check_out_date = '2019-08-24';
    // $guests = 10; 
    if( !isset( $destination ) || empty($destination) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter destination place', 'wpestate')), 400);
    }

    if( !isset( $check_in_date ) || empty($check_in_date)) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select first check in date', 'wpestate')), 400);
    }

    if( !isset( $check_out_date ) || empty($check_out_date)) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please select check out date', 'wpestate')), 400);
    }
    
    $today = date("Y-m-d");
    if (strtotime($today) > $check_in) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Check in date should be today and onwards.', 'wpestate')), 400);
    }
    if ($check_in > $check_out) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('You can not select date less than check in date', 'wpestate')), 400);
    }
    if( !isset( $guests ) || empty($guests) ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Please enter no. of guests', 'wpestate')), 400);
    } elseif(  $guests < 1 ) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Guest should be greater then one', 'wpestate')), 400);
    } elseif ($guests > $guest_dropdown_no) {
        return new WP_REST_Response(array('response_code' => '400', 'message' => esc_html__('Guest should be '.$guest_dropdown_no.' or less', 'wpestate')), 400);
    }
      
    //Convert it into a Unix timestamp using strtotime.
    $check_in  = strtotime($check_in_date);
    $check_out = strtotime($check_out_date);

    // START PAGINATION
    $current_page = sanitize_text_field( filter_input( INPUT_POST, 'current_page' ) );
    $per_page     = sanitize_text_field( filter_input( INPUT_POST, 'per_page' ) );
    $current_page = ( $current_page == '' || $current_page == 0 ) ? 1 : (int)$current_page;
    $per_page     = ( !isset( $per_page ) || $per_page == '' || $per_page == 0 ) ? 1 : $per_page;
    $offset       = ( $current_page - 1 ) * $per_page; 
    // END PAGINATION

    $search_res = search_res_IDs( $destination, $check_in_date, $check_out_date, $guests, $offset, $per_page );

    // $property_ids   =  array_values( array_unique( $properties ) );
    $property_ids   =  array_values( array_unique( $search_res['all_ID'] ) );
    
    // START SORT
    $p_price = array();
    for ($i=0; $i < count($property_ids); $i++) { 
           
        $price_per_day             = floatval( get_post_meta($property_ids[ $i ], 'property_price', true) );
        $property_basic_promation = floatval( get_post_meta($property_ids[ $i ], 'property_basic_promation', true) );

        // Basic Price 
        $price_per_day  = '' != get_post_meta($property_ids[ $i ], 'basic_price_ner', true ) ? floatval( get_post_meta($property_ids[ $i ], 'basic_price_ner', true ) ) : floatval(get_post_meta($property_ids[ $i ], 'property_price', true));

        $price_array   = wpml_custom_price_adjust($property_ids[ $i ]);
        $price_per_day = isset( $price_array[ $check_in ] ) && '' != $price_array[ $check_in ] || 0 != $price_array[ $check_in ]  ? $price_array[ $check_in ] : $price_per_day;

        // Basic promo rate
        $property_basic_promation = '' != get_post_meta($property_ids[ $i ], 'promorate_ner', true ) ? floatval(get_post_meta($property_ids[ $i ], 'promorate_ner', true )) : floatval (get_post_meta($property_ids[ $i ], 'property_basic_promation', true) );

        $mega_details_array = wpml_mega_details_adjust( $property_ids[ $i ] );
        $property_basic_promation = isset( $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate'] ) && $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate']!= '' ? $mega_details_array[ strtotime( date("Y-m-d") ) ]['promorate'] : $property_basic_promation;

        // Basic & promo rate check
        $properties_price = $property_basic_promation != 0 ? $property_basic_promation : $price_per_day;

        $p_price[$i]['p_price'] = $properties_price;
        $p_price[$i]['ID']      = $property_ids[ $i ];

    }

    if( isset( $_POST['sort_by'] ) && strcasecmp( $_POST['sort_by'], 'ASC') == 0 ) {
        uasort( $p_price , function( $aa, $bb ){
            return ( floatval( trim( $aa['p_price']  ) ) <= floatval(  trim( $bb['p_price']) ) ) ? -1 : 1;
        });
    } elseif( isset( $_POST['sort_by'] ) && strcasecmp( $_POST['sort_by'], 'DESC') == 0 ) {
        uasort( $p_price , function( $aa, $bb ){
            return ( floatval( trim( $aa['p_price'] ) ) <= floatval( trim( $bb['p_price']) ) ) ? 1 : -1;
        });
    }

    $sort_by = array(
        array(
            'key'  => 'Default',
            'value'=> esc_html__('Default','wpestate'),
        ),
        array(
            'key'  => 'ASC',
            'value'=> esc_html__('Price Low to High','wpestate'),
        ),
         array(
            'key'  => 'DESC',
            'value'=> esc_html__('Price High to Low','wpestate'),
        ),
    );
    // END SORT
    $properties = array_column($p_price, 'ID');
    // echo "By Sort"."\n";
    // print_r(count($properties)); die();
    // $all_ID = count($properties);
    // ==========================================================================
    // Pagination
    // $current_page   = sanitize_text_field( filter_input( INPUT_POST, 'current_page' ) );

    // $current_page = ( $current_page == '' || $current_page == 0 ) ? 1 : (int)$current_page;

    // $per_page = sanitize_text_field( filter_input( INPUT_POST, 'per_page' ) );
    // $per_page = ( $per_page == '' || $per_page == 0 ) ? $all_ID : (int)$per_page;
    // // $per_page = 10;

    $total_pages = ceil(count($properties) / $per_page);

    $p_id = array_chunk($properties, $per_page);
    $properties = $p_id [$current_page - 1];
    // print_r($properties);
    // die();

    // END pagination
    $userID = $token_id[1];
    $p_data = show_search_result( $properties, $check_in, $check_out, $userID );
    
    if (!empty($p_data)) {
        return new WP_REST_Response (
            array(
                "response_code" => '200',
                'per_page'      => $per_page,
                'current_page'  => $current_page,
                'total_pages'   => $total_pages,
                'sort_by'       => $sort_by,
                'data'          => $p_data
            ),
        200);
    } else {
        return new WP_REST_Response(
            array(
                "response_code" => "404",
                'message' => esc_html__('Could not find property', 'wpestate')), 404
            );
    }

}