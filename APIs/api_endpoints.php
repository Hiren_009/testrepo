<?php

/* START API's ENDPOINTS */

$dir  = get_stylesheet_directory().'/libs/APIs/milestone2/';

// require get_stylesheet_directory().'/libs/tvc_api.php';
require $dir.'guest_dropdown_no.php';
require $dir.'searchproperties.php';
require $dir.'favorite_properties.php';
require $dir.'filters_list.php';
require $dir.'filters_results.php';
require $dir.'single_property_details.php';
require $dir.'single_property_casareviews.php';
require $dir.'single_property_importreviews.php';
require $dir.'extra_options.php';
require $dir.'book_property.php';
require $dir.'apply_coupon.php';
require $dir.'remove_coupon.php';
// require $dir.'traveller_contact_form_popup_data.php';
require $dir.'favourite_list_properpties.php';
require $dir.'instant_booking.php';
require $dir.'traveller-details.php';
require $dir.'join_loyalty_reward_club.php';
require $dir.'apply_reward_points.php';
require $dir.'stripe_pament.php';
require $dir.'braintree_gen_token.php';

require $dir.'available_LRP.php';

require $dir.'common_functions.php';



/*
 * Listing of all Milestone 3 
 */
$dir_3 = get_stylesheet_directory().'/libs/APIs/milestone3/';
require $dir_3.'upcoming_trips.php';
require $dir_3.'trip_history.php';
require $dir_3.'booking_details_for_guest_side.php';
require $dir_3.'chats_messanger.php';
require $dir_3.'notification_options_list.php';
require $dir_3.'edit_notification_options.php';
require $dir_3.'privacy_policy.php';
require $dir_3.'user_disclimer.php';
require $dir_3.'change_password.php';
require $dir_3.'my_profile.php';
require $dir_3.'upload_profile_picture.php';
require $dir_3.'my_profile_data.php';
require $dir_3.'notification_data.php';
require $dir_3.'device_token.php';
require $dir_3.'read_notification.php';
require $dir_3.'fcm_unread_count.php';
require $dir_3.'guest_coupon_list.php';
require $dir_3.'property_list_host.php';

require $dir_3.'m3_functions.php';


// require $dir_3.'test_notification.php';


/* END API's ENDPOINTS */